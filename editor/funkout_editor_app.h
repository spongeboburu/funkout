#ifndef FUNKOUT_EDITOR_APP_H
#define FUNKOUT_EDITOR_APP_H

#include <gtk/gtk.h>
#include "funkout/defs.h"

G_BEGIN_DECLS

#define FUNKOUT_EDITOR_APP_TYPE funkout_editor_app_get_type()
G_DECLARE_DERIVABLE_TYPE(FunkoutEditorApp, funkout_editor_app, FUNKOUT_EDITOR, APP, GtkApplication)

struct _FunkoutEditorAppClass {
    GtkApplicationClass parent_class;
    int (*get_num_palette_entries)(FunkoutEditorApp *app);
    gpointer _padding[10];
};    

FunkoutEditorApp *funkout_editor_app_new(void);
int funkout_editor_app_get_num_palette_entries(FunkoutEditorApp *app);

G_END_DECLS

#endif

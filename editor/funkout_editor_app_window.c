#include "funkout_editor_app.h"
#include "funkout_editor_app_window.h"

static const char BUILDER_XML[] = ""
    "<?xml version=\"1.0\"?>\n"
    "<interface>\n" 
    "  <object class=\"GtkComboBoxText\" id=\"inspector_drop\">\n"
    "    <property name=\"visible\">True</property>\n"
    "      <items>\n"
    "        <item translatable=\"yes\" id=\"none\">None</item>\n"
    "        <item translatable=\"yes\" id=\"multiply\">Multiply</item>\n"
    "        <item translatable=\"yes\" id=\"sticky\">Sticky</item>\n"
    "        <item translatable=\"yes\" id=\"life\">Life</item>\n"
    "        <item translatable=\"yes\" id=\"death\">Death</item>\n"
    "      </items>\n"
    "  </object>\n"
    "  <object class=\"GtkComboBoxText\" id=\"inspector_spawn_enemy\">\n"
    "    <property name=\"visible\">True</property>\n"
    "      <items>\n"
    "        <item translatable=\"yes\" id=\"none\">None</item>\n"
    "        <item translatable=\"yes\" id=\"amoeba\">Amoeba</item>\n"
    "        <item translatable=\"yes\" id=\"virus\">Virus</item>\n"
    "        <item translatable=\"yes\" id=\"satellite_a\">Satellite A</item>\n"
    "        <item translatable=\"yes\" id=\"sattelite_b\">Satellite B</item>\n"
    "        <item translatable=\"yes\" id=\"sattelite_c\">Satellite C</item>\n"
    "        <item translatable=\"yes\" id=\"sattelite_d\">Satellite D</item>\n"
    "        <item translatable=\"yes\" id=\"plague\">Plague</item>\n"
    "        <item translatable=\"yes\" id=\"tank\">Tank</item>\n"
    "      </items>\n"
    "  </object>\n"
    "</interface>\n";

typedef struct _FunkoutEditorAppWindowPrivate FunkoutEditorAppWindowPrivate;

typedef struct BlockCell {
    struct funkout_BlockDef def;
    const struct funkout_BlockDef *orig;
} BlockCell;

struct _FunkoutEditorAppWindowPrivate
{
    GtkWidget *paned;
    GtkWidget *palette_scroll;
    GtkWidget *palette;
    GtkWidget *palette_tools;
    GtkWidget *inspector_scroll;
    GtkWidget *inspector_drop;
    GtkWidget *inspector_hitpoints;
    GtkWidget *inspector_color;
    GtkWidget *inspector_spawn_enemy;
    GtkWidget *inspector_spawn_max;
    GtkWidget *inspector_spawn_count;
    GtkWidget *inspector_spawn_cooldown;
    GtkWidget *inspector_spawn_cooldown_start;
    GtkWidget *editable_events;
    GtkWidget *editable;
    
    int num_palette_entries;
    PaletteEntry *palette_entries;
    BlockCell cells[FUNKOUT_GAME_HEIGHT][FUNKOUT_GAME_WIDTH];

    int selection_x;
    int selection_y;
};

G_DEFINE_TYPE_WITH_PRIVATE(FunkoutEditorAppWindow, funkout_editor_app_window, GTK_TYPE_APPLICATION_WINDOW);

#define NUM_DEFAULT_PALETTE_ENTRIES 9

static const struct funkout_BlockDef DEFAULT_PALETTE_ENTRIES[NUM_DEFAULT_PALETTE_ENTRIES] = {
    FUNKOUT_BLOCK_DEF_WALL,
    FUNKOUT_BLOCK_DEF_SPAWN,
    FUNKOUT_BLOCK_DEF_HP1,
    FUNKOUT_BLOCK_DEF_HP2,
    FUNKOUT_BLOCK_DEF_HP3,
    FUNKOUT_BLOCK_DEF_HP4,
    FUNKOUT_BLOCK_DEF_HP6,
    FUNKOUT_BLOCK_DEF_HP8,
    FUNKOUT_BLOCK_DEF_HP10
};

static void fit_rect(float pos_x, float pos_y,
                     float width, float height,
                     float bounding_pos_x, float bounding_pos_y,
                     float bounding_width, float bounding_height,
                     float *ret_x, float *ret_y, float *ret_w, float *ret_h)
{
    float w = 0, h = 0;
    if (width <= 0 || height <= 0) {
        w = (width <= 0 ? 0 : bounding_width);
        h = (height <= 0 ? 0 : bounding_height);
    } else {
        float aspect = (float)width / (float)height;
        if (bounding_height >= bounding_width / aspect) {
            w = bounding_width;
            h = bounding_width / aspect;
        } else {
            w = bounding_height * aspect;
            h = bounding_height;
        }
    }
    if (ret_x != NULL)
        *ret_x = bounding_pos_x + bounding_width / 2 - w / 2;
    if (ret_y != NULL)
        *ret_y = bounding_pos_y + bounding_height / 2 - h / 2;
    if (ret_w != NULL)
        *ret_w = w;
    if (ret_h != NULL)
        *ret_h = h;
}

static int get_active_tool(FunkoutEditorAppWindow *window)
{
    FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(window);
    guint num = gtk_tool_item_group_get_n_items(GTK_TOOL_ITEM_GROUP(priv->palette_tools));
    for (int i = 0; i < num; i++) {
        GtkToolItem *item = gtk_tool_item_group_get_nth_item(GTK_TOOL_ITEM_GROUP(priv->palette_tools), i);
        if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(item)))
            return i;
    }
    return -1;
}

static void update_inspector(FunkoutEditorAppWindow *window)
{
    FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(window);
    int inspectorMode = 0;
    if (priv->selection_x >= 0 && priv->selection_x < FUNKOUT_GAME_WIDTH
        && priv->selection_y >= 0 && priv->selection_y < FUNKOUT_GAME_HEIGHT) {
        struct funkout_BlockDef *def = &priv->cells[priv->selection_y][priv->selection_x].def;
        if (def->type == FUNKOUT_BLOCK_TYPE_NORMAL) {
            inspectorMode = 1;
            gtk_combo_box_set_active(GTK_COMBO_BOX(priv->inspector_drop), def->drop_type);
            gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->inspector_hitpoints), def->hitpoints);
            GdkRGBA color = {def->color.r, def->color.g, def->color.b, def->color.a};
            gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(priv->inspector_color), &color);
        } else if (def->type == FUNKOUT_BLOCK_TYPE_SPAWN) {
            inspectorMode = 2;
            GdkRGBA color = {def->color.r, def->color.g, def->color.b, def->color.a};
            gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(priv->inspector_color), &color);
            gtk_combo_box_set_active(GTK_COMBO_BOX(priv->inspector_spawn_enemy), def->spawn.enemy.appearance);
            gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->inspector_spawn_max), def->spawn.max_enemies);
            gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->inspector_spawn_count), def->spawn.max_count);
            gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->inspector_spawn_cooldown), def->spawn.cooldown);
            gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->inspector_spawn_cooldown_start), def->spawn.cooldown_start);
        }
    }

    if (inspectorMode == 1 || inspectorMode == 2)
        gtk_widget_show(priv->inspector_color);
    else
        gtk_widget_hide(priv->inspector_color);
    
    GtkWidget *widgetsA[] = {
        priv->inspector_drop,
        priv->inspector_hitpoints,
        NULL
    };
    GtkWidget *widgetsB[] = {
        priv->inspector_spawn_enemy,
        priv->inspector_spawn_max,
        priv->inspector_spawn_count,
        priv->inspector_spawn_cooldown,
        priv->inspector_spawn_cooldown_start,
        NULL
    };
    for (GtkWidget **w = widgetsA; *w != NULL; w++) {
        if (inspectorMode == 1)
            gtk_widget_show(*w);
        else
            gtk_widget_hide(*w);
    }
    for (GtkWidget **w = widgetsB; *w != NULL; w++) {
        if (inspectorMode == 2)
            gtk_widget_show(*w);
        else
            gtk_widget_hide(*w);
    }
}

static void tool_button_toggled(GtkToggleToolButton *toggle_tool_button, gpointer user_data)
{
    if (gtk_toggle_tool_button_get_active(toggle_tool_button)) {
        FunkoutEditorAppWindow *top = FUNKOUT_EDITOR_APP_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(toggle_tool_button)));
        if (top != NULL)
            update_inspector(top);
    }
}

gboolean draw_callback(GtkWidget *widget, cairo_t *cr, gpointer data)
{
    GtkStyleContext *context = gtk_widget_get_style_context (widget);
    guint width = gtk_widget_get_allocated_width(widget);
    guint height = gtk_widget_get_allocated_height(widget);
    float gx, gy, gw, gh;
    fit_rect(0, 0, FUNKOUT_GAME_WIDTH, FUNKOUT_GAME_HEIGHT, 0, 0, width, height, &gx, &gy, &gw, &gh);
    cairo_set_source_rgba (cr, 0, 0, 0, 1.0);
    cairo_rectangle(cr, gx, gy, gw, gh);
    cairo_fill(cr);
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgba(cr, 1, 1, 1, 1.0);
    for (int ry = 1; ry < FUNKOUT_GAME_HEIGHT; ry++) {
        cairo_move_to(cr, gx, gy + ry * gh / (float)FUNKOUT_GAME_HEIGHT);
        cairo_line_to(cr, gx + gw, gy + ry * gh / (float)FUNKOUT_GAME_HEIGHT);
    }
    for (int rx = 1; rx < FUNKOUT_GAME_WIDTH; rx++) {
        cairo_move_to(cr, gx + rx * gw / (float)FUNKOUT_GAME_WIDTH, gy);
        cairo_line_to(cr, gx + rx * gw / (float)FUNKOUT_GAME_WIDTH, gy + gh);
    }
    cairo_stroke(cr);
    FunkoutEditorAppWindow *top = FUNKOUT_EDITOR_APP_WINDOW(gtk_widget_get_toplevel(widget));
    if (top != NULL) {
        FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(top);
        for (int ry = 0; ry < FUNKOUT_GAME_HEIGHT; ry++) {
            for (int rx = 0; rx < FUNKOUT_GAME_WIDTH; rx++) {
                const struct funkout_BlockDef *def = &priv->cells[ry][rx].def;
                if (def->type > 0) {
                    cairo_set_source_rgba(cr, def->color.r, def->color.g, def->color.b, def->color.a);
                    cairo_rectangle(cr,
                                    gx + rx * gw / (float)FUNKOUT_GAME_WIDTH + (gw / (float)FUNKOUT_GAME_WIDTH) * 0.1,
                                    gy + ry * gh / (float)FUNKOUT_GAME_HEIGHT + (gh / (float)FUNKOUT_GAME_HEIGHT) * 0.1,
                                    gw / (float)FUNKOUT_GAME_WIDTH * 0.8,
                                    gh / (float)FUNKOUT_GAME_HEIGHT * 0.8);
                    cairo_fill(cr);
                }
            }
        }
        if (priv->selection_x >= 0 && priv->selection_x < FUNKOUT_GAME_WIDTH
            && priv->selection_y >= 0 && priv->selection_y < FUNKOUT_GAME_HEIGHT) {
            cairo_set_source_rgba(cr, 1, 1, 1, 1.0);
            cairo_set_line_width(cr, 4.0);
            cairo_rectangle(cr,
                            gx + priv->selection_x * gw / (float)FUNKOUT_GAME_WIDTH,
                            gy + priv->selection_y * gh / (float)FUNKOUT_GAME_HEIGHT,
                            gw / (float)FUNKOUT_GAME_WIDTH,
                            gh / (float)FUNKOUT_GAME_HEIGHT);
            cairo_stroke(cr);
        }
    }
    return FALSE;
}

gboolean button_press_callback(GtkWidget *widget, GdkEvent *event, gpointer data)
{
    gboolean down = false;
    if (event->type == GDK_BUTTON_PRESS)
        down = true;
    else if (event->type == GDK_MOTION_NOTIFY)
        down = event->motion.state & GDK_BUTTON1_MASK;
    if (down) {
        FunkoutEditorAppWindow *top = FUNKOUT_EDITOR_APP_WINDOW(gtk_widget_get_toplevel(widget));
        if (top != NULL) {
            FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(top);
            gdouble x = event->button.x;
            gdouble y = event->button.y;
            GtkAllocation allocation;
            gtk_widget_get_allocation(widget, &allocation);
            if (allocation.width > 0 && allocation.height > 0) {
                guint width = gtk_widget_get_allocated_width(widget);
                guint height = gtk_widget_get_allocated_height(widget);
                float gx, gy, gw, gh;
                fit_rect(0, 0, FUNKOUT_GAME_WIDTH, FUNKOUT_GAME_HEIGHT, 0, 0, width, height, &gx, &gy, &gw, &gh);
                priv->selection_x = ((float)(x - gx) / gw) * FUNKOUT_GAME_WIDTH;
                priv->selection_y = ((float)(y - gy) / gh) * FUNKOUT_GAME_HEIGHT;
                if (priv->selection_x < 0)
                    priv->selection_x = 0;
                else if (priv->selection_x >= FUNKOUT_GAME_WIDTH)
                    priv->selection_x = FUNKOUT_GAME_WIDTH - 1;
                if (priv->selection_y < 0)
                    priv->selection_y = 0;
                else if (priv->selection_y >= FUNKOUT_GAME_HEIGHT)
                    priv->selection_y = FUNKOUT_GAME_HEIGHT - 1;
                int active_tool = get_active_tool(top);
                if (active_tool == 1) {
                    memset(&priv->cells[priv->selection_y][priv->selection_x], 0, sizeof(struct funkout_BlockDef));
                } else if (active_tool > 1) {
                    const struct funkout_BlockDef *def = &DEFAULT_PALETTE_ENTRIES[active_tool - 2];
                    memcpy(&priv->cells[priv->selection_y][priv->selection_x].def, def, sizeof(struct funkout_BlockDef));
                    priv->cells[priv->selection_y][priv->selection_x].orig = def;
                }
                update_inspector(top);
                gtk_widget_queue_draw(widget);
            }
        }
    }
    return FALSE;
}

void inspector_hitpoints_value(GtkSpinButton *widget, gpointer data)
{
    FunkoutEditorAppWindow *top = FUNKOUT_EDITOR_APP_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(widget)));
    if (top != NULL) {
        FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(top);
        if (priv->selection_x >= 0 && priv->selection_x < FUNKOUT_GAME_WIDTH
            && priv->selection_y >= 0 && priv->selection_y < FUNKOUT_GAME_HEIGHT) {
            priv->cells[priv->selection_y][priv->selection_x].def.hitpoints = gtk_spin_button_get_value(widget);
        }
    }
}

void inspector_color_value(GtkColorButton *widget, gpointer data)
{
    FunkoutEditorAppWindow *top = FUNKOUT_EDITOR_APP_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(widget)));
    if (top != NULL) {
        FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(top);
        if (priv->selection_x >= 0 && priv->selection_x < FUNKOUT_GAME_WIDTH
            && priv->selection_y >= 0 && priv->selection_y < FUNKOUT_GAME_HEIGHT) {
            GdkRGBA color;
            gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(widget), &color);
            priv->cells[priv->selection_y][priv->selection_x].def.color.r = color.red;
            priv->cells[priv->selection_y][priv->selection_x].def.color.g = color.green;
            priv->cells[priv->selection_y][priv->selection_x].def.color.b = color.blue;
            priv->cells[priv->selection_y][priv->selection_x].def.color.a = color.alpha;
        }
    }
}

void inspector_spawn_enemy_value(GtkComboBox *widget, gpointer data)
{
}

void inspector_spawn_max_value(GtkSpinButton *widget, gpointer data)
{
    FunkoutEditorAppWindow *top = FUNKOUT_EDITOR_APP_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(widget)));
    if (top != NULL) {
        FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(top);
        if (priv->selection_x >= 0 && priv->selection_x < FUNKOUT_GAME_WIDTH
            && priv->selection_y >= 0 && priv->selection_y < FUNKOUT_GAME_HEIGHT) {
            priv->cells[priv->selection_y][priv->selection_x].def.spawn.max_enemies = gtk_spin_button_get_value(widget);
        }
    }
}

void inspector_spawn_count_value(GtkSpinButton *widget, gpointer data)
{
    FunkoutEditorAppWindow *top = FUNKOUT_EDITOR_APP_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(widget)));
    if (top != NULL) {
        FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(top);
        if (priv->selection_x >= 0 && priv->selection_x < FUNKOUT_GAME_WIDTH
            && priv->selection_y >= 0 && priv->selection_y < FUNKOUT_GAME_HEIGHT) {
            priv->cells[priv->selection_y][priv->selection_x].def.spawn.max_count = gtk_spin_button_get_value(widget);
        }
    }
}

void inspector_spawn_cooldown_value(GtkSpinButton *widget, gpointer data)
{
    FunkoutEditorAppWindow *top = FUNKOUT_EDITOR_APP_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(widget)));
    if (top != NULL) {
        FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(top);
        if (priv->selection_x >= 0 && priv->selection_x < FUNKOUT_GAME_WIDTH
            && priv->selection_y >= 0 && priv->selection_y < FUNKOUT_GAME_HEIGHT) {
            priv->cells[priv->selection_y][priv->selection_x].def.spawn.cooldown = gtk_spin_button_get_value(widget);
        }
    }
}

void inspector_spawn_cooldown_start_value(GtkSpinButton *widget, gpointer data)
{
    FunkoutEditorAppWindow *top = FUNKOUT_EDITOR_APP_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(widget)));
    if (top != NULL) {
        FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(top);
        if (priv->selection_x >= 0 && priv->selection_x < FUNKOUT_GAME_WIDTH
            && priv->selection_y >= 0 && priv->selection_y < FUNKOUT_GAME_HEIGHT) {
            priv->cells[priv->selection_y][priv->selection_x].def.spawn.cooldown_start = gtk_spin_button_get_value(widget);
        }
    }
}

static void funkout_editor_app_window_init(FunkoutEditorAppWindow *window)
{
    gtk_window_set_default_size(GTK_WINDOW(window), 512, 512);

    char tmps[1024];
    g_snprintf(tmps, sizeof(tmps), "Funkout Editor %s", VERSION);
    gtk_window_set_title(GTK_WINDOW(window), tmps);

    FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(window);
    priv->num_palette_entries = NUM_DEFAULT_PALETTE_ENTRIES;
    priv->palette_entries = malloc(sizeof(PaletteEntry) * priv->num_palette_entries);
    for (int i = 0; i < priv->num_palette_entries; i++) {
        memset(&priv->palette_entries[i].block_def, 0, sizeof(struct funkout_BlockDef));
        priv->palette_entries[i].immutable = true;
    }
    priv->selection_x = -1;
    priv->selection_y = -1;
    memset(priv->cells, 0, sizeof(priv->cells));

    GtkBuilder *builder = gtk_builder_new_from_string(BUILDER_XML, sizeof(BUILDER_XML) - 1);
    
    GtkWidget *main_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add(GTK_CONTAINER(window), main_box);
    
    // Paned
    priv->paned = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_box_pack_start(GTK_BOX(main_box), priv->paned, 1, 1, 0);
    gtk_paned_set_wide_handle(GTK_PANED(priv->paned), 1);

    // Palette
    priv->palette_scroll = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(priv->palette_scroll), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
    gtk_paned_pack1(GTK_PANED(priv->paned), priv->palette_scroll, FALSE, FALSE);
    priv->palette = gtk_tool_palette_new();
    gtk_container_add(GTK_CONTAINER(priv->palette_scroll), priv->palette);
    priv->palette_tools = gtk_tool_item_group_new("Tools");
    gtk_container_add(GTK_CONTAINER(priv->palette), priv->palette_tools);
    GtkToolItem *item;

    item = gtk_radio_tool_button_new(NULL);
    gtk_tool_button_set_label(GTK_TOOL_BUTTON(item), "Select");
    gtk_tool_item_group_insert(GTK_TOOL_ITEM_GROUP(priv->palette_tools), item, -1);
    gtk_widget_set_name(GTK_WIDGET(item), "tool_select");
    g_signal_connect(item, "toggled", G_CALLBACK(tool_button_toggled), NULL);

    item = gtk_radio_tool_button_new_from_widget(GTK_RADIO_TOOL_BUTTON(item));
    gtk_tool_button_set_label(GTK_TOOL_BUTTON(item), "Empty");
    gtk_tool_item_group_insert(GTK_TOOL_ITEM_GROUP(priv->palette_tools), item, -1);
    gtk_widget_set_name(GTK_WIDGET(item), "tool_empty");
    g_signal_connect(item, "toggled", G_CALLBACK(tool_button_toggled), NULL);

    // item = gtk_radio_tool_button_new_from_widget(GTK_RADIO_TOOL_BUTTON(item));
    // gtk_tool_button_set_label(GTK_TOOL_BUTTON(item), "Wall");
    // gtk_tool_item_group_insert(GTK_TOOL_ITEM_GROUP(priv->palette_tools), item, -1);
    // gtk_widget_set_name(GTK_WIDGET(item), "tool_wall");
    // g_signal_connect(item, "toggled", G_CALLBACK(tool_button_toggled), NULL);

    // item = gtk_radio_tool_button_new_from_widget(GTK_RADIO_TOOL_BUTTON(item));
    // gtk_tool_button_set_label(GTK_TOOL_BUTTON(item), "Spawn");
    // gtk_tool_item_group_insert(GTK_TOOL_ITEM_GROUP(priv->palette_tools), item, -1);
    // gtk_widget_set_name(GTK_WIDGET(item), "tool_spawn");
    // g_signal_connect(item, "toggled", G_CALLBACK(tool_button_toggled), NULL);
    
    for (int i = 0; i < priv->num_palette_entries; i++) {
        char tmps[128];
        if (priv->palette_entries[i].block_def.type == FUNKOUT_BLOCK_TYPE_NORMAL) {
            if (priv->palette_entries[i].block_def.hitpoints < 0)
                g_snprintf(tmps, sizeof(tmps), "%s", "Wall");
            else
                g_snprintf(tmps, sizeof(tmps), "%dHP", priv->palette_entries[i].block_def.hitpoints);
            
        } else if (priv->palette_entries[i].block_def.type == FUNKOUT_BLOCK_TYPE_SPAWN) {
            g_snprintf(tmps, sizeof(tmps), "%s", "Spawn");
        } else
            tmps[0] = 0;
        item = gtk_radio_tool_button_new_from_widget(GTK_RADIO_TOOL_BUTTON(item));
        gtk_tool_button_set_label(GTK_TOOL_BUTTON(item), tmps);
        gtk_tool_item_group_insert(GTK_TOOL_ITEM_GROUP(priv->palette_tools), item, -1);
        g_snprintf(tmps, sizeof(tmps), "tool_%d", i);
        gtk_widget_set_name(GTK_WIDGET(item), tmps);
        g_signal_connect(item, "toggled", G_CALLBACK(tool_button_toggled), NULL);
    }

    GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_paned_pack2(GTK_PANED(priv->paned), hbox, TRUE, FALSE);

    // Editor grid
    priv->editable_events = gtk_event_box_new();
    gtk_box_pack_start(GTK_BOX(hbox), priv->editable_events, 1, 1, 0);
    g_signal_connect(G_OBJECT(priv->editable_events), "button-press-event", G_CALLBACK(button_press_callback), NULL);
    g_signal_connect(G_OBJECT(priv->editable_events), "motion-notify-event", G_CALLBACK(button_press_callback), NULL);
    gtk_widget_add_events(priv->editable_events, GDK_POINTER_MOTION_MASK | GDK_BUTTON_PRESS_MASK);

    priv->editable = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(priv->editable_events), priv->editable);
    g_signal_connect(G_OBJECT(priv->editable), "draw", G_CALLBACK(draw_callback), NULL);
        
    // Inspector
    priv->inspector_scroll = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(priv->inspector_scroll), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start(GTK_BOX(hbox), priv->inspector_scroll, 0, 0, 0);
    GtkWidget *frame = gtk_frame_new("Inspector");
    gtk_container_add(GTK_CONTAINER(priv->inspector_scroll), frame);
    
    GtkWidget *grid = gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(frame), grid);

    gtk_grid_attach(GTK_GRID(grid), gtk_label_new("Drop"), 0, 2, 1, 1);
    priv->inspector_drop = GTK_WIDGET(gtk_builder_get_object(builder, "inspector_drop"));
    gtk_grid_attach(GTK_GRID(grid), priv->inspector_drop, 0, 3, 1, 1);

    gtk_grid_attach(GTK_GRID(grid), gtk_label_new("Hitpoints"), 0, 4, 1, 1);
    priv->inspector_hitpoints = gtk_spin_button_new_with_range(-1, 1000, 1);
    gtk_grid_attach(GTK_GRID(grid), priv->inspector_hitpoints, 0, 5, 1, 1);
    g_signal_connect(G_OBJECT(priv->inspector_hitpoints), "value-changed", G_CALLBACK(inspector_hitpoints_value), NULL);

    gtk_grid_attach(GTK_GRID(grid), gtk_label_new("Color"), 0, 6, 1, 1);
    priv->inspector_color = gtk_color_button_new();
    gtk_grid_attach(GTK_GRID(grid), priv->inspector_color, 0, 7, 1, 1);
    g_signal_connect(G_OBJECT(priv->inspector_color), "color-set", G_CALLBACK(inspector_color_value), NULL);

    gtk_grid_attach(GTK_GRID(grid), gtk_label_new("Spawn enemy"), 0, 8, 1, 1);
    priv->inspector_spawn_enemy = GTK_WIDGET(gtk_builder_get_object(builder, "inspector_spawn_enemy"));
    gtk_grid_attach(GTK_GRID(grid), priv->inspector_spawn_enemy, 0, 9, 1, 1);
    // g_signal_connect(G_OBJECT(priv->inspector_spawn_enemy), "value-changed", G_CALLBACK(inspector_spawn_enemy_value), NULL);

    gtk_grid_attach(GTK_GRID(grid), gtk_label_new("Max spawn"), 0, 10, 1, 1);
    priv->inspector_spawn_max = gtk_spin_button_new_with_range(0, 10, 1);
    gtk_grid_attach(GTK_GRID(grid), priv->inspector_spawn_max, 0, 11, 1, 1);
    g_signal_connect(G_OBJECT(priv->inspector_spawn_max), "value-changed", G_CALLBACK(inspector_spawn_max_value), NULL);

    gtk_grid_attach(GTK_GRID(grid), gtk_label_new("Spawn count"), 0, 12, 1, 1);
    priv->inspector_spawn_count = gtk_spin_button_new_with_range(0, 1000, 1);
    gtk_grid_attach(GTK_GRID(grid), priv->inspector_spawn_count, 0, 13, 1, 1);
    g_signal_connect(G_OBJECT(priv->inspector_spawn_count), "value-changed", G_CALLBACK(inspector_spawn_count_value), NULL);

    gtk_grid_attach(GTK_GRID(grid), gtk_label_new("Spawn cooldown"), 0, 14, 1, 1);
    priv->inspector_spawn_cooldown = gtk_spin_button_new_with_range(0, 600, 1);
    gtk_grid_attach(GTK_GRID(grid), priv->inspector_spawn_cooldown, 0, 15, 1, 1);
    g_signal_connect(G_OBJECT(priv->inspector_spawn_cooldown), "value-changed", G_CALLBACK(inspector_spawn_cooldown_value), NULL);

    gtk_grid_attach(GTK_GRID(grid), gtk_label_new("Spawn cooldown start"), 0, 16, 1, 1);
    priv->inspector_spawn_cooldown_start = gtk_spin_button_new_with_range(0, 600, 1);
    gtk_grid_attach(GTK_GRID(grid), priv->inspector_spawn_cooldown_start, 0, 17, 1, 1);
    g_signal_connect(G_OBJECT(priv->inspector_spawn_cooldown_start), "value-changed", G_CALLBACK(inspector_spawn_cooldown_start_value), NULL);

    g_object_unref(builder);
    
    // Show everything
    gtk_widget_show_all(GTK_WIDGET(main_box));
    update_inspector(window);
}

static void funkout_editor_app_window_finalize(GObject *obj)
{
    FunkoutEditorAppWindowPrivate *priv = funkout_editor_app_window_get_instance_private(FUNKOUT_EDITOR_APP_WINDOW(obj));
    if (priv->palette_entries != NULL)
        free(priv->palette_entries);
}

static void funkout_editor_app_window_class_init(FunkoutEditorAppWindowClass *class)
{
    G_OBJECT_CLASS(class)->finalize = funkout_editor_app_window_finalize;
}


FunkoutEditorAppWindow* funkout_editor_app_window_new(FunkoutEditorApp *app)
{
  return g_object_new(FUNKOUT_EDITOR_APP_WINDOW_TYPE, "application", app, NULL);
}


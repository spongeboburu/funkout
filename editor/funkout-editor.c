#include "funkout_editor_app.h"

int main(int argc, char *argv[])
{
    return g_application_run(G_APPLICATION(funkout_editor_app_new()), argc, argv);
}

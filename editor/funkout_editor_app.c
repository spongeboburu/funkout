#include "funkout_editor_app.h"
#include "funkout_editor_app_window.h"

// struct _FunkoutEditorApp
// {
//     GtkApplication parent;
// };

typedef struct _FunkoutEditorAppPrivate FunkoutEditorAppPrivate;

struct _FunkoutEditorAppPrivate
{
    int dummy;
};

G_DEFINE_TYPE_WITH_PRIVATE(FunkoutEditorApp, funkout_editor_app, GTK_TYPE_APPLICATION);

static const gchar APP_MENU_XML[] = ""
    "<?xml version=\"1.0\"?>\n"
    "<interface>\n" 
    "  <menu id=\"appmenu\">\n"
    "    <section>\n"
    "      <item>\n"
    "        <attribute name=\"label\">_New</attribute>\n"
    "        <attribute name=\"action\">app.new</attribute>\n"
    "        <attribute name=\"icon\">document-new</attribute>\n"
    "      </item>\n"
    "      <item>\n"
    "        <attribute name=\"label\">_Open</attribute>\n"
    "        <attribute name=\"action\">app.open</attribute>\n"
    "        <attribute name=\"icon\">document-open</attribute>\n"
    "      </item>\n"
    "      <item>\n"
    "        <attribute name=\"label\">_Save</attribute>\n"
    "        <attribute name=\"action\">app.save</attribute>\n"
    "        <attribute name=\"icon\">document-save</attribute>\n"
    "      </item>\n"
    "      <item>\n"
    "        <attribute name=\"label\">Save _As</attribute>\n"
    "        <attribute name=\"action\">app.save_as</attribute>\n"
    "        <attribute name=\"icon\">document-save-as</attribute>\n"
    "      </item>\n"
    "    </section>\n"
    "    <section>\n"
    "      <item>\n"
    "        <attribute name=\"label\">_Preferences</attribute>\n"
    "        <attribute name=\"action\">app.preferences</attribute>\n"
    "        <attribute name=\"icon\">preferences-system</attribute>\n"
    "      </item>\n"
    "    </section>\n"
    "    <section>\n"
    "      <item>\n"
    "        <attribute name=\"label\">_Quit</attribute>\n"
    "        <attribute name=\"action\">app.quit</attribute>\n"
    "        <attribute name=\"icon\">application-exit</attribute>\n"
    "      </item>\n"
    "    </section>\n"
    "  </menu>\n"
    "</interface>\n";


static void new_activated(GSimpleAction *action, GVariant *parameter, gpointer app)
{
    printf("NEW\n");
}

static void open_activated(GSimpleAction *action, GVariant *parameter, gpointer app)
{
    printf("OPEN\n");
}

static void save_activated(GSimpleAction *action, GVariant *parameter, gpointer app)
{
    printf("SAVE\n");
}

static void save_as_activated(GSimpleAction *action, GVariant *parameter, gpointer app)
{
    printf("SAVE AS\n");
}

static void preferences_activated(GSimpleAction *action, GVariant *parameter, gpointer app)
{
    printf("PREFS\n");
}

static void quit_activated(GSimpleAction *action, GVariant *parameter, gpointer app)
{
    g_application_quit(G_APPLICATION(app));
}

static GActionEntry app_entries[] =
{
    { "new", new_activated, NULL, NULL, NULL },
    { "open", open_activated, NULL, NULL, NULL },
    { "save", save_activated, NULL, NULL, NULL },
    { "save_as", save_as_activated, NULL, NULL, NULL },
    { "preferences", preferences_activated, NULL, NULL, NULL },
    { "quit", quit_activated, NULL, NULL, NULL }
};

static void funkout_editor_app_init(FunkoutEditorApp *app)
{
}

static void funkout_editor_app_activate(GApplication *app)
{
    FunkoutEditorAppWindow *window = funkout_editor_app_window_new(FUNKOUT_EDITOR_APP(app));
    // GtkWidget *window = gtk_application_window_new(GTK_APPLICATION(app));
    gtk_window_present(GTK_WINDOW(window));
}

// static void funkout_editor_app_open(GApplication *app, GFile **files, gint n_files, const gchar *hint)
// {
//     GList *windows;
//     FunkoutEditorAppWindow *win;
//     // int i;
//     windows = gtk_application_get_windows(GTK_APPLICATION(app));
//     if (windows)
//         win = FUNKOUT_EDITOR_APP_WINDOW(windows->data);
//     else
//         win = funkout_editor_app_window_new(FUNKOUT_EDITOR_APP(app));

//     for (i = 0; i < n_files; i++)
//         funkout_editor_app_window_open(win, files[i]);

//     gtk_window_present(GTK_WINDOW(win));
// }

static void funkout_editor_app_startup(GApplication *app)
{
    const gchar *new_accels[2] = { "<Ctrl>N", NULL };
    const gchar *open_accels[2] = { "<Ctrl>O", NULL };
    const gchar *save_accels[2] = { "<Ctrl>S", NULL };
    const gchar *quit_accels[2] = { "<Ctrl>Q", NULL };

    G_APPLICATION_CLASS(funkout_editor_app_parent_class)->startup(app);

    g_action_map_add_action_entries(G_ACTION_MAP (app),
                                    app_entries, G_N_ELEMENTS(app_entries),
                                    app);
    gtk_application_set_accels_for_action(GTK_APPLICATION (app),
                                          "app.new",
                                          new_accels);
    gtk_application_set_accels_for_action(GTK_APPLICATION (app),
                                          "app.open",
                                          open_accels);
    gtk_application_set_accels_for_action(GTK_APPLICATION (app),
                                          "app.save",
                                          save_accels);
    gtk_application_set_accels_for_action(GTK_APPLICATION (app),
                                          "app.quit",
                                          quit_accels);

    GtkBuilder *builder = gtk_builder_new_from_string(APP_MENU_XML, sizeof(APP_MENU_XML) - 1);
    GMenuModel *app_menu = G_MENU_MODEL(gtk_builder_get_object(builder, "appmenu"));
    gtk_application_set_app_menu(GTK_APPLICATION(app), app_menu);
    g_object_unref(builder);
}

static void funkout_editor_app_class_init(FunkoutEditorAppClass *class)
{
    G_APPLICATION_CLASS(class)->activate = funkout_editor_app_activate;
    // G_APPLICATION_CLASS(class)->open = funkout_editor_app_open;
    G_APPLICATION_CLASS(class)->startup = funkout_editor_app_startup;
}

FunkoutEditorApp * funkout_editor_app_new(void)
{
    return g_object_new(FUNKOUT_EDITOR_APP_TYPE,
                        "application-id", "org.gtk.funkout_editor_app",
                        "flags", G_APPLICATION_HANDLES_OPEN,
                        NULL);
}


#ifndef FUNKOUT_EDITOR_APP_WINDOW_H
#define FUNKOUT_EDITOR_APP_WINDOW_H

#include <gtk/gtk.h>
#include "funkout_editor_app.h"


G_BEGIN_DECLS

#define FUNKOUT_EDITOR_APP_WINDOW_TYPE funkout_editor_app_window_get_type()
G_DECLARE_DERIVABLE_TYPE(FunkoutEditorAppWindow, funkout_editor_app_window, FUNKOUT_EDITOR, APP_WINDOW, GtkApplicationWindow)

struct _FunkoutEditorAppWindowClass {
    GtkApplicationWindowClass parent_class;
};    
    
FunkoutEditorAppWindow *funkout_editor_app_window_new(FunkoutEditorApp *app);

G_END_DECLS

typedef struct PaletteEntry {
    struct funkout_BlockDef block_def;
    bool immutable;
} PaletteEntry;

#endif

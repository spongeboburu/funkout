#!/bin/env python3
# -*- coding: utf-8 -*-

from itertools import zip_longest

def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)

def convert_data(symbol, data):
    if data:
        reta = ["static unsigned char %s[%d] = {"%(symbol, len(data))]
        for d in grouper(data, 12):
            g = []
            end_comma = bool(not g and not None in d)
            for dd in d:
                if not dd is None:
                    g.append("0x%02x"%dd)
            if end_comma:
                g.append("")
            if g:
                reta.append("    %s"%", ".join(g))
        reta.append("};\n")
        return "\n".join(reta)
    return ""


def convert_file(symbol, f):
    data = f.read(65536)
    if len(data) > 65535:
        raise ValueError("data too large")
    elif len(data) == 0:
        raise ValueError("data too small")
    return convert_data(symbol, data)


if __name__ == "__main__":
    import sys
    from optparse import OptionParser
    parser = OptionParser(usage="%prog <symbol> <input> [output]")
    parser.add_option("-q", "--quiet",
                      action="store_true", dest="quiet", default=False,
                      help="don't print status messages to stdout")
    (options, args) = parser.parse_args()
    # if options.help:
    #     parser.print_help()
    #     sys.exit(0)
    if len(args) < 2:
        print("need an input file, try -h or --help")
        sys.exit(1)
    elif len(args) > 3:
        print("only one output file at a time, try -h or --help")
        sys.exit(1)
    symbol = args[0]
    for c in symbol:
        if not c.lower() in "abcdefghijklmnopqrstuvwxyz0123456789_":
            raise ValueError("invalid character '%c' in symbol: %s"%(c, symbol))
    if args[1] == "-":
        fin = sys.stdin.detach()
    else:
        fin = open(args[1], "rb")
    try:
        data = convert_file(symbol, fin)
    except ValueError:
        if args[1] != "-":
            fin.close()
        raise
    if args[1] != "-":
        fin.close()
    if len(args) < 3 or args[2] == "-":
        fout = sys.stdout
    else:
        fout = open(args[2], "wt")
    try:
        fout.write(data)
    except:
        if len(args) > 2 and args[2] != "-":
            fout.close()
        raise
    if len(args) > 2 and args[2] != "-":
        fout.close()

# BuildSDL2Image
#
# Will define:
#
#   SDL2_IMAGE_INCLUDE_DIRS - Include directories for SDL2.
#   SDL2_IMAGE_LIBRARIES - All linkable libraries for SDL2.
#   SDL2_IMAGE_LIBRARY_STATIC - A static version of SDL2.
#
#   ZLIB_INCLUDE_DIRS - Include directories for zlib.
#   ZLIB_LIBRARIES - All linkable libraries for zlib.
#   ZLIB_LIBRARY_STATIC - A static version of zlib.
#
#   LIBPNG_INCLUDE_DIRS - Include directories for libpng.
#   LIBPNG_LIBRARIES - All linkable libraries for libpng.
#   LIBPNG_LIBRARY_STATIC - A static version of libpng.

ExternalProject_Add(
    zlib
    DOWNLOAD_DIR ${Funkout_THIRD_PARTY_DOWNLOADS_DIR}
    URL https://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.4.zip
    URL_HASH SHA256=c95229485d6a0bdeccea63ce7580485ce1bfac723ff8a8545837d2d5a3b20446
    SOURCE_SUBDIR external/zlib-1.2.11
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}
    )

set(ZLIB_INCLUDE_DIR ${Funkout_THIRD_PARTY_INSTALL_DIR}/include)
set(ZLIB_LIBRARY ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libz.so)
set(ZLIB_LIBRARY_STATIC ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libz.a)
set(ZLIB_LIBRARY_DIR ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib)
set(ZLIB_INCLUDE_DIRS ${ZLIB_INCLUDE_DIR})
set(ZLIB_LIBRARIES ${ZLIB_LIBRARY})
mark_as_advanced(ZLIB_INCLUDE_DIR ZLIB_LIBRARY ZLIB_LIBRARY_DIR ZLIB_LIBRARY_STATIC)

set(_zlib_link_flags "-Wl,-rpath=${ZLIB_LIBRARY_DIR} -lz")
ExternalProject_Add(
    libpng
    DOWNLOAD_DIR ${Funkout_THIRD_PARTY_DOWNLOADS_DIR}
    URL https://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.4.zip
    URL_HASH SHA256=c95229485d6a0bdeccea63ce7580485ce1bfac723ff8a8545837d2d5a3b20446
    SOURCE_SUBDIR external/libpng-1.6.32
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR} -DCMAKE_INSTALL_LIBDIR=lib -DPNG_TESTS=OFF -DPNG_BUILD_ZLIB=ON -DPNG_FRAMEWORK=OFF -DZLIB_INCLUDE_DIR=${ZLIB_INCLUDE_DIR} -DZLIB_LIBRARY=${_zlib_link_flags}
    PATCH_COMMAND patch -R -f -d ${CMAKE_CURRENT_BINARY_DIR}/libpng-prefix/src/libpng/external/libpng-1.6.32 < ${CMAKE_CURRENT_BINARY_DIR}/libpng-prefix/src/libpng/external/libpng-1.6.32.patch
    )
add_dependencies(libpng zlib)
set(LIBPNG_INCLUDE_DIR ${Funkout_THIRD_PARTY_INSTALL_DIR}/include)
set(LIBPNG_LIBRARY ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libpng.so)
set(LIBPNG_LIBRARY_STATIC ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libpng.a)
set(LIBPNG_LIBRARY_DIR ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib)
set(LIBPNG_INCLUDE_DIRS ${LIBPNG_INCLUDE_DIR})
set(LIBPNG_LIBRARIES ${LIBPNG_LIBRARY})
mark_as_advanced(LIBPNG_INCLUDE_DIR LIBPNG_LIBRARY LIBPNG_LIBRARY_DIR LIBPNG_LIBRARY_STATIC)

set(SDL2_image_CONFIG_OPTIONS
    --prefix=${Funkout_THIRD_PARTY_INSTALL_DIR}
    --enable-static
    --enable-shared 
    --disable-bmp
    --disable-gif
    --disable-jpg
    --disable-jpg-shared
    --disable-lbm
    --disable-pcx
    --disable-pnm
    --disable-svg
    --disable-tga
    --disable-tif
    --disable-tif-shared
    --disable-xcf
    --disable-xpm
    --disable-xv
    --disable-webp
    --disable-webp-shared
    --disable-png-shared
    --enable-png
    )

if(Funkout_BUILD_SDL2_STACK)
    set(SDL2_image_CONFIG_OPTIONS ${SDL2_image_CONFIG_OPTIONS}
        --with-sdl-prefix=${CMAKE_CURRENT_BINARY_DIR})
endif()

set(_libpng_link_flags "-Wl,-rpath=${LIBPNG_LIBRARY_DIR} -lpng")
ExternalProject_Add(
    SDL2_image
    DOWNLOAD_DIR ${Funkout_THIRD_PARTY_DOWNLOADS_DIR}
    URL https://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.4.zip
    URL_HASH SHA256=c95229485d6a0bdeccea63ce7580485ce1bfac723ff8a8545837d2d5a3b20446
    CONFIGURE_COMMAND ${CMAKE_CURRENT_BINARY_DIR}/SDL2_image-prefix/src/SDL2_image/configure LIBPNG_CFLAGS=-I${LIBPNG_INCLUDE_DIR} LIBPNG_LIBS=${_libpng_link_flags} ${SDL2_image_CONFIG_OPTIONS}
    BUILD_COMMAND make
    INSTALL_COMMAND make install
    )
add_dependencies(SDL2_image SDL2 libpng)

set(SDL2_IMAGE_INCLUDE_DIR ${Funkout_THIRD_PARTY_INSTALL_DIR}/include/SDL2)
set(SDL2_IMAGE_LIBRARY ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libSDL2_image.so)
set(SDL2_IMAGE_LIBRARY_STATIC ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libSDL2_image.a)
set(SDL2_IMAGE_INCLUDE_DIRS ${SDL2_IMAGE_INCLUDE_DIR})
set(SDL2_IMAGE_LIBRARIES ${SDL2_IMAGE_LIBRARY})
mark_as_advanced(SDL2_IMAGE_INCLUDE_DIR SDL2_IMAGE_LIBRARY SDL2_IMAGE_LIBRARY_DIR SDL2_IMAGE_LIBRARY_STATIC)

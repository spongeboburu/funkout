# FindSDL2
#
# Will define:
#
#   SDL2_FOUND - Was SDL2 found.
#   SDL2_INCLUDE_DIRS - Include directories for SDL2.
#   SDL2_LIBRARIES - All linkable libraries for SDL2.

find_path(SDL2_INCLUDE_DIR SDL.h PATH_SUFFIXES SDL2 DOC "SDL2 include path")
find_library(SDL2_LIBRARY NAMES SDL2 DOC "SDL2 libraries")

set(SDL2_INCLUDE_DIRS ${SDL2_INCLUDE_DIR})
set(SDL2_LIBRARIES ${SDL2_LIBRARY})

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(SDL2 DEFAULT_MSG SDL2_INCLUDE_DIR SDL2_LIBRARY)
mark_as_advanced(SDL2_INCLUDE_DIR SDL2_LIBRARY)

# BuildSDL2Mixer
#
# Will define:
#
#   SDL2_MIXER_INCLUDE_DIRS - Include directories for SDL2.
#   SDL2_MIXER_LIBRARIES - All linkable libraries for SDL2.
#   SDL2_MIXER_LIBRARY_STATIC - A static version of SDL2.
#
#   LIBOGG_INCLUDE_DIRS - Include directories for libogg.
#   LIBOGG_LIBRARIES - All linkable libraries for libogg.
#   LIBOGG_LIBRARY_STATIC - A static version of libogg.
#
#   LIBVORBIS_INCLUDE_DIRS - Include directories for libvorbis.
#   LIBVORBIS_LIBRARIES - All linkable libraries for libvorbis.
#   LIBVORBIS_LIBRARY_STATIC - A static version of libvorbis.

ExternalProject_Add(
    libogg
    DOWNLOAD_DIR ${Funkout_THIRD_PARTY_DOWNLOADS_DIR}
    URL https://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.4.zip
    URL_HASH SHA256=9affb8c7bf6fbffda0f6906bfb99c0ea50dca9b188ba9e15be90042dc03c5ded
    SOURCE_SUBDIR external/libogg-1.3.2
    CONFIGURE_COMMAND ${CMAKE_CURRENT_BINARY_DIR}/libogg-prefix/src/libogg/external/libogg-1.3.2/configure --prefix=${CMAKE_CURRENT_BINARY_DIR} --enable-shared --enable-static
    BUILD_COMMAND make
    INSTALL_COMMAND make install
    )
set(LIBOGG_INCLUDE_DIR ${Funkout_THIRD_PARTY_INSTALL_DIR}/include)
set(LIBOGG_LIBRARY ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libogg.so)
set(LIBOGG_LIBRARY_STATIC ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libogg.a)
set(LIBOGG_LIBRARY_DIR ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib)
set(LIBOGG_INCLUDE_DIRS ${LIBOGG_INCLUDE_DIR})
set(LIBOGG_LIBRARIES ${LIBOGG_LIBRARY})
mark_as_advanced(LIBOGG_INCLUDE_DIR LIBOGG_LIBRARY LIBOGG_LIBRARY_DIR LIBOGG_LIBRARY_STATIC)

ExternalProject_Add(
    libvorbis
    DOWNLOAD_DIR ${Funkout_THIRD_PARTY_DOWNLOADS_DIR}
    URL https://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.4.zip
    URL_HASH SHA256=9affb8c7bf6fbffda0f6906bfb99c0ea50dca9b188ba9e15be90042dc03c5ded
    SOURCE_SUBDIR external/libvorbis-1.3.5
    CONFIGURE_COMMAND ${CMAKE_CURRENT_BINARY_DIR}/libvorbis-prefix/src/libvorbis/external/libvorbis-1.3.5/configure --prefix=${CMAKE_CURRENT_BINARY_DIR} --enable-shared --enable-static --disable-oggtest --with-ogg-libraries=${CMAKE_CURRENT_BINARY_DIR}
    BUILD_COMMAND make
    INSTALL_COMMAND make install
    )
add_dependencies(libvorbis libogg)
set(LIBVORBIS_INCLUDE_DIR ${Funkout_THIRD_PARTY_INSTALL_DIR}/include)
set(LIBVORBIS_LIBRARY ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libvorbisfile.so ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libvorbisenc.so ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libvorbis.so)
set(LIBVORBIS_LIBRARY_STATIC ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libvorbisfile.a ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libvorbisenc.a ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libvorbis.a)
set(LIBVORBIS_LIBRARY_DIR ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib)
set(LIBVORBIS_INCLUDE_DIRS ${LIBVORBIS_INCLUDE_DIR})
set(LIBVORBIS_LIBRARIES ${LIBVORBIS_LIBRARY})
mark_as_advanced(LIBVORBIS_INCLUDE_DIR LIBVORBIS_LIBRARY LIBVORBIS_LIBRARY_DIR LIBVORBIS_LIBRARY_STATIC)


set(SDL2_mixer_CONFIG_OPTIONS
    --prefix=${Funkout_THIRD_PARTY_INSTALL_DIR}
    --enable-static
    --enable-shared 
    --disable-music-cmd
    --disable-music-wave
    --disable-music-mod
    --disable-music-mod-modplug
    --disable-music-mod-modplug-shared
    --disable-music-mod-mikmod
    --disable-music-mod-mikmod-shared
    --disable-music-midi
    --disable-music-midi-timidity
    --disable-music-midi-native
    --disable-music-midi-fluidsynth
    --disable-music-midi-fluidsynth-shared
    --disable-music-ogg-tremor
    --disable-music-ogg-shared
    --disable-music-flac
    --disable-music-flac-shared
    --disable-music-mp3
    --disable-music-mp3-mad-gpl
    --disable-music-mp3-mad-gpl-dithering
    --disable-music-mp3-mpg123
    --disable-music-mp3-mpg123-shared
    --disable-music-opus
    --disable-music-opus-shared
    --enable-music-ogg
    )

if(Funkout_BUILD_SDL2_STACK)
    set(SDL2_mixer_CONFIG_OPTIONS ${SDL2_mixer_CONFIG_OPTIONS}
        --with-sdl-prefix=${CMAKE_CURRENT_BINARY_DIR})
endif()

set(_libogg_link_flags "-Wl,-rpath=${LIBOGG_LIBRARY_DIR} -logg")
ExternalProject_Add(
    SDL2_mixer
    DOWNLOAD_DIR ${Funkout_THIRD_PARTY_DOWNLOADS_DIR}
    URL https://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.4.zip
    URL_HASH SHA256=9affb8c7bf6fbffda0f6906bfb99c0ea50dca9b188ba9e15be90042dc03c5ded
    CONFIGURE_COMMAND ${CMAKE_CURRENT_BINARY_DIR}/SDL2_mixer-prefix/src/SDL2_mixer/configure LIBOGG_CFLAGS=-I${LIBOGG_INCLUDE_DIR} LIBOGG_LIBS=${_libogg_link_flags} ${SDL2_mixer_CONFIG_OPTIONS}
    BUILD_COMMAND make
    INSTALL_COMMAND make install
    )
add_dependencies(SDL2_mixer SDL2 libvorbis)

set(SDL2_MIXER_INCLUDE_DIR ${Funkout_THIRD_PARTY_INSTALL_DIR}/include/SDL2)
set(SDL2_MIXER_LIBRARY ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libSDL2_mixer.so)
set(SDL2_MIXER_LIBRARY_STATIC ${Funkout_THIRD_PARTY_INSTALL_DIR}/lib/libSDL2_mixer.a)
set(SDL2_MIXER_INCLUDE_DIRS ${SDL2_MIXER_INCLUDE_DIR})
set(SDL2_MIXER_LIBRARIES ${SDL2_MIXER_LIBRARY})
mark_as_advanced(SDL2_MIXER_INCLUDE_DIR SDL2_MIXER_LIBRARY SDL2_MIXER_LIBRARY_DIR SDL2_MIXER_LIBRARY_STATIC)


# # SDL_mixer
# ExternalProject_Add(
#     build-sdl2-mixer
#     DOWNLOAD_DIR ${Funkout_DOWNLOADS_DIR}
#     URL https://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.4.zip
#     URL_HASH SHA256=9affb8c7bf6fbffda0f6906bfb99c0ea50dca9b188ba9e15be90042dc03c5ded
#     CONFIGURE_COMMAND ${CMAKE_CURRENT_BINARY_DIR}/build-sdl2-mixer-prefix/src/build-sdl2-mixer/configure --disable-music-cmd --disable-music-wave --disable-music-mod --disable-music-mod-modplug --disable-music-mod-modplug-shared --disable-music-mod-mikmod --disable-music-mod-mikmod-shared --disable-music-midi --disable-music-midi-timidity --disable-music-midi-native --disable-music-midi-fluidsynth --disable-music-midi-fluidsynth-shared --disable-music-ogg-tremor --disable-music-ogg-shared --disable-music-flac --disable-music-flac-shared --disable-music-mp3 --disable-music-mp3-mad-gpl --disable-music-mp3-mad-gpl-dithering --disable-music-mp3-mpg123 --disable-music-mp3-mpg123-shared --disable-music-opus --disable-music-opus-shared --enable-music-ogg --prefix=${Funkout_EXTERNAL_INSTALL_DIR} --enable-static --enable-shared --with-sdl-prefix=${CMAKE_CURRENT_BINARY_DIR}
#     BUILD_COMMAND make
#     )
# add_dependencies(build-sdl2-mixer build-sdl2)
# set(SDL2_MIXER_INCLUDE_DIR ${Funkout_EXTERNAL_INSTALL_DIR}/include/SDL2)
# set(SDL2_MIXER_LIBRARY ${Funkout_EXTERNAL_INSTALL_DIR}/lib/libSDL2_mixer.so)
# set(SDL2_MIXER_LIBRARY_STATIC ${Funkout_EXTERNAL_INSTALL_DIR}/lib/libSDL2_mixer.a)
# set(SDL2_MIXER_INCLUDE_DIRS ${SDL2_MIXER_INCLUDE_DIR})
# set(SDL2_MIXER_LIBRARIES ${SDL2_MIXER_LIBRARY})
# mark_as_advanced(SDL2_MIXER_INCLUDE_DIR SDL2_MIXER_LIBRARY SDL2_MIXER_LIBRARY_DIR SDL2_MIXER_LIBRARY_STATIC)

/**
 * @file client.h
 * @brief Client (rendering, input, etc).
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_CLIENT_H
#define FUNKOUT_CLIENT_H

#include "funkout/game.h"
#include "funkout/video.h"
#include "funkout/sprite.h"
#include "funkout/audio.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Used for getting an average frame count.
 */
#define FUNKOUT_CLIENT_FPS_SAMPLES 2

/**
 * Damage effect time.
 **/
#define FUNKOUT_CLIENT_BLOCK_DAMAGE_COOLDOWN 0.5

/**
 * Number of title stars.
 */
#define FUNKOUT_CLIENT_NUM_TITLE_STARS 256

/**
 * Menu item title max len.
 */
#define FUNKOUT_CLIENT_MENU_MAX_LEN 16

/**
 * Binding alternatives.
 */
#define FUNKOUT_CLIENT_BINDING_ALTERNATIVES 2

/**
 * Control cooldown.
 */
#define FUNKOUT_CLIENT_CONTROL_COOLDOWN 0.2

/**
 * Client settings.
 */
struct funkout_ClientSettings {

    /**
     * Audio volume 0-100.
     */
    int volume;

    /**
     * Fullscreen.
     */
    bool fullscreen;
};

/**
 * Client state.
 */
enum funkout_ClientState {

    /**
     * Splash.
     **/
    FUNKOUT_CLIENT_STATE_NONE,

    /**
     * Title.
     **/
    FUNKOUT_CLIENT_STATE_TITLE,

    /**
     * Game is on.
     **/
    FUNKOUT_CLIENT_STATE_GAME,

    /**
     * Quit.
     **/
    FUNKOUT_CLIENT_STATE_QUIT,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_CLIENT_STATE
};

/**
 * Text.
 */
struct funkout_ClientText {

    /**
     * Position.
     */
    struct funkout_Vector pos;

    /**
     * The text.
     */
    char * text;

    /**
     * The size (in number of blocks).
     */
    float size;

    /**
     * TTL.
     */
    float ttl;

    /**
     * Timestep.
     */
    float timestep;
};

/**
 * Particles.
 */
struct funkout_ClientParticle {

    /**
     * Position.
     */
    struct funkout_Vector pos;

    /**
     * Velocity vector.
     */
    struct funkout_Vector vel;

    /**
     * TTL.
     */
    float ttl;

    /**
     * Timestep of ttl.
     */
    float timestep;

    /**
     * Size (in number of blocks).
     **/
    float size;

    /**
     * Color.
     */
    struct funkout_Color color;
};


/**
 * Client menu item.
 */
struct funkout_ClientMenuItem {

    /**
     * Title.
     */
    char title[FUNKOUT_CLIENT_MENU_MAX_LEN];

    /**
     * Range.
     */
    int range;

    /**
     * Group.
     */
    int group;

    /**
     * Enabled.
     */
    bool enabled;

    /**
     * Sub menu.
     */
    const struct funkout_ClientMenu * menu;
};

/**
 * Menu.
 */
struct funkout_ClientMenu {
    
    /**
     * Title.
     */
    char title[FUNKOUT_CLIENT_MENU_MAX_LEN];

    /**
     * Number of items.
     */
    int num_items;
    
    /**
     * Items.
     */
    const struct funkout_ClientMenuItem * items;

    /**
     * Parent menu.
     */
    const struct funkout_ClientMenu * parent;
};

/**
 * Controlling the client.
 */
enum funkout_ClientControl {

    /**
     * Nothing!
     */
    FUNKOUT_CLIENT_CONTROL_NONE,

    /**
     * Menu toggle.
     */
    FUNKOUT_CLIENT_CONTROL_UI_MENU,

    /**
     * UI right.
     */
    FUNKOUT_CLIENT_CONTROL_UI_RIGHT,

    /**
     * UI down.
     */
    FUNKOUT_CLIENT_CONTROL_UI_DOWN,

    /**
     * UI left.
     */
    FUNKOUT_CLIENT_CONTROL_UI_LEFT,

    /**
     * UI up.
     */
    FUNKOUT_CLIENT_CONTROL_UI_UP,

    /**
     * UI up.
     */
    FUNKOUT_CLIENT_CONTROL_UI_ACTIVATE,

    /**
     * UI up.
     */
    FUNKOUT_CLIENT_CONTROL_UI_BACK,

    /**
     * Player right.
     */
    FUNKOUT_CLIENT_CONTROL_PLAYER_RIGHT,

    /**
     * Player down.
     */
    FUNKOUT_CLIENT_CONTROL_PLAYER_DOWN,

    /**
     * Player left.
     */
    FUNKOUT_CLIENT_CONTROL_PLAYER_LEFT,

    /**
     * Player up.
     */
    FUNKOUT_CLIENT_CONTROL_PLAYER_UP,

    /**
     * Player shoot.
     */
    FUNKOUT_CLIENT_CONTROL_PLAYER_SHOOT,

    /**
     * Player slow.
     */
    FUNKOUT_CLIENT_CONTROL_PLAYER_SLOW,

    /**
     * Max num.
     */
    MAX_FUNKOUT_CLIENT_CONTROL
};

/**
 * Control bindings.
 */
struct funkout_ClientBindings {

    /**
     * Keyboard bindings.
     */
    SDL_Scancode key[MAX_FUNKOUT_CLIENT_CONTROL][FUNKOUT_CLIENT_BINDING_ALTERNATIVES];

    /**
     * Controller axis.
     */
    SDL_GameControllerAxis axis[MAX_FUNKOUT_CLIENT_CONTROL][FUNKOUT_CLIENT_BINDING_ALTERNATIVES];

    /**
     * Controller button.
     */
    SDL_GameControllerButton button[MAX_FUNKOUT_CLIENT_CONTROL][FUNKOUT_CLIENT_BINDING_ALTERNATIVES];
};

/**
 * Internal images.
 */
enum funkout_ClientImage {

    /**
     * NULL.
     */
    FUNKOUT_CLIENT_IMAGE_NONE,
    
    /**
     * Logo image.
     */
    FUNKOUT_CLIENT_IMAGE_LOGO,

    /**
     * Star image.
     */
    FUNKOUT_CLIENT_IMAGE_STAR,

    /**
     * Small font image.
     */
    FUNKOUT_CLIENT_IMAGE_SMALL_FONT,

    /**
     * Menu font image.
     */
    FUNKOUT_CLIENT_IMAGE_MENU_FONT,

    /**
     * Explosion image.
     */
    FUNKOUT_CLIENT_IMAGE_EXPLOSION,

    /**
     * Explosion2 image.
     */
    FUNKOUT_CLIENT_IMAGE_EXPLOSION2,

    /**
     * Flash image.
     */
    FUNKOUT_CLIENT_IMAGE_FLASH,

    /**
     * Projectile bullet image.
     */
    FUNKOUT_CLIENT_IMAGE_PROJECTILE_BULLET,

    /**
     * Projectile beam image.
     */
    FUNKOUT_CLIENT_IMAGE_PROJECTILE_BEAM,

    /**
     * Projectile beal point image.
     */
    FUNKOUT_CLIENT_IMAGE_PROJECTILE_BEAM_POINT,

    /**
     * Enemy amoeba.
     */
    FUNKOUT_CLIENT_IMAGE_ENEMY_AMOEBA,

    /**
     * Enemy virus.
     */
    FUNKOUT_CLIENT_IMAGE_ENEMY_VIRUS,

    /**
     * Enemy satellite A.
     */
    FUNKOUT_CLIENT_IMAGE_ENEMY_SATA,

    /**
     * Enemy satellite B.
     */
    FUNKOUT_CLIENT_IMAGE_ENEMY_SATB,

    /**
     * Enemy satellite C.
     */
    FUNKOUT_CLIENT_IMAGE_ENEMY_SATC,

    /**
     * Enemy satellite D.
     */
    FUNKOUT_CLIENT_IMAGE_ENEMY_SATD,

    /**
     * Enemy plague.
     */
    FUNKOUT_CLIENT_IMAGE_ENEMY_PLAGUE,

    /**
     * Enemy tank.
     */
    FUNKOUT_CLIENT_IMAGE_ENEMY_TANK,

    /**
     * Tile 1.
     */
    FUNKOUT_CLIENT_IMAGE_TILE1,

    /**
     * Tile 2.
     */
    FUNKOUT_CLIENT_IMAGE_TILE2,

    /**
     * Tile 3.
     */
    FUNKOUT_CLIENT_IMAGE_TILE3,

    /**
     * Tile 4.
     */
    FUNKOUT_CLIENT_IMAGE_TILE4,

    /**
     * Tile 5.
     */
    FUNKOUT_CLIENT_IMAGE_TILE5,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_CLIENT_IMAGE
};

/**
 * Internal audios.
 */
enum funkout_ClientAudio {

    /**
     * NULL.
     */
    FUNKOUT_CLIENT_AUDIO_NONE,

    /**
     * Click 1
     */
    FUNKOUT_CLIENT_AUDIO_CLICK1,

    /**
     * Click 2
     */
    FUNKOUT_CLIENT_AUDIO_CLICK2,

    /**
     * Click 3
     */
    FUNKOUT_CLIENT_AUDIO_CLICK3,

    /**
     * Player spawned.
     */
    FUNKOUT_CLIENT_AUDIO_PLAYER_SPAWNED,

    /**
     * Player destroyed.
     */
    FUNKOUT_CLIENT_AUDIO_PLAYER_DESTROYED,

    /**
     * Ball bounce.
     */
    FUNKOUT_CLIENT_AUDIO_BALL_BOUNCE,

    /**
     * Block damaged.
     */
    FUNKOUT_CLIENT_AUDIO_BLOCK_DAMAGED,

    /**
     * Block destroyed.
     */
    FUNKOUT_CLIENT_AUDIO_BLOCK_DESTROYED,

    /**
     * Pickup drop extra life.
     */
    FUNKOUT_CLIENT_AUDIO_DROP_LIFE,

    /**
     * Pickup drop multiply.
     */
    FUNKOUT_CLIENT_AUDIO_DROP_MULTIPLY,

    /**
     * Pickup drop sticky.
     */
    FUNKOUT_CLIENT_AUDIO_DROP_STICKY,

    /**
     * Pickup drop death.
     */
    FUNKOUT_CLIENT_AUDIO_DROP_DEATH,

    /**
     * Enemy damaged.
     */
    FUNKOUT_CLIENT_AUDIO_ENEMY_DAMAGED,

    /**
     * Enemy destroyed.
     */
    FUNKOUT_CLIENT_AUDIO_ENEMY_DESTROYED,

    /**
     * Shoot projectile.
     */
    FUNKOUT_CLIENT_AUDIO_SHOOT_PROJECTILE,

    /**
     * Shoot beam.
     */
    FUNKOUT_CLIENT_AUDIO_SHOOT_BEAM,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_CLIENT_AUDIO
};

/**
 * A client.
 */
struct funkout_Client {

    /**
     * Settings.
     */
    struct funkout_ClientSettings settings;

    /**
     * The local game.
     */
    struct funkout_Game game;

    /**
     * State.
     */
    enum funkout_ClientState state;

    /**
     * Last time update was called.
     */
    float update_timestamp;

    /**
     * Frames per second samples.
     */
    int fps_samples[FUNKOUT_CLIENT_FPS_SAMPLES];

    /** 
     * Frames per second.
     */
    int fps;

    /**
     * Game timers.
     */
    struct {
        /**
         * Time a second.
         */
        float second_timer;

        /**
         * Time 10 seconds.
         */
        float ten_second_timer;
    } game_timers;

    /**
     * Title timers.
     */
    struct {
        /**
         * Time a second.
         */
        float second_timer;

        /**
         * Time 10 seconds.
         */
        float ten_second_timer;
    } title_timers;

    /**
     * Title timers.
     */
    struct {
        /**
         * Time a second.
         */
        float second_timer;

        /**
         * Time 10 seconds.
         */
        float ten_second_timer;
    } menu_timers;
    
    /**
     * Scroll text offset.
     */
    float scroll_text_offset;

    /**
     * Sprites.
     */
    struct funkout_SpriteArray sprites;

    /**
     * Player images.
     */
    struct funkout_Image * player_images[FUNKOUT_MAX_PLAYERS];

    /**
     * Ball images.
     */
    struct funkout_Image * ball_images[FUNKOUT_MAX_PLAYERS];

    /**
     * Drop images.
     */
    struct funkout_Image * drop_images[MAX_FUNKOUT_DROP_TYPE];

    /**
     * All other images.
     */
    struct funkout_Image * images[MAX_FUNKOUT_CLIENT_IMAGE];

    /**
     * Audio samples.
     */
    struct funkout_Audio * audio[MAX_FUNKOUT_CLIENT_AUDIO];

    /**
     * Block effects.
     */
    struct {

        /**
         * Damage "flash"
         */
        float damage_cooldown;

        /**
         * Doors for spawn blocks.
         */
        float door_cooldown;

    } block_effects[FUNKOUT_GAME_HEIGHT][FUNKOUT_GAME_WIDTH];

    /**
     * Particles.
     */
    struct funkout_ClientParticle * particles;

    /**
     * Particles.
     */
    int num_particles;

    /**
     * Floating texts.
     */
    struct funkout_ClientText * texts;

    /**
     * The number of texts.
     */
    int num_texts;

    /**
     * Title stars.
     */
    float title_stars[FUNKOUT_CLIENT_NUM_TITLE_STARS][3];

    /**
     * The current menu.
     */
    const struct funkout_ClientMenu * menu;

    /**
     * Last menu in use.
     */
    const struct funkout_ClientMenu * last_menu;

    /**
     * Selected menu item.
     */
    int menu_item;

    /**
     * Bindings.
     */
    struct funkout_ClientBindings bindings;

    /**
     * Controls.
     */
    float controls[MAX_FUNKOUT_CLIENT_CONTROL];

    /**
     * Relative controls.
     */
    float rel_controls[MAX_FUNKOUT_CLIENT_CONTROL];

    /**
     * Control cooldown.
     */
    float control_cooldown;

    /**
     * Remember volume.
     **/
    float remember_volume;

    /**
     * Number of levels to play.
     */
    int num_levels;

    /**
     * Levels.
     */
    struct funkout_Level *levels;

    /**
     * Current level.
     */
    int current_level;
};

/**
 * Initialize a client.
 * @param client The client struct to init.
 * @param settings The client settings.
 * @returns True on success, false on failure.
 * @note Will set error on failure.
 */
bool funkout_client_init(struct funkout_Client * client,
                         const struct funkout_ClientSettings * settings);

/**
 * Deinitialize a client.
 * @param client The client to deinit.
 */
void funkout_client_deinit(struct funkout_Client * client);

/**
 * Set level chain.
 * @param client The client.
 * @param num_levels The number of levels.
 * @param levels The levels.
 */
void funkout_client_set_levels(struct funkout_Client * client,
                               int num_levels,
                               const struct funkout_Level * levels);

/**
 * Update a client.
 * @param client The client to update.
 * @returns True if client is running, false if not.
 */
bool funkout_client_update(struct funkout_Client * client);

/**
 * Draw a client.
 * @param client The client to draw.
 * @param x X.
 * @param y Y.
 * @param w Width.
 * @param h Height.
 */
void funkout_client_draw(const struct funkout_Client * client,
                         int x, int y, int w, int h);

/**
 * Add some particles in circular spread.
 * @param client The client to add particles for.
 * @param num The number of particles.
 * @param x X.
 * @param y Y.
 * @param direction_x The direction direction.
 * @param direction_y The direction direction.
 * @param spread Spread (in degrees).
 * @param speed Speed.
 * @param speed_random Speed random addition.
 * @param ttl Time to live.
 * @param size Size (in number of blocks).
 * @param r Red.
 * @param g Green.
 * @param b Blue.
 * @param a Alpha.
 */
void funkout_client_add_particles1(struct funkout_Client * client,
                                   int num,
                                   float x, float y,
                                   float direction_x, float direction_y,
                                   float spread,
                                   float speed,
                                   float speed_random,
                                   float ttl,
                                   float size,
                                   float r, float g, float b, float a);

/**
 * Add some particles in a rectangular spread.
 * @param client The client to add particles for.
 * @param subdivisions Subdivide rectangle.
 * @param x X.
 * @param y Y.
 * @param ex X extent.
 * @param ey Y extent.
 * @param speed Speed.
 * @param ttl Time to live.
 * @param r Red.
 * @param g Green.
 * @param b Blue.
 * @param a Alpha.
 */
void funkout_client_add_particles2(struct funkout_Client * client,
                                   int subdivisions,
                                   float x, float y,
                                   float ex, float ey,
                                   float speed,
                                   float ttl,
                                   float r, float g, float b, float a);

/**
 * Update the controls.
 * @param client The client.
 * @returns True if any controls changed.
 */
bool funkout_client_update_controls(struct funkout_Client * client);

#ifdef __cplusplus
}
#endif
    
#endif

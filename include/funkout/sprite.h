/**
 * @file sprite.h
 * @brief Handle sprites.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_SPRITE_H
#define FUNKOUT_SPRITE_H

#include <stdbool.h>
#include "funkout/video.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Forward declare sprite.
 */
struct funkout_Sprite;

/**
 * Callback for sprite deinit.
 * @param sprite The sprite to deinit.
 */
typedef void (*FunkoutSpriteDeinit)(struct funkout_Sprite * sprite);

/**
 * Callback for sprite update.
 * @param sprite The sprite to update.
 * @param timestep The timestep.
 */
typedef void (*FunkoutSpriteUpdate)(struct funkout_Sprite * sprite, float timestep);

/**
 * A sprite.
 */
struct funkout_Sprite {

    /**
     * The game.
     */
    struct funkout_Game * game;
    
    /**
     * The position of the sprite.
     */
    struct funkout_Vector pos;

    /**
     * The extents of the sprite.
     */
    struct funkout_Vector extents;

    /**
     * The alpha value of the sprite.
     */
    float alpha;
    
    /**
     * The rotation of the sprite (degrees).
     */
    float rotation;
    
    /**
     * The alignment of the sprite.
     */
    struct funkout_Vector align;

    /**
     * The animation.
     */
    struct funkout_Animation animation;

    /**
     * The image used for the animation.
     */
    const struct funkout_Image * image;

    /**
     * TTL, <= 0 will disable.
     */
    float ttl;

    /**
     * Velocity.
     */
    struct funkout_Vector velocity;
    
    /**
     * Flag the sprite for removal.
     */
    bool remove_me;

    /**
     * Deinitializer callback.
     */
    FunkoutSpriteDeinit f_deinit;

    /**
     * Update callback.
     */
    FunkoutSpriteUpdate f_update;
};

/**
 * An array of sprites.
 */
struct funkout_SpriteArray {

    /**
     * The length of the array.
     */
    int length;

    /**
     * The sprites.
     */
    struct funkout_Sprite ** sprites;
};

/**
 * Initialize a sprite.
 * @param sprite The sprite struct to init.
 * @param pos_x The horizontal position of the sprite.
 * @param pos_y The vertical position of the sprite.
 * @param extent_x The horizontal extent of the sprite.
 * @param extent_y The vertical extent of the sprite.
 * @param animation The animation, may be NULL.
 * @param image The image for the animation, may be NULL.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_sprite_init(
    struct funkout_Sprite * sprite,
    float pos_x, float pos_y,
    float extent_x, float extent_y,
    const struct funkout_Animation * animation,
    const struct funkout_Image * image);

/**
 * Deinitialize the sprite.
 * @param sprite The sprite to deinitialize.
 * @note Will call sprite->f_deinit if != NULL.
 */
void funkout_sprite_deinit(struct funkout_Sprite * sprite);

/**
 * Create a new sprite.
 * @param pos_x The horizontal position of the sprite.
 * @param pos_y The vertical position of the sprite.
 * @param extent_x The horizontal extent of the sprite.
 * @param extent_y The vertical extent of the sprite.
 * @param animation The animation, may be NULL.
 * @param image The image for the animation, may be NULL.
 * @returns A new sprite.
 */
struct funkout_Sprite * funkout_sprite_new(
    float pos_x, float pos_y,
    float extent_x, float extent_y,
    const struct funkout_Animation * animation,
    const struct funkout_Image * image);

/**
 * Free the sprite.
 * @param sprite The sprite to free.
 * @note Will call funkout_sprite_deinit() which will call sprite->f_deinit if != NULL.
 */
void funkout_sprite_free(struct funkout_Sprite * sprite);

/**
 * Draw the sprite.
 * @param sprite The sprite to draw.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_sprite_draw(const struct funkout_Sprite * sprite);

/**
 * Draw the sprite.
 * @param sprite The sprite to draw.
 * @param x X.
 * @param y Y.
 * @param w W.
 * @param h H.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_sprite_draw2(const struct funkout_Sprite * sprite, int x, int y, int w, int h);

/**
 * Update the sprite.
 * @param sprite The sprite to update.
 * @param timestep The timestep.
 * @note Will call sprite->f_update if != NULL.
 */
void funkout_sprite_update(struct funkout_Sprite * sprite,
                           float timestep);

/**
 * Initialize a sprite array.
 * @param array The array struct to init.
 */
void funkout_sprite_array_init(
    struct funkout_SpriteArray * array);

/**
 * Deinitialize a sprite array.
 * @param array The array to deinit.
 * @param keep_sprites True to skip deinitialization of all
 *  sprites in the array.
 */
void funkout_sprite_array_deinit(
    struct funkout_SpriteArray * array,
    bool keep_sprites);

/**
 * Get a sprite from the array.
 * @param array The array to get a sprite from.
 * @param index The index of the sprite to get, negative values
 *  will wrap from the end so that -1 is the last element.
 * @returns The sprite or NULL if @p index is out of range.
 */
struct funkout_Sprite * funkout_sprite_array_get(
    const struct funkout_SpriteArray * array, int index);

/**
 * Get the index of a sprite in the array.
 * @param array The array to find a sprite in.
 * @param sprite The sprite to find.
 * @returns The index of the sprite or -1 if not found.
 */
int funkout_sprite_array_find(
    const struct funkout_SpriteArray * array,
    const struct funkout_Sprite * sprite);

/**
 * Append a sprite to the array.
 * @param array The array to append a sprite to.
 * @param sprite The sprite to append.
 */
void funkout_sprite_array_append(
    struct funkout_SpriteArray * array,
    struct funkout_Sprite * sprite);

/**
 * Remove a sprite at an index from the array.
 * @param array The array to remove a sprite from.
 * @param index The index of the sprite to remove.
 * @param keep_sprite True to skip deinitialization of the
 *  sprite.
 * @returns True if the sprite was removed, false if @p index
 *  is out of range.
 */
bool funkout_sprite_array_remove_at(
    struct funkout_SpriteArray * array,
    int index,
    bool keep_sprite);

/**
 * Remove a sprite from the array.
 * @param array The array to remove a sprite from.
 * @param sprite The sprite to remove.
 * @param keep_sprite True to skip deinitialization of the
 *  sprite.
 * @returns True if the sprite was removed, false if @p sprite
 *  is not in the array.
 */
bool funkout_sprite_array_remove(
    struct funkout_SpriteArray * array,
    const struct funkout_Sprite * sprite,
    bool keep_sprite);

/**
 * Remove all sprites with the remove_me flag.
 * @param array The array to remove sprites from.
 * @param keep_sprite True to skip deinitialization of the
 *  sprites.
 * @returns The number of removed sprites.
 */
int funkout_sprite_array_prune(
    struct funkout_SpriteArray * array,
    bool keep_sprite);

/**
 * Update the sprite array.
 * @param array The sprite array to update.
 * @param timestep The timestep.
 * @note Will call funkout_sprite_update() for all sprites
 *  which will call sprite->f_update if != NULL.
 */
void funkout_sprite_array_update(
    struct funkout_SpriteArray * array,
    float timestep);

#ifdef __cplusplus
}
#endif
    
#endif

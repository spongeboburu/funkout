/**
 * @file geometry.h
 * @brief Basic geometry.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_GEOMETRY_H
#define FUNKOUT_GEOMETRY_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MAX
/**
 * Pretty tired of writing this over and over so here's a macro.
 * @param a Value A.
 * @param b Value B.
 * @returns The greater value.
 */
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif

#ifndef MIN
/**
 * Pretty tired of writing this over and over so here's a macro.
 * @param a Value A.
 * @param b Value B.
 * @returns The lesser value.
 */
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

#ifndef MINMAX
/**
 * Pretty tired of writing this over and over so here's a macro.
 * @param v The value to limit.
 * @param vmin Minimum.
 * @param vmax Maximum.
 * @returns A value limited so that @p v >= @p vmin and <= @p vmax.
 */
#define MINMAX(v, vmin, vmax) ((v) < (vmin) ? (vmin) : ((v) > (vmax) ? (vmax) : (v)))
#endif

/**
 * Flag to go into negative infinity when doing line intersections.
 */
#define FUNKOUT_INF_FLAG_NEG 1

/**
 * Flag to go into positive infinity when doing line intersections.
 */
#define FUNKOUT_INF_FLAG_POS 2

/**
 * Flag to go into infinite for both directions when doing line intersections.
 */
#define FUNKOUT_INF_FLAG 3

/**
 * An integer rectangle.
 */
struct funkout_IRect {

    /**
     * Horizontal position.
     */
    int x;

    /**
     * Vertical position.
     */
    int y;

    /**
     * Width.
     */
    int w;

    /**
     * Height.
     */
    int h;
};

/**
 * A 2D vector.
 */
struct funkout_Vector {

    /**
     * Horizontal position.
     */
    float x;

    /**
     * Vertical position.
     */
    float y;
};

/**
 * An axis aligned bounding box.
 */
struct funkout_AABB {

    /**
     * Position.
     */
    struct funkout_Vector pos;

    /**
     * Extents.
     */
    struct funkout_Vector extents;
};

/**
 * A line (2 points).
 */
struct funkout_Line {

    /**
     * Position 1.
     */
    struct funkout_Vector pos1;

    /**
     * Position 2.
     */
    struct funkout_Vector pos2;
};

/**
 * A circle.
 */
struct funkout_Circle {

    /**
     * Position.
     */
    struct funkout_Vector pos;

    /**
     * Radius.
     */
    float r;
};

/**
 * A (convex) polygon.
 */
struct funkout_Polygon {

    /**
     * Position.
     */
    struct funkout_Vector pos;

    /**
     * The number of points in the polygon (3 to 8).
     */
    int num;
    
    /**
     * Points.
     */
    struct funkout_Vector points[8];
};

/**
 * Different kinds of shapes.
 */
enum funkout_ShapeType {

    /**
     * AABB.
     */
    FUNKOUT_SHAPE_AABB,

    /**
     * Circle.
     */
    FUNKOUT_SHAPE_CIRCLE,

    /**
     * Polygon.
     */
    FUNKOUT_SHAPE_POLYGON,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_SHAPE
};

/**
 * Shape specific data.
 */
union funkout_ShapeData {

    /**
     * Any shape has at least this member.
     */
    struct {

        /**
         * All shape structs must have their first member as pos.
         */
        struct funkout_Vector pos;
        
    } any;
    
    /**
     * AABB.
     */
    struct funkout_AABB aabb;

    /**
     * Circle.
     */
    struct funkout_Circle circle;

    /**
     * Polygon.
     */
    struct funkout_Polygon polygon;
};

/**
 * A shape.
 */
struct funkout_Shape {

    /**
     * The type.
     */
    enum funkout_ShapeType type;

    /**
     * Shape specific data.
     */
    union funkout_ShapeData s;
};

/**
 * Normalize vector.
 * @param x X.
 * @param y Y.
 * @param[out] ret_vector Return the normalized vector.
 * @note If the vector has zero length the return vector will be zeroed.
 */
void funkout_vector_normalize(float x, float y,
                              struct funkout_Vector * ret_vector);

/**
 * Fit the AABB inside a bounding AABB.
 * @param pos_x Horizontal position for the AABB to fit.
 * @param pos_y Vertical position for the AABB to fit.
 * @param extent_x Horizontal extent for the AABB to fit.
 * @param extent_y Vertical extent for the AABB to fit.
 * @param bounding_pos_x Bounding AABB horizontal position.
 * @param bounding_pos_y Bounding AABB vertical position.
 * @param bounding_extent_x Bounding AABB horizontal extent.
 * @param bounding_extent_y Bounding AABB vertical extent.
 * @param[out] ret_aabb Return the fitted AABB.
 */
void funkout_aabb_fit(float pos_x, float pos_y,
                      float extent_x, float extent_y,
                      float bounding_pos_x, float bounding_pos_y,
                      float bounding_extent_x, float bounding_extent_y,
                      struct funkout_AABB * ret_aabb);

/**
 * Fit the integer rectangle inside a bounding integer rectangle.
 * @param pos_x Left position for the rectangle.
 * @param pos_y Top position for the rectangle.
 * @param width Width of the rectangle.
 * @param height Height of the rectangle.
 * @param bounding_pos_x Bounding rectangle left position.
 * @param bounding_pos_y Bounding rectangle top position.
 * @param bounding_width Bounding rectangle width.
 * @param bounding_height Bounding rectangle height.
 * @param[out] ret_irect Return the fitted AABB.
 */
void funkout_irect_fit(int pos_x, int pos_y,
                       int width, int height,
                       int bounding_pos_x, int bounding_pos_y,
                       int bounding_width, int bounding_height,
                       struct funkout_IRect * ret_irect);

/**
 * Get AABB from a polygon.
 * @param num_points THe number of points in the polygon.
 * @param points The points of the polygon.
 * @param[out] ret_aabb Return the AABB.
 */
void funkout_polygon_get_aabb(int num_points,
                              const struct funkout_Vector * points,
                              struct funkout_AABB * ret_aabb);

/**
 * Get the closest point on a line.
 * @param pos_x Horizontal position to measure from.
 * @param pos_y Vertical position to measure from.
 * @param line_pos1_x Line vertical position 1.
 * @param line_pos1_y Line horizontal position 1.
 * @param line_pos2_x Line vertical position 2.
 * @param line_pos2_y Line horizontal position 2.
 * @param iflags Infinity flags.
 * @param[out] ret_vector Return the point.
 */
void funkout_closest_point_on_line(float pos_x, float pos_y,
                                   float line_pos1_x, float line_pos1_y,
                                   float line_pos2_x, float line_pos2_y,
                                   unsigned int iflags,
                                   struct funkout_Vector * ret_vector);

/**
 * Get the point where two lines intersect.
 * @param line1_pos1_x Line 1 pos 1 X.
 * @param line1_pos1_y Line 1 pos 1 Y.
 * @param line1_pos2_x Line 1 pos 2 X.
 * @param line1_pos2_y Line 1 pos 2 Y.
 * @param iflags1 Infinity flags for line 1.
 * @param line2_pos1_x Line 2 pos 1 X.
 * @param line2_pos1_y Line 2 pos 1 Y.
 * @param line2_pos2_x Line 2 pos 2 X.
 * @param line2_pos2_y Line 2 pos 2 Y.
 * @param iflags2 Infinity flags for line 2.
 * @param[out] ret_vector Return the intersecting point, may be NULL.
 */
bool funkout_line_intersect(float line1_pos1_x, float line1_pos1_y,
                            float line1_pos2_x, float line1_pos2_y,
                            unsigned int iflags1,
                            float line2_pos1_x, float line2_pos1_y,
                            float line2_pos2_x, float line2_pos2_y,
                            unsigned int iflags2,
                            struct funkout_Vector * ret_vector);


/**
 * Check if a line intersects an AABB.
 * @param line_pos1_x Horizontal position of point 1.
 * @param line_pos1_y Vertical position of point 1.
 * @param line_pos2_x Horizontal position of point 2.
 * @param line_pos2_y Vertical position of point 2.
 * @param iflags Infinity flags for the line.
 * @param aabb_pos_x The horizontal position of the AABB.
 * @param aabb_pos_y The vertical position of the AABB.
 * @param aabb_extent_x The horizontal extent of the AABB.
 * @param aabb_extent_y The vertical extent of the AABB.
 * @param[out] ret_vectors Return intersecting points (maximum 2), may be NULL.
 * @returns The number of intersect points returned.
 */
int funkout_line_aabb_intersect(float line_pos1_x, float line_pos1_y,
                                float line_pos2_x, float line_pos2_y,
                                unsigned int iflags,
                                float aabb_pos_x, float aabb_pos_y,
                                float aabb_extent_x, float aabb_extent_y,
                                struct funkout_Vector ret_vectors[2]);


/**
 * Check if a line intersects a circle.
 * @param line_pos1_x Horizontal position of point 1.
 * @param line_pos1_y Vertical position of point 1.
 * @param line_pos2_x Horizontal position of point 2.
 * @param line_pos2_y Vertical position of point 2.
 * @param iflags Infinity flags for the line.
 * @param circle_pos_x Horizontal position the circle.
 * @param circle_pos_y Vertical position the circle.
 * @param circle_radius Radius of the circle.
 * @param[out] ret_vectors Return intersecting points (maximum 2), may be NULL.
 * @returns The number of intersect points returned.
 */
int funkout_line_circle_intersect(float line_pos1_x, float line_pos1_y,
                                  float line_pos2_x, float line_pos2_y,
                                  unsigned int iflags,
                                  float circle_pos_x, float circle_pos_y,
                                  float circle_radius,
                                  struct funkout_Vector ret_vectors[2]);

/**
 * Check if a line intersects a polygon.
 * @param line_pos1_x Horizontal position of point 1.
 * @param line_pos1_y Vertical position of point 1.
 * @param line_pos2_x Horizontal position of point 2.
 * @param line_pos2_y Vertical position of point 2.
 * @param iflags Infinity flags for the line.
 * @param polygon_pos_x Horizontal position the polygon.
 * @param polygon_pos_y Vertical position the polygon.
 * @param polygon_num The number of points in the polygon (3-8).
 * @param polygon_points The polygon points.
 * @param[out] ret_vectors Return intersecting points (maximum 2), may be NULL.
 * @returns The number of intersect points returned.
 */
int funkout_line_polygon_intersect(float line_pos1_x, float line_pos1_y,
                                   float line_pos2_x, float line_pos2_y,
                                   unsigned int iflags,
                                   float polygon_pos_x, float polygon_pos_y,
                                   int polygon_num,
                                   const struct funkout_Vector * polygon_points,
                                   struct funkout_Vector ret_vectors[2]);

/**
 * Check if two AABBs intersect.
 * @param aabb1_pos_x The horizontal position of AABB 1.
 * @param aabb1_pos_y The vertical position of AABB 1.
 * @param aabb1_extent_x The horizontal extent of AABB 1.
 * @param aabb1_extent_y The vertical extent of AABB 1.
 * @param aabb2_pos_x The horizontal position of AABB 2.
 * @param aabb2_pos_y The vertical position of AABB 2.
 * @param aabb2_extent_x The horizontal extent of AABB 2.
 * @param aabb2_extent_y The vertical extent of AABB 2.
 * @param[out] ret_min_translation Return the minimum translation to move 
 *  AABB 1 out from AABB 2, may be NULL.
 * @returns True if the AABBs intersect or one is inside the other.
 */
bool funkout_aabb_intersect(float aabb1_pos_x, float aabb1_pos_y,
                            float aabb1_extent_x, float aabb1_extent_y,
                            float aabb2_pos_x, float aabb2_pos_y,
                            float aabb2_extent_x, float aabb2_extent_y,
                            struct funkout_Vector * ret_min_translation);

/**
 * Check if two circles intersect.
 * @param circle1_pos_x The horizontal position of circle 1.
 * @param circle1_pos_y The vertical position of circle 1.
 * @param circle1_radius Radius for circle 1.
 * @param circle2_pos_x The horizontal position of circle 2.
 * @param circle2_pos_y The vertical position of circle 2.
 * @param circle2_radius Radius for circle 2.
 * @param[out] ret_min_translation Return the minimum translation to move 
 *  circle 1 out from circle 2, may be NULL.
 * @returns True if the circles intersect or one is inside the other.
 */
bool funkout_circle_intersect(float circle1_pos_x, float circle1_pos_y,
                              float circle1_radius,
                              float circle2_pos_x, float circle2_pos_y,
                              float circle2_radius,
                              struct funkout_Vector * ret_min_translation);

/**
 * Check if two (convex) polygons intersect.
 * @param polygon1_pos_x The horizontal position of polygon 1.
 * @param polygon1_pos_y The vertical position of polygon 1.
 * @param polygon1_num The number of points in polygon 1 (3-8).
 * @param polygon1_points The points of polygon 1.
 * @param polygon2_pos_x The horizontal position of polygon 2.
 * @param polygon2_pos_y The vertical position of polygon 2.
 * @param polygon2_num The number of points in polygon 2 (3-8).
 * @param polygon2_points The points of polygon 2.
 * @param[out] ret_min_translation Return the minimum translation to move 
 *  polygon 1 out from polygon 2, may be NULL.
 * @returns True if the polygons intersect or one is inside the other.
 */
bool funkout_polygon_intersect(float polygon1_pos_x, float polygon1_pos_y,
                               int polygon1_num,
                               const struct funkout_Vector * polygon1_points,
                               float polygon2_pos_x, float polygon2_pos_y,
                               int polygon2_num,
                               const struct funkout_Vector * polygon2_points,
                               struct funkout_Vector * ret_min_translation);

/**
 * Check if an AABB intersects a circle.
 * @param aabb_pos_x The horizontal position of the AABB.
 * @param aabb_pos_y The vertical position of the AABB.
 * @param aabb_extent_x The horizontal extent of the AABB.
 * @param aabb_extent_y The vertical extent of the AABB.
 * @param circle_pos_x The horizontal position of the circle.
 * @param circle_pos_y The vertical position of the circle.
 * @param circle_radius Radius for circle.
 * @param[out] ret_min_translation Return the minimum translation to move the 
 *  AABB out from the circle, may be NULL.
 * @returns True if the AABB intersects with the circle or one is inside the 
 *  other.
 */
bool funkout_aabb_circle_intersect(float aabb_pos_x, float aabb_pos_y,
                                   float aabb_extent_x, float aabb_extent_y,
                                   float circle_pos_x, float circle_pos_y,
                                   float circle_radius,
                                   struct funkout_Vector * ret_min_translation);


#ifdef __cplusplus
}
#endif
    
#endif

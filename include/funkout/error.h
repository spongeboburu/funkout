/**
 * @file error.h
 * @brief Handle error messages.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_ERROR_H
#define FUNKOUT_ERROR_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

/**
 * The size of the error message string, including zero terminator.
 */
#define FUNKOUT_ERROR_MSG_SIZE 8192

/**
 * Used to convey errors.
 */
struct funkout_Error {

    /**
     * The error code.
     */
    int code;

    /**
     * The error message.
     */
    const char * msg;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get error info for the last error that occurred.
 * @returns A pointer to the error struct for the last error that occurred.
 * @note Even if an error has not occurred yet this function will return a pointer to a struct with code set to zero and the msg pointing to an empty string.
 */
const struct funkout_Error * funkout_get_error(void);

/**
 * Set the error struct.
 * @param code The error code.
 * @param format The format for the error message.
 * @param args The argument list.
 * @note The maximum size of the message is 8k (@ref FUNKOUT_ERROR_MSG_SIZE) including the zero terminator.
 */
void funkout_set_error_va(int code, const char * format, va_list args);

/**
 * Set the error struct.
 * @param code The error code.
 * @param format The format for the error message.
 * @note The maximum size of the message is 8k (@ref FUNKOUT_ERROR_MSG_SIZE) including the zero terminator.
 */
#ifdef __GNUC__
void funkout_set_error(int code, const char * format, ...) __attribute__ ((format (printf, 2, 3)));
#else
void funkout_set_error(int code, const char * format, ...);
#endif

/**
 * Clear the error.
 */
void funkout_clear_error(void);

#ifdef __cplusplus
}
#endif
    
#endif

/**
 * @file collision.h
 * @brief Handle collisions.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_COLLISION_H
#define FUNKOUT_COLLISION_H

#include <stdbool.h>
#include "funkout/geometry.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Check collision between two possibly moving AABBs.
 * @param aabb1_pos_x The horizontal position of AABB 1.
 * @param aabb1_pos_y The vertical position of AABB 1.
 * @param aabb1_extent_x The horizontal extent of AABB 1.
 * @param aabb1_extent_y The vertical extent of AABB 1.
 * @param aabb1_delta_x Horizontal delta for AABB 1.
 * @param aabb1_delta_y Vertical delta for AABB 1.
 * @param aabb2_pos_x The horizontal position of AABB 2.
 * @param aabb2_pos_y The vertical position of AABB 2.
 * @param aabb2_extent_x The horizontal extent of AABB 2.
 * @param aabb2_extent_y The vertical extent of AABB 2.
 * @param aabb2_delta_x Horizontal delta for AABB 2.
 * @param aabb2_delta_y Vertical delta for AABB 2.
 * @param[out] ret_time Return T for the collision (0-1).
 * @param[out] ret_min_translation The minimum translation vector for AABB 1, NULL is permitted.
 * @param[out] ret_normal Return the collision normal for AABB 1, NULL is permitted.
 * @returns True on collision.
 * @note @p ret_min_translation Will be 0,0 if AABB 1 and AABB 2 did not start intersected.
 * @note @p ret_time Will be zero if AABB 1 and AABB2 started intersected.
 */
bool funkout_aabb_collision(float aabb1_pos_x, float aabb1_pos_y,
                            float aabb1_extent_x, float aabb1_extent_y,
                            float aabb1_delta_x, float aabb1_delta_y,
                            float aabb2_pos_x, float aabb2_pos_y,
                            float aabb2_extent_x, float aabb2_extent_y,
                            float aabb2_delta_x, float aabb2_delta_y,
                            float * ret_time,
                            struct funkout_Vector * ret_min_translation,
                            struct funkout_Vector * ret_normal);

/**
 * Check collision between two possibly moving circles.
 * @param circle1_pos_x The horizontal position of circle 1.
 * @param circle1_pos_y The vertical position of circle 1.
 * @param circle1_radius The radius of circle 1.
 * @param circle1_delta_x Horizontal delta for circle 1.
 * @param circle1_delta_y Vertical delta for circle 1.
 * @param circle2_pos_x The horizontal position of circle 2.
 * @param circle2_pos_y The vertical position of circle 2.
 * @param circle2_radius The radius of circle 2.
 * @param circle2_delta_x Horizontal delta for circle 2.
 * @param circle2_delta_y Vertical delta for circle 2.
 * @param[out] ret_time Return T for the collision (0-1).
 * @param[out] ret_min_translation The minimum translation vector for AABB 1, NULL is permitted.
 * @param[out] ret_normal Return the collision normal for AABB 1, NULL is permitted.
 * @returns True on collision.
 * @note @p ret_min_translation Will be 0,0 if AABB 1 and AABB 2 did not start intersected.
 * @note @p ret_time Will be zero if AABB 1 and AABB2 started intersected.
 */
bool funkout_circle_collision(float circle1_pos_x, float circle1_pos_y,
                              float circle1_radius,
                              float circle1_delta_x, float circle1_delta_y,
                              float circle2_pos_x, float circle2_pos_y,
                              float circle2_radius,
                              float circle2_delta_x, float circle2_delta_y,
                              float * ret_time,
                              struct funkout_Vector * ret_min_translation,
                              struct funkout_Vector * ret_normal);

/**
 * Check collision between two possibly moving circles.
 * @param aabb_pos_x The horizontal position of AABB.
 * @param aabb_pos_y The vertical position of AABB.
 * @param aabb_extent_x The horizontal extent of AABB.
 * @param aabb_extent_y The vertical extent of AABB.
 * @param aabb_delta_x Horizontal delta for AABB.
 * @param aabb_delta_y Vertical delta for AABB.
 * @param circle_pos_x The horizontal position of circle
 * @param circle_pos_y The vertical position of circle
 * @param circle_radius The radius of circle
 * @param circle_delta_x Horizontal delta for circle
 * @param circle_delta_y Vertical delta for circle
 * @param[out] ret_time Return T for the collision (0-1).
 * @param[out] ret_min_translation The minimum translation vector for AABB 1, NULL is permitted.
 * @param[out] ret_normal Return the collision normal for AABB 1, NULL is permitted.
 * @returns True on collision.
 * @note @p ret_min_translation Will be 0,0 if AABB 1 and AABB 2 did not start intersected.
 * @note @p ret_time Will be zero if AABB 1 and AABB2 started intersected.
 */
bool funkout_aabb_circle_collision(float aabb_pos_x, float aabb_pos_y,
                                   float aabb_extent_x, float aabb_extent_y,
                                   float aabb_delta_x, float aabb_delta_y,
                                   float circle_pos_x, float circle_pos_y,
                                   float circle_radius,
                                   float circle_delta_x, float circle_delta_y,
                                   float * ret_time,
                                   struct funkout_Vector * ret_min_translation,
                                   struct funkout_Vector * ret_normal);

/**
 * Check collision between two possibly moving shapes.
 * @param shape1 The first shape.
 * @param shape1_delta_x Horizontal delta for shape 1.
 * @param shape1_delta_y Vertical delta for shape 1.
 * @param shape2 The second shape.
 * @param shape2_delta_x Horizontal delta for shape 2.
 * @param shape2_delta_y Vertical delta for shape 2.
 * @param[out] ret_time Return T for the collision (0-1).
 * @param[out] ret_min_translation The minimum translation vector for @p shape1, NULL is permitted.
 * @param[out] ret_normal Return the collision normal for @p shape1, NULL is permitted.
 * @returns True on collision.
 * @note @p ret_min_translation Will be 0,0 if @p shape1 and @p shape2 did not start intersected.
 * @note @p ret_time Will be zero if @p shape1 and @p shape2 started intersected.
 */
bool funkout_shape_collision(const struct funkout_Shape * shape1,
                             float shape1_delta_x, float shape1_delta_y,
                             const struct funkout_Shape * shape2,
                             float shape2_delta_x, float shape2_delta_y,
                             float * ret_time,
                             struct funkout_Vector * ret_min_translation,
                             struct funkout_Vector * ret_normal);


#ifdef __cplusplus
}
#endif
    
#endif

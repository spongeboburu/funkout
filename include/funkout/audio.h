/**
 * @file audio.h
 * @brief Handle audio.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_AUDIO_H
#define FUNKOUT_AUDIO_H

#include <stdlib.h>
#include <stdbool.h>

/**
 * The number of channels to use.
 */
#define FUNKOUT_AUDIO_NUM_CHANNELS 16

/**
 * The number of reserved channels to use.
 */
#define FUNKOUT_AUDIO_NUM_RESERVED_CHANNELS 4

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Opaque audio type.
 */
struct funkout_Audio;

/**
 * Initialize audio.
 * @param frequency Mix freq.
 * @returns True on success, false on failure.
 * @note Will set error on failure.
 */
bool funkout_init_audio(int frequency);

/**
 * Deinitialize audio.
 */
void funkout_deinit_audio(void);

/**
 * Check if we have audio or not.
 * @returns True if audio is available, false if not.
 */
bool funkout_has_audio(void);

/**
 * Load new audio.
 * @param data The data.
 * @param size The size of the data.
 * @returns A new image on success and NULL on failure.
 * @note Will set error on failure.
 */
struct funkout_Audio * funkout_audio_new(const void * data, size_t size);

/**
 * Free the audio.
 * @param audio The audio to free.
 */
void funkout_audio_free(struct funkout_Audio * audio);

/**
 * Free the audio.
 * @param audio The audio to play.
 * @param channel The channel to use, -1 for any available.
 * @param loops The number of loops, -1 infinite.
 * @returns True on success, false on failure.
 * @note Will set error on failure.
 */
bool funkout_audio_play(const struct funkout_Audio * audio, int channel, int loops);

/**
 * Get the current volume.
 * @returns The current volume 0-1.
 */
float funkout_audio_get_volume(void);

/**
 * Set the volume.
 * @param volume The volume 0-1.
 */
void funkout_audio_set_volume(float volume);

#ifdef __cplusplus
}
#endif
    
#endif

/**
 * @file time.h
 * @brief Get timestamp.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_TIME_H
#define FUNKOUT_TIME_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get the current time in seconds.
 * @returns The current time.
 */
double funkout_time(void);

#ifdef __cplusplus
}
#endif
    
#endif

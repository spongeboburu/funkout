/**
 * @file version.h
 * @brief Hold the version string.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_VERSION_H
#define FUNKOUT_VERSION_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get the version string.
 * @returns The version string.
 */
const char* funkout_get_version(void);

#ifdef __cplusplus
}
#endif
    
#endif

/**
 * @file game.h
 * @brief Common game types and functions.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_GAME_H
#define FUNKOUT_GAME_H

#include <stdbool.h>
#include <stdlib.h>
#include "funkout/geometry.h"
#include "funkout/color.h"
#include "funkout/animation.h"

/**
 * The number of blocks in width.
 **/
#define FUNKOUT_GAME_WIDTH 15

/**
 * The number of blocks in height.
 **/
#define FUNKOUT_GAME_HEIGHT 19

/**
 * The default player radius (in number of blocks).
 */
#define FUNKOUT_PLAYER_RADIUS 1.0

/**
 * The default player speed (in number of blocks per second).
 */
#define FUNKOUT_PLAYER_SPEED 8.0

/**
 * The maximum number of players.
 */
#define FUNKOUT_MAX_PLAYERS 4

/**
 * The ball radius (in number of blocks).
 */
#define FUNKOUT_BALL_RADIUS 0.25

/**
 * The ball speed (in number of blocks per second).
 */
#define FUNKOUT_BALL_SPEED 8.0

/**
 * The time to cooldown start.
 */
#define FUNKOUT_GAME_START_COOLDOWN 2.0

/**
 * The end to cooldown start.
 */
#define FUNKOUT_GAME_END_COOLDOWN 3.0

/**
 * The time to cooldown respawn.
 */
#define FUNKOUT_GAME_RESPAWN_COOLDOWN 1.0

/**
 * The time to cooldown destroyed.
 */
#define FUNKOUT_GAME_DESTROYED_COOLDOWN 1.0

/**
 * The speed of drops.
 */
#define FUNKOUT_GAME_DROP_SPEED 3.0

/**
 * Enemy stun cooldown.
 */
#define FUNKOUT_GAME_ENEMY_STUN_COOLDOWN 1.0

/**
 * Max score accumulator.
 */
#define FUNKOUT_GAME_MAX_SCORE_ACCUMULATOR 10

/**
 * Get a block type string.
 */
#define FUNKOUT_BLOCK_TYPE_STRING(t) ((t) >= FUNKOUT_BLOCK_TYPE_NONE && (t) < MAX_FUNKOUT_BLOCK_TYPE ? FUNKOUT_BLOCK_TYPE_STRINGS[(t)] : "")

/**
 * Get a drop type string.
 */
#define FUNKOUT_DROP_TYPE_STRING(t) ((t) >= FUNKOUT_DROP_TYPE_NONE && (t) < MAX_FUNKOUT_DROP_TYPE ? FUNKOUT_DROP_TYPE_STRINGS[(t)] : "")

/**
 * Get a enemy appearance string.
 */
#define FUNKOUT_ENEMY_APPEARANCE_STRING(t) ((t) >= FUNKOUT_ENEMY_APPEARANCE_NONE && (t) < MAX_FUNKOUT_ENEMY_APPEARANCE ? FUNKOUT_ENEMY_APPEARANCE_STRINGS[(t)] : "")

/**
 * Get a weapon type string.
 */
#define FUNKOUT_WEAPON_TYPE_STRING(t) ((t) >= FUNKOUT_WEAPON_TYPE_NONE && (t) < MAX_FUNKOUT_WEAPON_TYPE ? FUNKOUT_WEAPON_TYPE_STRINGS[(t)] : "")


/**
 * A weapon type.
 */
enum funkout_WeaponType {

    /**
     * No weapon.
     */
    FUNKOUT_WEAPON_TYPE_NONE,

    /**
     * A projectile that travels in a straight line.
     */
    FUNKOUT_WEAPON_TYPE_STRAIGHT,

    /**
     * A beam in such as a laser.
     */
    FUNKOUT_WEAPON_TYPE_BEAM,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_WEAPON_TYPE
};

/**
 * Weapon definition.
 */
struct funkout_WeaponDef {

    /**
     * Weapon type.
     */
    enum funkout_WeaponType type;

    /**
     * Cooldown between each shot.
     */
    float cooldown;

    /**
     * Add random cooldown time between each shot.
     */
    float cooldown_random;

    /**
     * Cooldown while firing.
     */
    float firing_cooldown;

    /**
     * Cooldown after fired.
     */
    float fired_cooldown;

    /**
     * The speed of the projectile or total time of beam.
     */
    float speed;

    /**
     * The size of the bullet or beam.
     */
    float size;

    /**
     * TTL.
     */
    float ttl;
};

/**
 * A projectile.
 */
struct funkout_Projectile {

    /**
     * Type.
     */
    enum funkout_WeaponType type;

    /**
     * Position.
     */
    struct funkout_Vector pos;

    /**
     * Direction normal.
     */
    struct funkout_Vector normal;

    /**
     * Speed.
     */
    float speed;

    /**
     * Radius.
     */
    float radius;

    /**
     * Timestep.
     */
    float timestep;

    /**
     * Time to live.
     */
    float ttl;

    /**
     * Length for beams.
     */
    float length;

    /**
     * Possibly from this enemy or NULL.
     **/
    const struct funkout_Enemy * enemy;

    /**
     * Flagged for removal.
     */
    bool remove_me;
};

/**
 *
 */
enum funkout_EnemyAppearance {

    /**
     * Will draw a red square.
     */
    FUNKOUT_ENEMY_APPEARANCE_NONE,

    /**
     * Amoeba.
     */
    FUNKOUT_ENEMY_APPEARANCE_AMOEBA,

    /**
     * Virus.
     */
    FUNKOUT_ENEMY_APPEARANCE_VIRUS,

    /**
     * Satellite A.
     */
    FUNKOUT_ENEMY_APPEARANCE_SATA,

    /**
     * Satellite B.
     */
    FUNKOUT_ENEMY_APPEARANCE_SATB,

    /**
     * Satellite C.
     */
    FUNKOUT_ENEMY_APPEARANCE_SATC,

    /**
     * Satellite D.
     */
    FUNKOUT_ENEMY_APPEARANCE_SATD,

    /**
     * Plague.
     */
    FUNKOUT_ENEMY_APPEARANCE_PLAGUE,

    /**
     * Tank.
     */
    FUNKOUT_ENEMY_APPEARANCE_TANK,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_ENEMY_APPEARANCE,
};

/**
 * Enemy definition.
 */
struct funkout_EnemyDef {

    /**
     * Enemy name.
     */
    char name[32];

    /**
     * Appearance.
     */
    enum funkout_EnemyAppearance appearance;

    /**
     * The movement speed.
     */
    float speed;

    /**
     * Hitpoints, < 0 is indestructible.
     */
    int hitpoints;

    /**
     * The weapon definition.
     */
    struct funkout_WeaponDef weapon;

    /**
     * The collision radius.
     */
    float radius;

    /**
     * True if this enemy can move through blocks.
     */
    bool ghost_blocks;

    /**
     * 0 = never random, 1 = take every opportunity to be random.
     */
    float randomness;

    /**
     * True if weapons will be aimed, otherwise straight down.
     */
    bool aim;
};

/**
 * An enemy.
 */
struct funkout_Enemy {

    /**
     * The definition.
     */
    struct funkout_EnemyDef definition;

    /**
     * Position.
     */
    struct funkout_Vector pos;

    /**
     * Hitpoints.
     */
    int hitpoints;

    /**
     * Weapon cooldown.
     */
    float weapon_cooldown;

    /**
     * Cooldown while firing.
     */
    float firing_cooldown;

    /**
     * Cooldown after fired.
     */
    float fired_cooldown;

    /**
     * The direction in which the enemy is moving.
     */
    struct funkout_Vector direction;

    /**
     * The current target for shooting (only when aiming).
     */
    struct funkout_Vector target;

    /**
     * Timestep.
     */
    float timestep;

    /**
     * Stun cooldown.
     */
    float stun_cooldown;

    /**
     * The block from which this enemy spawned.
     **/
    int spawn_pos[2];
};

/**
 * A drop type.
 */
enum funkout_DropType {

    /**
     * No drop.
     */
    FUNKOUT_DROP_TYPE_NONE,

    /**
     * Double the number of balls.
     */
    FUNKOUT_DROP_TYPE_MULTIPLY,

    /**
     * Sticky (balls sticks to bat when they hit it).
     */
    FUNKOUT_DROP_TYPE_STICKY,

    /**
     * Extra life.
     */
    FUNKOUT_DROP_TYPE_LIFE,

    /**
     * Drops a deadly one.
     */
    FUNKOUT_DROP_TYPE_DEATH,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_DROP_TYPE
};

/**
 * A drop.
 */
struct funkout_Drop {

    /**
     * Drop type.
     */
    enum funkout_DropType type;

    /**
     * The position.
     */
    struct funkout_Vector pos;
};

/**
 * A block type.
 */
enum funkout_BlockType {

    /**
     * No block.
     */
    FUNKOUT_BLOCK_TYPE_NONE,

    /**
     * A normal block.
     */
    FUNKOUT_BLOCK_TYPE_NORMAL,

    /**
     * An enemy spawner.
     */
    FUNKOUT_BLOCK_TYPE_SPAWN,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_BLOCK_TYPE
};

/**
 * A block definition.
 */
struct funkout_BlockDef {

    /**
     * The block type.
     */
    enum funkout_BlockType type;

    /**
     * When destroyed, does it drop anything?
     */
    enum funkout_DropType drop_type;

    /**
     * Hitpoints, < 0 is indestructible.
     */
    int hitpoints;

    /**
     * The color of the block.
     */
    struct funkout_Color color;

    /**
     * Used by FUNKOUT_BLOCK_TYPE_SPAWN.
     */
    struct {

        /**
         * The maximum number of enemies at once.
         */
        int max_enemies;
        
        /**
         * Maximum number of enemies to spawn, -1 = no limit.
         */
        int max_count;

        /**
         * The enemy def to spawn.
         */
        struct funkout_EnemyDef enemy;

        /**
         * Spawn cooldown.
         */
        float cooldown;

        /**
         * Spawn cooldown start.
         */
        float cooldown_start;

    } spawn;
};

/**
 * A block.
 */
struct funkout_Block {

    /**
     * Block definition.
     */
    struct funkout_BlockDef definition;

    /**
     * Hitpoints, ignored if definition.hitpoints < 0.
     */
    int hitpoints;

    /**
     * Total number of spawned enemies.
     */
    int spawn_count;

    /**
     * Spawn cooldown.
     */
    float spawn_cooldown;

    /**
     * Current number of enemies.
     */
    int current_spawn;
};

/**
 * @cond
 */
struct funkout_Ball;
/**
 * @endcond
 */

/**
 * A player.
 */
struct funkout_Player {

    /**
     * The INDEX of the player.
     */
    int index;
    
    /**
     * The name of the player.
     */
    char name[128];

    /**
     * Position of the player.
     */
    struct funkout_Vector pos;

    /**
     * Radius of the player (number of blocks).
     */
    float radius;

    /**
     * Speed.
     */
    float speed;

    /**
     * Score.
     */
    unsigned int score;

    /**
     * Score for the current level.
     */
    unsigned int level_score;

    /**
     * Number of lives.
     */
    int lives;

    /**
     * The longest life.
     **/
    float longest_life;

    /**
     * Balls stick to the player.
     */
    bool sticky;

    /**
     * Number of balls the player owns.
     */
    int num_balls;

    /**
     * The balls.
     */
    struct funkout_Ball * balls;

    /**
     * Controls.
     */
    struct {

        /**
         * Horizontal control, < 0 is left, > 0 is right and 0 is still.
         */
        float horizontal;

        /**
         * Vertical control, < 0 is left, > 0 is right and 0 is still.
         */
        float vertical;

        /**
         * Shoot the first ball the player is holding.
         */
        bool shoot;

        /**
         * Slow down for more precision.
         */
        bool slow;

    } controls;

    /**
     * Respawn cooldown.
     */
    float respawn_cooldown;

    /**
     * Destroyed cooldown.
     */
    float destroyed_cooldown;

    /**
     * Immortal until ball is released.
     */
    bool immortal;
};

/**
 * Balls are fun.
 */
struct funkout_Ball {

    /**
     * The player that owns the ball.
     */
    struct funkout_Player * player;

    /**
     * Position of the player.
     */
    struct funkout_Vector pos;

    /**
     * Radius of the player.
     */
    float radius;

    /**
     * Speed.
     */
    float speed;

    /**
     * Normalized direction.
     */
    struct funkout_Vector direction;

    /**
     * Is it sticking to the player?
     */
    bool sticky;

    /**
     * Flag for removal.
     **/
    bool remove_me;

    /**
     * Score accumulator.
     */
    int score_accumulator;
};

/**
 * Game difficulty.
 */
enum funkout_GameDifficulty {

    /**
     * Easy is soooo easy!
     */
    FUNKOUT_GAME_DIFFICULTY_EASY,

    /**
     * Normal.
     */
    FUNKOUT_GAME_DIFFICULTY_NORMAL,

    /**
     * Getting harder. (that's what she said!)
     */
    FUNKOUT_GAME_DIFFICULTY_HARD,

    /**
     * AAARGH!
     */
    FUNKOUT_GAME_DIFFICULTY_INSANE,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_GAME_DIFFICULTY
};

/**
 * Game difficulty tweaks.
 */
struct funkout_GameDifficultyTweaks {

    /**
     * The speed of the ball.
     */
    float ball_speed;

    /**
     * Enemy speed.
     */
    float enemy_speed;

    /**
     * Tweapon weapon cooldown.
     */
    float weapon_cooldown;

    /**
     * Spawn cooldown.
     */
    float spawn_cooldown;

    /**
     * Spawn max enemies.
     */
    float spawn_max_enemies;

    /**
     * Extra life on score limits.
     */
    float extra_life_score;

    /**
     * Chance that a non-drop block drops death.
     * 0 never, 1 always.
     */
    float death_drop_chance;
};

/**
 * A level.
 */
struct funkout_Level {

    /**
     * The name of the level.
     */
    char name[128];

    /**
     * Background tile (0 disabled).
     */
    int background_tile;

    /**
     * Background color.
     */
    struct funkout_Color background_color;

    /**
     * The blocks.
     */
    struct funkout_BlockDef block_defs[FUNKOUT_GAME_HEIGHT][FUNKOUT_GAME_WIDTH];

};

/**
 * The state of the game.
 */
enum funkout_GameState {

    /**
     * The game is doing nothing.
     */
    FUNKOUT_GAME_STATE_NONE,

    /**
     * The game is starting.
     */
    FUNKOUT_GAME_STATE_START,

    /**
     * The game is being played.
     */
    FUNKOUT_GAME_STATE_PLAY,

    /**
     * The game has ended.
     */
    FUNKOUT_GAME_STATE_END,

    /**
     * Next level please.
     */
    FUNKOUT_GAME_STATE_NEXT,

    /**
     * Game over man, game over!
     */
    FUNKOUT_GAME_STATE_OVER,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_GAME_STATE
    
};

/**
 * Game event type.
 */
enum funkout_GameEventType {

    /**
     * No event!
     */
    FUNKOUT_GAME_EVENT_NONE,

    /**
     * State changed.
     */
    FUNKOUT_GAME_EVENT_STATE_CHANGED,

    /**
     * A player was added.
     */
    FUNKOUT_GAME_EVENT_PLAYER_ADDED,

    /**
     * A player was removed.
     */
    FUNKOUT_GAME_EVENT_PLAYER_REMOVED,

    /**
     * A player was spawned.
     */
    FUNKOUT_GAME_EVENT_PLAYER_SPAWNED,

    /**
     * A player was destroyed.
     */
    FUNKOUT_GAME_EVENT_PLAYER_DESTROYED,

    /**
     * A ball was spawned.
     */
    FUNKOUT_GAME_EVENT_BALL_SPAWNED,

    /**
     * A ball was destroyed.
     */
    FUNKOUT_GAME_EVENT_BALL_DESTROYED,

    /**
     * A ball bounced.
     */
    FUNKOUT_GAME_EVENT_BALL_BOUNCED,

    /**
     * One or more blocks were updated.
     */
    FUNKOUT_GAME_EVENT_BLOCKS_UPDATED,

    /**
     * A block was damaged.
     */
    FUNKOUT_GAME_EVENT_BLOCK_DAMAGED,

    /**
     * A block was destroyed.
     */
    FUNKOUT_GAME_EVENT_BLOCK_DESTROYED,

    /**
     * A drop was spawned.
     */
    FUNKOUT_GAME_EVENT_DROP_SPAWNED,

    /**
     * A drop was destroyed.
     */
    FUNKOUT_GAME_EVENT_DROP_DESTROYED,

    /**
     * A drop was picked up.
     */
    FUNKOUT_GAME_EVENT_DROP_PICKUP,

    /**
     * A player just got an extra life.
     */
    FUNKOUT_GAME_EVENT_EXTRA_LIFE,

    /**
     * A player just scored.
     */
    FUNKOUT_GAME_EVENT_SCORE,

    /**
     * Enemy spawned.
     */
    FUNKOUT_GAME_EVENT_ENEMY_SPAWNED,

    /**
     * Enemy destroyed.
     */
    FUNKOUT_GAME_EVENT_ENEMY_DESTROYED,

    /**
     * Enemy damaged.
     */
    FUNKOUT_GAME_EVENT_ENEMY_DAMAGED,

    /**
     * Projectile spawned.
     */
    FUNKOUT_GAME_EVENT_PROJECTILE_SPAWNED,

    /**
     * Projectile destroyed.
     */
    FUNKOUT_GAME_EVENT_PROJECTILE_DESTROYED,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_GAME_EVENT
};

/**
 * An event.
 */
struct funkout_GameEvent {

    /**
     * Type.
     */
    enum funkout_GameEventType type;

    /**
     * Event specific data.
     */
    union {

        /**
         * FUNKOUT_GAME_EVENT_STATE_CHANGED
         */
        struct {

            /**
             * The previous state.
             */
            enum funkout_GameState previous;

            /**
             * The new (current) state.
             */
            enum funkout_GameState state;

        } state_changed;

        /**
         * FUNKOUT_GAME_EVENT_PLAYER_ADDED
         */
        struct {

            /**
             * The player that was spawned.
             */
            struct funkout_Player * player;
            
        } player_added;

        /**
         * FUNKOUT_GAME_EVENT_PLAYER_REMOVED
         */
        struct {

            /**
             * The INDEX of the player that was removed.
             */
            int index;

            /**
             * The name of the player that was removed.
             */
            char name[128];

        } player_removed;

        /**
         * FUNKOUT_GAME_EVENT_PLAYER_SPAWNED
         */
        struct {

            /**
             * The player that was spawned.
             */
            struct funkout_Player * player;

        } player_spawned;

        /**
         * FUNKOUT_GAME_EVENT_PLAYER_DESTROYED
         */
        struct {

            /**
             * The player that was destroyed.
             */
            struct funkout_Player * player;

        } player_destroyed;

        /**
         * FUNKOUT_GAME_EVENT_BALL_SPAWNED
         */
        struct {

            /**
             * The ball that was spawned.
             */
            struct funkout_Ball * ball;

        } ball_spawned;

        /**
         * FUNKOUT_GAME_EVENT_BALL_DESTROYED
         */
        struct {

            /**
             * The destroyed ball (will be removed next update).
             */
            struct funkout_Ball ball;

        } ball_destroyed;

        /**
         * FUNKOUT_GAME_EVENT_BALL_BOUNCED
         */
        struct {

            /**
             * The ball in question.
             */
            struct funkout_Ball * ball;

            /**
             * The impact position.
             */
            struct funkout_Vector pos;

            /**
             * The normal.
             */
            struct funkout_Vector normal;

            /**
             * Will be non-NULL if the ball bounced on a player.
             */
            struct funkout_Player * player;

            /**
             * Will be non-NULL if the ball bounced on a player.
             */
            struct funkout_Enemy * enemy;

            /**
             * The X position of the collided block, will be -1 if no block.
             */
            int block_x;

            /**
             * The X position of the collided block, will be -1 if no block.
             */
            int block_y;

        } ball_bounced;

        /**
         * FUNKOUT_GAME_EVENT_BLOCKS_UPDATED
         */
        struct {

            /**
             * Dummy member.
             */
            int dummy;

        } blocks_updated;

        /**
         * FUNKOUT_GAME_EVENT_BLOCK_DAMAGED
         */
        struct {

            /**
             * The X position of the damaged block.
             */
            int block_x;

            /**
             * The Y position of the damaged block.
             */
            int block_y;

            /**
             * The ball that damaged the block.
             */
            struct funkout_Ball * ball;

            /**
             * Where the ball impacted.
             */
            struct funkout_Vector impact;

            /**
             * The normal of the impact.
             */
            struct funkout_Vector normal;

        } block_damaged;

        /**
         * FUNKOUT_GAME_EVENT_BLOCK_DESTROYED
         */
        struct {

            /**
             * A copy of the destroyed block.
             */
            struct funkout_Block block;

            /**
             * The position of the destroyed block.
             */
            int block_x;

            /**
             * The position of the destroyed block.
             */
            int block_y;

            /**
             * The ball that damaged the block.
             */
            struct funkout_Ball * ball;

            /**
             * Where the ball impacted.
             */
            struct funkout_Vector impact;

            /**
             * The normal.
             */
            struct funkout_Vector normal;

        } block_destroyed;

        /**
         * FUNKOUT_GAME_EVENT_DROP_SPAWNED
         */
        struct {

            /**
             * The drop in question.
             */
            struct funkout_Drop * drop;

            /**
             * The ball that destroyed the block to drop.
             */
            struct funkout_Ball * ball;

        } drop_spawned;

        /**
         * FUNKOUT_GAME_EVENT_DROP_DESTROYED
         */
        struct {

            /**
             * The type of drop that was destroyed.
             */
            struct funkout_Drop drop;
            
        } drop_destroyed;

        /**
         * FUNKOUT_GAME_EVENT_DROP_PICKUP
         */
        struct {

            /**
             * The drop that was picked up.
             */
            struct funkout_Drop drop;

            /**
             * The player that picked it up.
             */
            int player_index;

        } drop_pickup;

        /**
         * FUNKOUT_GAME_EVENT_EXTRA_LIFE
         */
        struct {

            /**
             * The awarded player.
             */
            int player_index;
            
        } extra_life;

        /**
         * FUNKOUT_GAME_EVENT_SCORE
         */
        struct {

            /**
             * The awarded player.
             */
            struct funkout_Player * player;

            /**
             * The score.
             */
            int score;

            /**
             * Where the score came from.
             */
            struct funkout_Vector pos;

        } score;

        /**
         * FUNKOUT_GAME_EVENT_ENEMY_SPAWNED
         */
        struct {

            /**
             * The spawned enemy.
             */
            struct funkout_Enemy * enemy;

        } enemy_spawned;

        /**
         * FUNKOUT_GAME_EVENT_ENEMY_DESTROYED
         */
        struct {

            /**
             * The def of the destroyed enemy.
             */
            struct funkout_Enemy enemy;

            /**
             * The ball that destroyed the enemy.
             */
            struct funkout_Ball * ball;

        } enemy_destroyed;

        /**
         * FUNKOUT_GAME_EVENT_ENEMY_DAMAGED
         */
        struct {

            /**
             * The damaged enemy.
             */
            struct funkout_Enemy * enemy;

            /**
             * The ball that damaged the enemy.
             */
            struct funkout_Ball * ball;

            /**
             * Point of impact.
             */
            struct funkout_Vector impact;

            /**
             * Impact normal.
             */
            struct funkout_Vector normal;

        } enemy_damaged;

        /**
         * FUNKOUT_GAME_EVENT_PROJECTILE_SPAWNED
         */
        struct {

            /**
             * The spawned projectile.
             */
            struct funkout_Projectile * projectile;

        } projectile_spawned;

        /**
         * FUNKOUT_GAME_EVENT_PROJECTILE_DESTROYED
         */
        struct {

            /**
             * The def of the destroyed projectile.
             */
            struct funkout_Projectile projectile;

            /**
             * The ball that destroyed the projectile.
             */
            struct funkout_Ball * ball;

        } projectile_destroyed;

    } e;
};

/**
 * A game.
 */
struct funkout_Game {

    /**
     * The name of the game.
     */
    char name[128];

    /**
     * The game difficulty.
     */
    enum funkout_GameDifficulty difficulty;

    /**
     * The players.
     */
    struct funkout_Player * players[FUNKOUT_MAX_PLAYERS];

    /**
     * The number of drops.
     */
    int num_drops;

    /**
     * The drops.
     */
    struct funkout_Drop * drops;

    /**
     * The number of enemies.
     */
    int num_enemies;

    /**
     * The enemies.
     */
    struct funkout_Enemy * enemies;

    /**
     * The number of projectiles.
     */
    int num_projectiles;

    /**
     * The projectiles.
     */
    struct funkout_Projectile * projectiles;

    /**
     * The currently played level.
     */
    struct funkout_Level level;

    /**
     * Maximum score for the level.
     */
    unsigned int max_level_score;

    /**
     * The blocks.
     */
    struct funkout_Block blocks[FUNKOUT_GAME_HEIGHT][FUNKOUT_GAME_WIDTH];

    /**
     * Game state.
     */
    enum funkout_GameState state;

    /**
     * The number of pending events.
     */
    int num_events;

    /**
     * An index to the oldest pending event.
     */
    int event_index;

    /**
     * Events.
     */
    struct funkout_GameEvent * event_queue;

    /**
     * After changing to start we get a cooldown.
     */
    float start_cooldown;

    /**
     * After changing to end we get a cooldown.
     */
    float end_cooldown;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Block type strings.
 */
extern const char *FUNKOUT_BLOCK_TYPE_STRINGS[MAX_FUNKOUT_BLOCK_TYPE];

/**
 * Drop type strings.
 */
extern const char *FUNKOUT_DROP_TYPE_STRINGS[MAX_FUNKOUT_DROP_TYPE];

/**
 * Enemy appearance strings.
 */
extern const char *FUNKOUT_ENEMY_APPEARANCE_STRINGS[MAX_FUNKOUT_ENEMY_APPEARANCE];

/**
 * Weapon type strings.
 */
extern const char *FUNKOUT_WEAPON_TYPE_STRINGS[MAX_FUNKOUT_WEAPON_TYPE];

/**
 * Initialize a game.
 * @param game The game struct to init.
 * @param name The name of the game.
 * @param difficulty The difficulty.
 * @param level The level to start with.
 */
void funkout_game_init(struct funkout_Game * game,
                       const char * name,
                       enum funkout_GameDifficulty difficulty,
                       const struct funkout_Level * level);

/**
 * Deinitialize a game.
 * @param game The game to deinit.
 */
void funkout_game_deinit(struct funkout_Game * game);

/**
 * Get the current game state.
 * @param game The game to get the state from.
 * @returns The current game state.
 */
enum funkout_GameState funkout_game_get_state(
    const struct funkout_Game * game);

/**
 * Set the current level.
 * @param game The game to set the level for.
 * @param level The level.
 * @note This will reset the game to state 
 *  FUNKOUT_GAME_STATE_NONE.
 */
void funkout_game_set_level(struct funkout_Game * game,
                            const struct funkout_Level * level);

/**
 * Restart the current level.
 * @param game The game to restart the level for.
 * @note This will reset the game to state
 *  FUNKOUT_GAME_STATE_NONE.
 */
void funkout_game_reset_level(struct funkout_Game * game);

/**
 * Add a player.
 * @param game The game to add a player for.
 * @param name The name of the player.
 * @returns The newly added player.
 */
struct funkout_Player * funkout_game_add_player(
    struct funkout_Game * game,
    const char * name);

/**
 * Remove a player.
 * @param game The game to remove a player from.
 * @param player The player to remove.
 * @returns True if the player was removed.
 * @note If the last player is removed the state is changed to
 *  FUNKOUT_GAME_STATE_OVER.
 */
bool funkout_game_remove_player(
    struct funkout_Game * game,
    const struct funkout_Player * player);

/**
 * Find a player.
 * @param game The game to find a player for.
 * @param player the player to find.
 * @returns The player index or -1 if the player isn't in the game.
 */
int funkout_game_find_player(
    struct funkout_Game * game,
    const struct funkout_Player * player);

/**
 * Remove all players.
 * @param game The game to remove all players from.
 * @note The state is changed to FUNKOUT_GAME_STATE_OVER.
 */
void funkout_game_remove_all_players(struct funkout_Game * game);

/**
 * Update the game.
 * @param game The game to update.
 * @param timestep The timestep.
 */
void funkout_game_update(struct funkout_Game * game,
                         float timestep);

/**
 * Poll events.
 * @param game The game to poll.
 * @param max_events The maximum number of events to return.
 * @param ret_events Return events here.
 * @returns The number of returned events.
 */
int funkout_game_poll(struct funkout_Game * game,
                      int max_events,
                      struct funkout_GameEvent * ret_events);

/**
 * Control player.
 * @param game The game that has the player to control.
 * @param pindex The player index or -1 for all players.
 * @param horizontal The horizontal control, < 0 is left > 0 is right and 0 is sit still.
 * @param vertical The vertical control, < 0 is left > 0 is right and 0 is sit still.
 * @param shoot True if the player should shoot the first ball it holds.
 * @returns True if the controls were applied, false if @p pindex is an invalid player.
 */
bool funkout_game_control_player(const struct funkout_Game * game,
                                 int pindex,
                                 float horizontal,
                                 float vertical,
                                 bool shoot,
                                 bool slow);

/**
 * Add a ball for a player.
 * @param game The game.
 * @param pindex The player index.
 * @param sticky True if the ball is sticking to the player.
 * @returns The new ball or NULL if @p pindex is an invalid/non-extistant player.
 */
struct funkout_Ball * funkout_game_add_ball(struct funkout_Game * game,
                                            int pindex,
                                            bool sticky);

/**
 * Remove a ball for a player.
 * @param game The game.
 * @param pindex The player index.
 * @param bindex The ball index.
 * @returns True if a ball was removed, false if not.
 */
bool funkout_game_remove_ball(struct funkout_Game * game, int pindex, int bindex);

/**
 * Add a drop.
 * @param game The game.
 * @param pos_x Horizontal position.
 * @param pos_y Vertical position.
 * @param type Drop type.
 * @param dropper The ball that made the drop appear.
 * @returns The new drop.
 */
struct funkout_Drop * funkout_game_add_drop(struct funkout_Game * game, float pos_x, float pos_y, enum funkout_DropType type, struct funkout_Ball * dropper);

/**
 * Add an enemy.
 * @param game The game.
 * @param pos_x Horizontal position.
 * @param pos_y Vertical position.
 * @param enemy_def The enemy definition.
 * @returns The new enemy
 */
struct funkout_Enemy * funkout_game_add_enemy(struct funkout_Game * game, float pos_x, float pos_y, const struct funkout_EnemyDef * enemy_def);

/**
 * Destroy an enemy.
 * @param game The game.
 * @param eindex The enemy index.
 * @returns True if the enemy was destroyed or false if @p eindex is invalid/non-existant enemy.
 */
bool funkout_game_destroy_enemy(struct funkout_Game * game, int eindex);

/**
 * Destroy a player.
 * @param game The game.
 * @param pindex The player index.
 * @returns True if the player was destroyed or false if @p pindex is invalid/non-existant player.
 */
bool funkout_game_destroy_player(struct funkout_Game * game, int pindex);

/**
 * Destroy a block.
 * @param game The game.
 * @param x The horizontal position.
 * @param y The vertical position.
 * @param ball The ball that destroyed the block (may be NULL).
 * @param impact The point of impact.
 * @param normal The normal of the impact.
 * @returns True if the block was destroyed or false if @p x or @p y is out of range or no block there.
 */
bool funkout_game_destroy_block(struct funkout_Game * game,
                                int x, int y,
                                struct funkout_Ball * ball,
                                const struct funkout_Vector * impact,
                                const struct funkout_Vector * normal);

/**
 * Add a projectile.
 * @param game The game.
 * @param pos_x Horizontal position.
 * @param pos_y Vertical position.
 * @param normal_x The X direction normal.
 * @param normal_y The Y direction normal.
 * @param projectile_def The projectile definition.
 * @param enemy Possibly from this enimy or NULL.
 * @returns The new projectile or NULL if invalid weapon.
 */
struct funkout_Projectile * funkout_game_add_projectile(struct funkout_Game * game,
                                                        float pos_x, float pos_y,
                                                        float normal_x, float normal_y,
                                                        const struct funkout_WeaponDef * weapon_def,
                                                        const struct funkout_Enemy * enemy);

/**
 * Destroy a projectile.
 * @param game The game.
 * @param index The projectile index.
 * @returns True if the projectile was destroyed or false if @p index is invalid/non-existant projectile.
 */
bool funkout_game_destroy_projectile(struct funkout_Game * game, int index);

/**
 * Get the maximum possible score for a level.
 * @param level The level.
 * @returns The maximum score that can be achieved on this level.
 */
unsigned int funkout_level_get_max_score(const struct funkout_Level * level);

/**
 * Serialize level.
 * @param level The level.
 * @param json The JSON data.
 * @returns The JSON data or NULL on error.
 */
char* funkout_level_serialize(const struct funkout_Level * level);

/**
 * Deserialize level.
 * @param level The level.
 * @param json The JSON data.
 * @returns True on success false on error.
 */
bool funkout_level_deserialize(struct funkout_Level * level, const char *json);

/**
 * Save level to file.
 * @param level The level.
 * @param filename Save the level to this file.
 * @returns The number of written bytes or zero on error.
 */
size_t funkout_level_save(const struct funkout_Level * level, const char *filename);

/**
 * Load level from file.
 * @param level The level.
 * @param filename Load the level from this file.
 * @returns The number of written bytes or zero on error.
 */
size_t funkout_level_load(struct funkout_Level * level, const char *filename);

#ifdef __cplusplus
}
#endif
    
#endif

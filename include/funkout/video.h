/**
 * @file video.h
 * @brief Handle video.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_VIDEO_H
#define FUNKOUT_VIDEO_H

#include <stdlib.h>
#include <stdbool.h>
#include "funkout/geometry.h"
#include "funkout/color.h"
#include "funkout/animation.h"

/**
 * Horizontal size of the game area (for aspect purposes only).
 */
#define FUNKOUT_HORIZONTAL_SIZE 8.0

/**
 * Used as an aspect ratio when rendering in horizontal mode.
 */
#define FUNKOUT_VERTICAL_SIZE 9.0

/**
 * The grid size for font images, currently 8x8.
 */
#define FUNKOUT_FONT_GRID_SIZE 8

#ifdef __cplusplus
extern "C" {
#endif

/**
 * What's the window mode?
 */
enum funkout_WindowMode {

    /**
     * Normal.
     */
    FUNKOUT_WINDOW_MODE_NORMAL,

    /**
     * Maximized.
     */
    FUNKOUT_WINDOW_MODE_MAXIMIZED,

    /**
     * Minimized.
     */
    FUNKOUT_WINDOW_MODE_MINIMIZED,

    /**
     * Fullscreen.
     */
    FUNKOUT_WINDOW_MODE_FULLSCREEN,

    /**
     * Max enum.
     */
    MAX_FUNKOUT_WINDOW_MODE
};

/**
 * An image.
 */
struct funkout_Image;


/**
 * Initialize a window and renderer.
 * @param width The width of the window.
 * @param height The height of the window.
 * @param mode The window mode.
 * @param vsync True to enable vsync.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_init_video(int width, int height,
                        enum funkout_WindowMode mode,
                        bool vsync);

/**
 * Destroy the window and renderer.
 */
void funkout_deinit_video(void);

/**
 * Get the size of the window (in pixels).
 * @param[out] ret_w Return width, NULL to omit.
 * @param[out] ret_h Return width, NULL to omit.
 */
void funkout_get_window_size(int * ret_w, int * ret_h);

/**
 * Set the size of the window (in pixels).
 * @param w The width.
 * @param h The height.
 */
void funkout_set_window_size(int w, int h);

/**
 * Get the window mode.
 * @returns The window mode, will be FUNKOUT_WINDOW_MODE_NORMAL if no 
 *  window is available.
 */
enum funkout_WindowMode funkout_get_window_mode(void);

/**
 * Set the window mode.
 * @param mode The window mode.
 * @returns True if successful or false if the mode is unsupported.
 * @note Will set error on failure.
 */
bool funkout_set_window_mode(enum funkout_WindowMode mode);

/**
 * Clear the video.
 * @param r Red.
 * @param g Green.
 * @param b Blue.
 * @param a Alpha.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_render_clear(float r, float g, float b, float a);

/**
 * Present the video.
 */
void funkout_render_present(void);

/**
 * Draw a rectangle.
 * @param x X.
 * @param y Y.
 * @param w Width.
 * @param h Height.
 * @param r Red.
 * @param g Green.
 * @param b Blue.
 * @param a Alpha.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_render_draw_rect(int x, int y, int w, int h,
                              float r, float g, float b, float a);

/**
 * Fill a rectangle.
 * @param x X.
 * @param y Y.
 * @param w Width.
 * @param h Height.
 * @param r Red.
 * @param g Green.
 * @param b Blue.
 * @param a Alpha.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_render_fill_rect(int x, int y, int w, int h,
                              float r, float g, float b, float a);

/**
 * Draw a line.
 * @param x1 X1.
 * @param y1 Y1.
 * @param x2 X2.
 * @param y2 Y2.
 * @param r Red.
 * @param g Green.
 * @param b Blue.
 * @param a Alpha.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_render_draw_line(int x1, int y1, int x2, int y2,
                              float r, float g, float b, float a);

/**
 * Draw a circle.
 * @param x X.
 * @param y Y.
 * @param radius The radius.
 * @param r Red.
 * @param g Green.
 * @param b Blue.
 * @param a Alpha.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_render_draw_circle(int x, int y, int radius,
                                float r, float g, float b, float a);

/**
 * Draw a triangle.
 * @param x1 X point 1.
 * @param y1 Y point 1.
 * @param x2 X point 2.
 * @param y2 Y point 2.
 * @param x3 X point 3.
 * @param y3 Y point 3.
 * @param r Red.
 * @param g Green.
 * @param b Blue.
 * @param a Alpha.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_render_draw_triangle(int x1, int y1,
                                  int x2, int y2,
                                  int x3, int y3,
                                  float r, float g, float b, float a);

/**
 * Draw a polygon.
 * @param x X.
 * @param y Y.
 * @param num_points The number of points to draw.
 * @param points The points, must be >= 3.
 * @param r Red.
 * @param g Green.
 * @param b Blue.
 * @param a Alpha.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 * @note The polygon will be closed automatically.
 */
bool funkout_render_draw_polygon(int x, int y,
                                 int num_points,
                                 const struct funkout_Vector * points,
                                 float r, float g, float b, float a);

/**
 * Draw a shape.
 * @param shape The shape to draw.
 * @param r Red.
 * @param g Green.
 * @param b Blue.
 * @param a Alpha.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_render_draw_shape(const struct funkout_Shape * shape,
                               float r, float g, float b, float a);

/**
 * Create a new image from data.
 * @param data The data.
 * @param size The size of the data.
 * @returns A new image on success and NULL on failure.
 * @note Will set error on failure.
 */
struct funkout_Image * funkout_image_new(const void * data, size_t size);

/**
 * Free an image.
 * @param image The image to free.
 */
void funkout_image_free(struct funkout_Image * image);

/**
 * Get the image resolution in pixels.
 * @param image The image to get the resolution for.
 * @param[out] ret_w The width in pixels.
 * @param[out] ret_h The height in pixels.
 */
void funkout_image_resolution(const struct funkout_Image * image, int * ret_w, int * ret_h);

/**
 * Draw the image.
 * @param image The image to draw.
 * @param x X.
 * @param y Y.
 * @param w Width.
 * @param h Height.
 * @param src_x Source X.
 * @param src_y Source Y.
 * @param src_w Source Width, <= 0 will use entire image width.
 * @param src_h Source Height, <= 0 will use the entire image height.
 * @param alpha The alpha intensity.
 * @param angle Apply rotation (degrees).
 * @param align_x Align X, 0 means center.
 * @param align_y Align Y, 0 means center.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_image_draw(const struct funkout_Image * image,
                        int x, int y, int w, int h,
                        int src_x, int src_y, int src_w, int src_h,
                        float alpha,
                        float angle,
                        float align_x,
                        float align_y);

/**
 * Draw a font image as character.
 * @param image The font image to draw a character from, pass NULL to use
 *  the default font image.
 * @param c The character to draw.
 * @param x X.
 * @param y Y.
 * @param w Width.
 * @param h Height.
 * @param alpha The alpha intensity.
 * @param angle Apply rotation (degrees).
 * @param align_x Align X, 0 means center.
 * @param align_y Align Y, 0 means center.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_image_draw_char(const struct funkout_Image * image,
                             char c,
                             int x, int y, int w, int h,
                             float alpha,
                             float angle,
                             float align_x,
                             float align_y);

/**
 * Draw a font image as text.
 * @param image The font image to draw characters from, pass NULL to use 
 *  the default font image.
 * @param text The character to draw.
 * @param x X.
 * @param y Y.
 * @param size The size of each character.
 * @param alpha The alpha intensity.
 * @param angle Apply rotation (degrees).
 * @param align_x Align X, 0 means center.
 * @param align_y Align Y, 0 means center.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_image_draw_text(const struct funkout_Image * image,
                             const char * text,
                             int x, int y,
                             int size,
                             float alpha,
                             float angle,
                             float align_x,
                             float align_y);

/**
 * Draw an animation.
 * @param animation The animation.
 * @param image The image.
 * @param x X.
 * @param y Y.
 * @param w Width.
 * @param h Height.
 * @param alpha The alpha intensity.
 * @param angle Apply rotation (degrees).
 * @param align_x Align X, 0 means center.
 * @param align_y Align Y, 0 means center.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_animation_draw(const struct funkout_Animation * animation,
                            const struct funkout_Image * image,
                            int x, int y, int w, int h,
                            float alpha,
                            float angle,
                            float align_x,
                            float align_y);

/**
 * Set render clip rect.
 * @param clip The clip rect or NULL for no clip.
 * @returns True on success and false on failure.
 * @note Will set error on failure.
 */
bool funkout_render_set_clip(const struct funkout_IRect * clip);

#ifdef __cplusplus
}
#endif
    
#endif

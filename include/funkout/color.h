/**
 * @file color.h
 * @brief Handle color.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_COLOR_H
#define FUNKOUT_COLOR_H


#ifdef __cplusplus
extern "C" {
#endif

/**
 * A funkout color.
 */
struct funkout_Color {

    /**
     * Red.
     */
    float r;

    /**
     * Green.
     */
    float g;

    /**
     * Blue.
     */
    float b;

    /**
     * Alpha.
     */
    float a;
};

/**
 * Cap the color.
 * @param color The color to cap.
 * @param[out] ret_color Return the capped color.
 */
void funkout_color_cap(const struct funkout_Color * color,
                       struct funkout_Color * ret_color);

/**
 * Blend two colors.
 * @param color The color to blend
 * @param blend The color to blend with
 * @param factor Blend factor where 0 is all the values of @p color and 1.0
 *  is all the values from @p blend.
 * @param[out] ret_color Return the blended color.
 */
void funkout_color_blend(const struct funkout_Color * color,
                         const struct funkout_Color * blend,
                         float factor,
                         struct funkout_Color * ret_color);

/**
 * Multiplication for color.
 * @param color The color to multiply with.
 * @param multiplier The multiplier.
 * @param[out] ret_color Return the blended color.
 */
void funkout_color_mul(const struct funkout_Color * color,
                       float multiplier,
                       struct funkout_Color * ret_color);

/**
 * Add/sub for color.
 * @param color The color to multiply with.
 * @param addition The addition, can be negative.
 * @param[out] ret_color Return the blended color.
 */
void funkout_color_add(const struct funkout_Color * color,
                       float addition,
                       struct funkout_Color * ret_color);

#ifdef __cplusplus
}
#endif
    
#endif

/**
 * @file animation.h
 * @brief Handle animation.
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#ifndef FUNKOUT_ANIMATION_H
#define FUNKOUT_ANIMATION_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * An animation.
 */
struct funkout_Animation {

    /**
     * The width and height (in pixels) of each frame in the image.
     */
    int size;

    /**
     * The duration for the animation.
     */
    float duration;

    /**
     * The number of loops, negative is forever.
     */
    int loops;

    /**
     * The current timestep.
     */
    float timestep;
};

/**
 * Get the progress of the step.
 * @param animation The animation to get the progress for.
 * @returns The progress from 0 to 1.
 * @note Animations with infinite looping will return the
 *  progress of each loop and never quite reach 1.
 * @note If @p animation has no frames this function returns 1.
 */
float funkout_animation_progress(
    const struct funkout_Animation * animation);


/**
 * Update the animation.
 * @param animation The animation to update.
 * @param timestep The timestep.
 */
void funkout_animation_update(
    struct funkout_Animation * animation,
    float timestep);

#ifdef __cplusplus
}
#endif
    
#endif

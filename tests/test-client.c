/**
 * @file test-client.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include <SDL2/SDL.h>

#include "funkout/client.h"
#include "funkout/error.h"
#include "funkout/defs.h"
#include "funkout/audio.h"


int main(int argc, char * argv[])
{
    if (SDL_Init(0) < 0) {
        fprintf(stderr, "ERROR: %s\n", SDL_GetError());
        return 1;
    }

    if (!funkout_init_video(1024, 1024, FUNKOUT_WINDOW_MODE_NORMAL, true)) {
        fprintf(stderr, "ERROR: %s\n", funkout_get_error()->msg);
        SDL_Quit();
        return 1;
    }

    if (!funkout_init_audio(22050)) {
        fprintf(stderr, "ERROR: %s\n", funkout_get_error()->msg);
        funkout_deinit_video();
        SDL_Quit();
        return 1;
    }
    
    struct funkout_Client client;
    if (!funkout_client_init(&client, NULL)) {
        fprintf(stderr, "ERROR: %s\n", funkout_get_error()->msg);
        return 2;
    }

    const struct funkout_Level levels[4] = {
        FUNKOUT_LEVEL_TEST1,
        FUNKOUT_LEVEL_TEST2,
        FUNKOUT_LEVEL_TEST3,
        FUNKOUT_LEVEL_TEST4
    };
    funkout_client_set_levels(&client, 4, levels);

    while (funkout_client_update(&client)) {
        int windoww, windowh;
        funkout_get_window_size(&windoww, &windowh);
        funkout_client_draw(&client, 0, 0, windoww, windowh);
    }

    funkout_client_deinit(&client);
    funkout_deinit_audio();
    funkout_deinit_video();
    SDL_Quit();
    return 0;
}

/**
 * @file test-client.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include <SDL2/SDL.h>

#include "funkout/client.h"
#include "funkout/error.h"
#include "funkout/defs.h"


int main(int argc, char * argv[])
{
    if (SDL_Init(0) < 0) {
        fprintf(stderr, "ERROR: %s\n", SDL_GetError());
        return 1;
    }

    if (!funkout_init_video(1024, 1024, FUNKOUT_WINDOW_MODE_NORMAL, true)) {
        fprintf(stderr, "ERROR: %s\n", funkout_get_error()->msg);
        return 1;
    }
    
    struct funkout_Client client;
    if (!funkout_client_init(&client, NULL)) {
        fprintf(stderr, "ERROR: %s\n", funkout_get_error()->msg);
        return 2;
    }

    funkout_game_set_level(&client.game, &FUNKOUT_LEVEL_TEST4);
    // for (int by = 0; by < FUNKOUT_GAME_HEIGHT; by++)
    //     for (int bx = 0; bx < FUNKOUT_GAME_WIDTH; bx++)
    //         if (FUNKOUT_DROP_TYPE_MULTIPLY + (bx % (MAX_FUNKOUT_DROP_TYPE - 1)) != FUNKOUT_DROP_TYPE_DEATH)
    //             client.game.blocks[by][bx].definition.drop_type = FUNKOUT_DROP_TYPE_MULTIPLY + bx % (MAX_FUNKOUT_DROP_TYPE - 1);
    struct funkout_Player * player = funkout_game_add_player(&client.game, "Player 1");

    while (funkout_client_update(&client)) {
        int windoww, windowh;
        funkout_get_window_size(&windoww, &windowh);
        funkout_client_draw(&client, 0, 0, windoww, windowh);
    }
    
    funkout_client_deinit(&client);
    funkout_deinit_video();
    SDL_Quit();
    return 0;
}

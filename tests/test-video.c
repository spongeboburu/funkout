/**
 * @file test-video.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include <SDL2/SDL.h>

#include "funkout/video.h"
#include "funkout/error.h"



int main(int argc, char * argv[])
{
    if (SDL_Init(0) < 0) {
        fprintf(stderr, "ERROR: %s\n", SDL_GetError());
        return 1;
    }
    if (!funkout_init_video(1024, 1024, FUNKOUT_WINDOW_MODE_NORMAL, true)) {
        fprintf(stderr, "ERROR: %s\n", funkout_get_error()->msg);
        return 1;
    }

    struct funkout_AABB aabb = {{0, 0}, {10, 20}};
    struct funkout_IRect irect = {0, 0, 10, 20};
    
    bool running = true;
    while (running) {
        SDL_Event event;
        while (running && SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                running = false;
                break;
            }
        }
        if (running) {
            funkout_render_clear(0, 0, 0.25, 1.0);
            int windoww, windowh;
            funkout_get_window_size(&windoww, &windowh);
            struct funkout_AABB bounding_aabb = {windoww / 2, windowh / 2, windoww / 2, windowh / 2};
            struct funkout_IRect bounding_irect = {0, 0, windoww, windowh};
            struct funkout_AABB fitted_aabb;
            funkout_aabb_fit(aabb.pos.x, aabb.pos.y, aabb.extents.x, aabb.extents.y,
                             bounding_aabb.pos.x, bounding_aabb.pos.y, bounding_aabb.extents.x, bounding_aabb.extents.y,
                             &fitted_aabb);
            funkout_render_fill_rect(fitted_aabb.pos.x - fitted_aabb.extents.x, fitted_aabb.pos.y - fitted_aabb.extents.y,
                                     fitted_aabb.extents.x * 2, fitted_aabb.extents.y * 2,
                                     1, 0, 0, 0.25);
            struct funkout_IRect fitted_irect;
            funkout_irect_fit(irect.x, irect.y, irect.w, irect.h,
                              bounding_irect.x, bounding_irect.y,
                              bounding_irect.w, bounding_irect.h,
                              &fitted_irect);
            funkout_render_draw_rect(fitted_irect.x, fitted_irect.y,
                                     fitted_irect.w, fitted_irect.h,
                                     1, 1, 0, 1);
            if (!funkout_image_draw_text(NULL, "the score!", 0, 0, 32, 1.0, 0.0, 0.0, 0.0))
                fprintf(stderr, "ERROR: %s\n", funkout_get_error()->msg);
            funkout_render_present();
        }
    }
    
    funkout_deinit_video();
    SDL_Quit();
    return 0;
}

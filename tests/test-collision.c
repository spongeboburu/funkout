/**
 * @file test-collision.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include <SDL2/SDL.h>

#include "funkout/error.h"
#include "funkout/video.h"
#include "funkout/collision.h"


int main(int argc, char * argv[])
{
    if (SDL_Init(0) < 0) {
        fprintf(stderr, "ERROR: %s\n", SDL_GetError());
        return 1;
    }
    if (!funkout_init_video(1024, 1024, FUNKOUT_WINDOW_MODE_NORMAL, true)) {
        fprintf(stderr, "ERROR: %s\n", funkout_get_error()->msg);
        return 1;
    }

    struct funkout_Shape shape = {FUNKOUT_SHAPE_AABB, {.aabb={{350, 350}, {150, 50}}}};
    struct funkout_Shape test_shape = {FUNKOUT_SHAPE_AABB, {.aabb={{512, 512}, {50, 25}}}};
    struct funkout_Vector test_delta = {0, 0};
    
    bool zero_delta = false;
    bool running = true;
    while (running) {
        SDL_Event event;
        while (running && SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                running = false;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                case SDLK_ESCAPE:
                    running = false;
                    break;
                case SDLK_w:
                    shape.s.aabb.pos.y -= 10;
                    break;
                case SDLK_a:
                    shape.s.aabb.pos.x -= 10;
                    break;
                case SDLK_s:
                    shape.s.aabb.pos.y += 10;
                    break;
                case SDLK_d:
                    shape.s.aabb.pos.x += 10;
                    break;
                case SDLK_SPACE:
                    zero_delta = !zero_delta;
                    break;
                case SDLK_c:
                    if (shape.type == FUNKOUT_SHAPE_AABB) {
                        struct funkout_Circle c = {shape.s.aabb.pos, MAX(shape.s.aabb.extents.x, shape.s.aabb.extents.y)};
                        shape.type = FUNKOUT_SHAPE_CIRCLE;
                        shape.s.circle = c;
                    } else if (shape.type == FUNKOUT_SHAPE_CIRCLE) {
                        struct funkout_AABB c = {shape.s.circle.pos, {shape.s.circle.r, shape.s.circle.r}};
                        shape.type = FUNKOUT_SHAPE_AABB;
                        shape.s.aabb = c;
                    }
                    break;
                case SDLK_v:
                    if (test_shape.type == FUNKOUT_SHAPE_AABB) {
                        struct funkout_Circle c = {test_shape.s.aabb.pos, MAX(test_shape.s.aabb.extents.x, test_shape.s.aabb.extents.y)};
                        test_shape.type = FUNKOUT_SHAPE_CIRCLE;
                        test_shape.s.circle = c;
                    } else if (test_shape.type == FUNKOUT_SHAPE_CIRCLE) {
                        struct funkout_AABB c = {test_shape.s.circle.pos, {test_shape.s.circle.r, test_shape.s.circle.r}};
                        test_shape.type = FUNKOUT_SHAPE_AABB;
                        test_shape.s.aabb = c;
                    }
                    break;
                case SDLK_UP:
                    test_delta.y -= 10;
                    break;
                case SDLK_LEFT:
                    test_delta.x -= 10;
                    break;
                case SDLK_DOWN:
                    test_delta.y += 10;
                    break;
                case SDLK_RIGHT:
                    test_delta.x += 10;
                    break;
                }
            }
        }
        if (running) {
            funkout_render_clear(0, 0, 0.25, 1);
            
            // Draw cursor
            funkout_render_draw_shape(&shape, 1,1,1,1);

            // Draw test
            funkout_render_draw_shape(&test_shape, 1,1,0,1);

            // Delta
            int mouse_x, mouse_y;
            SDL_GetMouseState(&mouse_x, &mouse_y);
            struct funkout_Vector delta = {mouse_x - shape.s.any.pos.x, mouse_y - shape.s.any.pos.y};
            if (zero_delta) {
                delta.x = 0;
                delta.y = 0;
            }
            struct funkout_Shape delta_shape = shape;
            delta_shape.s.any.pos.x += delta.x;
            delta_shape.s.any.pos.y += delta.y;
            funkout_render_draw_line(shape.s.any.pos.x, shape.s.any.pos.y, delta_shape.s.any.pos.x, delta_shape.s.any.pos.y, 0.6,0.6,0.6,1);
            funkout_render_draw_shape(&delta_shape, 0.6,0.6,0.6,1);

            delta_shape = test_shape;
            delta_shape.s.any.pos.x += test_delta.x;
            delta_shape.s.any.pos.y += test_delta.y;
            funkout_render_draw_line(test_shape.s.any.pos.x, test_shape.s.any.pos.y, delta_shape.s.any.pos.x, delta_shape.s.any.pos.y, 0.6,0.6,0.6,1);
            funkout_render_draw_shape(&delta_shape, 0.6,0.6,0.6,1);
            
            
            // Collision
            float tau;
            struct funkout_Vector mtv, normal;
            bool collision = funkout_shape_collision(&shape, delta.x, delta.y, &test_shape, test_delta.x, test_delta.y, &tau, &mtv, &normal);
            if (collision) {
                struct funkout_Shape collision_shape = shape;
                collision_shape.s.any.pos.x += mtv.x * 0.5 + delta.x * tau;
                collision_shape.s.any.pos.y += mtv.y * 0.5 + delta.y * tau;
                funkout_render_draw_shape(&collision_shape, 1,0,0,1);
                collision_shape = test_shape;
                collision_shape.s.any.pos.x += mtv.x * -0.5 + test_delta.x * tau;
                collision_shape.s.any.pos.y += mtv.y * -0.5 + test_delta.y * tau;
                funkout_render_draw_shape(&collision_shape, 1,0,0,1);
                funkout_render_draw_line(collision_shape.s.any.pos.x,
                                         collision_shape.s.any.pos.y,
                                         collision_shape.s.any.pos.x + normal.x * 50,
                                         collision_shape.s.any.pos.y + normal.y * 50,
                                         1, 0, 1, 1);
            }
            funkout_render_present();
        }
    }
    
    funkout_deinit_video();
    SDL_Quit();
    return 0;
}

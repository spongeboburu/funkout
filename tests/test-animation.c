/**
 * @file test-animation.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include <SDL2/SDL.h>

#include "funkout/video.h"
#include "funkout/error.h"
#include "1234png.c"


int main(int argc, char * argv[])
{
    if (SDL_Init(0) < 0) {
        fprintf(stderr, "ERROR: %s\n", SDL_GetError());
        return 1;
    }
    if (!funkout_init_video(1024, 1024, FUNKOUT_WINDOW_MODE_NORMAL, true)) {
        fprintf(stderr, "ERROR: %s\n", funkout_get_error()->msg);
        return 1;
    }

    struct funkout_Image * image = funkout_image_new(FUNKOUT_1234_PNG, sizeof(FUNKOUT_1234_PNG));
    struct funkout_Animation animation = {
        32,
        1.0,
        -1
    };
    

    bool running = true;
    while (running) {
        SDL_Event event;
        while (running && SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                running = false;
                break;
            }
        }
        if (running) {
            funkout_render_clear(0, 0, 0.25, 1.0);
            funkout_animation_draw(&animation, image, 256, 256, 512, 512, 1.0, 0, 0, 0);
            animation.timestep += 1/60.0;
            funkout_render_present();
        }
    }
    if (image != NULL)
        funkout_image_free(image);
    funkout_deinit_video();
    SDL_Quit();
    return 0;
}

/**
 * @file test-geometry.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include <SDL2/SDL.h>

#include "funkout/video.h"
#include "funkout/error.h"
#include "funkout/geometry.h"


void draw_dot(float x, float y)
{
    funkout_render_fill_rect(x - 2, y - 2, 5, 5, 0, 1, 1, 1);
}

int main(int argc, char * argv[])
{
    if (SDL_Init(0) < 0) {
        fprintf(stderr, "ERROR: %s\n", SDL_GetError());
        return 1;
    }
    if (!funkout_init_video(1024, 1024, FUNKOUT_WINDOW_MODE_NORMAL, true)) {
        fprintf(stderr, "ERROR: %s\n", funkout_get_error()->msg);
        return 1;
    }

   
    struct funkout_Line line = {{0, 50}, {100, 20}};
    struct funkout_AABB aabb = {{100, 100}, {50, 25}};
    struct funkout_Circle circle = {{-100, 100}, 50};
    // struct funkout_Polygon polygon = {{0, -200}, 4, {{0,0}, {50, 50}, {0, 100}, {-50, 50}}};
    struct funkout_Polygon polygon = {{0, -200}, 4, {{0,0}, {-50, 50}, {0, 100}, {50, 50}}};

    struct funkout_Line test_line = {{140, 500}, {250, 100}};
    struct funkout_AABB test_aabb = {{500, 300}, {150, 50}};
    struct funkout_Circle test_circle = {{500, 600}, 100};
    struct funkout_Polygon test_polygon = {{700, 400}, 4, {{0,-20}, {50, 80}, {0, 100}, {-50, 40}}};

    // Control test
    struct funkout_Vector retv[100];
    struct funkout_Vector cursor = {512.0, 512.0};
    unsigned int iflags = 0;
    bool running = true;
    while (running) {
        SDL_Event event;
        while (running && SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                running = false;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                case SDLK_i:
                    iflags = (iflags + 1) % 4;
                    break;
                case SDLK_w:
                    cursor.y -= 1.0;
                    break;
                case SDLK_s:
                    cursor.y += 1.0;
                    break;
                case SDLK_a:
                    cursor.x -= 1.0;
                    break;
                case SDLK_d:
                    cursor.x += 1.0;
                    break;
                }
                break;
            case SDL_MOUSEMOTION:
                cursor.x = event.motion.x;
                cursor.y = event.motion.y;
                break;
            }
        }
        if (running) {
            funkout_render_clear(0, 0, 0.25, 1.0);
            // Cursor
            funkout_render_draw_line(cursor.x + line.pos1.x, cursor.y + line.pos1.y, cursor.x + line.pos2.x, cursor.y + line.pos2.y, 1, 1, 1, 1);
            funkout_render_draw_rect(cursor.x + aabb.pos.x - aabb.extents.x, cursor.y + aabb.pos.y - aabb.extents.y,
                                     aabb.extents.x * 2, aabb.extents.y * 2, 1, 1, 1, 1);
            funkout_render_draw_circle(cursor.x + circle.pos.x, cursor.y + circle.pos.y,
                                       circle.r, 1, 1, 1, 1);
            funkout_render_draw_polygon(cursor.x + polygon.pos.x, cursor.y + polygon.pos.y,
                                        polygon.num, polygon.points, 1, 1, 1, 1);
            struct funkout_AABB poly_aabb;
            funkout_polygon_get_aabb(polygon.num, polygon.points, &poly_aabb);
            funkout_render_draw_rect(cursor.x + polygon.pos.x + poly_aabb.pos.x - poly_aabb.extents.x,
                                     cursor.y + polygon.pos.y + poly_aabb.pos.y - poly_aabb.extents.y,
                                     poly_aabb.extents.x * 2, poly_aabb.extents.y * 2, 0, 1, 0, 1);
            funkout_render_draw_line(0, 0, cursor.x, cursor.y, 0.5, 0.5, 0.5, 1);
            funkout_render_draw_line(0, 1024, cursor.x, cursor.y, 0.5, 0.5, 0.5, 1);

            // Collision shapes
            funkout_render_draw_line(test_line.pos1.x, test_line.pos1.y, test_line.pos2.x, test_line.pos2.y, 1, 1, 0, 1);
            funkout_render_draw_rect(test_aabb.pos.x - test_aabb.extents.x, test_aabb.pos.y - test_aabb.extents.y,
                                     test_aabb.extents.x * 2, test_aabb.extents.y * 2, 1, 1, 0, 1);
            funkout_render_draw_circle(test_circle.pos.x, test_circle.pos.y, test_circle.r, 1, 1, 0, 1);
            funkout_render_draw_polygon(test_polygon.pos.x, test_polygon.pos.y,
                                        test_polygon.num, test_polygon.points, 1, 1, 0, 1);
            funkout_polygon_get_aabb(test_polygon.num, test_polygon.points, &poly_aabb);
            funkout_render_draw_rect(test_polygon.pos.x + poly_aabb.pos.x - poly_aabb.extents.x,
                                     test_polygon.pos.y + poly_aabb.pos.y - poly_aabb.extents.y,
                                     poly_aabb.extents.x * 2, poly_aabb.extents.y * 2, 0, 1, 0, 1);
            
            // Closest point
            funkout_closest_point_on_line(cursor.x, cursor.y, test_line.pos1.x, test_line.pos1.y, test_line.pos2.x, test_line.pos2.y, iflags, retv);
            draw_dot(retv[0].x, retv[0].y);
            
            // Lines intersect
            if (funkout_line_intersect(
                    cursor.x + line.pos1.x, cursor.y + line.pos1.y, cursor.x + line.pos2.x, cursor.y + line.pos2.y, iflags,
                    test_line.pos1.x, test_line.pos1.y, test_line.pos2.x, test_line.pos2.y, iflags,
                    retv)
                )
                draw_dot(retv[0].x, retv[0].y);
            if (funkout_line_intersect(
                    0, 0, cursor.x, cursor.y, iflags,
                    test_line.pos1.x, test_line.pos1.y, test_line.pos2.x, test_line.pos2.y, iflags,
                    retv)
                )
                draw_dot(retv[0].x, retv[0].y);

            int num;
            // Line vs AABB 
            num = funkout_line_aabb_intersect(cursor.x + line.pos1.x, cursor.y + line.pos1.y,
                                              cursor.x + line.pos2.x, cursor.y + line.pos2.y,
                                              iflags,
                                              test_aabb.pos.x, test_aabb.pos.y, test_aabb.extents.x, test_aabb.extents.y,
                                              retv);
            for (int i = 0; i < num; i++)
                draw_dot(retv[i].x, retv[i].y);
            num = funkout_line_aabb_intersect(test_line.pos1.x, test_line.pos1.y, test_line.pos2.x, test_line.pos2.y,
                                              iflags,
                                              cursor.x + aabb.pos.x, cursor.y + aabb.pos.y, aabb.extents.x, aabb.extents.y,
                                              retv);
            for (int i = 0; i < num; i++)
                draw_dot(retv[i].x, retv[i].y);
            num = funkout_line_aabb_intersect(0, 0,
                                              cursor.x, cursor.y,
                                              iflags,
                                              test_aabb.pos.x, test_aabb.pos.y, test_aabb.extents.x, test_aabb.extents.y,
                                              retv);
            for (int i = 0; i < num; i++)
                draw_dot(retv[i].x, retv[i].y);
            num = funkout_line_aabb_intersect(0, 1024,
                                              cursor.x, cursor.y,
                                              iflags,
                                              test_aabb.pos.x, test_aabb.pos.y, test_aabb.extents.x, test_aabb.extents.y,
                                              retv);
            for (int i = 0; i < num; i++)
                draw_dot(retv[i].x, retv[i].y);
            
            // Line vs circle
            num = funkout_line_circle_intersect(test_line.pos1.x, test_line.pos1.y,
                                                test_line.pos2.x, test_line.pos2.y,
                                                iflags,
                                                cursor.x + circle.pos.x, cursor.y + circle.pos.y, circle.r,
                                                retv);
            for (int i = 0; i < num; i++)
                draw_dot(retv[i].x, retv[i].y);
            num = funkout_line_circle_intersect(cursor.x + line.pos1.x, cursor.y + line.pos1.y,
                                                cursor.x + line.pos2.x, cursor.y + line.pos2.y,
                                                iflags,
                                                test_circle.pos.x, test_circle.pos.y, test_circle.r,
                                                retv);
            for (int i = 0; i < num; i++)
                draw_dot(retv[i].x, retv[i].y);
            // Line vs polygon
            num = funkout_line_polygon_intersect(test_line.pos1.x, test_line.pos1.y,
                                                 test_line.pos2.x, test_line.pos2.y,
                                                 iflags,
                                                 cursor.x + polygon.pos.x, cursor.y + polygon.pos.y,
                                                 polygon.num, polygon.points,
                                                 retv);
            for (int i = 0; i < num; i++)
                draw_dot(retv[i].x, retv[i].y);
            num = funkout_line_polygon_intersect(cursor.x + line.pos1.x, cursor.y + line.pos1.y,
                                                 cursor.x + line.pos2.x, cursor.y + line.pos2.y,
                                                 iflags,
                                                 test_polygon.pos.x, test_polygon.pos.y,
                                                 test_polygon.num, test_polygon.points,
                                                 retv);
            for (int i = 0; i < num; i++)
                draw_dot(retv[i].x, retv[i].y);
            // Rect rect
            if (funkout_aabb_intersect(cursor.x + aabb.pos.x, cursor.y + aabb.pos.y,
                                       aabb.extents.x, aabb.extents.y,
                                       test_aabb.pos.x, test_aabb.pos.y,
                                       test_aabb.extents.x, test_aabb.extents.y,
                                       retv))
                funkout_render_draw_rect(cursor.x + aabb.pos.x + retv[0].x - aabb.extents.x,
                                         cursor.y + aabb.pos.y + retv[0].y - aabb.extents.y,
                                         aabb.extents.x * 2,
                                         aabb.extents.y * 2,
                                         1, 0, 1, 1);
            // Circle circle
            if (funkout_circle_intersect(cursor.x + circle.pos.x, cursor.y + circle.pos.y, circle.r,
                                         test_circle.pos.x, test_circle.pos.y, test_circle.r,
                                         retv))
                funkout_render_draw_circle(cursor.x + circle.pos.x + retv[0].x, cursor.y + circle.pos.y + retv[0].y, circle.r, 1, 0, 1, 1);
            // Polygon polygon
            if (funkout_polygon_intersect(cursor.x + polygon.pos.x, cursor.y + polygon.pos.y, polygon.num, polygon.points,
                                          test_polygon.pos.x, test_polygon.pos.y, test_polygon.num, test_polygon.points,
                                          retv))
                funkout_render_draw_polygon(cursor.x + polygon.pos.x + retv[0].x, cursor.y + polygon.pos.y + retv[0].y, polygon.num, polygon.points, 1, 0, 1, 1);
            // AABB circle
            if (funkout_aabb_circle_intersect(cursor.x + aabb.pos.x, cursor.y + aabb.pos.y,
                                              aabb.extents.x, aabb.extents.y,
                                              test_circle.pos.x, test_circle.pos.y, test_circle.r,
                                              retv))
                funkout_render_draw_rect(cursor.x + aabb.pos.x + retv[0].x - aabb.extents.x,
                                         cursor.y + aabb.pos.y + retv[0].y - aabb.extents.y,
                                         aabb.extents.x * 2,
                                         aabb.extents.y * 2,
                                         1, 0, 1, 1);
            // Circle AABB
            if (funkout_aabb_circle_intersect(test_aabb.pos.x, test_aabb.pos.y,
                                              test_aabb.extents.x, test_aabb.extents.y,
                                              cursor.x + circle.pos.x, cursor.y + circle.pos.y, circle.r,
                                              retv))
                funkout_render_draw_circle(cursor.x + circle.pos.x - retv[0].x, cursor.y + circle.pos.y - retv[0].y, circle.r, 1, 0, 1, 1);
            // // Circle rect
            // if (funkout_circle_rect_intersect(cursor.x + test_circle.x, cursor.y + test_circle.y, test_circle.r,
            //                                   rect.x, rect.y, rect.w, rect.h,
            //                                   &retv.x, &retv.y))
            //     funkout_render_draw_circle(cursor.x + test_circle.x + retv.x,
            //                                cursor.y + test_circle.y + retv.y,
            //                                test_circle.r,
            //                                1, 0, 1, 1);
            funkout_render_present();
        }
    }
    
    funkout_deinit_video();
    SDL_Quit();
    return 0;
}

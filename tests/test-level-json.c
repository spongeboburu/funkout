/**
 * @file test-level-json.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include "funkout/game.h"
#include "funkout/defs.h"

#include <stdio.h>

int main(int argc, char * argv[])
{
    struct funkout_Level level = FUNKOUT_LEVEL_TEST4;
    char *json = funkout_level_serialize(&level);
    if (json != NULL) {
        struct funkout_Level new_level = {0};
        bool ret = funkout_level_deserialize(&new_level, json);
        printf("%d\n", ret);
        free(json);
    }
    return 0;
}

/**
 * @file video.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include "funkout/error.h"
#include "funkout/video.h"
#include "font_default.c"

#include <limits.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

struct funkout_Image {
    SDL_Texture * texture;
    int w;
    int h;
};


char FUNKOUT_FONT_CHARACTERS[128];
const char FUNKOUT_VALID_CHARACTERS[] = "abcdefghijklmnopqrstuvwxyz.,:;?!0123456789+-*/()_%";

SDL_Window * FUNKOUT_WINDOW;
SDL_Renderer * FUNKOUT_RENDERER;

struct funkout_Image * FUNKOUT_FONT_IMAGE;

static Uint32 funkout_get_window_mode_flags(enum funkout_WindowMode mode)
{
    Uint32 flags = SDL_WINDOW_RESIZABLE;
    switch (mode) {
    case FUNKOUT_WINDOW_MODE_NORMAL:
        break;
    case FUNKOUT_WINDOW_MODE_MAXIMIZED:
        flags |= SDL_WINDOW_MAXIMIZED;
        break;
    case FUNKOUT_WINDOW_MODE_MINIMIZED:
        flags |= SDL_WINDOW_MINIMIZED;
        break;
    case FUNKOUT_WINDOW_MODE_FULLSCREEN:
        flags |= SDL_WINDOW_FULLSCREEN;
        break;
    default:
        break;
    }
    return flags;
}

bool funkout_init_video(int width,
                        int height,
                        enum funkout_WindowMode mode,
                        bool vsync)
{
    int ret = SDL_InitSubSystem(SDL_INIT_VIDEO);
    if (ret < 0) {
        funkout_set_error(ret, SDL_GetError());
        return false;
    }
    ret = IMG_Init(IMG_INIT_PNG);
    if (ret != IMG_INIT_PNG) {
        funkout_set_error(ret, IMG_GetError());
        SDL_QuitSubSystem(SDL_INIT_VIDEO);
        return false;
    }
    Uint32 wflags = funkout_get_window_mode_flags(mode);
    FUNKOUT_WINDOW = SDL_CreateWindow(
        "Funkout",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        width,
        height,
        wflags
        );
    if (FUNKOUT_WINDOW == NULL) {
        funkout_set_error(ret, SDL_GetError());
        IMG_Quit();
        SDL_QuitSubSystem(SDL_INIT_VIDEO);
        return false;
    }
    Uint32 rflags = 0; //SDL_RENDERER_ACCELERATED;
    if (vsync)
        rflags |= SDL_RENDERER_PRESENTVSYNC;
    FUNKOUT_RENDERER = SDL_CreateRenderer(FUNKOUT_WINDOW, -1, rflags);
    if (FUNKOUT_RENDERER == NULL) {
        funkout_set_error(ret, SDL_GetError());
        SDL_DestroyWindow(FUNKOUT_WINDOW);
        FUNKOUT_WINDOW = NULL;
        IMG_Quit();
        SDL_QuitSubSystem(SDL_INIT_VIDEO);
        return false;
    }
    if (SDL_SetRenderDrawBlendMode(FUNKOUT_RENDERER, SDL_BLENDMODE_BLEND) < 0) {
        fprintf(stderr,
                "WARNING: could not set blend mode: %s",
                SDL_GetError());
    }
    FUNKOUT_FONT_IMAGE = funkout_image_new(
        FUNKOUT_DEFAULT_FONT_PNG,
        sizeof(FUNKOUT_DEFAULT_FONT_PNG)
        );
    if (FUNKOUT_FONT_IMAGE == NULL)
        fprintf(stderr,
                "WARNING: could not initialize default font: %s",
                funkout_get_error()->msg);
    for (int i = 0; i < 128; i++)
        FUNKOUT_FONT_CHARACTERS[i] = -1;
    int index = 0;
    for (const char * p = FUNKOUT_VALID_CHARACTERS; *p != 0; p++)
        if (*p > 32)
            FUNKOUT_FONT_CHARACTERS[(int)*p] = index++;
    return true;
}

void funkout_deinit_video(void)
{
    if (FUNKOUT_FONT_IMAGE != NULL)
        funkout_image_free(FUNKOUT_FONT_IMAGE);
    SDL_DestroyRenderer(FUNKOUT_RENDERER);
    SDL_DestroyWindow(FUNKOUT_WINDOW);
    IMG_Quit();
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

void funkout_get_window_size(int * ret_w, int * ret_h)
{
    SDL_GetWindowSize(FUNKOUT_WINDOW, ret_w, ret_h);
}

void funkout_set_window_size(int w, int h)
{
    SDL_SetWindowSize(FUNKOUT_WINDOW, w, h);
}

enum funkout_WindowMode funkout_get_window_mode(void)
{
    Uint32 wflags = SDL_GetWindowFlags(FUNKOUT_WINDOW);
    if (wflags & SDL_WINDOW_MINIMIZED)
        return FUNKOUT_WINDOW_MODE_MINIMIZED;
    else if (wflags & SDL_WINDOW_FULLSCREEN)
        return FUNKOUT_WINDOW_MODE_FULLSCREEN;
    else if (wflags & SDL_WINDOW_MAXIMIZED)
        return FUNKOUT_WINDOW_MODE_MAXIMIZED;
    return FUNKOUT_WINDOW_MODE_NORMAL;
}

bool funkout_set_window_mode(enum funkout_WindowMode mode)
{
    SDL_DisplayMode dmode;
    int i;
    switch (mode) {
    case FUNKOUT_WINDOW_MODE_NORMAL:
        SDL_RestoreWindow(FUNKOUT_WINDOW);
        if (SDL_SetWindowFullscreen(FUNKOUT_WINDOW, 0) < 0) {
            funkout_set_error(-1, SDL_GetError());
            return false;
        }
        break;
    case FUNKOUT_WINDOW_MODE_MAXIMIZED:
        SDL_MaximizeWindow(FUNKOUT_WINDOW);
        break;
    case FUNKOUT_WINDOW_MODE_MINIMIZED:
        SDL_MinimizeWindow(FUNKOUT_WINDOW);
        break;
    case FUNKOUT_WINDOW_MODE_FULLSCREEN:
        i = SDL_GetWindowDisplayIndex(FUNKOUT_WINDOW);
        if (i < 0) {
            funkout_set_error(-1, SDL_GetError());
            return false;
        }
        if (SDL_GetDesktopDisplayMode(i, &dmode) < 0) {
            funkout_set_error(-1, SDL_GetError());
            return false;
        }
        if (SDL_SetWindowDisplayMode(FUNKOUT_WINDOW, &dmode) < 0) {
            funkout_set_error(-1, SDL_GetError());
            return false;
        }
        if (SDL_SetWindowFullscreen(FUNKOUT_WINDOW, SDL_WINDOW_FULLSCREEN) < 0) {
            funkout_set_error(-1, SDL_GetError());
            return false;
        }
        break;
    default:
        funkout_set_error(-1, "invalid window mode");
        return false;
    }
    return true;
}

bool funkout_render_clear(float r, float g, float b, float a)
{
    r = (r < 0 ? 0 : (r > 1.0 ? 1.0 : r));
    g = (g < 0 ? 0 : (g > 1.0 ? 1.0 : g));
    b = (b < 0 ? 0 : (b > 1.0 ? 1.0 : b));
    a = (a < 0 ? 0 : (a > 1.0 ? 1.0 : a));
    if (SDL_SetRenderDrawColor(FUNKOUT_RENDERER,
                               r * 255, g * 255, b * 255, a * 255) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    if (SDL_RenderClear(FUNKOUT_RENDERER) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    return true;
}

void funkout_render_present(void)
{
    SDL_RenderPresent(FUNKOUT_RENDERER);
}

bool funkout_render_draw_rect(int x, int y, int w, int h,
                               float r, float g, float b, float a)
{
    if (w == 0 || h == 0)
        return true;
    r = (r < 0 ? 0 : (r > 1.0 ? 1.0 : r));
    g = (g < 0 ? 0 : (g > 1.0 ? 1.0 : g));
    b = (b < 0 ? 0 : (b > 1.0 ? 1.0 : b));
    a = (a < 0 ? 0 : (a > 1.0 ? 1.0 : a));
    if (SDL_SetRenderDrawColor(FUNKOUT_RENDERER,
                               r * 255, g * 255, b * 255, a * 255) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    SDL_Rect rect = {x, y, w, h};
    if (SDL_RenderDrawRect(FUNKOUT_RENDERER, &rect) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    return true;
}

bool funkout_render_fill_rect(int x, int y, int w, int h,
                               float r, float g, float b, float a)
{
    if (w == 0 || h == 0)
        return true;
    r = (r < 0 ? 0 : (r > 1.0 ? 1.0 : r));
    g = (g < 0 ? 0 : (g > 1.0 ? 1.0 : g));
    b = (b < 0 ? 0 : (b > 1.0 ? 1.0 : b));
    a = (a < 0 ? 0 : (a > 1.0 ? 1.0 : a));
    if (SDL_SetRenderDrawColor(FUNKOUT_RENDERER,
                               r * 255, g * 255, b * 255, a * 255) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    SDL_Rect rect = {x, y, w, h};
    if (SDL_RenderFillRect(FUNKOUT_RENDERER, &rect) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    return true;
}

bool funkout_render_draw_line(int x1, int y1, int x2, int y2,
                               float r, float g, float b, float a)
{
    r = (r < 0 ? 0 : (r > 1.0 ? 1.0 : r));
    g = (g < 0 ? 0 : (g > 1.0 ? 1.0 : g));
    b = (b < 0 ? 0 : (b > 1.0 ? 1.0 : b));
    a = (a < 0 ? 0 : (a > 1.0 ? 1.0 : a));
    if (SDL_SetRenderDrawColor(FUNKOUT_RENDERER,
                               r * 255, g * 255, b * 255, a * 255) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    if (SDL_RenderDrawLine(FUNKOUT_RENDERER, x1, y1, x2, y2) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    return true;
}

bool funkout_render_draw_circle(int x, int y, int radius,
                                float r, float g, float b, float a)
{
    if (radius <= 0)
        return true;
    r = (r < 0 ? 0 : (r > 1.0 ? 1.0 : r));
    g = (g < 0 ? 0 : (g > 1.0 ? 1.0 : g));
    b = (b < 0 ? 0 : (b > 1.0 ? 1.0 : b));
    a = (a < 0 ? 0 : (a > 1.0 ? 1.0 : a));
    if (SDL_SetRenderDrawColor(FUNKOUT_RENDERER,
                               r * 255, g * 255, b * 255, a * 255) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    int prevx, prevy;
    for (int i = 0; i <= radius; i++) {
        int xx = x + roundf(
            (float)radius
            * cos((float)i * 6.283185307179586f / (float)radius)
            );
        int yy = y + roundf(
            (float)radius
            * sin((float)i * 6.283185307179586f / (float)radius)
            );
        if (i > 0) {
            if (SDL_RenderDrawLine(FUNKOUT_RENDERER, prevx, prevy, xx, yy) < 0) {
                funkout_set_error(-1, SDL_GetError());
                return false;
            }
        }
        prevx = xx;
        prevy = yy;
    }
    return true;
}

bool funkout_render_draw_triangle(int x1, int y1,
                                  int x2, int y2,
                                  int x3, int y3,
                                  float r, float g, float b, float a)
{
    struct funkout_Vector points[3] = {{x1, y1}, {x2, y2}, {x3, y3}};
    return funkout_render_draw_polygon(0, 0, 3, points, r, g, b, a);
}

bool funkout_render_draw_polygon(int x, int y, 
                                 int num_points,
                                 const struct funkout_Vector * points,
                                 float r, float g, float b, float a)
{
    if (num_points < 3 || points == NULL)
        return true;
    r = (r < 0 ? 0 : (r > 1.0 ? 1.0 : r));
    g = (g < 0 ? 0 : (g > 1.0 ? 1.0 : g));
    b = (b < 0 ? 0 : (b > 1.0 ? 1.0 : b));
    a = (a < 0 ? 0 : (a > 1.0 ? 1.0 : a));
    if (SDL_SetRenderDrawColor(FUNKOUT_RENDERER,
                               r * 255, g * 255, b * 255, a * 255) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    int prevx, prevy;
    for (int i = 0; i < num_points; i++) {
        if (i > 0) {
            if (SDL_RenderDrawLine(FUNKOUT_RENDERER, prevx + x, prevy + y, points[i].x + x, points[i].y + y) < 0) {
                funkout_set_error(-1, SDL_GetError());
                return false;
            }
        }
        prevx = points[i].x;
        prevy = points[i].y;
    }
    if (SDL_RenderDrawLine(FUNKOUT_RENDERER,
                           points[num_points - 1].x + x, points[num_points - 1].y + y,
                           points[0].x + x, points[0].y + y) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    return true;
}

bool funkout_render_draw_shape(const struct funkout_Shape * shape,
                              float r, float g, float b, float a)
{
    switch (shape->type) {
    case FUNKOUT_SHAPE_AABB:
        return funkout_render_draw_rect(shape->s.aabb.pos.x - shape->s.aabb.extents.x,
                                        shape->s.aabb.pos.y - shape->s.aabb.extents.y,
                                        shape->s.aabb.extents.x * 2,
                                        shape->s.aabb.extents.y * 2,
                                        r, g, b, a);
    case FUNKOUT_SHAPE_CIRCLE:
        return funkout_render_draw_circle(shape->s.circle.pos.x,
                                          shape->s.circle.pos.y,
                                          shape->s.circle.r,
                                          r, g, b, a);
    default:
        break;
    }
    return true;
}

struct funkout_Image * funkout_image_new(const void * data, size_t size)
{
    if (data == NULL || size == 0) {
        funkout_set_error(-1, "no data");
        return NULL;
    }
    if (size > INT_MAX) {
        funkout_set_error(-1, "image size too large");
        return NULL;
    }
    SDL_RWops * ops = SDL_RWFromConstMem(data, size);
    if (ops == NULL) {
        funkout_set_error(-1, "image size too large");
        return NULL;
    }
    SDL_Surface * surface = IMG_LoadTyped_RW(ops, 0, "PNG");
    if (surface == NULL) {
        SDL_FreeRW(ops);
        funkout_set_error(-1, IMG_GetError());
        return NULL;
    }
    SDL_FreeRW(ops);
    SDL_Texture * texture = SDL_CreateTextureFromSurface(
        FUNKOUT_RENDERER,
        surface
        );
    SDL_FreeSurface(surface);
    if (texture == NULL) {
        funkout_set_error(-1, SDL_GetError());
        return NULL;
    }
    struct funkout_Image * ret = calloc(1, sizeof(struct funkout_Image));
    ret->texture = texture;
    SDL_QueryTexture(texture, NULL, NULL, &ret->w, &ret->h);
    return ret;
}

void funkout_image_free(struct funkout_Image * image)
{
    if (image->texture != NULL)
        SDL_DestroyTexture(image->texture);
    free(image);
}

void funkout_image_resolution(const struct funkout_Image * image, int * ret_w, int * ret_h)
{
    if (ret_w != NULL)
        *ret_w = image->w;
    if (ret_h != NULL)
        *ret_h = image->h;
}

bool funkout_image_draw(const struct funkout_Image * image,
                        int x, int y, int w, int h,
                        int src_x, int src_y, int src_w, int src_h,
                        float alpha,
                        float angle,
                        float align_x,
                        float align_y)
{
    if (image == NULL || image->texture == NULL)
        return true;
    SDL_Point center = {w / 2 + (align_x * w / 2.0),
                        h / 2 + (align_y * h / 2.0)};
    SDL_SetTextureAlphaMod(
        image->texture,
        (alpha < 0 ? 0 : (alpha > 1.0 ? 1.0 : alpha * 255))
        );
    SDL_Rect dstrect = {x, y, w, h};
    SDL_Rect srcrect = {src_x, src_y, src_w, src_h};
    if (SDL_RenderCopyEx(
            FUNKOUT_RENDERER,
            image->texture,
            (src_w > 0 && src_h > 0 ? &srcrect : NULL),
            &dstrect,
            angle,
            &center,
            0
            ) < 0) {
        funkout_set_error(-1, SDL_GetError());
        return false;
    }
    return true;
}
    
bool funkout_image_draw_char(const struct funkout_Image * image,
                             char c,
                             int x, int y, int w, int h,
                             float alpha,
                             float angle,
                             float align_x,
                             float align_y)
{
    if (image == NULL)
        image = FUNKOUT_FONT_IMAGE;
    if (image == NULL || image->texture == NULL)
        return true;
    if (c > 32) {
        char idx = FUNKOUT_FONT_CHARACTERS[(int)c];
        if (idx >= 0 && idx < 64) {
            SDL_Rect srcrect;
            srcrect.w = image->w / FUNKOUT_FONT_GRID_SIZE;
            srcrect.h = image->h / FUNKOUT_FONT_GRID_SIZE;
            srcrect.x = (((idx % FUNKOUT_FONT_GRID_SIZE) * image->w)
                         / FUNKOUT_FONT_GRID_SIZE);
            srcrect.y = (((idx / FUNKOUT_FONT_GRID_SIZE) * image->h)
                         / FUNKOUT_FONT_GRID_SIZE);
            return funkout_image_draw(
                image,
                x, y, w, h,
                srcrect.x, srcrect.y, srcrect.w, srcrect.h,
                alpha,
                angle,
                align_x,
                align_y
                );
        }
        funkout_set_error(-1, "invalid char: %u\n", (unsigned char)c);
        return false;
    }
    return true;
}

bool funkout_image_draw_text(const struct funkout_Image * image,
                             const char * text,
                             int x, int y,
                             int size,
                             float alpha,
                             float angle,
                             float align_x,
                             float align_y)
{
    for (const char *p = text; *p != 0; p++) {
        if (!funkout_image_draw_char(
                image,
                *p,
                x, y, size, size,
                alpha,
                angle,
                align_x,
                align_y
                )
            )
            return false;
        x += size;
    }
    return true;
}

bool funkout_animation_draw(const struct funkout_Animation * animation,
                            const struct funkout_Image * image,
                            int x, int y, int w, int h,
                            float alpha,
                            float angle,
                            float align_x,
                            float align_y)
{
    if (image == NULL)
        return true;
    int iw, ih;
    funkout_image_resolution(image, &iw, &ih);
    if (animation->size <= 0)
        return funkout_image_draw(image, x, y, w, h, 0, 0, iw, ih, alpha, angle, align_x, align_y);
    int num_frames = iw / animation->size;
    if (num_frames <= 0)
        return funkout_image_draw(image, x, y, w, h, 0, 0, iw, ih, alpha, angle, align_x, align_y);
    if (animation->duration > 0) {
        float duration = animation->duration;
        float timestep = animation->timestep;
        if (animation->loops >= 0)
            duration = animation->duration * (animation->loops + 1);
        else
            timestep = fmodf(timestep, animation->duration);
        float f = timestep / duration;
        int ix = MINMAX(f, 0, 1) * num_frames;
        if (ix >= num_frames)
            ix = num_frames - 1;
        return funkout_image_draw(image, x, y, w, h, ix * animation->size, 0, animation->size, ih, alpha, angle, align_x, align_y);
    }
    return funkout_image_draw(image, x, y, w, h, 0, 0, animation->size, ih, alpha, angle, align_x, align_y);
}

bool funkout_render_set_clip(const struct funkout_IRect * clip)
{
    if (clip != NULL) {
        SDL_Rect rclip = {clip->x, clip->y, clip->w, clip->h};
        if (SDL_RenderSetClipRect(FUNKOUT_RENDERER, &rclip) < 0) {
            funkout_set_error(-1, "%s", SDL_GetError());
            return false;
        }
    } else if (SDL_RenderSetClipRect(FUNKOUT_RENDERER, NULL) < 0) {
        funkout_set_error(-1, "%s", SDL_GetError());
        return false;
    }
    return true;
}

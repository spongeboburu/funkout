#include "funkout/defs.h"

#define WEAPON_BULLET {FUNKOUT_WEAPON_TYPE_STRAIGHT, 6, 2, 1, 1, 4.0, 0.25, 0}
#define WEAPON_BULLET_FAST {FUNKOUT_WEAPON_TYPE_STRAIGHT, 2, 1, 0.25, 0.25, 4.0, 0.25, 0}
#define WEAPON_BEAM {FUNKOUT_WEAPON_TYPE_BEAM, 8, 2, 2, 0.5, 4.0, 0.2, 3.0}

const struct funkout_EnemyDef FUNKOUT_ENEMY_DEF_AOMEBA = {
    "amoeba",
    FUNKOUT_ENEMY_APPEARANCE_AMOEBA,
    0.75,
    1,
    {0},
    0.5,
    false,
    0.0,
    false
};

const struct funkout_EnemyDef FUNKOUT_ENEMY_DEF_VIRUS = {
    "virus",
    FUNKOUT_ENEMY_APPEARANCE_VIRUS,
    1.0,
    3,
    {0},
    0.5,
    false,
    0.0,
    false
};

const struct funkout_EnemyDef FUNKOUT_ENEMY_DEF_SATA = {
    "satellite a",
    FUNKOUT_ENEMY_APPEARANCE_SATA,
    1.2,
    2,
    WEAPON_BULLET,
    0.5,
    false,
    0.0,
    false
};

const struct funkout_EnemyDef FUNKOUT_ENEMY_DEF_SATB = {
    "satellite b",
    FUNKOUT_ENEMY_APPEARANCE_SATB,
    1.0,
    3,
    WEAPON_BULLET,
    0.5,
    false,
    0.0,
    true
};

const struct funkout_EnemyDef FUNKOUT_ENEMY_DEF_SATC = {
    "satellite c",
    FUNKOUT_ENEMY_APPEARANCE_SATC,
    1.0,
    3,
    WEAPON_BEAM,
    0.5,
    false,
    0.0,
    false
};

const struct funkout_EnemyDef FUNKOUT_ENEMY_DEF_SATD = {
    "satellite d",
    FUNKOUT_ENEMY_APPEARANCE_SATD,
    1.2,
    3,
    WEAPON_BEAM,
    0.5,
    false,
    0.0,
    true
};

const struct funkout_EnemyDef FUNKOUT_ENEMY_DEF_PLAGUE = {
    "plague",
    FUNKOUT_ENEMY_APPEARANCE_PLAGUE,
    2.0,
    3,
    WEAPON_BULLET_FAST,
    0.5,
    true,
    0.0,
    true
};

const struct funkout_EnemyDef FUNKOUT_ENEMY_DEF_TANK = {
    "tank",
    FUNKOUT_ENEMY_APPEARANCE_TANK,
    1.5,
    -1,
    {0},
    0.5,
    true,
    0.0,
    false
};

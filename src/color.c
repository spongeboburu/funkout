#include "funkout/color.h"

void funkout_color_cap(const struct funkout_Color * color, struct funkout_Color * ret_color)
{
    ret_color->r = color->r < 0 ? 0 : (color->r > 1.0 ? 1 : color->r);
    ret_color->g = color->g < 0 ? 0 : (color->g > 1.0 ? 1 : color->g);
    ret_color->b = color->b < 0 ? 0 : (color->b > 1.0 ? 1 : color->b);
    ret_color->a = color->a < 0 ? 0 : (color->a > 1.0 ? 1 : color->a);
}

void funkout_color_blend(const struct funkout_Color * color, const struct funkout_Color * blend, float factor, struct funkout_Color * ret_color)
{
    factor = factor < 0 ? 0 : (factor > 1 ? 1 : factor);
    ret_color->r = color->r * (1.0 - factor) + blend->r * factor;
    ret_color->g = color->g * (1.0 - factor) + blend->g * factor;
    ret_color->b = color->b * (1.0 - factor) + blend->b * factor;
    ret_color->a = color->a * (1.0 - factor) + blend->a * factor;
    ret_color->r = ret_color->r < 0 ? 0 : (ret_color->r > 1.0 ? 1
                                           : ret_color->r);
    ret_color->g = ret_color->g < 0 ? 0 : (ret_color->g > 1.0 ? 1
                                           : ret_color->g);
    ret_color->b = ret_color->b < 0 ? 0 : (ret_color->b > 1.0 ? 1
                                           : ret_color->b);
    ret_color->a = ret_color->a < 0 ? 0 : (ret_color->a > 1.0 ? 1
                                           : ret_color->a);
}

void funkout_color_mul(const struct funkout_Color * color,
                       float multiplier,
                       struct funkout_Color * ret_color)
{
    ret_color->r = color->r * multiplier;
    ret_color->g = color->g * multiplier;
    ret_color->b = color->b * multiplier;
    ret_color->a = color->a * multiplier;
    ret_color->r = ret_color->r < 0 ? 0 : (ret_color->r > 1.0 ? 1
                                           : ret_color->r);
    ret_color->g = ret_color->g < 0 ? 0 : (ret_color->g > 1.0 ? 1
                                           : ret_color->g);
    ret_color->b = ret_color->b < 0 ? 0 : (ret_color->b > 1.0 ? 1
                                           : ret_color->b);
    ret_color->a = ret_color->a < 0 ? 0 : (ret_color->a > 1.0 ? 1
                                           : ret_color->a);
}

void funkout_color_add(const struct funkout_Color * color,
                       float addition,
                       struct funkout_Color * ret_color)
{
    ret_color->r = color->r + addition;
    ret_color->g = color->g + addition;
    ret_color->b = color->b + addition;
    ret_color->a = color->a + addition;
    ret_color->r = ret_color->r < 0 ? 0 : (ret_color->r > 1.0 ? 1
                                           : ret_color->r);
    ret_color->g = ret_color->g < 0 ? 0 : (ret_color->g > 1.0 ? 1
                                           : ret_color->g);
    ret_color->b = ret_color->b < 0 ? 0 : (ret_color->b > 1.0 ? 1
                                           : ret_color->b);
    ret_color->a = ret_color->a < 0 ? 0 : (ret_color->a > 1.0 ? 1
                                           : ret_color->a);
}

/**
 * @file collision.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include <stdlib.h>
#include <math.h>
#include "funkout/collision.h"

bool funkout_aabb_collision(float aabb1_pos_x, float aabb1_pos_y,
                            float aabb1_extent_x, float aabb1_extent_y,
                            float aabb1_delta_x, float aabb1_delta_y,
                            float aabb2_pos_x, float aabb2_pos_y,
                            float aabb2_extent_x, float aabb2_extent_y,
                            float aabb2_delta_x, float aabb2_delta_y,
                            float * ret_time,
                            struct funkout_Vector * ret_min_translation,
                            struct funkout_Vector * ret_normal)
{
    if (aabb1_extent_x <= 0 || aabb1_extent_y <= 0
        || aabb2_extent_x <= 0 || aabb2_extent_y <= 0)
        return false;
    struct funkout_Vector mtv;
    if (funkout_aabb_intersect(aabb1_pos_x, aabb1_pos_y,
                               aabb1_extent_x, aabb1_extent_y,
                               aabb2_pos_x, aabb2_pos_y,
                               aabb2_extent_x, aabb2_extent_y,
                               &mtv)) {
        if (ret_time != NULL)
            *ret_time = 0;
        if (ret_min_translation != NULL)
            *ret_min_translation = mtv;
        if (ret_normal != NULL) {
            float l = sqrtf(mtv.x * mtv.x + mtv.y * mtv.y);
            if (l > 0) {
                ret_normal->x = mtv.x / l;
                ret_normal->y = mtv.y / l;
            } else {
                ret_normal->x = 0;
                ret_normal->y = 0;
            }
        }
        return true;
    }
    struct funkout_Vector delta = {aabb1_delta_x - aabb2_delta_x, aabb1_delta_y - aabb2_delta_y};
    float delta_length = sqrtf(delta.x * delta.x + delta.y * delta.y);
    if (delta_length == 0)
        return false;
    struct funkout_AABB mink = {{aabb2_pos_x, aabb2_pos_y},
                                {aabb2_extent_x + aabb1_extent_x,
                                 aabb2_extent_y + aabb1_extent_y}};
    struct funkout_Vector ipoints[2];
    int num = funkout_line_aabb_intersect(aabb1_pos_x, aabb1_pos_y,
                                          aabb1_pos_x + delta.x, aabb1_pos_y + delta.y,
                                          FUNKOUT_INF_FLAG_POS,
                                          mink.pos.x, mink.pos.y,
                                          mink.extents.x, mink.extents.y,
                                          ipoints);
    if (num > 0) {
        float distance;
        struct funkout_Vector ipoint;
        for (int i = 0; i < num; i++) {
            struct funkout_Vector idelta = {ipoints[i].x - aabb1_pos_x, ipoints[i].y - aabb1_pos_y};
            float l = sqrtf(idelta.x * idelta.x + idelta.y * idelta.y);
            if (i == 0 || l < distance) {
                distance = l;
                ipoint = ipoints[i];
            }
        }
        if (distance == 0 || distance >= delta_length)
            return false;
        if (ret_time != NULL)
            *ret_time = distance / delta_length;
        if (ret_min_translation != NULL) {
            ret_min_translation->x = 0;
            ret_min_translation->y = 0;
        }
        if (ret_normal != NULL) {
            if (ipoint.x + aabb1_extent_x > aabb2_pos_x - aabb2_extent_x && ipoint.x - aabb1_extent_x < aabb2_pos_x + aabb2_extent_x) {
                ret_normal->x = 0;
                ret_normal->y = ipoint.y < aabb2_pos_y ? -1 : 1;
            } else if (ipoint.y + aabb1_extent_y > aabb2_pos_y - aabb2_extent_y && ipoint.y - aabb1_extent_y < aabb2_pos_y + aabb2_extent_y) {
                ret_normal->x = ipoint.x < aabb2_pos_x ? -1 : 1;
                ret_normal->y = 0;
            } else {
                ret_normal->x = ipoint.x < aabb2_pos_x ? -1 : 1;
                ret_normal->y = ipoint.y < aabb2_pos_y ? -1 : 1;
            }
        }
        return true;
    }
    return false;
}

bool funkout_circle_collision(float circle1_pos_x, float circle1_pos_y,
                              float circle1_radius,
                              float circle1_delta_x, float circle1_delta_y,
                              float circle2_pos_x, float circle2_pos_y,
                              float circle2_radius,
                              float circle2_delta_x, float circle2_delta_y,
                              float * ret_time,
                              struct funkout_Vector * ret_min_translation,
                              struct funkout_Vector * ret_normal)
{
    if (circle1_radius <= 0 || circle2_radius <= 0)
        return false;
    struct funkout_Vector mtv;
    if (funkout_circle_intersect(circle1_pos_x, circle1_pos_y, circle1_radius,
                                 circle2_pos_x, circle2_pos_y, circle2_radius,
                                 &mtv)) {
        if (ret_time != NULL)
            *ret_time = 0;
        if (ret_min_translation != NULL)
            *ret_min_translation = mtv;
        if (ret_normal != NULL) {
            float l = sqrtf(mtv.x * mtv.x + mtv.y * mtv.y);
            if (l > 0) {
                ret_normal->x = mtv.x / l;
                ret_normal->y = mtv.y / l;
            } else {
                ret_normal->x = 0;
                ret_normal->y = 0;
            }
        }
        return true;
    }
    struct funkout_Vector delta = {circle1_delta_x - circle2_delta_x, circle1_delta_y - circle2_delta_y};
    float delta_length = sqrtf(delta.x * delta.x + delta.y * delta.y);
    if (delta_length == 0)
        return false;
    struct funkout_Circle mink = {{circle2_pos_x, circle2_pos_y}, circle2_radius + circle1_radius};
    struct funkout_Vector ipoints[2];
    int num = funkout_line_circle_intersect(circle1_pos_x, circle1_pos_y,
                                            circle1_pos_x + delta.x, circle1_pos_y + delta.y,
                                            FUNKOUT_INF_FLAG_POS,
                                            mink.pos.x, mink.pos.y,
                                            mink.r,
                                            ipoints);
    if (num > 0) {
        float distance;
        for (int i = 0; i < num; i++) {
            struct funkout_Vector idelta = {ipoints[i].x - circle1_pos_x, ipoints[i].y - circle1_pos_y};
            float l = sqrtf(idelta.x * idelta.x + idelta.y * idelta.y);
            if (i == 0 || l < distance) {
                distance = l;
            }
        }
        if (distance <= 0 || distance >= delta_length)
            return false;
        if (ret_time != NULL)
            *ret_time = distance / delta_length;
        if (ret_min_translation != NULL) {
            ret_min_translation->x = 0;
            ret_min_translation->y = 0;
        }
        if (ret_normal != NULL) {
            float l = distance / delta_length;
            struct funkout_Vector tmpp = {(circle2_pos_x + circle2_delta_x * l) - (circle1_pos_x + circle1_delta_x * l),
                                          (circle2_pos_y + circle2_delta_y * l) - (circle1_pos_y + circle1_delta_y * l)};
            float tmppl = sqrtf(tmpp.x * tmpp.x + tmpp.y * tmpp.y);
            if (tmppl > 0) {
                ret_normal->x = -tmpp.x / tmppl;
                ret_normal->y = -tmpp.y / tmppl;
            } else {
                ret_normal->x = 0;
                ret_normal->y = 0;
            }
        }
        return true;
    }
    return false;
}

bool funkout_aabb_circle_collision(float aabb_pos_x, float aabb_pos_y,
                                   float aabb_extent_x, float aabb_extent_y,
                                   float aabb_delta_x, float aabb_delta_y,
                                   float circle_pos_x, float circle_pos_y,
                                   float circle_radius,
                                   float circle_delta_x, float circle_delta_y,
                                   float * ret_time,
                                   struct funkout_Vector * ret_min_translation,
                                   struct funkout_Vector * ret_normal)
{
    if (aabb_extent_x <= 0 || aabb_extent_y <= 0 || circle_radius <= 0)
        return false;
    struct funkout_Vector mtv;
    if (funkout_aabb_circle_intersect(aabb_pos_x, aabb_pos_y,
                                      aabb_extent_x, aabb_extent_y,
                                      circle_pos_x, circle_pos_y, circle_radius,
                                      &mtv)) {
        if (ret_time != NULL)
            *ret_time = 0;
        if (ret_min_translation != NULL)
            *ret_min_translation = mtv;
        if (ret_normal != NULL) {
            float l = sqrtf(mtv.x * mtv.x + mtv.y * mtv.y);
            if (l > 0) {
                ret_normal->x = mtv.x / l;
                ret_normal->y = mtv.y / l;
            } else {
                ret_normal->x = 0;
                ret_normal->y = 0;
            }
        }
        return true;
    }
    struct funkout_Vector delta = {aabb_delta_x - circle_delta_x, aabb_delta_y - circle_delta_y};
    float delta_length = sqrtf(delta.x * delta.x + delta.y * delta.y);
    if (delta_length == 0)
        return false;
    struct funkout_AABB capsule_aabb = {
        {circle_pos_x, circle_pos_y},
        {circle_radius + aabb_extent_x, circle_radius + aabb_extent_y}
    };
    struct funkout_Vector normal = {0};
    struct funkout_Vector ipoints[2];
    int num = funkout_line_aabb_intersect(aabb_pos_x, aabb_pos_y,
                                          aabb_pos_x + delta.x, aabb_pos_y + delta.y,
                                          FUNKOUT_INF_FLAG_POS,
                                          capsule_aabb.pos.x, capsule_aabb.pos.y,
                                          capsule_aabb.extents.x, capsule_aabb.extents.y,
                                          ipoints);
    float distance = -1;
    if (num > 0) {
        struct funkout_Vector ipoint;
        for (int i = 0; i < num; i++) {
            struct funkout_Vector idelta = {ipoints[i].x - aabb_pos_x, ipoints[i].y - aabb_pos_y};
            float l = sqrtf(idelta.x * idelta.x + idelta.y * idelta.y);
            if (l > 0 && (distance < 0 || l < distance)) {
                struct funkout_Vector closest = {aabb_pos_x + delta.x * l / distance,
                                                 aabb_pos_y + delta.y * l / distance};
                if (closest.x + aabb_extent_x > circle_pos_x && closest.x - aabb_extent_x < circle_pos_x) {
                    normal.x = 0;
                    normal.y = closest.y < circle_pos_y ? -1 : 1;
                } else if (closest.y + aabb_extent_y > circle_pos_y && closest.y - aabb_extent_y < circle_pos_y) {
                    normal.x = closest.x < circle_pos_x ? -1 : 1;
                    normal.y = 0;
                } else
                    continue;
                ipoint = ipoints[i];
                distance = l;
            }
        }
        if (distance >= 0) {
            struct funkout_Vector cdelta = {ipoint.x - circle_pos_x, ipoint.y - circle_pos_y};
            if (fabsf(cdelta.x) > aabb_extent_x && fabsf(cdelta.y) > aabb_extent_y)
                distance = -1;
        }
    }
    struct funkout_Circle capsule_circles[4] = {
        {{circle_pos_x - aabb_extent_x, circle_pos_y - aabb_extent_y}, circle_radius},
        {{circle_pos_x + aabb_extent_x, circle_pos_y - aabb_extent_y}, circle_radius},
        {{circle_pos_x + aabb_extent_x, circle_pos_y + aabb_extent_y}, circle_radius},
        {{circle_pos_x - aabb_extent_x, circle_pos_y + aabb_extent_y}, circle_radius},
    };
    for (int ci = 0; ci < 4; ci++) {
        num = funkout_line_circle_intersect(aabb_pos_x, aabb_pos_y,
                                            aabb_pos_x + delta.x, aabb_pos_y + delta.y,
                                            FUNKOUT_INF_FLAG_POS,
                                            capsule_circles[ci].pos.x, capsule_circles[ci].pos.y,
                                            capsule_circles[ci].r,
                                            ipoints);
        for (int i = 0; i < num; i++) {
            struct funkout_Vector idelta = {ipoints[i].x - aabb_pos_x, ipoints[i].y - aabb_pos_y};
            float l = sqrtf(idelta.x * idelta.x + idelta.y * idelta.y);
            if (l > 0 && (distance < 0 || l < distance)) {
                struct funkout_Vector cd = {capsule_circles[ci].pos.x - (aabb_pos_x + delta.x * l / delta_length),
                                            capsule_circles[ci].pos.y - (aabb_pos_y + delta.y * l / delta_length)};
                float cdl = sqrtf(cd.x * cd.x + cd.y * cd.y);
                if (cdl > 0) {
                    normal.x = -cd.x / cdl;
                    normal.y = -cd.y / cdl;
                } else {
                    normal.x = 0;
                    normal.y = 1;
                }
                distance = l;
            }
        }
    }
    if (distance <= 0 || distance >= delta_length)
        return false;
    if (ret_time != NULL)
        *ret_time = distance / delta_length;
    if (ret_min_translation != NULL) {
        ret_min_translation->x = 0;
        ret_min_translation->y = 0;
    }
    if (ret_normal != NULL) 
        *ret_normal = normal;
    return true;
}

bool funkout_shape_collision(const struct funkout_Shape * shape1,
                             float shape1_delta_x, float shape1_delta_y,
                             const struct funkout_Shape * shape2,
                             float shape2_delta_x, float shape2_delta_y,
                             float * ret_time,
                             struct funkout_Vector * ret_min_translation,
                             struct funkout_Vector * ret_normal)
{
    struct funkout_Vector mtv, normal;
    switch (shape1->type) {
    case FUNKOUT_SHAPE_AABB:
        switch (shape2->type) {
        case FUNKOUT_SHAPE_AABB:
            return funkout_aabb_collision(shape1->s.aabb.pos.x, shape1->s.aabb.pos.y,
                                          shape1->s.aabb.extents.x, shape1->s.aabb.extents.y,
                                          shape1_delta_x, shape1_delta_y,
                                          shape2->s.aabb.pos.x, shape2->s.aabb.pos.y,
                                          shape2->s.aabb.extents.x, shape2->s.aabb.extents.y,
                                          shape2_delta_x, shape2_delta_y,
                                          ret_time,
                                          ret_min_translation,
                                          ret_normal);
        case FUNKOUT_SHAPE_CIRCLE:
            return funkout_aabb_circle_collision(shape1->s.aabb.pos.x, shape1->s.aabb.pos.y,
                                                 shape1->s.aabb.extents.x, shape1->s.aabb.extents.y,
                                                 shape1_delta_x, shape1_delta_y,
                                                 shape2->s.circle.pos.x, shape2->s.circle.pos.y,
                                                 shape2->s.circle.r,
                                                 shape2_delta_x, shape2_delta_y,
                                                 ret_time,
                                                 ret_min_translation,
                                                 ret_normal);
        default:
            break;
        }
        break;
    case FUNKOUT_SHAPE_CIRCLE:
        switch (shape2->type) {
        case FUNKOUT_SHAPE_CIRCLE:
            return funkout_circle_collision(shape1->s.circle.pos.x, shape1->s.circle.pos.y,
                                            shape1->s.circle.r,
                                            shape1_delta_x, shape1_delta_y,
                                            shape2->s.circle.pos.x, shape2->s.circle.pos.y,
                                            shape2->s.circle.r,
                                            shape2_delta_x, shape2_delta_y,
                                            ret_time,
                                            ret_min_translation,
                                            ret_normal);
        case FUNKOUT_SHAPE_AABB:
            if (funkout_aabb_circle_collision(shape2->s.aabb.pos.x, shape2->s.aabb.pos.y,
                                              shape2->s.aabb.extents.x, shape2->s.aabb.extents.y,
                                              shape2_delta_x, shape2_delta_y,
                                              shape1->s.circle.pos.x, shape1->s.circle.pos.y,
                                              shape1->s.circle.r,
                                              shape1_delta_x, shape1_delta_y,
                                              ret_time,
                                              &mtv,
                                              &normal)) {
                if (ret_min_translation != NULL) {
                    ret_min_translation->x = -mtv.x;
                    ret_min_translation->y = -mtv.y;
                }
                if (ret_normal != NULL) {
                    ret_normal->x = -normal.x;
                    ret_normal->y = -normal.y;
                }
                return true;
            }
        default:
            break;
        }
        break;
    default:
        break;
    }
    return false;
}

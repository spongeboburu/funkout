#include <limits.h>
#include <SDL2/SDL_mixer.h>
#include "funkout/audio.h"
#include "funkout/error.h"

struct funkout_Audio {
    Mix_Chunk * chunk;
};

static bool FUNKOUT_HAS_AUDIO;
static int current_channel;

bool funkout_init_audio(int frequency)
{
    if (Mix_Init(MIX_INIT_OGG) != MIX_INIT_OGG) {
        funkout_set_error(-1, "%s", Mix_GetError());
        return false;
    }

    if (Mix_OpenAudio(frequency, MIX_DEFAULT_FORMAT, 2, 1024) < 0) {
        funkout_set_error(-1, "%s", Mix_GetError());
        Mix_Quit();
        return false;
    }
    Mix_AllocateChannels(FUNKOUT_AUDIO_NUM_RESERVED_CHANNELS + FUNKOUT_AUDIO_NUM_CHANNELS);
    int qfrequency, qchannels;
    Uint16 qformat;
    FUNKOUT_HAS_AUDIO = Mix_QuerySpec(&qfrequency, &qformat, &qchannels);
    if (!FUNKOUT_HAS_AUDIO) {
        Mix_CloseAudio();
        Mix_Quit();
        return false;
    }
    const char * format_str = "Unknown";
    switch(qformat) {
    case AUDIO_U8: format_str="U8"; break;
    case AUDIO_S8: format_str="S8"; break;
    case AUDIO_U16LSB: format_str="U16LSB"; break;
    case AUDIO_S16LSB: format_str="S16LSB"; break;
    case AUDIO_U16MSB: format_str="U16MSB"; break;
    case AUDIO_S16MSB: format_str="S16MSB"; break;
    }
    printf("Audio: %dhz %dch %s\n", qfrequency, qchannels, format_str);
    return true;
}

void funkout_deinit_audio(void)
{
    FUNKOUT_HAS_AUDIO = false;
    Mix_CloseAudio();
    Mix_Quit();
}

bool funkout_has_audio(void)
{
    return FUNKOUT_HAS_AUDIO;
}

struct funkout_Audio * funkout_audio_new(const void * data, size_t size)
{
    if (data == NULL || size == 0) {
        funkout_set_error(-1, "no data");
        return NULL;
    }
    if (size > INT_MAX) {
        funkout_set_error(-1, "audio size too large");
        return NULL;
    }
    SDL_RWops * ops = SDL_RWFromConstMem(data, size);
    if (ops == NULL) {
        funkout_set_error(-1, "audio size too large");
        return NULL;
    }
    Mix_Chunk * chunk = Mix_LoadWAV_RW(ops, 0);
    if (chunk == NULL) {
        SDL_FreeRW(ops);
        funkout_set_error(-1, "%s", Mix_GetError());
        return NULL;
    }
    SDL_FreeRW(ops);
    struct funkout_Audio * ret = calloc(1, sizeof(struct funkout_Audio));
    ret->chunk = chunk;
    return ret;
}

void funkout_audio_free(struct funkout_Audio * audio)
{
    if (audio->chunk != NULL)
        Mix_FreeChunk(audio->chunk);
    free(audio);
}

bool funkout_audio_play(const struct funkout_Audio * audio, int channel, int loops)
{
    if (audio != NULL && audio->chunk != NULL) {
        if (channel < 0) {
            current_channel = (current_channel + 1) % FUNKOUT_AUDIO_NUM_CHANNELS;
            channel = FUNKOUT_AUDIO_NUM_RESERVED_CHANNELS + current_channel;
        }
        if (Mix_PlayChannel(channel, audio->chunk, loops) < 0) {
            funkout_set_error(-1, "%s", Mix_GetError());
            return false;
        }
    }
    return true;
}

float funkout_audio_get_volume(void)
{
    int ret = Mix_Volume(-1, -1);
    float f = (float)ret / MIX_MAX_VOLUME;
    if (f > 1.0)
        f = 1.0;
    else if (f < 0)
        f = 0;
    return f;
}

void funkout_audio_set_volume(float volume)
{
    if (volume > 1.0)
        volume = 1.0;
    else if (volume < 0)
        volume = 0;
    Mix_Volume(-1, volume * MIX_MAX_VOLUME);
}

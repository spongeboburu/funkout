#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "funkout/game.h"
#include "funkout/collision.h"
#include "cjson/cJSON.h"

#define FUNKOUT_GAME_LETHAL_COLLISION_BENEFIT 0.9
#define FUNKOUT_GAME_EVENT_QUEUE_SIZE 1024

const char *FUNKOUT_BLOCK_TYPE_STRINGS[MAX_FUNKOUT_BLOCK_TYPE] = {
    "none",
    "normal",
    "spawn"
};

const char *FUNKOUT_DROP_TYPE_STRINGS[MAX_FUNKOUT_DROP_TYPE] = {
    "none",
    "multiply",
    "sticky",
    "life",
    "deat"
};

const char *FUNKOUT_ENEMY_APPEARANCE_STRINGS[MAX_FUNKOUT_ENEMY_APPEARANCE] = {
    "none",
    "amoeba",
    "virus",
    "sata",
    "satb",
    "satc",
    "satd",
    "plague",
    "tank"
};


const char *FUNKOUT_WEAPON_TYPE_STRINGS[MAX_FUNKOUT_WEAPON_TYPE] = {
    "none",
    "straight",
    "beam"
};
        

struct BlockCollision {
    float tau;
    struct funkout_Vector mtv;
    struct funkout_Vector normal;
    struct funkout_Vector pos;
    int block_x;
    int block_y;
};

static bool funkout_game_push_event(struct funkout_Game * game, const struct funkout_GameEvent * event)
{
    if (game->num_events < FUNKOUT_GAME_EVENT_QUEUE_SIZE) {
        int index = (game->event_index + game->num_events++) % FUNKOUT_GAME_EVENT_QUEUE_SIZE;
        game->event_queue[index] = *event;
        return true;
    }
    return false;
}

static void funkout_game_set_state(struct funkout_Game * game, enum funkout_GameState state)
{
    if (game->state != state) {
        struct funkout_GameEvent e = {
            FUNKOUT_GAME_EVENT_STATE_CHANGED,
            {.state_changed={game->state, state}}
        };
        funkout_game_push_event(game, &e);
        if (state == FUNKOUT_GAME_STATE_START)
            game->start_cooldown = FUNKOUT_GAME_START_COOLDOWN;
        else if (state == FUNKOUT_GAME_STATE_END)
            game->end_cooldown = FUNKOUT_GAME_END_COOLDOWN;
        game->state = state;
    }
}

static void funkout_player_free(struct funkout_Player * player)
{
    if (player->balls != NULL)
        free(player->balls);
    free(player);
}

// static int funkout_game_get_closest_player_target(const struct funkout_Game * game,
//                                                   float pos_x, float pos_y,
//                                                   struct funkout_Vector *ret_normal,
//                                                   float *ret_distance)
// {
//     struct funkout_Vector normal = {0, 1};
//     float distance = -1;
//     int index = -1;
//     for (int pindex = 0; pindex < FUNKOUT_MAX_PLAYERS; pindex++) {
//         if (game->players[pindex] != NULL) {
//             struct funkout_Vector pdt = {game->players[pindex]->pos.x - pos_x,
//                                          game->players[pindex]->pos.y - pos_y};
//             float l = sqrtf(pdt.x * pdt.x + pdt.y * pdt.y);
//             if (distance < 0 || l < distance) {
//                 distance = l;
//                 index = pindex;
//                 if (l > 0) {
//                     normal.x = pdt.x / l;
//                     normal.y = pdt.y / l;
//                 } else {
//                     normal.x = 0;
//                     normal.y = 1;
//                 }
//             }
//         }
//     }
//     if (ret_normal != NULL)
//         *ret_normal = normal;
//     if (ret_distance != NULL)
//         *ret_distance = distance;
//     return index;
// }

static int funkout_game_get_random_player_target(const struct funkout_Game * game)
{
    int prand = rand() % FUNKOUT_MAX_PLAYERS;
    for (int pindex = 0; pindex < FUNKOUT_MAX_PLAYERS; pindex++) {
        int index = (pindex + prand) % FUNKOUT_MAX_PLAYERS;
        if (game->players[index] != NULL)
            return index;
    }
    return -1;
}

static bool funkout_ball_block_collision(const struct funkout_Game * game, const struct funkout_Ball * ball,
                                         const struct funkout_Vector * delta,
                                         struct BlockCollision * ret_collision)
{
    struct funkout_Vector use_delta = {0};
    if (delta != NULL)
        use_delta = *delta;
    struct funkout_Shape ball_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={ball->pos, ball->radius}}};
    int num = 0;
    float tau;
    struct funkout_Vector mtv;
    struct funkout_Vector normal;
    struct BlockCollision collisions[4];
    int left, right, top, bottom;
    if (use_delta.x < 0) {
        left = ball->pos.x + use_delta.x - ball->radius - 1;
        right = ball->pos.x + ball->radius + 1;
    } else {
        left = ball->pos.x - ball->radius - 1;
        right = ball->pos.x + use_delta.x + ball->radius + 1;
    }
    if (use_delta.y < 0) {
        top = ball->pos.y + use_delta.y - ball->radius - 1;
        bottom = ball->pos.y + ball->radius + 1;
    } else {
        top = ball->pos.y - ball->radius - 1;
        bottom = ball->pos.y + use_delta.y + ball->radius + 1;
    }
    if (top < 0)
        top = 0;
    if (left < 0)
        left = 0;
    for (int dy = top; num < 4 && dy <= bottom && dy < FUNKOUT_GAME_HEIGHT; dy++) {
        for (int dx = left; num < 4 && dx <= right && dx < FUNKOUT_GAME_WIDTH; dx++) {
            if (game->blocks[dy][dx].definition.type > FUNKOUT_BLOCK_TYPE_NONE
                && game->blocks[dy][dx].definition.type < MAX_FUNKOUT_BLOCK_TYPE
                && game->blocks[dy][dx].definition.type != FUNKOUT_BLOCK_TYPE_SPAWN) {
                struct funkout_Shape aabb_shape = {FUNKOUT_SHAPE_AABB, {.aabb={{dx+0.5,dy+0.5}, {0.5, 0.5}}}};
                if (funkout_shape_collision(&ball_shape,
                                            use_delta.x, use_delta.y,
                                            &aabb_shape, 0, 0,
                                            &tau,
                                            &mtv,
                                            &normal)) {
                    collisions[num].tau = tau;
                    collisions[num].mtv = mtv;
                    collisions[num].normal = normal;
                    collisions[num].pos.x = ball->pos.x + use_delta.x * tau + mtv.x;
                    collisions[num].pos.y = ball->pos.y + use_delta.y * tau + mtv.y;
                    collisions[num].block_x = dx;
                    collisions[num].block_y = dy;
                    num++;
                }
            }
        }
    }
    if (num > 0) {
        struct BlockCollision col = collisions[0];
        for (int i = 1; i < num; i++) 
            if (collisions[i].tau < col.tau)
                col = collisions[i];
        if (ret_collision != NULL)
            *ret_collision = col;
        return true;
    }
    return false;
}

static bool funkout_ball_player_collision(const struct funkout_Player * player, const struct funkout_Ball * ball,
                                          const struct funkout_Vector * delta,
                                          struct BlockCollision * ret_collision)
{
    struct funkout_Vector use_delta = {0};
    if (delta != NULL)
        use_delta = *delta;
    struct funkout_Shape ball_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={ball->pos, ball->radius}}};
    struct funkout_Shape player_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={player->pos, player->radius}}};
    float tau;
    struct funkout_Vector mtv;
    struct funkout_Vector normal;
    if (funkout_shape_collision(&ball_shape,
                                use_delta.x, use_delta.y,
                                &player_shape, 0, 0,
                                &tau,
                                &mtv,
                                &normal)) {
        if (ret_collision != NULL) {
            ret_collision->tau = tau;
            ret_collision->mtv = mtv;
            ret_collision->normal = normal;
            ret_collision->pos.x = ball->pos.x + use_delta.x * tau + mtv.x;
            ret_collision->pos.y = ball->pos.y + use_delta.y * tau + mtv.y;
            ret_collision->block_x = -1;
            ret_collision->block_y = -1;
        }
        return true;
    }
    return false;
}

static bool funkout_player_block_collision(const struct funkout_Game * game, const struct funkout_Player * player,
                                           const struct funkout_Vector * delta,
                                           struct BlockCollision * ret_collision)
{
    struct funkout_Vector use_delta = {0};
    if (delta != NULL)
        use_delta = *delta;
    struct funkout_Shape ball_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={player->pos, player->radius}}};
    int num = 0;
    float tau;
    struct funkout_Vector mtv;
    struct funkout_Vector normal;
    struct BlockCollision collisions[4];
    int left, right, top, bottom;
    if (use_delta.x < 0) {
        left = player->pos.x + use_delta.x - player->radius - 1;
        right = player->pos.x + player->radius + 1;
    } else {
        left = player->pos.x - player->radius - 1;
        right = player->pos.x + use_delta.x + player->radius + 1;
    }
    if (use_delta.y < 0) {
        top = player->pos.y + use_delta.y - player->radius - 1;
        bottom = player->pos.y + player->radius + 1;
    } else {
        top = player->pos.y - player->radius - 1;
        bottom = player->pos.y + use_delta.y + player->radius + 1;
    }
    if (top < 0)
        top = 0;
    if (left < 0)
        left = 0;
    for (int dy = top; num < 4 && dy <= bottom && dy < FUNKOUT_GAME_HEIGHT; dy++) {
        for (int dx = left; num < 4 && dx <= right && dx < FUNKOUT_GAME_WIDTH; dx++) {
            if (game->blocks[dy][dx].definition.type > FUNKOUT_BLOCK_TYPE_NONE
                && game->blocks[dy][dx].definition.type < MAX_FUNKOUT_BLOCK_TYPE
                && game->blocks[dy][dx].definition.type != FUNKOUT_BLOCK_TYPE_SPAWN) {
                struct funkout_Shape aabb_shape = {FUNKOUT_SHAPE_AABB, {.aabb={{dx+0.5,dy+0.5}, {0.5, 0.5}}}};
                if (funkout_shape_collision(&ball_shape,
                                            use_delta.x, use_delta.y,
                                            &aabb_shape, 0, 0,
                                            &tau,
                                            &mtv,
                                            &normal)) {
                    collisions[num].tau = tau;
                    collisions[num].mtv = mtv;
                    collisions[num].normal = normal;
                    collisions[num].pos.x = player->pos.x + use_delta.x * tau + mtv.x;
                    collisions[num].pos.y = player->pos.y + use_delta.y * tau + mtv.y;
                    collisions[num].block_x = dx;
                    collisions[num].block_y = dy;
                    num++;
                }
            }
        }
    }
    if (num > 0) {
        struct BlockCollision col = collisions[0];
        for (int i = 1; i < num; i++) 
            if (collisions[i].tau < col.tau)
                col = collisions[i];
        if (ret_collision != NULL)
            *ret_collision = col;
        return true;
    }
    return false;
}

static bool funkout_game_player_drop_collision(const struct funkout_Player * player, const struct funkout_Drop * drop, float delta_y)
{
    struct funkout_Shape drop_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={drop->pos, 0.45}}};
    struct funkout_Shape player_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={player->pos, player->radius}}};
    return funkout_shape_collision(&drop_shape, 0, delta_y, &player_shape, 0, 0, NULL, NULL, NULL);
}

static bool funkout_enemy_block_collision(const struct funkout_Game * game, const struct funkout_Enemy * enemy,
                                          const struct funkout_Vector * delta,
                                          struct BlockCollision * ret_collision)
{
    struct funkout_Vector use_delta = {0};
    if (delta != NULL)
        use_delta = *delta;
    float radius = enemy->definition.radius * FUNKOUT_GAME_LETHAL_COLLISION_BENEFIT;
    struct funkout_Shape enemy_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={enemy->pos, radius}}};
    int num = 0;
    float tau;
    struct funkout_Vector mtv;
    struct funkout_Vector normal;
    struct BlockCollision collisions[4];
    int left, right, top, bottom;
    if (use_delta.x < 0) {
        left = enemy->pos.x + use_delta.x - radius - 1;
        right = enemy->pos.x + radius + 1;
    } else {
        left = enemy->pos.x - radius - 1;
        right = enemy->pos.x + use_delta.x + radius + 1;
    }
    if (use_delta.y < 0) {
        top = enemy->pos.y + use_delta.y - radius - 1;
        bottom = enemy->pos.y + radius + 1;
    } else {
        top = enemy->pos.y - radius - 1;
        bottom = enemy->pos.y + use_delta.y + radius + 1;
    }
    if (top < 0)
        top = 0;
    if (left < 0)
        left = 0;
    for (int dy = top; num < 4 && dy <= bottom && dy < FUNKOUT_GAME_HEIGHT; dy++) {
        for (int dx = left; num < 4 && dx <= right && dx < FUNKOUT_GAME_WIDTH; dx++) {
            if (game->blocks[dy][dx].definition.type > FUNKOUT_BLOCK_TYPE_NONE
                && game->blocks[dy][dx].definition.type < MAX_FUNKOUT_BLOCK_TYPE
                && game->blocks[dy][dx].definition.type != FUNKOUT_BLOCK_TYPE_SPAWN) {
                struct funkout_Shape aabb_shape = {FUNKOUT_SHAPE_AABB, {.aabb={{dx+0.5,dy+0.5}, {0.5, 0.5}}}};
                if (funkout_shape_collision(&enemy_shape,
                                            use_delta.x, use_delta.y,
                                            &aabb_shape, 0, 0,
                                            &tau,
                                            &mtv,
                                            &normal)) {
                    collisions[num].tau = tau;
                    collisions[num].mtv = mtv;
                    collisions[num].normal = normal;
                    collisions[num].pos.x = enemy->pos.x + use_delta.x * tau + mtv.x;
                    collisions[num].pos.y = enemy->pos.y + use_delta.y * tau + mtv.y;
                    collisions[num].block_x = dx;
                    collisions[num].block_y = dy;
                    num++;
                }
            }
        }
    }
    if (num > 0) {
        struct BlockCollision col = collisions[0];
        for (int i = 1; i < num; i++) 
            if (collisions[i].tau < col.tau)
                col = collisions[i];
        if (ret_collision != NULL)
            *ret_collision = col;
        return true;
    }
    return false;
}

static bool funkout_enemy_player_collision(const struct funkout_Player * player, const struct funkout_Enemy * enemy,
                                           const struct funkout_Vector * delta,
                                           struct BlockCollision * ret_collision)
{
    struct funkout_Vector use_delta = {0};
    if (delta != NULL)
        use_delta = *delta;
    float radius = enemy->definition.radius * FUNKOUT_GAME_LETHAL_COLLISION_BENEFIT;
    struct funkout_Shape enemy_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={enemy->pos, radius}}};
    struct funkout_Shape player_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={player->pos, player->radius}}};
    float tau;
    struct funkout_Vector mtv;
    struct funkout_Vector normal;
    if (funkout_shape_collision(&enemy_shape,
                                use_delta.x, use_delta.y,
                                &player_shape, 0, 0,
                                &tau,
                                &mtv,
                                &normal)) {
        if (ret_collision != NULL) {
            ret_collision->tau = tau;
            ret_collision->mtv = mtv;
            ret_collision->normal = normal;
            ret_collision->pos.x = enemy->pos.x + use_delta.x * tau + mtv.x;
            ret_collision->pos.y = enemy->pos.y + use_delta.y * tau + mtv.y;
            ret_collision->block_x = -1;
            ret_collision->block_y = -1;
        }
        return true;
    }
    return false;
}

static bool funkout_ball_enemy_collision(const struct funkout_Enemy * enemy, const struct funkout_Ball * ball,
                                          const struct funkout_Vector * delta,
                                          struct BlockCollision * ret_collision)
{
    struct funkout_Vector use_delta = {0};
    if (delta != NULL)
        use_delta = *delta;
    float radius = enemy->definition.radius * FUNKOUT_GAME_LETHAL_COLLISION_BENEFIT;
    struct funkout_Shape ball_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={ball->pos, ball->radius}}};
    struct funkout_Shape enemy_shape = {FUNKOUT_SHAPE_CIRCLE, {.circle={enemy->pos, radius}}};
    float tau;
    struct funkout_Vector mtv;
    struct funkout_Vector normal;
    if (funkout_shape_collision(&ball_shape,
                                use_delta.x, use_delta.y,
                                &enemy_shape, 0, 0,
                                &tau,
                                &mtv,
                                &normal)) {
        if (ret_collision != NULL) {
            ret_collision->tau = tau;
            ret_collision->mtv = mtv;
            ret_collision->normal = normal;
            ret_collision->pos.x = ball->pos.x + use_delta.x * tau + mtv.x;
            ret_collision->pos.y = ball->pos.y + use_delta.y * tau + mtv.y;
            ret_collision->block_x = -1;
            ret_collision->block_y = -1;
        }
        return true;
    }
    return false;
}












void funkout_game_init(struct funkout_Game * game,
                       const char * name,
                       enum funkout_GameDifficulty difficulty,
                       const struct funkout_Level * level)
{
    memset(game, 0, sizeof(struct funkout_Game));
    snprintf(game->name, sizeof(game->name), "%s", name != NULL ? name : "");
    game->difficulty = MINMAX(difficulty, FUNKOUT_GAME_DIFFICULTY_EASY, MAX_FUNKOUT_GAME_DIFFICULTY - 1);
    game->event_queue = malloc(sizeof(struct funkout_GameEvent) * FUNKOUT_GAME_EVENT_QUEUE_SIZE);
    memset(game->event_queue, 0, sizeof(struct funkout_GameEvent) * FUNKOUT_GAME_EVENT_QUEUE_SIZE);
}

void funkout_game_deinit(struct funkout_Game * game)
{
    for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++)
        if (game->players[i] != NULL)
            funkout_player_free(game->players[i]);
    if (game->enemies != NULL) 
        free(game->enemies);
    if (game->projectiles != NULL) 
        free(game->projectiles);
    if (game->drops != NULL) 
        free(game->drops);
    if (game->event_queue != NULL)
        free(game->event_queue);
}

enum funkout_GameState funkout_game_get_state(
    const struct funkout_Game * game)
{
    return game->state;
}

void funkout_game_set_level(struct funkout_Game * game,
                            const struct funkout_Level * level)
{
    if (level != NULL) 
        memcpy(&game->level, level, sizeof(struct funkout_Level));
    else 
        memset(&game->level, 0, sizeof(struct funkout_Level));
    game->max_level_score = funkout_level_get_max_score(&game->level);
    funkout_game_reset_level(game);
}

void funkout_game_reset_level(struct funkout_Game * game)
{
    for (int y = 0; y < FUNKOUT_GAME_HEIGHT; y++) {
        for (int x = 0; x < FUNKOUT_GAME_WIDTH; x++) {
            game->blocks[y][x].definition = game->level.block_defs[y][x];
            game->blocks[y][x].hitpoints = game->blocks[y][x].definition.hitpoints;
            game->blocks[y][x].spawn_cooldown = game->blocks[y][x].definition.spawn.cooldown_start;
        }
    }
    for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++)
        if (game->players[i] != NULL) {
            if (game->players[i]->balls != NULL) {
                free(game->players[i]->balls);
                game->players[i]->balls = NULL;
                game->players[i]->num_balls = 0;
            }
            game->players[i]->sticky = false;
            game->players[i]->level_score = 0;
            funkout_game_add_ball(game, i, true);
        }
    if (game->enemies != NULL) {
        free(game->enemies);
        game->enemies = NULL;
        game->num_enemies = 0;
    }
    if (game->projectiles != NULL) {
        free(game->projectiles);
        game->projectiles = NULL;
        game->num_projectiles = 0;
    }
    if (game->drops != NULL) {
        free(game->drops);
        game->drops = NULL;
        game->num_drops = 0;
    }
    memset(game->event_queue, 0, sizeof(struct funkout_GameEvent) * FUNKOUT_GAME_EVENT_QUEUE_SIZE);
    game->num_events = 0;
    game->event_index = 0;
    funkout_game_set_state(game, FUNKOUT_GAME_STATE_START);
}

struct funkout_Player * funkout_game_add_player(
    struct funkout_Game * game,
    const char * name)
{
    for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++)
        if (game->players[i] == NULL) {
            game->players[i] = malloc(sizeof(struct funkout_Player));
            memset(game->players[i], 0, sizeof(struct funkout_Player));
            game->players[i]->index = i;
            game->players[i]->radius = FUNKOUT_PLAYER_RADIUS;
            game->players[i]->speed = FUNKOUT_PLAYER_SPEED;
            game->players[i]->pos.x = FUNKOUT_GAME_WIDTH / 2;
            game->players[i]->pos.y = FUNKOUT_GAME_HEIGHT - 2;
            game->players[i]->lives = 2;
            game->players[i]->sticky= true; // FIXME
            struct funkout_GameEvent e = {
                FUNKOUT_GAME_EVENT_PLAYER_ADDED,
                {.player_added={game->players[i]}}
            };
            funkout_game_push_event(game, &e);
            // funkout_game_add_ball(game, i, true);
            return game->players[i];
        }
    return NULL;
}

bool funkout_game_remove_player(
    struct funkout_Game * game,
    const struct funkout_Player * player)
{
    if (player == NULL)
        return false;
    for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++)
        if (game->players[i] == player) {
            struct funkout_GameEvent e = {
                FUNKOUT_GAME_EVENT_PLAYER_REMOVED,
                {.player_removed={game->players[i]->index}}
            };
            snprintf(e.e.player_removed.name, sizeof(e.e.player_removed.name), "%s", game->players[i]->name);
            funkout_player_free(game->players[i]);
            game->players[i] = NULL;
            return true;
        }
    return false;
}

int funkout_game_find_player(
    struct funkout_Game * game,
    const struct funkout_Player * player)
{
    if (player != NULL)
        for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++)
            if (game->players[i] == player)
                return i;
    return -1;
}

void funkout_game_remove_all_players(struct funkout_Game * game)
{
    if (game->players != NULL) {
        for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++) {
            if (game->players[i] != NULL) {
                struct funkout_GameEvent e = {
                    FUNKOUT_GAME_EVENT_PLAYER_REMOVED,
                    {.player_removed={game->players[i]->index}}
                };
                snprintf(e.e.player_removed.name, sizeof(e.e.player_removed.name), "%s", game->players[i]->name);
                funkout_player_free(game->players[i]);
                game->players[i] = NULL;
            }
        }
    }
}

void funkout_game_update(struct funkout_Game * game,
                         float timestep)
{
    if (game->state == FUNKOUT_GAME_STATE_START) {
        game->start_cooldown -= timestep;
        if (game->start_cooldown <= 0)
            funkout_game_set_state(game, FUNKOUT_GAME_STATE_PLAY);
    } else if (game->state == FUNKOUT_GAME_STATE_END) {
        game->end_cooldown -= timestep;
        if (game->end_cooldown <= 0)
            funkout_game_set_state(game, FUNKOUT_GAME_STATE_NEXT);
    } else if (game->state == FUNKOUT_GAME_STATE_PLAY) {
        int block_count = 0;
        for (int by = 0; by < FUNKOUT_GAME_HEIGHT; by++) {
            for (int bx = 0; bx < FUNKOUT_GAME_WIDTH; bx++) {
                if (game->blocks[by][bx].definition.type == FUNKOUT_BLOCK_TYPE_NORMAL
                    && game->blocks[by][bx].definition.hitpoints >= 0) {
                    if (game->blocks[by][bx].hitpoints <= 0)
                        funkout_game_destroy_block(game, bx, by, NULL, NULL, NULL);
                    else
                        block_count++;
                } else if (game->blocks[by][bx].definition.type == FUNKOUT_BLOCK_TYPE_SPAWN
                    && game->blocks[by][bx].spawn_count < game->blocks[by][bx].definition.spawn.max_count) {
                    if (game->blocks[by][bx].current_spawn < game->blocks[by][bx].definition.spawn.max_enemies) {
                        if (game->blocks[by][bx].spawn_cooldown <= 0) {
                            struct funkout_Enemy * enemy = funkout_game_add_enemy(game, bx + 0.5, by + 0.5, &game->blocks[by][bx].definition.spawn.enemy);
                            enemy->spawn_pos[0] = bx;
                            enemy->spawn_pos[1] = by;
                            struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_ENEMY_SPAWNED, {.enemy_spawned={enemy}}};
                            funkout_game_push_event(game, &e);
                            game->blocks[by][bx].current_spawn++;
                            game->blocks[by][bx].spawn_count++;
                            game->blocks[by][bx].spawn_cooldown = game->blocks[by][bx].definition.spawn.cooldown;
                        } else
                            game->blocks[by][bx].spawn_cooldown -= timestep;
                    }
                }
            }
        }
        if (block_count == 0) {
            funkout_game_set_state(game, FUNKOUT_GAME_STATE_END);
            return;
        }
        int new_drops_len = game->num_drops;
        for (int i = 0; i < new_drops_len; i++) {
            bool picked_up = false;
            for (int j = 0; !picked_up && j < FUNKOUT_MAX_PLAYERS; j++) {
                if (game->players[j] != NULL
                    && !game->players[j]->immortal
                    && game->players[j]->destroyed_cooldown <= 0
                    && game->players[j]->respawn_cooldown <= 0
                    && funkout_game_player_drop_collision(game->players[j], game->drops + i, FUNKOUT_GAME_DROP_SPEED * timestep)) {
                    struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_DROP_PICKUP, {.drop_pickup={game->drops[i], j}}};
                    funkout_game_push_event(game, &e);
                    switch (game->drops[i].type) {
                    case FUNKOUT_DROP_TYPE_MULTIPLY:
                        funkout_game_add_ball(game, j, true);
                        break;
                    case FUNKOUT_DROP_TYPE_STICKY:
                        game->players[j]->sticky = true;
                        break;
                    case FUNKOUT_DROP_TYPE_LIFE:
                    {
                        struct funkout_GameEvent el = {FUNKOUT_GAME_EVENT_EXTRA_LIFE, {.extra_life={j}}};
                        funkout_game_push_event(game, &el);
                        game->players[j]->lives++;
                        break;
                    }
                    case FUNKOUT_DROP_TYPE_DEATH:
                        if (!game->players[j]->immortal)
                            funkout_game_destroy_player(game, j);
                        break;
                    default:
                        break;
                    }
                    picked_up = true;
                }
            }
            if (!picked_up)
                game->drops[i].pos.y += FUNKOUT_GAME_DROP_SPEED * timestep;
            if (picked_up || game->drops[i].pos.y >= FUNKOUT_GAME_HEIGHT + 1) {
                if (new_drops_len > 1) {
                    if (i < new_drops_len - 1)
                        memmove(game->drops + i, game->drops + i + 1, sizeof(struct funkout_Drop) * (new_drops_len - i - 1));
                    i--;
                    struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_DROP_DESTROYED, {.drop_destroyed={game->drops[i]}}};
                    funkout_game_push_event(game, &e);
                }
                new_drops_len--;
            }
        }
        if (new_drops_len != game->num_drops) {
            if (new_drops_len > 0) 
                game->drops = realloc(game->drops, sizeof(struct funkout_Drop) * new_drops_len);
            else {
                free(game->drops);
                game->drops = NULL;
            }
            game->num_drops = new_drops_len;
        }
        int player_count = 0;
        for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++) {
            struct funkout_Player * player = game->players[i];
            if (player == NULL)
                continue;
            if (player->lives < 0) {
                continue;
            }
            player_count++;
            if (player->destroyed_cooldown > 0) {
                player->destroyed_cooldown -= timestep;
                if (player->destroyed_cooldown <= 0) {
                    player->respawn_cooldown = FUNKOUT_GAME_RESPAWN_COOLDOWN;
                    struct funkout_GameEvent event = {FUNKOUT_GAME_EVENT_PLAYER_SPAWNED, {.player_spawned={player}}};
                    funkout_game_push_event(game, &event);
                    player->immortal = true;
                    player->pos.y = FUNKOUT_GAME_HEIGHT + player->radius;
                }
                continue;
            }
            if (player->respawn_cooldown > 0) {
                player->pos.y = FUNKOUT_GAME_HEIGHT - 2;
                player->pos.y += (2.0 + player->radius) * (player->respawn_cooldown / FUNKOUT_GAME_RESPAWN_COOLDOWN);
                player->respawn_cooldown -= timestep;
                if (player->respawn_cooldown <= 0)
                    funkout_game_add_ball(game, i, true);
                continue;
            }
            if (player->num_balls <= 0) {
                funkout_game_destroy_player(game, i);
                continue;
            }
            struct funkout_Vector player_delta = {0};
            if (player->controls.horizontal < 0)
                player_delta.x -= player->speed * timestep;
            else if (player->controls.horizontal > 0)
                player_delta.x += player->speed * timestep;
            if (player->controls.vertical < 0)
                player_delta.y -= player->speed * timestep;
            else if (player->controls.vertical > 0)
                player_delta.y += player->speed * timestep;
            struct BlockCollision player_block_collision;
            if (funkout_player_block_collision(game, player, &player_delta, &player_block_collision)) {
                player->pos.x += player_block_collision.mtv.x;
                player->pos.y += player_block_collision.mtv.y;
                if (fabsf(player_block_collision.normal.x) >= fabsf(player_block_collision.normal.y))
                    player_delta.x = 0;
                if (fabsf(player_block_collision.normal.x) <= fabsf(player_block_collision.normal.y))
                    player_delta.y = 0;
            }
            player->pos.x += player_delta.x;
            player->pos.y += player_delta.y;
            if (player->pos.x < player->radius)
                player->pos.x = player->radius;
            else if (player->pos.x > FUNKOUT_GAME_WIDTH - player->radius)
                player->pos.x = FUNKOUT_GAME_WIDTH - player->radius;
            if (player->pos.y < FUNKOUT_GAME_HEIGHT - 4)
                player->pos.y = FUNKOUT_GAME_HEIGHT - 4;
            else if (player->pos.y > FUNKOUT_GAME_HEIGHT)
                player->pos.y = FUNKOUT_GAME_HEIGHT;
            for (int j = 0; j < player->num_balls; j++) {
                struct funkout_Ball * ball = player->balls + j;
                if (ball->remove_me)
                    continue;
                if (player->controls.shoot) {
                    ball->sticky = false;
                    player->immortal = false;
                }
                if (!ball->sticky && (ball->pos.x < -ball->radius
                                      || ball->pos.x > FUNKOUT_GAME_WIDTH + ball->radius
                                      || ball->pos.y < -ball->radius
                                      || ball->pos.y > FUNKOUT_GAME_HEIGHT + ball->radius)
                    ) {
                    funkout_game_remove_ball(game, i, j);
                    j--;
                    continue;
                }
                funkout_vector_normalize(ball->direction.x, ball->direction.y, &ball->direction);
                struct funkout_Vector ball_delta = {ball->direction.x * ball->speed * timestep,
                                                    ball->direction.y * ball->speed * timestep};
                if (ball->direction.x == 0 && ball->direction.y == 0)
                    ball->direction.y = -1;
                struct BlockCollision block_collision = {0}, player_collision = {0};
                if (ball->sticky) {
                    ball->pos.x = player->pos.x + ball->direction.x * (player->radius + ball->radius);
                    ball->pos.y = player->pos.y + ball->direction.y * (player->radius + ball->radius);
                    if (funkout_ball_block_collision(game, ball, NULL, &block_collision)) {
                        struct funkout_Vector nn = {player->pos.x - (block_collision.block_x + 0.5),
                                                    player->pos.y - (block_collision.block_y + 0.5)};
                        if (fabsf(nn.x) >= fabsf(nn.y)) 
                            ball->pos.x = block_collision.block_x + (nn.x < 0 ? -ball->radius : 1.0 + ball->radius);
                        else if (fabsf(nn.x) <= fabsf(nn.y)) 
                            ball->pos.y = block_collision.block_y + (nn.y < 0 ? -ball->radius : 1.0 + ball->radius);
                    }
                } // Ball vs player
                else if (funkout_ball_player_collision(player, player->balls + j, &ball_delta, &player_collision)) {
                    ball->direction.x = player_collision.normal.x;
                    ball->direction.y = player_collision.normal.y;
                    if (player->sticky && !player->controls.shoot) {
                        ball->sticky = true;
                        ball->pos.x = player->pos.x + ball->direction.x * (player->radius + ball->radius);
                        ball->pos.y = player->pos.y + ball->direction.y * (player->radius + ball->radius);
                    } else {
                        ball->pos.x = player_collision.pos.x + ball->direction.x * (1.05 - player_collision.tau) * timestep;
                        ball->pos.y = player_collision.pos.y + ball->direction.y * (1.05 - player_collision.tau) * timestep;
                    }
                    if (funkout_ball_block_collision(game, ball, NULL, &block_collision)) {
                        struct funkout_Vector nn = {player->pos.x - (block_collision.block_x + 0.5),
                                                    player->pos.y - (block_collision.block_y + 0.5)};
                        if (fabsf(nn.x) >= fabsf(nn.y)) 
                            ball->pos.x = block_collision.block_x + (nn.x < 0 ? -ball->radius : 1.0 + ball->radius);
                        else if (fabsf(nn.x) <= fabsf(nn.y)) 
                            ball->pos.y = block_collision.block_y + (nn.y < 0 ? -ball->radius : 1.0 + ball->radius);
                    } else {
                        struct funkout_Vector dt = {-player_collision.normal.x, -player_collision.normal.y};
                        dt.x *= ball->radius;
                        dt.y *= ball->radius;
                        dt.x += player_collision.pos.x;
                        dt.y += player_collision.pos.y;
                        struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_BALL_BOUNCED,
                                                      {.ball_bounced={ball, dt, player_collision.normal, player, NULL, -1, -1}}};
                        funkout_game_push_event(game, &e);
                    }
                    ball->score_accumulator = 0;
                }
                // Ball vs block
                else if (funkout_ball_block_collision(game, player->balls + j, &ball_delta, &block_collision)) {
                    struct funkout_Vector dt = {-block_collision.normal.x, -block_collision.normal.y};
                    dt.x *= ball->radius;
                    dt.y *= ball->radius;
                    dt.x += block_collision.pos.x;
                    dt.y += block_collision.pos.y;
                    struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_BALL_BOUNCED,
                                                  {.ball_bounced={ball, dt, player_collision.normal, NULL, NULL, block_collision.block_x, block_collision.block_y}}};
                    funkout_game_push_event(game, &e);
                    struct funkout_Block * block = &game->blocks[block_collision.block_y][block_collision.block_x];
                    if (block->hitpoints > 0) {
                        block->hitpoints--;
                        if (block->hitpoints == 0) {
                            funkout_game_destroy_block(game, block_collision.block_x, block_collision.block_y, ball, &block_collision.pos, &block_collision.normal);
                        } else {
                            struct funkout_GameEvent bde = {
                                FUNKOUT_GAME_EVENT_BLOCK_DAMAGED,
                                {.block_damaged={block_collision.block_x,
                                                 block_collision.block_y,
                                                 ball,
                                                 block_collision.pos,
                                                 block_collision.normal}}
                            };
                            funkout_game_push_event(game, &bde);
                        }
                        ball->score_accumulator++;
                        if (ball->score_accumulator > FUNKOUT_GAME_MAX_SCORE_ACCUMULATOR)
                            ball->score_accumulator = FUNKOUT_GAME_MAX_SCORE_ACCUMULATOR;
                        ball->player->score += ball->score_accumulator;
                        ball->player->level_score += ball->score_accumulator;
                        struct funkout_GameEvent se = {
                            FUNKOUT_GAME_EVENT_SCORE,
                            {.score={ball->player,
                                     ball->score_accumulator,
                                     {block_collision.block_x + 0.5,
                                      block_collision.block_y + 0.5}}}
                        };
                        funkout_game_push_event(game, &se);
                    }
                    float dot = (ball->direction.x * block_collision.normal.x
                                 + ball->direction.y * block_collision.normal.y);
                    struct funkout_Vector newdir = {ball->direction.x - 2 * block_collision.normal.x * dot,
                                                    ball->direction.y - 2 * block_collision.normal.y * dot};
                    funkout_vector_normalize(newdir.x, newdir.y, &newdir);
                    if (newdir.x == 0 && newdir.y == 0)
                        newdir.y = -1;
                    ball->direction = newdir;
                    float dl = sqrtf(ball_delta.x * ball_delta.x + ball_delta.y * ball_delta.y);
                    ball->pos.x = block_collision.pos.x + newdir.x * dl * (1.05 - block_collision.tau) * timestep;
                    ball->pos.y = block_collision.pos.y + newdir.y * dl * (1.05 - block_collision.tau) * timestep;
                } // end ball block collision
                else {
                    ball->pos.x += ball_delta.x;
                    ball->pos.y += ball_delta.y;
                }
            }
        }
        if (player_count == 0) {
            funkout_game_set_state(game, FUNKOUT_GAME_STATE_OVER);
            return;
        }
        for (int i = 0; i < game->num_enemies; i++) {
            struct funkout_Enemy * enemy = game->enemies + i;
            bool random_movement = false;
            enemy->timestep += timestep;
            if (enemy->timestep > 1.0) {
                random_movement = (rand() % 100) < (int)(100 * enemy->definition.randomness);
                enemy->timestep = fmodf(enemy->timestep, 1.0);
            }
            while (random_movement || (enemy->direction.x == 0 && enemy->direction.y == 0)) {
                enemy->direction.x = (rand() % 3) - 1;
                enemy->direction.y = (rand() % 3) - 1;
                funkout_vector_normalize(enemy->direction.x, enemy->direction.y, &enemy->direction);
                random_movement = false;
            }
            struct funkout_Vector enemy_delta = {enemy->direction.x * enemy->definition.speed * timestep,
                                                 enemy->direction.y * enemy->definition.speed * timestep};
            struct BlockCollision enemy_collision;
            // Enemy block collision
            if (!enemy->definition.ghost_blocks
                && funkout_enemy_block_collision(game, enemy, &enemy_delta, &enemy_collision)) {
                enemy->pos = enemy_collision.pos;
                if (fabsf(enemy_collision.normal.x) >= fabsf(enemy_collision.normal.y)) {
                    enemy_delta.x = 0;
                    enemy->direction.x = enemy_collision.normal.x > 0 ? 1 : -1;
                }
                if (fabsf(enemy_collision.normal.x) <= fabsf(enemy_collision.normal.y)) {
                    enemy_delta.y = 0;
                    enemy->direction.y = enemy_collision.normal.y > 0 ? 1 : -1;
                }
                if (enemy_delta.x == 0 && enemy_delta.y == 0) {
                    if (enemy->direction.x == 0)
                        enemy->direction.x = (rand() % 2) ? 1 : -1;
                    if (enemy->direction.y == 0)
                        enemy->direction.y = (rand() % 2) ? 1 : -1;
                    funkout_vector_normalize(enemy->direction.x, enemy->direction.y, &enemy->direction);
                }
            }
            // Move fire and what not
            if (enemy->stun_cooldown > 0) {
                enemy->stun_cooldown -= timestep;
            } else if (enemy->definition.weapon.type > FUNKOUT_WEAPON_TYPE_NONE
                       && enemy->definition.weapon.type < MAX_FUNKOUT_WEAPON_TYPE
                       && enemy->weapon_cooldown <= 0) {
                enemy->firing_cooldown -= timestep;
                if (enemy->firing_cooldown <= 0) {
                    struct funkout_Vector projnorm = {0, 1};
                    if (enemy->definition.aim) {
                        struct funkout_Vector pdiff = {enemy->target.x - enemy->pos.x, enemy->target.y - enemy->pos.y};
                        funkout_vector_normalize(pdiff.x, pdiff.y, &projnorm);
                    }
                    funkout_game_add_projectile(game, enemy->pos.x, enemy->pos.y, projnorm.x, projnorm.y, &enemy->definition.weapon, enemy);
                    enemy->weapon_cooldown = enemy->definition.weapon.cooldown;
                    enemy->weapon_cooldown += (rand() % 1000) * enemy->definition.weapon.cooldown_random / 1000.0;
                    enemy->firing_cooldown = enemy->definition.weapon.firing_cooldown;
                    enemy->fired_cooldown = enemy->definition.weapon.fired_cooldown;
                }
            } else {
                if (enemy->weapon_cooldown > 0) {
                    enemy->weapon_cooldown -= timestep;
                    if (enemy->weapon_cooldown <= 0 && enemy->definition.aim) {
                        int pindex = funkout_game_get_random_player_target(game);
                        if (pindex >= 0)
                            enemy->target = game->players[pindex]->pos;
                    }
                }
                if (enemy->fired_cooldown <= 0) {
                    enemy->pos.x += enemy_delta.x;
                    enemy->pos.y += enemy_delta.y;
                } else 
                    enemy->fired_cooldown -= timestep;
            }
            if (enemy->pos.x < enemy->definition.radius) {
                enemy->pos.x = enemy->definition.radius;
                enemy->direction.x = 1.0;
                enemy->direction.y = (rand() % 3) - 1;
                funkout_vector_normalize(enemy->direction.x, enemy->direction.y, &enemy->direction);
            } else if (enemy->pos.x > FUNKOUT_GAME_WIDTH - enemy->definition.radius) {
                enemy->pos.x = FUNKOUT_GAME_WIDTH - enemy->definition.radius;
                enemy->direction.x = -1.0;
                enemy->direction.y = (rand() % 3) - 1;
                funkout_vector_normalize(enemy->direction.x, enemy->direction.y, &enemy->direction);
            }
            if (enemy->pos.y < enemy->definition.radius) {
                enemy->pos.y = enemy->definition.radius;
                enemy->direction.x = (rand() % 3) - 1;
                enemy->direction.y = 1.0;
                funkout_vector_normalize(enemy->direction.x, enemy->direction.y, &enemy->direction);
            } else if (enemy->pos.y > FUNKOUT_GAME_HEIGHT - enemy->definition.radius) {
                enemy->pos.y = FUNKOUT_GAME_HEIGHT - enemy->definition.radius;
                enemy->direction.x = (rand() % 3) - 1;
                enemy->direction.y = -1.0;
                funkout_vector_normalize(enemy->direction.x, enemy->direction.y, &enemy->direction);
            }
            for (int j = 0; j < FUNKOUT_MAX_PLAYERS; j++) {
                if (game->players[j] == NULL
                    || game->players[j]->immortal
                    || game->players[j]->destroyed_cooldown > 0
                    || game->players[j]->respawn_cooldown > 0)
                    continue;
                struct BlockCollision enemy_player_collision;
                if (funkout_enemy_player_collision(game->players[j], enemy, NULL, &enemy_player_collision))
                    funkout_game_destroy_player(game, j);
                struct BlockCollision enemy_ball_collision;
                for (int k = 0; k < game->players[j]->num_balls; k++) {
                    struct funkout_Ball * ball = game->players[j]->balls + k;
                    if (funkout_ball_enemy_collision(enemy, ball, NULL, &enemy_ball_collision)) {
                        if (!ball->sticky) {
                            ball->direction.x = enemy_ball_collision.normal.x;
                            ball->direction.y = enemy_ball_collision.normal.y;
                            ball->pos.x = enemy_ball_collision.pos.x + ball->direction.x * (1.05 - enemy_ball_collision.tau) * timestep;
                            ball->pos.y = enemy_ball_collision.pos.y + ball->direction.y * (1.05 - enemy_ball_collision.tau) * timestep;
                            struct BlockCollision block_collision;
                            if (funkout_ball_block_collision(game, ball, NULL, &block_collision)) {
                                struct funkout_Vector nn = {enemy->pos.x - (block_collision.block_x + 0.5),
                                                            enemy->pos.y - (block_collision.block_y + 0.5)};
                                if (fabsf(nn.x) >= fabsf(nn.y)) 
                                    ball->pos.x = block_collision.block_x + (nn.x < 0 ? -ball->radius : 1.0 + ball->radius);
                                else if (fabsf(nn.x) <= fabsf(nn.y)) 
                                    ball->pos.y = block_collision.block_y + (nn.y < 0 ? -ball->radius : 1.0 + ball->radius);
                            } else {
                                struct funkout_Vector dt = {-enemy_ball_collision.normal.x, -enemy_ball_collision.normal.y};
                                dt.x *= ball->radius;
                                dt.y *= ball->radius;
                                dt.x += enemy_ball_collision.pos.x;
                                dt.y += enemy_ball_collision.pos.y;
                                struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_BALL_BOUNCED,
                                                              {.ball_bounced={ball, dt, enemy_ball_collision.normal, NULL, enemy, -1, -1}}};
                                funkout_game_push_event(game, &e);
                            }
                        }
                        // struct funkout_Vector epos = enemy->pos;
                        if (enemy->hitpoints > 0 && enemy->stun_cooldown <= FUNKOUT_GAME_ENEMY_STUN_COOLDOWN / 2) {
                            enemy->hitpoints--;
                            if (enemy->hitpoints == 0) {
                                funkout_game_destroy_enemy(game, i);
                            } else {
                                struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_ENEMY_DAMAGED,
                                                              {.enemy_damaged={enemy, ball,
                                                                               enemy_ball_collision.pos,
                                                                               enemy_ball_collision.normal}}};
                                funkout_game_push_event(game, &e);
                                enemy->stun_cooldown = FUNKOUT_GAME_ENEMY_STUN_COOLDOWN;
                                enemy->weapon_cooldown = enemy->definition.weapon.cooldown;
                                enemy->firing_cooldown = enemy->definition.weapon.firing_cooldown;
                            }
                        }
                    }
                } // End enemy vs balls
            }
        } // end enemy loop
        // Projectiles
        for (int i = 0; i < game->num_projectiles; i++) {
            if (game->projectiles[i].pos.x < -game->projectiles[i].radius
                || game->projectiles[i].pos.x > FUNKOUT_GAME_WIDTH + game->projectiles[i].radius
                || game->projectiles[i].pos.y < -game->projectiles[i].radius
                || game->projectiles[i].pos.y > FUNKOUT_GAME_HEIGHT + game->projectiles[i].radius
                || game->projectiles[i].type <= FUNKOUT_WEAPON_TYPE_NONE
                || game->projectiles[i].type >= MAX_FUNKOUT_WEAPON_TYPE
                || (game->projectiles[i].timestep >= game->projectiles[i].ttl && game->projectiles[i].ttl > 0)) {
                funkout_game_destroy_projectile(game, i);
                i--;
            } else {
                struct funkout_Vector projdelta = {
                    game->projectiles[i].normal.x * game->projectiles[i].speed * timestep,
                    game->projectiles[i].normal.y * game->projectiles[i].speed * timestep
                };
                if (game->projectiles[i].type == FUNKOUT_WEAPON_TYPE_BEAM
                    && game->projectiles[i].enemy != NULL) {
                    game->projectiles[i].length = MAX(FUNKOUT_GAME_HEIGHT, FUNKOUT_GAME_WIDTH);
                    if (game->projectiles[i].enemy->definition.aim) {
                        struct funkout_Vector pdiff = {game->projectiles[i].enemy->target.x - game->projectiles[i].enemy->pos.x,
                                                       game->projectiles[i].enemy->target.y - game->projectiles[i].enemy->pos.y};
                        funkout_vector_normalize(pdiff.x, pdiff.y, &game->projectiles[i].normal);
                    }
                    game->projectiles[i].pos.x = game->projectiles[i].enemy->pos.x;
                    game->projectiles[i].pos.y = game->projectiles[i].enemy->pos.y;
                    projdelta.x = game->projectiles[i].normal.x * game->projectiles[i].length;
                    projdelta.y = game->projectiles[i].normal.y * game->projectiles[i].length;
                }
                game->projectiles[i].length = MAX(FUNKOUT_GAME_HEIGHT, FUNKOUT_GAME_WIDTH);
                struct funkout_Shape projshape;
                projshape.type = FUNKOUT_SHAPE_CIRCLE;
                projshape.s.circle.pos = game->projectiles[i].pos;
                projshape.s.circle.r = game->projectiles[i].radius;
                for (int j = 0; !game->projectiles[i].remove_me && j < FUNKOUT_MAX_PLAYERS; j++) {
                    if (game->players[j] != NULL
                        && !(game->players[j]->immortal || game->players[j]->destroyed_cooldown > 0 || game->players[j]->respawn_cooldown > 0)) {
                        for (int k = 0; !game->projectiles[i].remove_me && k < game->players[j]->num_balls; k++) {
                            struct funkout_Shape ballshape = {FUNKOUT_SHAPE_CIRCLE, {.circle={game->players[j]->balls[k].pos, game->players[j]->balls[k].radius}}};
                            if (funkout_shape_collision(&projshape, projdelta.x, projdelta.y, &ballshape, 0, 0, NULL, NULL, NULL)) {
                                struct funkout_Vector ppdiff = {game->players[j]->balls[k].pos.x - game->projectiles[i].pos.x,
                                                                game->players[j]->balls[k].pos.y - game->projectiles[i].pos.y};
                                float l = sqrtf(ppdiff.x * ppdiff.x + ppdiff.y * ppdiff.y);
                                if (game->projectiles[i].type == FUNKOUT_WEAPON_TYPE_BEAM && l < game->projectiles[i].length) {
                                    game->projectiles[i].length = l;
                                } else if (game->projectiles[i].type == FUNKOUT_WEAPON_TYPE_STRAIGHT)
                                    game->projectiles[i].remove_me = true;
                            }
                        }
                    }
                }
                projshape.s.circle.r *= FUNKOUT_GAME_LETHAL_COLLISION_BENEFIT;
                if (game->projectiles[i].type == FUNKOUT_WEAPON_TYPE_BEAM) {
                    projdelta.x = game->projectiles[i].normal.x * game->projectiles[i].length;
                    projdelta.y = game->projectiles[i].normal.y * game->projectiles[i].length;
                }
                for (int j = 0; !game->projectiles[i].remove_me && j < FUNKOUT_MAX_PLAYERS; j++) {
                    if (game->players[j] != NULL
                        && !(game->players[j]->immortal || game->players[j]->destroyed_cooldown > 0 || game->players[j]->respawn_cooldown > 0)) {
                        struct funkout_Shape pshape = {FUNKOUT_SHAPE_CIRCLE, {.circle={game->players[j]->pos, game->players[j]->radius}}};
                        if (funkout_shape_collision(&projshape, projdelta.x, projdelta.y, &pshape, 0, 0, NULL, NULL, NULL)) {
                            funkout_game_destroy_player(game, j);
                            if (game->projectiles[i].type == FUNKOUT_WEAPON_TYPE_STRAIGHT)
                                game->projectiles[i].remove_me = true;
                        }
                    }
                }
                if (game->projectiles[i].remove_me) {
                    funkout_game_destroy_projectile(game, i);
                    i--;
                } else {
                    game->projectiles[i].timestep += timestep;
                    if (game->projectiles[i].type == FUNKOUT_WEAPON_TYPE_STRAIGHT) {
                        game->projectiles[i].pos.x += projdelta.x;
                        game->projectiles[i].pos.y += projdelta.y;
                    }
                }
            }
        }
    }
}

int funkout_game_poll(struct funkout_Game * game,
                      int max_events,
                      struct funkout_GameEvent * ret_events)
{
    int copylen = MIN(game->num_events, max_events);
    if (copylen) {
        if (ret_events != NULL) {
            for (int i = 0; i < copylen; i++)
                ret_events[i] = game->event_queue[(i + game->event_index) % FUNKOUT_GAME_EVENT_QUEUE_SIZE];
        }
        game->event_index = (game->event_index + copylen) % FUNKOUT_GAME_EVENT_QUEUE_SIZE;
        game->num_events -= copylen;
    }
    return copylen;
}

bool funkout_game_control_player(const struct funkout_Game * game,
                                 int pindex,
                                 float horizontal,
                                 float vertical,
                                 bool shoot,
                                 bool slow)
{
    if (pindex < 0) {
        for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++) {
            if (game->players[i] != NULL) {
                game->players[i]->controls.horizontal = horizontal;
                game->players[i]->controls.vertical = vertical;
                game->players[i]->controls.shoot = shoot;
                game->players[i]->controls.slow = slow;
            }
        }
        return true;
    } else if (pindex < FUNKOUT_MAX_PLAYERS && game->players[pindex] != NULL) {
        game->players[pindex]->controls.horizontal = horizontal;
        game->players[pindex]->controls.vertical = vertical;
        game->players[pindex]->controls.shoot = shoot;
        game->players[pindex]->controls.slow = slow;
        return true;
    }
    return false;
}

struct funkout_Ball * funkout_game_add_ball(struct funkout_Game * game,
                                            int pindex,
                                            bool sticky)
{
    struct funkout_Player * player = (pindex >= 0 && pindex < FUNKOUT_MAX_PLAYERS ? game->players[pindex] : NULL);
    if (player != NULL) {
        player->balls = realloc(player->balls, sizeof(struct funkout_Ball) * (player->num_balls + 1));
        memset(player->balls + player->num_balls, 0, sizeof(struct funkout_Ball));
        player->balls[player->num_balls].player = player;
        player->balls[player->num_balls].pos = player->pos;
        player->balls[player->num_balls].pos.y -= player->radius + FUNKOUT_BALL_RADIUS;
        player->balls[player->num_balls].sticky = sticky;
        player->balls[player->num_balls].speed = FUNKOUT_BALL_SPEED;
        player->balls[player->num_balls].radius = FUNKOUT_BALL_RADIUS;
        player->balls[player->num_balls].direction.x = 0;
        player->balls[player->num_balls].direction.y = -1;
        funkout_vector_normalize(player->balls[player->num_balls].direction.x,
                                 player->balls[player->num_balls].direction.y,
                                 &player->balls[player->num_balls].direction);
        struct funkout_GameEvent e = {
            FUNKOUT_GAME_EVENT_BALL_SPAWNED,
            {.ball_spawned={player->balls + player->num_balls}}
        };
        funkout_game_push_event(game, &e);
        return player->balls + player->num_balls++;
    }
    return NULL;
}

bool funkout_game_remove_ball(struct funkout_Game * game, int pindex, int bindex)
{
    struct funkout_Player * player = (pindex >= 0 && pindex < FUNKOUT_MAX_PLAYERS ? game->players[pindex] : NULL);
    if (player != NULL) {
        if (bindex >= 0 && bindex < player->num_balls) {
            struct funkout_GameEvent e = {
                FUNKOUT_GAME_EVENT_BALL_DESTROYED,
                {.ball_destroyed={player->balls[bindex]}}
            };
            funkout_game_push_event(game, &e);
            if (player->num_balls > 1) {
                if (bindex < player->num_balls - 1)
                    memmove(player->balls + bindex, player->balls + bindex + 1, sizeof(struct funkout_Ball) * (player->num_balls - bindex - 1));
                player->balls = realloc(player->balls, sizeof(struct funkout_Ball) * (player->num_balls - 1));
                player->num_balls--;
            } else {
                free(player->balls);
                player->balls = NULL;
                player->num_balls = 0;
            }
            return true;
        }
    }
    return false;
}

struct funkout_Drop * funkout_game_add_drop(struct funkout_Game * game, float pos_x, float pos_y, enum funkout_DropType type, struct funkout_Ball * dropper)
{
    game->drops = realloc(game->drops, sizeof(struct funkout_Drop) * (game->num_drops + 1));
    memset(game->drops + game->num_drops, 0, sizeof(struct funkout_Drop));
    game->drops[game->num_drops].type = type;
    game->drops[game->num_drops].pos.x = pos_x;
    game->drops[game->num_drops].pos.y = pos_y;
    struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_DROP_SPAWNED, {.drop_spawned={game->drops + game->num_drops, dropper}}};
    funkout_game_push_event(game, &e);
    return game->drops + game->num_drops++;
}

struct funkout_Enemy * funkout_game_add_enemy(struct funkout_Game * game, float pos_x, float pos_y, const struct funkout_EnemyDef * enemy_def)
{
    game->enemies = realloc(game->enemies, sizeof(struct funkout_Enemy) * (game->num_enemies + 1));
    memset(game->enemies + game->num_enemies, 0, sizeof(struct funkout_Enemy));
    game->enemies[game->num_enemies].definition = *enemy_def;
    game->enemies[game->num_enemies].pos.x = pos_x;
    game->enemies[game->num_enemies].pos.y = pos_y;
    game->enemies[game->num_enemies].hitpoints = game->enemies[game->num_enemies].definition.hitpoints;
    game->enemies[game->num_enemies].weapon_cooldown = game->enemies[game->num_enemies].definition.weapon.cooldown;
    game->enemies[game->num_enemies].weapon_cooldown += (rand() % 1000) * game->enemies[game->num_enemies].definition.weapon.cooldown_random / 1000.0;
    game->enemies[game->num_enemies].firing_cooldown = game->enemies[game->num_enemies].definition.weapon.firing_cooldown;
    int dir = rand() % 8;
    switch (dir) {
    case 0:
        game->enemies[game->num_enemies].direction.x = 1;
        break;
    case 1:
        game->enemies[game->num_enemies].direction.x = 1;
        game->enemies[game->num_enemies].direction.y = 1;
        break;
    case 2:
        game->enemies[game->num_enemies].direction.y = 1;
        break;
    case 3:
        game->enemies[game->num_enemies].direction.x = -1;
        game->enemies[game->num_enemies].direction.y = 1;
        break;
    case 4:
        game->enemies[game->num_enemies].direction.x = -1;
        break;
    case 5:
        game->enemies[game->num_enemies].direction.x = -1;
        game->enemies[game->num_enemies].direction.y = -1;
        break;
    case 6:
        game->enemies[game->num_enemies].direction.y = -1;
        break;
    case 7:
        game->enemies[game->num_enemies].direction.x = 1;
        game->enemies[game->num_enemies].direction.y = -1;
        break;
    }
    game->enemies[game->num_enemies].direction.x = 1;
    game->enemies[game->num_enemies].direction.y = 0;
    funkout_vector_normalize(game->enemies[game->num_enemies].direction.x,
                             game->enemies[game->num_enemies].direction.y,
                             &game->enemies[game->num_enemies].direction);
    struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_ENEMY_SPAWNED, {.enemy_spawned={game->enemies + game->num_enemies}}};
    funkout_game_push_event(game, &e);
    return game->enemies + game->num_enemies++;
}

bool funkout_game_destroy_enemy(struct funkout_Game * game, int eindex)
{
    struct funkout_Enemy * enemy = (eindex >= 0 && eindex < game->num_enemies ? game->enemies + eindex : NULL);
    if (enemy != NULL) {
        for (int i = 0; i < game->num_projectiles; i++) {
            if (game->projectiles[i].type == FUNKOUT_WEAPON_TYPE_BEAM
                && game->projectiles[i].enemy == enemy) {
                funkout_game_destroy_projectile(game, i);
                i--;
            }
        }
        game->blocks[enemy->spawn_pos[1]][enemy->spawn_pos[0]].current_spawn--;
        if (game->blocks[enemy->spawn_pos[1]][enemy->spawn_pos[0]].current_spawn < 0)
            game->blocks[enemy->spawn_pos[1]][enemy->spawn_pos[0]].current_spawn = 0;
        struct funkout_GameEvent e = {
            FUNKOUT_GAME_EVENT_ENEMY_DESTROYED,
            {.enemy_destroyed={*enemy}}
        };
        funkout_game_push_event(game, &e);
        if (game->num_enemies > 1) {
            if (eindex < game->num_enemies - 1)
                memmove(game->enemies + eindex, game->enemies + eindex + 1, sizeof(struct funkout_Enemy) * (game->num_enemies - eindex - 1));
            game->enemies = realloc(game->enemies, sizeof(struct funkout_Enemy) * (game->num_enemies - 1));
            game->num_enemies--;
        } else {
            free(game->enemies);
            game->enemies = 0;
            game->num_enemies = 0;
        }        
        return true;
    }
    return false;
}

bool funkout_game_destroy_player(struct funkout_Game * game, int pindex)
{
    struct funkout_Player * player = (pindex >= 0 && pindex < FUNKOUT_MAX_PLAYERS ? game->players[pindex] : NULL);
    if (player != NULL) {
        player->destroyed_cooldown = FUNKOUT_GAME_DESTROYED_COOLDOWN;
        player->lives--;
        if (player->balls != NULL) {
            free(player->balls);
            player->balls = NULL;
            player->num_balls = 0;
        }
        struct funkout_GameEvent event = {FUNKOUT_GAME_EVENT_PLAYER_DESTROYED, {.player_destroyed={player}}};
        funkout_game_push_event(game, &event);
        return true;
    }
    return false;
}

bool funkout_game_destroy_block(struct funkout_Game * game,
                                int x, int y,
                                struct funkout_Ball * ball,
                                const struct funkout_Vector * impact,
                                const struct funkout_Vector * normal)
{
    if (game->blocks[y][x].definition.type > FUNKOUT_BLOCK_TYPE_NONE) {
        struct funkout_Vector use_impact = {0}, use_normal = {0};
        if (impact != NULL)
            use_impact = *impact;
        if (normal != NULL)
            use_normal = *normal;
        struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_BLOCK_DESTROYED, {.block_destroyed={game->blocks[y][x],x, y, ball, use_impact, use_normal}}};
        funkout_game_push_event(game, &e);
        if (game->blocks[y][x].definition.drop_type > FUNKOUT_DROP_TYPE_NONE
            && game->blocks[y][x].definition.drop_type < MAX_FUNKOUT_DROP_TYPE) {
            funkout_game_add_drop(game,
                                  e.e.block_destroyed.block_x + 0.5,
                                  e.e.block_destroyed.block_y + 0.5,
                                  game->blocks[y][x].definition.drop_type,
                                  ball);
        }
        memset(&game->blocks[y][x], 0, sizeof(struct funkout_Block));
        return true;
    }
    return false;
}

struct funkout_Projectile * funkout_game_add_projectile(struct funkout_Game * game,
                                                        float pos_x, float pos_y,
                                                        float normal_x, float normal_y,
                                                        const struct funkout_WeaponDef * weapon_def,
                                                        const struct funkout_Enemy * enemy)
{
    if (weapon_def->type > FUNKOUT_WEAPON_TYPE_NONE
        && weapon_def->type < MAX_FUNKOUT_WEAPON_TYPE) {
        game->projectiles = realloc(game->projectiles, sizeof(struct funkout_Enemy) * (game->num_projectiles + 1));
        memset(game->projectiles + game->num_projectiles, 0, sizeof(struct funkout_Enemy));
        game->projectiles[game->num_projectiles].type = weapon_def->type;
        game->projectiles[game->num_projectiles].enemy = enemy;
        game->projectiles[game->num_projectiles].speed = weapon_def->speed;
        game->projectiles[game->num_projectiles].pos.x = pos_x;
        game->projectiles[game->num_projectiles].pos.y = pos_y;
        game->projectiles[game->num_projectiles].radius = weapon_def->size;
        game->projectiles[game->num_projectiles].length = FUNKOUT_GAME_HEIGHT * 2;
        game->projectiles[game->num_projectiles].ttl = weapon_def->ttl;
        funkout_vector_normalize(normal_x, normal_y, &game->projectiles[game->num_projectiles].normal);
        struct funkout_GameEvent e = {FUNKOUT_GAME_EVENT_PROJECTILE_SPAWNED, {.projectile_spawned={game->projectiles + game->num_projectiles}}};
        funkout_game_push_event(game, &e);
        return game->projectiles + game->num_projectiles++;
    }
    return NULL;
}

bool funkout_game_destroy_projectile(struct funkout_Game * game, int index)
{
    struct funkout_Projectile * projectile = (index >= 0 && index < game->num_projectiles ? game->projectiles + index : NULL);
    if (projectile != NULL) {
        struct funkout_GameEvent e = {
            FUNKOUT_GAME_EVENT_PROJECTILE_DESTROYED,
            {.projectile_destroyed={*projectile}}
        };
        funkout_game_push_event(game, &e);
        if (game->num_projectiles > 1) {
            if (index < game->num_projectiles - 1)
                memmove(game->projectiles + index, game->projectiles + index + 1, sizeof(struct funkout_Projectile) * (game->num_projectiles - index - 1));
            game->projectiles = realloc(game->projectiles, sizeof(struct funkout_Projectile) * (game->num_projectiles - 1));
            game->num_projectiles--;
        } else {
            free(game->projectiles);
            game->projectiles = 0;
            game->num_projectiles = 0;
        }
        return true;
    }
    return false;
}

unsigned int funkout_level_get_max_score(const struct funkout_Level * level)
{
    unsigned int total_hitpoints = 0;
    for (int by = 0; by < FUNKOUT_GAME_HEIGHT; by++) {
        for (int bx = 0; bx < FUNKOUT_GAME_WIDTH; bx++) {
            if (level->block_defs[by][bx].type == FUNKOUT_BLOCK_TYPE_NORMAL
                && level->block_defs[by][bx].hitpoints > 0) {
                total_hitpoints += level->block_defs[by][bx].hitpoints;
            }
        }
    }
    unsigned int ret = 0;
    for (int i = 1; i < 10 && total_hitpoints > 0; i++) {
        ret += 1;
        total_hitpoints--;
    }
    ret += 10 * total_hitpoints;
    return ret;
}


static cJSON* json_color_serialize(const struct funkout_Color *color)
{
    cJSON *scolor = cJSON_CreateArray();
    cJSON_AddItemToArray(scolor, cJSON_CreateNumber(color->r));
    cJSON_AddItemToArray(scolor, cJSON_CreateNumber(color->g));
    cJSON_AddItemToArray(scolor, cJSON_CreateNumber(color->b));
    cJSON_AddItemToArray(scolor, cJSON_CreateNumber(color->a));
    return scolor;
}

static int json_color_deserialize(struct funkout_Color *color, cJSON *json)
{
    if (json != NULL && cJSON_IsArray(json)) {
        float *ret[4] = {&color->r, &color->g, &color->b, &color->a};
        int len = cJSON_GetArraySize(json);
        if (len < 3)
            return 0;
        for (int i = 0; i < 4 && i < len; i++) {
            cJSON *o = cJSON_GetArrayItem(json, i);
            if (!cJSON_IsNumber(o))
                return 0;
            *ret[i] = o->valuedouble;
        }
        if (len < 4)
            *ret[3] = 1;
        return len;
    }
    return 0;
}

char* funkout_level_serialize(const struct funkout_Level * level)
{
    cJSON *slevel = cJSON_CreateObject();
    cJSON_AddItemToObject(slevel, "name", cJSON_CreateString(level->name));
    cJSON_AddItemToObject(slevel, "background_tile", cJSON_CreateNumber(level->background_tile));
    cJSON_AddItemToObject(slevel, "background_color", json_color_serialize(&level->background_color));
    cJSON *srows = cJSON_CreateArray();
    for (int by = 0; by < FUNKOUT_GAME_HEIGHT; by++) {
        cJSON *scolumns = cJSON_CreateArray();
        for (int bx = 0; bx < FUNKOUT_GAME_WIDTH; bx++) {
            cJSON *sblock = cJSON_CreateObject();
            const struct funkout_BlockDef *def = &level->block_defs[by][bx];
            cJSON_AddItemToObject(sblock, "type", cJSON_CreateString(FUNKOUT_BLOCK_TYPE_STRING(def->type)));
            cJSON_AddItemToObject(sblock, "hitpoints", cJSON_CreateNumber(def->hitpoints));
            cJSON_AddItemToObject(sblock, "color", json_color_serialize(&def->color));
            cJSON_AddItemToObject(sblock, "drop_type", cJSON_CreateString(FUNKOUT_DROP_TYPE_STRING(def->drop_type)));
            if (def->type == FUNKOUT_BLOCK_TYPE_SPAWN) {
                cJSON_AddItemToObject(sblock, "s.max_enemies", cJSON_CreateNumber(def->spawn.max_enemies));
                cJSON_AddItemToObject(sblock, "s.max_count", cJSON_CreateNumber(def->spawn.max_count));
                cJSON_AddItemToObject(sblock, "s.cooldown", cJSON_CreateNumber(def->spawn.cooldown));
                cJSON_AddItemToObject(sblock, "s.cooldown_start", cJSON_CreateNumber(def->spawn.cooldown_start));
                cJSON_AddItemToObject(sblock, "s.e.name", cJSON_CreateString(def->spawn.enemy.name));
                cJSON_AddItemToObject(sblock, "s.e.appearance", cJSON_CreateString(FUNKOUT_ENEMY_APPEARANCE_STRING(def->spawn.enemy.appearance)));
                cJSON_AddItemToObject(sblock, "s.e.speed", cJSON_CreateNumber(def->spawn.enemy.speed));
                cJSON_AddItemToObject(sblock, "s.e.hitpoints", cJSON_CreateNumber(def->spawn.enemy.hitpoints));
                cJSON_AddItemToObject(sblock, "s.e.speed", cJSON_CreateNumber(def->spawn.enemy.speed));
                cJSON_AddItemToObject(sblock, "s.e.radius", cJSON_CreateNumber(def->spawn.enemy.radius));
                cJSON_AddItemToObject(sblock, "s.e.ghost_blocks", cJSON_CreateBool(def->spawn.enemy.ghost_blocks));
                cJSON_AddItemToObject(sblock, "s.e.randomness", cJSON_CreateNumber(def->spawn.enemy.radius));
                cJSON_AddItemToObject(sblock, "s.e.aim", cJSON_CreateBool(def->spawn.enemy.aim));
                cJSON_AddItemToObject(sblock, "s.e.w.type", cJSON_CreateString(FUNKOUT_WEAPON_TYPE_STRING(def->spawn.enemy.weapon.type)));
                cJSON_AddItemToObject(sblock, "s.e.w.cooldown", cJSON_CreateNumber(def->spawn.enemy.weapon.cooldown));
                cJSON_AddItemToObject(sblock, "s.e.w.cooldown_random", cJSON_CreateNumber(def->spawn.enemy.weapon.cooldown_random));
                cJSON_AddItemToObject(sblock, "s.e.w.firing_cooldown", cJSON_CreateNumber(def->spawn.enemy.weapon.firing_cooldown));
                cJSON_AddItemToObject(sblock, "s.e.w.fired_cooldown", cJSON_CreateNumber(def->spawn.enemy.weapon.fired_cooldown));
                cJSON_AddItemToObject(sblock, "s.e.w.speed", cJSON_CreateNumber(def->spawn.enemy.weapon.speed));
                cJSON_AddItemToObject(sblock, "s.e.w.size", cJSON_CreateNumber(def->spawn.enemy.weapon.size));
                cJSON_AddItemToObject(sblock, "s.e.w.ttl", cJSON_CreateNumber(def->spawn.enemy.weapon.ttl));
            }
            cJSON_AddItemToArray(scolumns, sblock);
        }
        cJSON_AddItemToArray(srows, scolumns);
    }
    cJSON_AddItemToObject(slevel, "blocks", srows);
    char *ret = cJSON_Print(slevel);
    cJSON_Delete(slevel);
    return ret;
}

bool funkout_level_deserialize(struct funkout_Level * level, const char *json)
{
    memset(level, 0, sizeof(struct funkout_Level));
    cJSON *slevel = cJSON_Parse(json);
    if (slevel == NULL)
        return false;
    cJSON *o = cJSON_GetObjectItemCaseSensitive(slevel, "name");
    if (cJSON_IsString(o) && o->valuestring != NULL)
        snprintf(level->name, sizeof(level->name), "%s", o->valuestring);
    o = cJSON_GetObjectItemCaseSensitive(slevel, "background_tile");
    if (cJSON_IsNumber(o))
        level->background_tile = o->valueint;
    o = cJSON_GetObjectItemCaseSensitive(slevel, "background_color");
    json_color_deserialize(&level->background_color, o);
    cJSON *oblockdefs = cJSON_GetObjectItemCaseSensitive(slevel, "blocks");
    if (cJSON_IsArray(oblockdefs)) {
    }
    cJSON_Delete(slevel);
    return true;
}

size_t funkout_level_save(const struct funkout_Level * level, const char *filename);
size_t funkout_level_load(struct funkout_Level * level, const char *filename);

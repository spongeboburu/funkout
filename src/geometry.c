/**
 * @file geometry.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include <stdlib.h>
#include <math.h>
#include "funkout/geometry.h"

void funkout_vector_normalize(float x, float y,
                              struct funkout_Vector * ret_vector)
{
    float l = sqrtf(x * x + y * y);
    if (l > 0.0f) {
        ret_vector->x = x / l;
        ret_vector->y = y / l;
    } else {
        ret_vector->x = 0.0f;
        ret_vector->y = 0.0f;
    }
}

void funkout_aabb_fit(float pos_x, float pos_y,
                      float extent_x, float extent_y,
                      float bounding_pos_x, float bounding_pos_y,
                      float bounding_extent_x, float bounding_extent_y,
                      struct funkout_AABB * ret_aabb)
{
    float ex = 0, ey = 0;
    if (extent_x <= 0 || extent_y <= 0) {
        ex = (extent_x <= 0 ? 0 : bounding_extent_x);
        ey = (extent_y <= 0 ? 0 : bounding_extent_y);
    } else {
        float aspect = extent_x / extent_y;
        if (bounding_extent_y >= bounding_extent_x / aspect) {
            ex = bounding_extent_x;
            ey = bounding_extent_x / aspect;
        } else {
            ex = bounding_extent_y * aspect;
            ey = bounding_extent_y;
        }
    }
    ret_aabb->pos.x = bounding_pos_x;
    ret_aabb->pos.y = bounding_pos_y;
    ret_aabb->extents.x = ex;
    ret_aabb->extents.y = ey;
}

void funkout_irect_fit(int pos_x, int pos_y,
                       int width, int height,
                       int bounding_pos_x, int bounding_pos_y,
                       int bounding_width, int bounding_height,
                       struct funkout_IRect * ret_irect)
{
    int w = 0, h = 0;
    if (width <= 0 || height <= 0) {
        w = (width <= 0 ? 0 : bounding_width);
        h = (height <= 0 ? 0 : bounding_height);
    } else {
        float aspect = (float)width / (float)height;
        if (bounding_height >= roundf(bounding_width / aspect)) {
            w = bounding_width;
            h = roundf(bounding_width / aspect);
        } else {
            w = roundf(bounding_height * aspect);
            h = bounding_height;
        }
    }
    if (ret_irect != NULL) {
        ret_irect->x = bounding_pos_x + bounding_width / 2 - w / 2;
        ret_irect->y = bounding_pos_y + bounding_height / 2 - h / 2;
        ret_irect->w = w;
        ret_irect->h = h;
    }
}


void funkout_polygon_get_aabb(int num_points,
                              const struct funkout_Vector * points,
                              struct funkout_AABB * ret_aabb)
{
    if (num_points > 0) {
        struct funkout_Vector mmin = points[0];
        struct funkout_Vector mmax = points[0];
        for (int i = 1; i < num_points; i++) {
            if (points[i].x > mmax.x)
                mmax.x = points[i].x;
            else if (points[i].x < mmin.x)
                mmin.x = points[i].x;
            if (points[i].y > mmax.y)
                mmax.y = points[i].y;
            else if (points[i].y < mmin.y)
                mmin.y = points[i].y;
        }
        ret_aabb->pos.x = (mmax.x + mmin.x) / 2;
        ret_aabb->pos.y = (mmax.y + mmin.y) / 2;
        ret_aabb->extents.x = (fabsf(mmin.x) + fabsf(mmax.x)) / 2;
        ret_aabb->extents.y = (fabsf(mmin.y) + fabsf(mmax.y)) / 2;
    } else {
        ret_aabb->pos.x = 0;
        ret_aabb->pos.y = 0;
        ret_aabb->extents.x = 0;
        ret_aabb->extents.y = 0;
    }
}

void funkout_closest_point_on_line(float pos_x, float pos_y,
                                   float line_pos1_x, float line_pos1_y,
                                   float line_pos2_x, float line_pos2_y,
                                   unsigned int iflags,
                                   struct funkout_Vector * ret_vector)
{
    float a1 = line_pos2_y - line_pos1_y;
    float b1 = line_pos1_x - line_pos2_x;
    float c1 = (line_pos2_y - line_pos1_y) * line_pos1_x + (line_pos1_x - line_pos2_x) * line_pos1_y;
    float c2 = -b1 * pos_x + a1 * pos_y;
    float det = a1 * a1 - -b1 * b1;
    if (det != 0){
        float rx = (a1 * c1 - b1 * c2) / det;
        float ry = (a1 * c2 - -b1 * c1) / det;
        if ((iflags & FUNKOUT_INF_FLAG_NEG) == 0) {
            if ((line_pos1_x <= line_pos2_x && rx < line_pos1_x) || (line_pos1_x >= line_pos2_x && rx > line_pos1_x))
                rx = line_pos1_x;
            if ((line_pos1_y <= line_pos2_y && ry < line_pos1_y) || (line_pos1_y >= line_pos2_y && ry > line_pos1_y))
                ry = line_pos1_y;
        }
        if ((iflags & FUNKOUT_INF_FLAG_POS) == 0) {
            if ((line_pos2_x <= line_pos1_x && rx < line_pos2_x) || (line_pos2_x >= line_pos1_x && rx > line_pos2_x))
                rx = line_pos2_x;
            if ((line_pos2_y <= line_pos1_y && ry < line_pos2_y) || (line_pos2_y >= line_pos1_y && ry > line_pos2_y))
                ry = line_pos2_y;
        }
        ret_vector->x = rx;
        ret_vector->y = ry;
     } else {
        ret_vector->x = pos_x;
        ret_vector->y = pos_y;
    } 
}

bool funkout_line_intersect(float line1_pos1_x, float line1_pos1_y,
                            float line1_pos2_x, float line1_pos2_y,
                            unsigned int iflags1,
                            float line2_pos1_x, float line2_pos1_y,
                            float line2_pos2_x, float line2_pos2_y,
                            unsigned int iflags2,
                            struct funkout_Vector * ret_vector)
{

    if ((line1_pos1_x == line1_pos2_x && line1_pos1_y == line1_pos2_y)
        || (line2_pos1_x == line2_pos2_x && line2_pos1_y == line2_pos2_y))
        return false;
    if ((line1_pos1_x == line2_pos1_x && line1_pos1_y == line2_pos1_y)
        || (line1_pos2_x == line2_pos1_x && line1_pos2_y == line2_pos1_y)
        || (line1_pos1_x == line2_pos2_x && line1_pos1_y == line2_pos2_y)
        || (line1_pos2_x == line2_pos2_x && line1_pos2_y == line2_pos2_y)) 
        return false;
    line1_pos2_x -= line1_pos1_x; line1_pos2_y -= line1_pos1_y;
    line2_pos1_x -= line1_pos1_x; line2_pos1_y -= line1_pos1_y;
    line2_pos2_x -= line1_pos1_x; line2_pos2_y -= line1_pos1_y;
    float dist_ab = sqrt(line1_pos2_x * line1_pos2_x
                         + line1_pos2_y * line1_pos2_y);
    float tmpcos = line1_pos2_x / dist_ab;
    float tmpsin = line1_pos2_y / dist_ab;
    float new_x = line2_pos1_x * tmpcos + line2_pos1_y * tmpsin;
    line2_pos1_y = line2_pos1_y * tmpcos - line2_pos1_x * tmpsin;
    line2_pos1_x = new_x;
    new_x = line2_pos2_x * tmpcos + line2_pos2_y * tmpsin;
    line2_pos2_y = line2_pos2_y * tmpcos - line2_pos2_x * tmpsin;
    line2_pos2_x = new_x;
    if ((iflags2 & FUNKOUT_INF_FLAG_POS) == 0
        && line2_pos1_y < 0 && line2_pos2_y < 0)
        return false;
    if ((iflags2 & FUNKOUT_INF_FLAG_NEG) == 0
        && line2_pos1_y >= 0 && line2_pos2_y >= 0)
        return false;
    float ab_pos = (line2_pos2_x
                    + (line2_pos1_x - line2_pos2_x)
                    * line2_pos2_y / (line2_pos2_y - line2_pos1_y));
    if ((iflags1 & FUNKOUT_INF_FLAG_NEG) == 0 && ab_pos < 0)
        return false;
    if ((iflags1 & FUNKOUT_INF_FLAG_POS) == 0 && ab_pos > dist_ab)
        return false;
    if (ret_vector != NULL) {
        ret_vector->x = line1_pos1_x + ab_pos * tmpcos;
        ret_vector->y = line1_pos1_y + ab_pos * tmpsin;
    }
    return true;
}

int funkout_line_aabb_intersect(float line_pos1_x, float line_pos1_y,
                                float line_pos2_x, float line_pos2_y,
                                unsigned int iflags,
                                float aabb_pos_x, float aabb_pos_y,
                                float aabb_extent_x, float aabb_extent_y,
                                struct funkout_Vector ret_vectors[2])
{
    struct funkout_Line aabb_lines[4] = {
        {{aabb_pos_x - aabb_extent_x, aabb_pos_y - aabb_extent_y},
         {aabb_pos_x + aabb_extent_x, aabb_pos_y - aabb_extent_y}},
        {{aabb_pos_x + aabb_extent_x, aabb_pos_y - aabb_extent_y},
         {aabb_pos_x + aabb_extent_x, aabb_pos_y + aabb_extent_y}},
        {{aabb_pos_x + aabb_extent_x, aabb_pos_y + aabb_extent_y},
         {aabb_pos_x - aabb_extent_x, aabb_pos_y + aabb_extent_y}},
        {{aabb_pos_x - aabb_extent_x, aabb_pos_y + aabb_extent_y},
         {aabb_pos_x - aabb_extent_x, aabb_pos_y - aabb_extent_y}}
    };
    int num = 0;
    for (int i = 0; i < 4 && num < 2; i++)
        if (funkout_line_intersect(line_pos1_x, line_pos1_y,
                                   line_pos2_x, line_pos2_y,
                                   iflags,
                                   aabb_lines[i].pos1.x, aabb_lines[i].pos1.y,
                                   aabb_lines[i].pos2.x, aabb_lines[i].pos2.y,
                                   0,
                                   ret_vectors + num)) 
            num++;
    return num;
}

int funkout_line_circle_intersect(float line_pos1_x, float line_pos1_y,
                                  float line_pos2_x, float line_pos2_y,
                                  unsigned int iflags,
                                  float circle_pos_x, float circle_pos_y,
                                  float circle_radius,
                                  struct funkout_Vector ret_vectors[2])
{
    if (circle_radius <= 0)
        return false;
    float dx = line_pos2_x - line_pos1_x;
    float dy = line_pos2_y - line_pos1_y;
    float a = dx * dx + dy * dy;
    float b = 2 * (dx * (line_pos1_x - circle_pos_x)
                   + dy * (line_pos1_y - circle_pos_y));
    float c = ((line_pos1_x - circle_pos_x)
               * (line_pos1_x - circle_pos_x)
               + (line_pos1_y - circle_pos_y)
               * (line_pos1_y - circle_pos_y)
               - circle_radius * circle_radius);
    float det = b * b - 4 * a * c;
    if (a <= 0.0000001 || det < 0)
        return 0;
    if (det == 0) {
        float t = -b / (2 * a);
        if ((iflags & FUNKOUT_INF_FLAG_NEG) == 0 && t < 0.0)
            return 0;
        if ((iflags & FUNKOUT_INF_FLAG_POS) == 0 && t > 1.0)
            return 0;
        if (ret_vectors != NULL) {
            ret_vectors[0].x = line_pos1_x + t * dx;
            ret_vectors[0].y = line_pos1_y + t * dy;
        }
        return 1;
    } else {
        int num = 0;
        float t1 = (float)((-b + sqrtf(det)) / (2 * a));
        if (((iflags & FUNKOUT_INF_FLAG_NEG) != 0 || t1 >= 0.0)
            && ((iflags & FUNKOUT_INF_FLAG_POS) != 0 || t1 <= 1.0)) {
            if (ret_vectors != NULL) {
                ret_vectors[0].x = line_pos1_x + t1 * dx;
                ret_vectors[0].y = line_pos1_y + t1 * dy;
            }
            num++;
        }
        float t2 = (float)((-b - sqrtf(det)) / (2 * a));
        if (((iflags & FUNKOUT_INF_FLAG_NEG) != 0 || t2 >= 0.0)
            && ((iflags & FUNKOUT_INF_FLAG_POS) != 0 || t2 <= 1.0)) {
            if (ret_vectors != NULL) {
                ret_vectors[num].x = line_pos1_x + t2 * dx;
                ret_vectors[num].y = line_pos1_y + t2 * dy;
            }
            num++;
        }
        return num;
    }
    return 0;
}

int funkout_line_polygon_intersect(float line_pos1_x, float line_pos1_y,
                                   float line_pos2_x, float line_pos2_y,
                                   unsigned int iflags,
                                   float polygon_pos_x, float polygon_pos_y,
                                   int polygon_num,
                                   const struct funkout_Vector * polygon_points,
                                   struct funkout_Vector ret_vectors[2])
{
    if (polygon_num < 3)
        return 0;
    int num = 0;
    struct funkout_Vector prev = polygon_points[polygon_num - 1];
    for (int i = 0; i < 8 && i < polygon_num && num < 2; i++) {
        struct funkout_Vector ipoint;
        if (funkout_line_intersect(line_pos1_x, line_pos1_y,
                                   line_pos2_x, line_pos2_y,
                                   iflags,
                                   polygon_pos_x + prev.x,
                                   polygon_pos_y + prev.y,
                                   polygon_pos_x + polygon_points[i].x,
                                   polygon_pos_y + polygon_points[i].y,
                                   0,
                                   &ipoint)) {
            if (ret_vectors != NULL) 
                ret_vectors[num] = ipoint;
            num++;
        }
        prev = polygon_points[i];
    }
    return num;
}

bool funkout_aabb_intersect(float aabb1_pos_x, float aabb1_pos_y,
                            float aabb1_extent_x, float aabb1_extent_y,
                            float aabb2_pos_x, float aabb2_pos_y,
                            float aabb2_extent_x, float aabb2_extent_y,
                            struct funkout_Vector * ret_min_translation)
{
    if (aabb1_extent_x <= 0 || aabb1_extent_y <= 0
        || aabb2_extent_x <= 0 || aabb2_extent_y <= 0)
        return false;
    float left = aabb2_pos_x - aabb2_extent_x - aabb1_pos_x - aabb1_extent_x;
    float right = aabb2_pos_x + aabb2_extent_x - aabb1_pos_x + aabb1_extent_x;
    float top = aabb2_pos_y - aabb2_extent_y - aabb1_pos_y - aabb1_extent_y;
    float bottom = aabb2_pos_y + aabb2_extent_y - aabb1_pos_y + aabb1_extent_y;
    if (right > 0 && bottom > 0 && left < 0 && top < 0) {
        if (ret_min_translation != NULL) {
            float aleft = fabsf(left);
            float highhor = (aleft < right ? left : right);
            float atop = fabsf(top);
            float highver = (atop < bottom ? top : bottom);
            if (fabsf(highhor) < fabsf(highver)) {
                ret_min_translation->x = highhor;
                ret_min_translation->y = 0;
            } else {
                ret_min_translation->x = 0;
                ret_min_translation->y = highver;
            }
        }
        return true;
    }
    return false;
}

bool funkout_circle_intersect(float circle1_pos_x, float circle1_pos_y,
                              float circle1_radius,
                              float circle2_pos_x, float circle2_pos_y,
                              float circle2_radius,
                              struct funkout_Vector * ret_min_translation)
{
    if (circle1_radius <= 0 || circle2_radius <= 0)
        return false;
    float dx = circle1_pos_x - circle2_pos_x;
    float dy = circle1_pos_y - circle2_pos_y;
    float l = sqrtf(dx * dx + dy * dy);
    if (l == 0) {
        if (ret_min_translation != NULL) {
            ret_min_translation->x = 0;
            ret_min_translation->y = circle1_radius + circle2_radius;
        }
        return true;
    }
    if (l < circle1_radius + circle2_radius) {
        if (ret_min_translation != NULL) {
            dx /= l;
            dy /= l;
            ret_min_translation->x = dx * (circle1_radius + circle2_radius - l);
            ret_min_translation->y = dy * (circle1_radius + circle2_radius - l);
        }
        return true;
    }
    return false;
}

static void funkout_polygon_projection_axis(int num_points,
                                            const struct funkout_Vector * points,
                                            int edge_index,
                                            bool clockwise,
                                            struct funkout_Vector * ret_axis)
{
    struct funkout_Line edge = {
        {points[edge_index].x,
         points[edge_index].y},
        {points[(edge_index + 1) % num_points].x,
         points[(edge_index + 1) % num_points].y}
    };
    ret_axis->x = edge.pos2.y - edge.pos1.y;
    ret_axis->y = edge.pos2.x - edge.pos1.x;
    if (clockwise)
        ret_axis->y = -ret_axis->y;
    else 
        ret_axis->x = -ret_axis->x;
    float l = sqrtf(ret_axis->x * ret_axis->x + ret_axis->y * ret_axis->y);
    if (l != 0) {
        ret_axis->x /= l;
        ret_axis->y /= l;
    } else {
        ret_axis->x = 0;
        ret_axis->y = 0;
    }
}

static void funkout_polygon_project(const struct funkout_Vector * axis,
                               const struct funkout_Vector * pos,
                               int num_points,
                               const struct funkout_Vector * points,
                               float * ret_min, float * ret_max)
{
    *ret_min = axis->x * (pos->x + points[0].x) + axis->y * (pos->y + points[0].y);
    *ret_max = *ret_min;
    for (int i = 1; i < num_points; i++) {
        float d = axis->x * (pos->x + points[i].x) + axis->y * (pos->y + points[i].y);
        if (d < *ret_min)
            *ret_min = d;
        else if (d > *ret_max)
            *ret_max = d;
    }
}

bool funkout_polygon_intersect(float polygon1_pos_x, float polygon1_pos_y,
                               int polygon1_num,
                               const struct funkout_Vector * polygon1_points,
                               float polygon2_pos_x, float polygon2_pos_y,
                               int polygon2_num,
                               const struct funkout_Vector * polygon2_points,
                               struct funkout_Vector * ret_min_translation)
{
    if (polygon1_num < 3 || polygon2_num < 3)
        return false;
    // Broad AABB test
    struct funkout_Vector mmin1 = polygon1_points[0];
    struct funkout_Vector mmax1 = polygon1_points[0];
    for (int i = 1; i < polygon1_num; i++) {
        if (polygon1_points[i].x > mmax1.x)
            mmax1.x = polygon1_points[i].x;
        else if (polygon1_points[i].x < mmin1.x)
            mmin1.x = polygon1_points[i].x;
        if (polygon1_points[i].y > mmax1.y)
            mmax1.y = polygon1_points[i].y;
        else if (polygon1_points[i].y < mmin1.y)
            mmin1.y = polygon1_points[i].y;
    }
    struct funkout_Vector mmin2 = polygon2_points[0];
    struct funkout_Vector mmax2 = polygon2_points[0];
    for (int i = 1; i < polygon2_num; i++) {
        if (polygon2_points[i].x > mmax2.x)
            mmax2.x = polygon2_points[i].x;
        else if (polygon2_points[i].x < mmin2.x)
            mmin2.x = polygon2_points[i].x;
        if (polygon2_points[i].y > mmax2.y)
            mmax2.y = polygon2_points[i].y;
        else if (polygon2_points[i].y < mmin2.y)
            mmin2.y = polygon2_points[i].y;
    }
    if (polygon1_pos_x + mmax1.x < polygon2_pos_x + mmin2.x
        || polygon1_pos_y + mmax1.y < polygon2_pos_y + mmin2.y
        || polygon1_pos_x + mmin1.x > polygon2_pos_x + mmax2.x
        || polygon1_pos_y + mmin1.y > polygon2_pos_y + mmax2.y)
        return false;
    // Narrow SAT test
    // struct funkout_Vector mtv_axis = {0, 0};
    // float mtv_length = 0;
    struct funkout_Vector pos[2] = {{polygon1_pos_x, polygon1_pos_y},
                                    {polygon2_pos_x, polygon2_pos_y}};
    int num_points[2] = {polygon1_num, polygon2_num};
    const struct funkout_Vector * points[2] = {polygon1_points, polygon2_points};
    for (int pindex = 0; pindex < 2; pindex++) {
        struct funkout_Vector winding = {points[pindex][0].x - points[pindex][1].x,
                                         points[pindex][0].y - points[pindex][1].y};
        bool winding_cw = (winding.x * winding.y - winding.y * winding.x) < 0;
        for (int i = 0; i < num_points[pindex]; i++) {
            struct funkout_Vector axis;
            funkout_polygon_projection_axis(num_points[pindex], points[pindex], i, winding_cw, &axis);
            float min_dot1, max_dot1, min_dot2, max_dot2;
            funkout_polygon_project(&axis, pos + pindex, num_points[pindex], points[pindex], &min_dot1, &max_dot1);
            funkout_polygon_project(&axis, pos + (pindex + 1) % 2, num_points[(pindex + 1) % 2], points[(pindex + 1) % 2], &min_dot2, &max_dot2);
            // float distance = min_dot2 - max_dot1;
            // if (distance > 0)
            //     return false;
            if (max_dot1 <= min_dot2 || min_dot1 >= max_dot2)
                return false;
            // if (axisl > mtv_length) {
            //     mtv_length
        }
    }
    if (ret_min_translation != NULL) {
        ret_min_translation->x = 0;
        ret_min_translation->y = 0;
    }
    return true;
}

bool funkout_aabb_circle_intersect(float aabb_pos_x, float aabb_pos_y,
                                   float aabb_extent_x, float aabb_extent_y,
                                   float circle_pos_x, float circle_pos_y,
                                   float circle_radius,
                                   struct funkout_Vector * ret_min_translation)
{
    if (aabb_extent_x <= 0 || aabb_extent_y <= 0
        || circle_radius <= 0)
        return false;
    float left = circle_pos_x - circle_radius - aabb_pos_x - aabb_extent_x;
    float right = circle_pos_x + circle_radius - aabb_pos_x + aabb_extent_x;
    float top = circle_pos_y - circle_radius - aabb_pos_y - aabb_extent_y;
    float bottom = circle_pos_y + circle_radius - aabb_pos_y + aabb_extent_y;
    if (right > 0 && bottom > 0 && left < 0 && top < 0) {
        if (left <= -circle_radius && right >= circle_radius) {
            if (ret_min_translation != NULL) {
                ret_min_translation->x = 0;
                ret_min_translation->y = (fabsf(top) < fabsf(bottom) ? top : bottom);
            }
            return true;
        } if (top <= -circle_radius && bottom >= circle_radius) {
            if (ret_min_translation != NULL) {
                ret_min_translation->x = (fabsf(left) < fabsf(right) ? left : right);
                ret_min_translation->y = 0;
            }
            return true;
        } else {
            float cornerx = circle_pos_x > aabb_pos_x ? aabb_pos_x + aabb_extent_x : aabb_pos_x - aabb_extent_x;
            float cornery = circle_pos_y > aabb_pos_y ? aabb_pos_y + aabb_extent_y : aabb_pos_y - aabb_extent_y;
            float cx = cornerx - circle_pos_x;
            float cy = cornery - circle_pos_y;
            float cl = sqrtf(cx * cx + cy * cy);
            // funkout_render_draw_line(cornerx, cornery, circle_pos_x, circle_pos_y, 0, 1,1,1);
            if (cl < circle_radius) {
                if (ret_min_translation != NULL) {
                    if (cl == 0) {
                        ret_min_translation->x = 0;
                        ret_min_translation->y = circle_radius + aabb_extent_y;
                    } else {
                        ret_min_translation->x = (cx / cl) * (circle_radius - cl);
                        ret_min_translation->y = (cy / cl) * (circle_radius - cl);
                    }
                }
                return true;
            }
        }
    }
    return false;
}

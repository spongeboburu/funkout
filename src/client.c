#include <string.h>
#include <limits.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include "funkout/client.h"
#include "funkout/time.h"
#include "funkout/defs.h"

#include "logo.c"
#include "star.c"
#include "font_small.c"
#include "font_menu.c"
#include "explosion.c"
#include "explosion2.c"
#include "flash.c"
#include "player1.c"
#include "ball1.c"
#include "drop_multiply.c"
#include "drop_sticky.c"
#include "drop_life.c"
#include "drop_death.c"
#include "projectile_bullet.c"
#include "projectile_beam.c"
#include "projectile_beam_point.c"
#include "enemy_amoeba.c"
#include "enemy_virus.c"
#include "enemy_sata.c"
#include "enemy_satb.c"
#include "enemy_satc.c"
#include "enemy_satd.c"
#include "enemy_plague.c"
#include "enemy_tank.c"
#include "tile1.c"
#include "tile2.c"
#include "tile3.c"
#include "tile4.c"
#include "tile5.c"

#include "click1_ogg.c"
#include "click2_ogg.c"
#include "click3_ogg.c"
#include "bounce_ogg.c"
#include "player_spawned_ogg.c"
#include "player_destroyed_ogg.c"
#include "block_damaged_ogg.c"
#include "block_destroyed_ogg.c"
#include "drop_life_ogg.c"
#include "drop_multiply_ogg.c"
#include "drop_sticky_ogg.c"
#include "drop_death_ogg.c"
#include "enemy_damaged_ogg.c"
#include "enemy_destroyed_ogg.c"
#include "shoot_projectile_ogg.c"
#include "shoot_beam_ogg.c"

#define ENEMY_IMAGE_SIZE 64
#define AUDIO_INCREMENT 5

struct DataMap {
    const unsigned char * data;
    size_t size;
};

static const struct DataMap IMAGE_DATA[] = {
    {NULL, 0},
    {FUNKOUT_LOGO_PNG, sizeof(FUNKOUT_LOGO_PNG)},
    {FUNKOUT_STAR_PNG, sizeof(FUNKOUT_STAR_PNG)},
    {FUNKOUT_SMALL_FONT_PNG, sizeof(FUNKOUT_SMALL_FONT_PNG)},
    {FUNKOUT_MENU_FONT_PNG, sizeof(FUNKOUT_MENU_FONT_PNG)},
    {FUNKOUT_EXPLOSION_PNG, sizeof(FUNKOUT_EXPLOSION_PNG)},
    {FUNKOUT_EXPLOSION2_PNG, sizeof(FUNKOUT_EXPLOSION2_PNG)},
    {FUNKOUT_FLASH_PNG, sizeof(FUNKOUT_FLASH_PNG)},
    {FUNKOUT_PROJECTILE_BULLET_PNG, sizeof(FUNKOUT_PROJECTILE_BULLET_PNG)},
    {FUNKOUT_PROJECTILE_BEAM_PNG, sizeof(FUNKOUT_PROJECTILE_BEAM_PNG)},
    {FUNKOUT_PROJECTILE_BEAM_POINT_PNG, sizeof(FUNKOUT_PROJECTILE_BEAM_POINT_PNG)},
    {FUNKOUT_ENEMY_AMOEBA_PNG, sizeof(FUNKOUT_ENEMY_AMOEBA_PNG)},
    {FUNKOUT_ENEMY_VIRUS_PNG, sizeof(FUNKOUT_ENEMY_VIRUS_PNG)},
    {FUNKOUT_ENEMY_SATA_PNG, sizeof(FUNKOUT_ENEMY_SATA_PNG)},
    {FUNKOUT_ENEMY_SATB_PNG, sizeof(FUNKOUT_ENEMY_SATB_PNG)},
    {FUNKOUT_ENEMY_SATC_PNG, sizeof(FUNKOUT_ENEMY_SATC_PNG)},
    {FUNKOUT_ENEMY_SATD_PNG, sizeof(FUNKOUT_ENEMY_SATD_PNG)},
    {FUNKOUT_ENEMY_PLAGUE_PNG, sizeof(FUNKOUT_ENEMY_PLAGUE_PNG)},
    {FUNKOUT_ENEMY_TANK_PNG, sizeof(FUNKOUT_ENEMY_TANK_PNG)},
    {FUNKOUT_TILE1_PNG, sizeof(FUNKOUT_TILE1_PNG)},
    {FUNKOUT_TILE2_PNG, sizeof(FUNKOUT_TILE2_PNG)},
    {FUNKOUT_TILE3_PNG, sizeof(FUNKOUT_TILE3_PNG)},
    {FUNKOUT_TILE4_PNG, sizeof(FUNKOUT_TILE4_PNG)},
    {FUNKOUT_TILE5_PNG, sizeof(FUNKOUT_TILE5_PNG)}
};

static const struct DataMap AUDIO_DATA[] = {
    {NULL, 0},
    {FUNKOUT_CLICK1_OGG, sizeof(FUNKOUT_CLICK1_OGG)},
    {FUNKOUT_CLICK2_OGG, sizeof(FUNKOUT_CLICK2_OGG)},
    {FUNKOUT_CLICK3_OGG, sizeof(FUNKOUT_CLICK3_OGG)},
    {FUNKOUT_PLAYER_SPAWNED_OGG, sizeof(FUNKOUT_PLAYER_SPAWNED_OGG)},
    {FUNKOUT_PLAYER_DESTROYED_OGG, sizeof(FUNKOUT_PLAYER_DESTROYED_OGG)},
    {FUNKOUT_BOUNCE_OGG, sizeof(FUNKOUT_BOUNCE_OGG)},
    {FUNKOUT_BLOCK_DAMAGED_OGG, sizeof(FUNKOUT_BLOCK_DAMAGED_OGG)},
    {FUNKOUT_BLOCK_DESTROYED_OGG, sizeof(FUNKOUT_BLOCK_DESTROYED_OGG)},
    {FUNKOUT_DROP_LIFE_OGG, sizeof(FUNKOUT_DROP_LIFE_OGG)},
    {FUNKOUT_DROP_MULTIPLY_OGG, sizeof(FUNKOUT_DROP_MULTIPLY_OGG)},
    {FUNKOUT_DROP_STICKY_OGG, sizeof(FUNKOUT_DROP_STICKY_OGG)},
    {FUNKOUT_DROP_DEATH_OGG, sizeof(FUNKOUT_DROP_DEATH_OGG)},
    {FUNKOUT_ENEMY_DAMAGED_OGG, sizeof(FUNKOUT_ENEMY_DAMAGED_OGG)},
    {FUNKOUT_ENEMY_DESTROYED_OGG, sizeof(FUNKOUT_ENEMY_DESTROYED_OGG)},
    {FUNKOUT_SHOOT_PROJECTILE_OGG, sizeof(FUNKOUT_SHOOT_PROJECTILE_OGG)},
    {FUNKOUT_SHOOT_BEAM_OGG, sizeof(FUNKOUT_SHOOT_BEAM_OGG)}
};

static const char SCROLL_TEXT[] = "hello and welcome to funkout!     http://gitlab.com/spongeboburu/funkout";

static const struct funkout_ClientMenu MENU_MAIN;
static const struct funkout_ClientMenu MENU_OPTIONS;
static const struct funkout_ClientMenu MENU_DIFFICULTY;

static const struct funkout_ClientMenuItem MENU_ITEMS_MAIN[] = {
    {"close menu", 0, 0, true, NULL},
    {"start new game", 0, 0, true, &MENU_DIFFICULTY},
    {"options", 0, 0, true, &MENU_OPTIONS},
    {"quit", 0, 0, true, NULL},
};

static const struct funkout_ClientMenu MENU_MAIN = {
    {"main"}, 4, MENU_ITEMS_MAIN, NULL
};

static const struct funkout_ClientMenuItem MENU_ITEMS_OPTIONS[] = {
    {"back", 0, 0, true, &MENU_MAIN},
    {"audio", 10, 0, true, NULL},
    {"fullscreen", 1, 0, true, NULL},
    {"keyboard", 0, 0, true, NULL},
    {"controller", 0, 0, true, NULL}
};

static const struct funkout_ClientMenu MENU_OPTIONS = {
    {"options"}, 5, MENU_ITEMS_OPTIONS, &MENU_MAIN
};

static const struct funkout_ClientMenuItem MENU_ITEMS_DIFFICULTY[] = {
    {"back", 0, 0, true, &MENU_MAIN},
    {"easy", 0, 0, true, NULL},
    {"normal", 0, 0, true, NULL},
    {"hard", 0, 0, true, NULL},
    {"insane", 0, 0, true, NULL}
};

static const struct funkout_ClientMenu MENU_DIFFICULTY = {
    {"difficulty"}, 5, MENU_ITEMS_DIFFICULTY, &MENU_MAIN
};


static const struct funkout_Image * funkout_client_appearance_image(const struct funkout_Client * client, enum funkout_EnemyAppearance appearance)
{
    const struct funkout_Image * image;
    switch (appearance) {
    case FUNKOUT_ENEMY_APPEARANCE_AMOEBA:
        image = client->images[FUNKOUT_CLIENT_IMAGE_ENEMY_AMOEBA];
        break;
    case FUNKOUT_ENEMY_APPEARANCE_VIRUS:
        image = client->images[FUNKOUT_CLIENT_IMAGE_ENEMY_VIRUS];
        break;
    case FUNKOUT_ENEMY_APPEARANCE_SATA:
        image = client->images[FUNKOUT_CLIENT_IMAGE_ENEMY_SATA];
        break;
    case FUNKOUT_ENEMY_APPEARANCE_SATB:
        image = client->images[FUNKOUT_CLIENT_IMAGE_ENEMY_SATB];
        break;
    case FUNKOUT_ENEMY_APPEARANCE_SATC:
        image = client->images[FUNKOUT_CLIENT_IMAGE_ENEMY_SATC];
        break;
    case FUNKOUT_ENEMY_APPEARANCE_SATD:
        image = client->images[FUNKOUT_CLIENT_IMAGE_ENEMY_SATD];
        break;
    case FUNKOUT_ENEMY_APPEARANCE_PLAGUE:
        image = client->images[FUNKOUT_CLIENT_IMAGE_ENEMY_PLAGUE];
        break;
    case FUNKOUT_ENEMY_APPEARANCE_TANK:
        image = client->images[FUNKOUT_CLIENT_IMAGE_ENEMY_TANK];
        break;
    default:
        image = NULL;
        break;
    }
    return image;
}

static struct funkout_Sprite * funkout_client_add_explosion(struct funkout_Client * client, float pos_x, float pos_y, float size)
{
    struct funkout_Animation animation = {64, 0.5, 0, 0};
    struct funkout_Sprite * sprite = funkout_sprite_new(pos_x, pos_y, size, size, &animation, client->images[FUNKOUT_CLIENT_IMAGE_EXPLOSION]);
    sprite->ttl = animation.duration;
    funkout_sprite_array_append(&client->sprites, sprite);
    return sprite;
}

static struct funkout_Sprite * funkout_client_add_explosion2(struct funkout_Client * client, float pos_x, float pos_y, float size)
{
    struct funkout_Animation animation = {32, 0.25, 0, 0};
    struct funkout_Sprite * sprite = funkout_sprite_new(pos_x, pos_y, size, size, &animation, client->images[FUNKOUT_CLIENT_IMAGE_EXPLOSION2]);
    sprite->ttl = animation.duration;
    funkout_sprite_array_append(&client->sprites, sprite);
    return sprite;
}

static struct funkout_Sprite * funkout_client_add_flash(struct funkout_Client * client, float pos_x, float pos_y, float size)
{
    struct funkout_Animation animation = {32, 0.25, 0, 0};
    struct funkout_Sprite * sprite = funkout_sprite_new(pos_x, pos_y, size, size, &animation, client->images[FUNKOUT_CLIENT_IMAGE_FLASH]);
    sprite->ttl = animation.duration;
    funkout_sprite_array_append(&client->sprites, sprite);
    return sprite;
}

static void funkout_client_add_text(struct funkout_Client * client, float pos_x, float pos_y, float size, const char * format, ...)
// #ifdef __GNUC__
//     __attribute__ ((format (printf, 5, 6)))
// #endif
{
    client->texts = realloc(client->texts, sizeof(struct funkout_ClientText) * (client->num_texts + 1));
    client->texts[client->num_texts].pos.x = pos_x;
    client->texts[client->num_texts].pos.y = pos_y;
    client->texts[client->num_texts].size = size;
    client->texts[client->num_texts].ttl = 1.0;
    client->texts[client->num_texts].timestep = 0;
    va_list args, args2;
    va_start(args, format);
    va_copy(args2, args);
    int len = vsnprintf(NULL, 0, format, args2);
    client->texts[client->num_texts].text = malloc(len + 1);
    vsnprintf(client->texts[client->num_texts].text, len + 1, format, args);
    va_end(args);
    va_end(args2);
    client->num_texts++;
}

bool funkout_client_init(struct funkout_Client * client,
                         const struct funkout_ClientSettings * settings)
{
    memset(client, 0, sizeof(struct funkout_Client));
    if (settings != NULL)
        client->settings = *settings;
    else {
        client->settings.volume = 100;
    }
    funkout_audio_set_volume(client->settings.volume);
    funkout_game_init(&client->game, "Game", FUNKOUT_GAME_DIFFICULTY_EASY, NULL);
    client->state = FUNKOUT_CLIENT_STATE_TITLE;
    for (int i = 1; i < MAX_FUNKOUT_CLIENT_IMAGE; i++)
        client->images[i] = funkout_image_new(IMAGE_DATA[i].data, IMAGE_DATA[i].size);
    for (int i = 1; i < MAX_FUNKOUT_CLIENT_AUDIO; i++)
        client->audio[i] = funkout_audio_new(AUDIO_DATA[i].data, AUDIO_DATA[i].size);
    client->player_images[0] = funkout_image_new(FUNKOUT_PLAYER1_PNG, sizeof(FUNKOUT_PLAYER1_PNG));
    client->ball_images[0] = funkout_image_new(FUNKOUT_BALL1_PNG, sizeof(FUNKOUT_BALL1_PNG));
    client->drop_images[FUNKOUT_DROP_TYPE_MULTIPLY] = funkout_image_new(FUNKOUT_DROP_MULTIPLY_PNG, sizeof(FUNKOUT_DROP_MULTIPLY_PNG));
    client->drop_images[FUNKOUT_DROP_TYPE_STICKY] = funkout_image_new(FUNKOUT_DROP_STICKY_PNG, sizeof(FUNKOUT_DROP_STICKY_PNG));
    client->drop_images[FUNKOUT_DROP_TYPE_LIFE] = funkout_image_new(FUNKOUT_DROP_LIFE_PNG, sizeof(FUNKOUT_DROP_LIFE_PNG));
    client->drop_images[FUNKOUT_DROP_TYPE_DEATH] = funkout_image_new(FUNKOUT_DROP_DEATH_PNG, sizeof(FUNKOUT_DROP_DEATH_PNG));
    for (int i = 0; i < FUNKOUT_CLIENT_NUM_TITLE_STARS; i++) {
        client->title_stars[i][0] = (rand() % 201) / 100.0 - 1.0;
        client->title_stars[i][1] = (rand() % 201) / 100.0 - 1.0;
        client->title_stars[i][2] = (rand() % 100) / 100.0;
    }
    funkout_sprite_array_init(&client->sprites);
    // Default keyboard bindings
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_MENU][0] = SDL_GetScancodeFromKey(SDLK_ESCAPE);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_RIGHT][0] = SDL_GetScancodeFromKey(SDLK_d);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_RIGHT][1] = SDL_GetScancodeFromKey(SDLK_RIGHT);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_DOWN][0] = SDL_GetScancodeFromKey(SDLK_s);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_DOWN][1] = SDL_GetScancodeFromKey(SDLK_DOWN);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_LEFT][0] = SDL_GetScancodeFromKey(SDLK_a);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_LEFT][1] = SDL_GetScancodeFromKey(SDLK_LEFT);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_UP][0] = SDL_GetScancodeFromKey(SDLK_w);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_UP][1] = SDL_GetScancodeFromKey(SDLK_UP);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_ACTIVATE][0] = SDL_GetScancodeFromKey(SDLK_SPACE);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_ACTIVATE][1] = SDL_GetScancodeFromKey(SDLK_RETURN);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_UI_BACK][0] = SDL_GetScancodeFromKey(SDLK_BACKSPACE);

    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_RIGHT][0] = SDL_GetScancodeFromKey(SDLK_d);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_RIGHT][1] = SDL_GetScancodeFromKey(SDLK_RIGHT);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_DOWN][0] = SDL_GetScancodeFromKey(SDLK_s);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_DOWN][1] = SDL_GetScancodeFromKey(SDLK_DOWN);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_LEFT][0] = SDL_GetScancodeFromKey(SDLK_a);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_LEFT][1] = SDL_GetScancodeFromKey(SDLK_LEFT);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_UP][0] = SDL_GetScancodeFromKey(SDLK_w);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_UP][1] = SDL_GetScancodeFromKey(SDLK_UP);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_SHOOT][0] = SDL_GetScancodeFromKey(SDLK_SPACE);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_SHOOT][1] = SDL_GetScancodeFromKey(SDLK_RETURN);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_SLOW][0] = SDL_GetScancodeFromKey(SDLK_LSHIFT);
    client->bindings.key[FUNKOUT_CLIENT_CONTROL_PLAYER_SLOW][1] = SDL_GetScancodeFromKey(SDLK_RSHIFT);

    client->menu = &MENU_MAIN;
    client->menu_item = 1;
    return true;
}

void funkout_client_deinit(struct funkout_Client * client)
{
    funkout_sprite_array_deinit(&client->sprites, false);
    if (client->particles != NULL)
        free(client->particles);
    if (client->texts != NULL) {
        for (int i = 0; i < client->num_texts; i++)
            if (client->texts[i].text != NULL)
                free(client->texts[i].text);
        free(client->texts);
    }
    for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++) {
        if (client->player_images[i] != NULL)
            funkout_image_free(client->player_images[i]);
        if (client->ball_images[i] != NULL)
            funkout_image_free(client->ball_images[i]);
    }
    for (int i = FUNKOUT_DROP_TYPE_NONE; i < MAX_FUNKOUT_DROP_TYPE; i++) {
        if (client->drop_images[i] != NULL)
            funkout_image_free(client->drop_images[i]);
    }
    for (int i = 1; i < MAX_FUNKOUT_CLIENT_IMAGE; i++)
        if (client->images[i] != NULL)
            funkout_image_free(client->images[i]);
    for (int i = 1; i < MAX_FUNKOUT_CLIENT_AUDIO; i++)
        if (client->audio[i] != NULL)
            funkout_audio_free(client->audio[i]);
    funkout_game_deinit(&client->game);
}

void funkout_client_set_levels(struct funkout_Client * client,
                               int num_levels,
                               const struct funkout_Level * levels)
{
    if (client->levels != NULL)
        free(client->levels);
    if (num_levels > 0 && levels != NULL) {
        client->levels = malloc(sizeof(struct funkout_Level) * num_levels);
        memcpy(client->levels, levels, sizeof(struct funkout_Level) * num_levels);
        client->num_levels = num_levels;
    } else {
        client->num_levels = 0;
        client->levels = NULL;
    }
    client->current_level = 0;
}

bool funkout_client_update(struct funkout_Client * client)
{
    SDL_Event e;
    while (client->state != FUNKOUT_CLIENT_STATE_QUIT && SDL_PollEvent(&e)) {
        switch (e.type) {
        case SDL_QUIT:
            client->state = FUNKOUT_CLIENT_STATE_QUIT;
            break;
        }
    }
    funkout_client_update_controls(client);
    if (client->state == FUNKOUT_CLIENT_STATE_QUIT)
        return false;
    float t = funkout_time();
    float diff = 0;
    if (client->update_timestamp > 0 && t > client->update_timestamp) {
        diff = t - client->update_timestamp;
        for (int i = FUNKOUT_CLIENT_FPS_SAMPLES - 1; i > 0; i--)
            client->fps_samples[i] = client->fps_samples[i - 1];
        client->fps_samples[0] = 1.0 / diff;
        int tot = 0;
        for (int i = 0; i < FUNKOUT_CLIENT_FPS_SAMPLES; i++)
            tot += client->fps_samples[i];
        client->fps = tot / FUNKOUT_CLIENT_FPS_SAMPLES;
    } else
        client->fps = 0;
    client->update_timestamp = t;
    if (client->state == FUNKOUT_CLIENT_STATE_GAME) {
        if (client->menu == NULL) {
            client->game_timers.second_timer = fmodf(client->game_timers.second_timer + diff, 1.0);
            client->game_timers.ten_second_timer = fmodf(client->game_timers.ten_second_timer + diff, 10.0);
            funkout_sprite_array_prune(&client->sprites, false);
            float horizontal = client->controls[FUNKOUT_CLIENT_CONTROL_PLAYER_RIGHT] - client->controls[FUNKOUT_CLIENT_CONTROL_PLAYER_LEFT];
            float vertical = client->controls[FUNKOUT_CLIENT_CONTROL_PLAYER_DOWN] - client->controls[FUNKOUT_CLIENT_CONTROL_PLAYER_UP];
            bool shoot = client->controls[FUNKOUT_CLIENT_CONTROL_PLAYER_SHOOT] > 0;
            bool slow = client->controls[FUNKOUT_CLIENT_CONTROL_PLAYER_SLOW] > 0;
            funkout_game_control_player(&client->game, -1, horizontal, vertical, shoot, slow);
            funkout_sprite_array_update(&client->sprites, diff);
            int new_particle_len = client->num_particles;
            for (int i = 0; i < new_particle_len; i++) {
                if (client->particles[i].timestep < client->particles[i].ttl) {
                    float f = client->particles[i].timestep / client->particles[i].ttl;
                    f = 1.0 - MINMAX(f, 0, 1);
                    client->particles[i].timestep += diff;
                    client->particles[i].pos.x += client->particles[i].vel.x * diff * f;
                    client->particles[i].pos.y += client->particles[i].vel.y * diff * f;
                } else {
                    if (i < new_particle_len - 1)
                        memmove(client->particles + i,
                                client->particles + i + 1,
                                sizeof(struct funkout_ClientParticle) * (new_particle_len - i - 1));
                    new_particle_len--;
                    i--;
                }
            }
            if (new_particle_len != client->num_particles) {
                if (new_particle_len > 0) {
                    client->particles = realloc(client->particles, sizeof(struct funkout_ClientParticle) * new_particle_len);
                    client->num_particles = new_particle_len;
                } else {
                    free(client->particles);
                    client->particles = NULL;
                    client->num_particles = 0;
                }
            }
            int new_text_len = client->num_texts;
            for (int i = 0; i < new_text_len; i++) {
                if (client->texts[i].timestep < client->texts[i].ttl) {
                    client->texts[i].timestep += diff;
                    client->texts[i].pos.y -= diff;
                } else {
                    if (i < new_text_len - 1)
                        memmove(client->texts + i,
                                client->texts + i + 1,
                                sizeof(struct funkout_ClientText) * (new_text_len - i - 1));
                    new_text_len--;
                    i--;
                }
            }
            if (new_text_len != client->num_texts) {
                if (new_text_len > 0) {
                    client->texts = realloc(client->texts, sizeof(struct funkout_ClientText) * new_text_len);
                    client->num_texts = new_text_len;
                } else {
                    free(client->texts);
                    client->texts = NULL;
                    client->num_texts = 0;
                }
            }
            for (int dy = 0; dy < FUNKOUT_GAME_HEIGHT; dy++)
                for (int dx = 0; dx < FUNKOUT_GAME_WIDTH; dx++) {
                    if (client->block_effects[dy][dx].damage_cooldown > 0)
                        client->block_effects[dy][dx].damage_cooldown -= diff;
                    if (client->game.blocks[dy][dx].definition.type == FUNKOUT_BLOCK_TYPE_SPAWN) {
                        if (client->game.blocks[dy][dx].spawn_cooldown <= 1.0)
                            client->block_effects[dy][dx].door_cooldown = 1.0;
                        else if (client->block_effects[dy][dx].door_cooldown > 0)
                            client->block_effects[dy][dx].door_cooldown -= diff;
                    }
                }
            funkout_game_update(&client->game, diff);
            struct funkout_GameEvent gevent;
            while (funkout_game_poll(&client->game, 1, &gevent)) {
                switch (gevent.type) {
                case FUNKOUT_GAME_EVENT_STATE_CHANGED:
                    switch (gevent.e.state_changed.state) {
                    default:
                        break;
                    }
                    break;
                case FUNKOUT_GAME_EVENT_PLAYER_SPAWNED:
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_PLAYER_SPAWNED], 1, 0);
                    break;
                case FUNKOUT_GAME_EVENT_PLAYER_DESTROYED:
                    funkout_client_add_explosion(client,
                                                 gevent.e.player_destroyed.player->pos.x,
                                                 gevent.e.player_destroyed.player->pos.y,
                                                 gevent.e.player_destroyed.player->radius * 1.5);
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_PLAYER_DESTROYED], 0, 0);
                    break;
                case FUNKOUT_GAME_EVENT_BALL_BOUNCED:
                    funkout_client_add_flash(client, gevent.e.ball_bounced.pos.x, gevent.e.ball_bounced.pos.y, 0.25);
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_BALL_BOUNCE], -1, 0);
                    break;
                case FUNKOUT_GAME_EVENT_BLOCK_DAMAGED:
                    client->block_effects[gevent.e.block_damaged.block_y][gevent.e.block_damaged.block_x].damage_cooldown = FUNKOUT_CLIENT_BLOCK_DAMAGE_COOLDOWN;
                    funkout_client_add_particles1(client, 10,
                                                  gevent.e.block_damaged.impact.x,
                                                  gevent.e.block_damaged.impact.y,
                                                  gevent.e.block_damaged.normal.x,
                                                  gevent.e.block_damaged.normal.y,
                                                  90,
                                                  10.0, 5,
                                                  0.25,
                                                  0.05,
                                                  client->game.blocks[gevent.e.block_damaged.block_y][gevent.e.block_damaged.block_x].definition.color.r,
                                                  client->game.blocks[gevent.e.block_damaged.block_y][gevent.e.block_damaged.block_x].definition.color.g,
                                                  client->game.blocks[gevent.e.block_damaged.block_y][gevent.e.block_damaged.block_x].definition.color.b,
                                                  client->game.blocks[gevent.e.block_damaged.block_y][gevent.e.block_damaged.block_x].definition.color.a);
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_BLOCK_DAMAGED], -1, 0);
                    break;
                case FUNKOUT_GAME_EVENT_BLOCK_DESTROYED:
                    funkout_client_add_particles2(client, 10,
                                                  gevent.e.block_destroyed.block_x + 0.5,
                                                  gevent.e.block_destroyed.block_y + 0.5,
                                                  0.5, 0.5,
                                                  20.0,
                                                  0.5,
                                                  gevent.e.block_destroyed.block.definition.color.r + 0.5,
                                                  gevent.e.block_destroyed.block.definition.color.g + 0.5,
                                                  gevent.e.block_destroyed.block.definition.color.b + 0.5,
                                                  gevent.e.block_destroyed.block.definition.color.a);
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_BLOCK_DESTROYED], -1, 0);
                    break;
                case FUNKOUT_GAME_EVENT_DROP_PICKUP:
                    funkout_client_add_flash(client, gevent.e.drop_pickup.drop.pos.x, gevent.e.drop_pickup.drop.pos.y, 1.0);
                    switch (gevent.e.drop_pickup.drop.type) {
                    case FUNKOUT_DROP_TYPE_LIFE:
                        break;
                    case FUNKOUT_DROP_TYPE_MULTIPLY:
                        funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_DROP_MULTIPLY], 3, 0);
                        break;
                    case FUNKOUT_DROP_TYPE_STICKY:
                        funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_DROP_STICKY], 3, 0);
                        break;
                    case FUNKOUT_DROP_TYPE_DEATH:
                        funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_DROP_DEATH], 2, 0);
                        break;
                    default:
                        break;
                    }
                    break;
                case FUNKOUT_GAME_EVENT_SCORE:
                    funkout_client_add_text(client, gevent.e.score.pos.x, gevent.e.score.pos.y, 0.5, "+%d", gevent.e.score.score);
                    break;
                case FUNKOUT_GAME_EVENT_EXTRA_LIFE:
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_DROP_LIFE], 3, 0);
                    break;
                case FUNKOUT_GAME_EVENT_ENEMY_DAMAGED:
                    funkout_client_add_explosion2(client, gevent.e.enemy_damaged.impact.x, gevent.e.enemy_damaged.impact.y, 0.25);
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_ENEMY_DAMAGED], -1, 0);
                    break;
                case FUNKOUT_GAME_EVENT_ENEMY_DESTROYED:
                    funkout_client_add_explosion2(client,
                                                  gevent.e.enemy_destroyed.enemy.pos.x,
                                                  gevent.e.enemy_destroyed.enemy.pos.y,
                                                  gevent.e.enemy_destroyed.enemy.definition.radius * 1.5);
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_ENEMY_DESTROYED], -1, 0);
                    break;
                case FUNKOUT_GAME_EVENT_PROJECTILE_SPAWNED:
                    if (gevent.e.projectile_spawned.projectile->type == FUNKOUT_WEAPON_TYPE_STRAIGHT)
                        funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_SHOOT_PROJECTILE], -1, 0);
                    else if (gevent.e.projectile_spawned.projectile->type == FUNKOUT_WEAPON_TYPE_BEAM)
                        funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_SHOOT_BEAM], 4, -1);
                    break;
                case FUNKOUT_GAME_EVENT_PROJECTILE_DESTROYED:
                    if (gevent.e.projectile_destroyed.projectile.type == FUNKOUT_WEAPON_TYPE_STRAIGHT
                        && gevent.e.projectile_destroyed.projectile.pos.x > -gevent.e.projectile_destroyed.projectile.radius
                        && gevent.e.projectile_destroyed.projectile.pos.x < FUNKOUT_GAME_WIDTH + gevent.e.projectile_destroyed.projectile.radius
                        && gevent.e.projectile_destroyed.projectile.pos.y > -gevent.e.projectile_destroyed.projectile.radius
                        && gevent.e.projectile_destroyed.projectile.pos.y < FUNKOUT_GAME_HEIGHT + gevent.e.projectile_destroyed.projectile.radius)
                        funkout_client_add_explosion2(client,
                                                      gevent.e.projectile_destroyed.projectile.pos.x,
                                                      gevent.e.projectile_destroyed.projectile.pos.y,
                                                      gevent.e.projectile_destroyed.projectile.radius);
                    break;
                default:
                    break;
                }
            }
        }
        if (client->game.state == FUNKOUT_GAME_STATE_NEXT) {
            if (client->current_level < client->num_levels - 1) {
                client->current_level++;
                funkout_game_set_level(&client->game, client->levels + client->current_level);
            }
        }
    } else if (client->state == FUNKOUT_CLIENT_STATE_TITLE) {
        client->title_timers.second_timer = fmodf(client->title_timers.second_timer + diff, 1.0);
        client->title_timers.ten_second_timer = fmodf(client->title_timers.ten_second_timer + diff, 10.0);
        client->scroll_text_offset += diff * 4;
        if (client->scroll_text_offset >= strlen(SCROLL_TEXT) + FUNKOUT_GAME_WIDTH)
            client->scroll_text_offset = 0;
        for (int i = 0; i < FUNKOUT_CLIENT_NUM_TITLE_STARS; i++) {
            client->title_stars[i][2] += sin(client->title_timers.ten_second_timer * 0.1 * 6.283185) * diff;
            if (client->title_stars[i][2] >= 1.0) {
                client->title_stars[i][2] = 0.0;
                client->title_stars[i][0] = (rand() % 201) / 100.0 - 1.0;
                client->title_stars[i][1] = (rand() % 201) / 100.0 - 1.0;
            } else if (client->title_stars[i][2] < 0) {
                client->title_stars[i][2] = 1.0;
                client->title_stars[i][0] = (rand() % 201) / 100.0 - 1.0;
                client->title_stars[i][1] = (rand() % 201) / 100.0 - 1.0;
            }
        }
    }
    if (client->menu != NULL) {
        client->menu_timers.second_timer = fmodf(client->menu_timers.second_timer + diff, 1.0);
        client->menu_timers.ten_second_timer = fmodf(client->menu_timers.ten_second_timer + diff, 10.0);
        if (client->controls[FUNKOUT_CLIENT_CONTROL_UI_MENU]
            && client->rel_controls[FUNKOUT_CLIENT_CONTROL_UI_MENU]) {
            client->last_menu = client->menu;
            client->menu = NULL;            
            funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_CLICK3], -1, 0);
        } else {
            if (client->menu->num_items > 0) {
                if (client->controls[FUNKOUT_CLIENT_CONTROL_UI_UP]
                    && (client->rel_controls[FUNKOUT_CLIENT_CONTROL_UI_UP]
                        || client->control_cooldown <= 0)) {
                    client->menu_item--;
                    if (client->menu_item < 0)
                        client->menu_item = client->menu->num_items - 1;
                    client->control_cooldown = FUNKOUT_CLIENT_CONTROL_COOLDOWN;
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_CLICK1], -1, 0);
                } else if (client->controls[FUNKOUT_CLIENT_CONTROL_UI_DOWN]
                               && (client->rel_controls[FUNKOUT_CLIENT_CONTROL_UI_DOWN]
                                   || client->control_cooldown <= 0)) {
                    client->menu_item++;
                    if (client->menu_item >= client->menu->num_items)
                        client->menu_item = 0;
                    client->control_cooldown = FUNKOUT_CLIENT_CONTROL_COOLDOWN;
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_CLICK1], -1, 0);
                }
            }
            if (client->controls[FUNKOUT_CLIENT_CONTROL_UI_BACK]
                && (client->rel_controls[FUNKOUT_CLIENT_CONTROL_UI_BACK]
                    || client->control_cooldown <= 0)) {
                client->menu = client->menu->parent;
                client->control_cooldown = FUNKOUT_CLIENT_CONTROL_COOLDOWN;
            } else if (client->controls[FUNKOUT_CLIENT_CONTROL_UI_ACTIVATE]
                       && client->rel_controls[FUNKOUT_CLIENT_CONTROL_UI_ACTIVATE]) {
                if (client->menu->items[client->menu_item].menu != NULL) {
                    client->menu = client->menu->items[client->menu_item].menu;
                    if (client->menu == &MENU_DIFFICULTY)
                        client->menu_item = 2;
                    else if (client->menu == &MENU_MAIN
                             && client->state != FUNKOUT_CLIENT_STATE_GAME)
                        client->menu_item = 1;
                    else
                        client->menu_item = 0;
                    funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_CLICK1], -1, 0);
                } else {
                    if (client->menu == &MENU_MAIN) {
                        if (client->menu_item == 0) {
                            client->menu = NULL;
                            funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_CLICK3], -1, 0);
                        } else if (client->menu_item == 3)
                            client->state = FUNKOUT_CLIENT_STATE_QUIT;
                    } else if (client->menu == &MENU_OPTIONS) {
                        if (client->menu_item == 1) {
                            float volume = funkout_audio_get_volume();
                            if (volume != 0) {
                                funkout_audio_set_volume(0);
                            } else {
                                funkout_audio_set_volume(client->settings.volume / 100.0);
                            }
                            funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_CLICK1], -1, 0);
                        } else if (client->menu_item == 2) {
                            if (funkout_get_window_mode() == FUNKOUT_WINDOW_MODE_FULLSCREEN)
                                funkout_set_window_mode(FUNKOUT_WINDOW_MODE_NORMAL);
                            else
                                funkout_set_window_mode(FUNKOUT_WINDOW_MODE_FULLSCREEN);
                        }
                    } else if (client->menu == &MENU_DIFFICULTY) {
                        if (client->menu_item > 0) {
                            if (client->num_levels > 0) {
                                client->game.difficulty = client->menu_item - 1;
                                client->state = FUNKOUT_CLIENT_STATE_GAME;
                                funkout_game_remove_all_players(&client->game);
                                funkout_game_add_player(&client->game, "player 1");
                                client->current_level = 0;
                                funkout_game_set_level(&client->game, client->levels + client->current_level);
                                client->menu = NULL;
                            }
                        }
                    }
                }
            } else if (client->controls[FUNKOUT_CLIENT_CONTROL_UI_RIGHT]
                       && (client->rel_controls[FUNKOUT_CLIENT_CONTROL_UI_RIGHT]
                           || client->control_cooldown <= 0)) {
                if (client->menu == &MENU_OPTIONS) {
                    if (client->menu_item == 1) {
                        client->settings.volume += AUDIO_INCREMENT;
                        if (client->settings.volume > 100)
                            client->settings.volume = 100;
                        funkout_audio_set_volume(client->settings.volume / 100.0);
                        funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_CLICK1], -1, 0);
                    }
                    client->control_cooldown = FUNKOUT_CLIENT_CONTROL_COOLDOWN;
                }
            } else if (client->controls[FUNKOUT_CLIENT_CONTROL_UI_LEFT]
                       && (client->rel_controls[FUNKOUT_CLIENT_CONTROL_UI_LEFT]
                           || client->control_cooldown <= 0)) {
                if (client->menu == &MENU_OPTIONS) {
                    if (client->menu_item == 1) {
                        client->settings.volume -= AUDIO_INCREMENT;
                        if (client->settings.volume < 0)
                            client->settings.volume = 0;
                        funkout_audio_set_volume(client->settings.volume / 100.0);
                        funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_CLICK1], -1, 0);
                    }
                    client->control_cooldown = FUNKOUT_CLIENT_CONTROL_COOLDOWN;
                }
            }
        }
        if (client->control_cooldown > 0)
            client->control_cooldown -= diff;
    } else {
        if (client->controls[FUNKOUT_CLIENT_CONTROL_UI_MENU]
            && client->rel_controls[FUNKOUT_CLIENT_CONTROL_UI_MENU]) {
            client->menu = (client->last_menu != NULL ? client->last_menu : &MENU_MAIN);
            funkout_audio_play(client->audio[FUNKOUT_CLIENT_AUDIO_CLICK2], -1, 0);
        }
    }
    client->settings.fullscreen = funkout_get_window_mode() == FUNKOUT_WINDOW_MODE_FULLSCREEN;
    return client->state != FUNKOUT_CLIENT_STATE_QUIT;
}

static void funkout_client_draw_game(const struct funkout_Client * client,
                                     int x, int y, int w, int h)
{
    struct funkout_AABB aabb = {{0, 0}, {FUNKOUT_GAME_WIDTH, FUNKOUT_GAME_HEIGHT + 1}};
    struct funkout_AABB bounding_aabb = {{x + w / 2, y + h / 2}, {w / 2, h / 2}};
    struct funkout_AABB fitted_aabb;
    funkout_aabb_fit(aabb.pos.x, aabb.pos.y, aabb.extents.x, aabb.extents.y,
                     bounding_aabb.pos.x, bounding_aabb.pos.y, bounding_aabb.extents.x, bounding_aabb.extents.y,
                     &fitted_aabb);
    struct funkout_IRect clip_irect = {fitted_aabb.pos.x - fitted_aabb.extents.x,
                                       fitted_aabb.pos.y - fitted_aabb.extents.y,
                                       fitted_aabb.extents.x * 2,
                                       fitted_aabb.extents.y * 2};
    funkout_render_set_clip(&clip_irect);
    int block_size_x = fitted_aabb.extents.x * 2.0 / (float)FUNKOUT_GAME_WIDTH;
    int block_size_y = fitted_aabb.extents.y * 2.0 / (float)(FUNKOUT_GAME_HEIGHT + 1);
    int block_size = MIN(block_size_x, block_size_y);
    if (block_size <= 0)
        return;
    funkout_render_fill_rect(fitted_aabb.pos.x - fitted_aabb.extents.x, fitted_aabb.pos.y - fitted_aabb.extents.y,
                             fitted_aabb.extents.x * 2, fitted_aabb.extents.y * 2,
                             0, 0, 0, 1);
    char tmps[128];
    snprintf(tmps, sizeof(tmps), "%010u", client->game.players[0] != NULL ? client->game.players[0]->score : 0);
    funkout_image_draw_text(NULL, tmps,
                            fitted_aabb.pos.x - fitted_aabb.extents.x,
                            fitted_aabb.pos.y - fitted_aabb.extents.y,
                            block_size, 1.0, 0.0, 0.0, 0.0);
    snprintf(tmps, sizeof(tmps), "x%02d", client->game.players[0] != NULL ? MAX(0, client->game.players[0]->lives) : 99);
    funkout_image_draw_text(NULL, tmps,
                            fitted_aabb.pos.x + fitted_aabb.extents.x - block_size * 3,
                            fitted_aabb.pos.y - fitted_aabb.extents.y,
                            block_size, 1.0, 0.0, 0.0, 0.0);
    struct funkout_IRect grid_rect = {
        fitted_aabb.pos.x - (block_size * FUNKOUT_GAME_WIDTH) / 2,
        fitted_aabb.pos.y - (block_size * FUNKOUT_GAME_HEIGHT - block_size) / 2,
        block_size * FUNKOUT_GAME_WIDTH,
        block_size * FUNKOUT_GAME_HEIGHT
    };
    funkout_render_fill_rect(grid_rect.x, grid_rect.y, grid_rect.w, grid_rect.h,
                             client->game.level.background_color.r,
                             client->game.level.background_color.g,
                             client->game.level.background_color.b,
                             client->game.level.background_color.a);
    if (client->game.level.background_tile > 0) {
        int tile_image = FUNKOUT_CLIENT_IMAGE_TILE1 + (client->game.level.background_tile - 1) % 5;
        for (int ty = 0; ty < FUNKOUT_GAME_HEIGHT; ty += 2) {
            for (int tx = 0; tx < FUNKOUT_GAME_WIDTH; tx += 2) {
                struct funkout_IRect tile_rect = {grid_rect.x + tx * block_size,
                                                  grid_rect.y + ty * block_size,
                                                  block_size * 2,
                                                  block_size * 2};
                funkout_image_draw(client->images[tile_image],
                                   tile_rect.x, tile_rect.y, tile_rect.w, tile_rect.h,
                                   0, 0, 0, 0,
                                   1.0, 0, 0, 0);
            }
        }
    }
    for (int dy = 0; dy < FUNKOUT_GAME_HEIGHT; dy++) {
        for (int dx = 0; dx < FUNKOUT_GAME_WIDTH; dx++) {
            if (client->game.blocks[dy][dx].definition.type > FUNKOUT_BLOCK_TYPE_NONE
                && client->game.blocks[dy][dx].definition.type < MAX_FUNKOUT_BLOCK_TYPE) {
                struct funkout_Color c = client->game.blocks[dy][dx].definition.color, c1, c2;
                funkout_color_add(&c, 0.5, &c1);
                funkout_color_mul(&c, 0.5, &c2);
                c1.a = c.a;
                c2.a = c.a;
                struct funkout_Vector offset = {0};
                if (client->block_effects[dy][dx].damage_cooldown > 0
                    && FUNKOUT_CLIENT_BLOCK_DAMAGE_COOLDOWN > 0) {
                    struct funkout_Color white = {1,1,1,1};
                    float f = client->block_effects[dy][dx].damage_cooldown / FUNKOUT_CLIENT_BLOCK_DAMAGE_COOLDOWN;
                    funkout_color_blend(&c, &white, f, &c);
                    offset.x = 0.1 * block_size * f * cosf(6.283185307179586 * 10 * sinf(client->game_timers.second_timer * 6.283185307179586 * 2));
                    offset.y = 0.1 * block_size * f * sinf(6.283185307179586 * 7 * cosf(client->game_timers.second_timer * 6.283185307179586 * 4.5));
                }
                struct funkout_IRect rect = {grid_rect.x + dx * block_size + offset.x,
                                             grid_rect.y + dy * block_size + offset.y,
                                             block_size, block_size};
                if (client->game.blocks[dy][dx].definition.type == FUNKOUT_BLOCK_TYPE_SPAWN) {
                    struct funkout_IRect modrect = rect;
                    int ofs = 0;
                    if (client->game.blocks[dy][dx].definition.spawn.cooldown >= 2.0) {
                        if (client->game.blocks[dy][dx].spawn_cooldown < 1.0) {
                            ofs = block_size * (1.0 - MINMAX(client->game.blocks[dy][dx].spawn_cooldown, 0, 1));
                        } else if (client->block_effects[dy][dx].door_cooldown > 0) {
                            float f = client->block_effects[dy][dx].door_cooldown;
                            ofs = block_size * MINMAX(f, 0, 1);
                        }
                        if (ofs > block_size * 0.8)
                            ofs = block_size * 0.8;
                    } else
                        ofs = block_size * 0.8;
                    funkout_render_fill_rect(rect.x, rect.y, rect.w, rect.h, 0, 0, 0, 1);
                    if (client->game.blocks[dy][dx].spawn_cooldown >= 0 && client->game.blocks[dy][dx].spawn_cooldown < 1.0) {
                        const struct funkout_EnemyDef * edef = &client->game.blocks[dy][dx].definition.spawn.enemy;
                        if (edef != NULL) {
                            const struct funkout_Image * image = funkout_client_appearance_image(client, edef->appearance);
                            funkout_image_draw(image,
                                               rect.x, rect.y, rect.w, rect.h,
                                               0, 0, ENEMY_IMAGE_SIZE, ENEMY_IMAGE_SIZE,
                                               c.a, 0, 0, 0);
                        }
                    }
                    modrect.w /= 2;
                    modrect.w -= ofs / 2;
                    funkout_render_fill_rect(modrect.x, modrect.y, modrect.w, modrect.h, c.r, c.g, c.b, c.a);
                    funkout_render_draw_line(modrect.x + 1, modrect.y + modrect.h - 1, modrect.x + modrect.w - 1, modrect.y + modrect.h - 1, c2.r, c2.g, c2.b, c2.a);
                    funkout_render_draw_line(modrect.x + modrect.w - 1, modrect.y + 1, modrect.x + modrect.w - 1, modrect.y + modrect.h - 1, c2.r, c2.g, c2.b, c2.a);
                    funkout_render_draw_line(modrect.x, modrect.y, modrect.x + modrect.w - 2, modrect.y, c1.r, c1.g, c1.b, c1.a);
                    funkout_render_draw_line(modrect.x, modrect.y, modrect.x, modrect.y + modrect.h - 2, c1.r, c1.g, c1.b, c1.a);
                    modrect.x += block_size * 0.5;
                    modrect.x += ofs / 2;
                    funkout_render_fill_rect(modrect.x, modrect.y, modrect.w, modrect.h, c.r, c.g, c.b, c.a);
                    funkout_render_draw_line(modrect.x + 1, modrect.y + modrect.h - 1, modrect.x + modrect.w - 1, modrect.y + modrect.h - 1, c2.r, c2.g, c2.b, c2.a);
                    funkout_render_draw_line(modrect.x + modrect.w - 1, modrect.y + 1, modrect.x + modrect.w - 1, modrect.y + modrect.h - 1, c2.r, c2.g, c2.b, c2.a);
                    funkout_render_draw_line(modrect.x, modrect.y, modrect.x + modrect.w - 2, modrect.y, c1.r, c1.g, c1.b, c1.a);
                    funkout_render_draw_line(modrect.x, modrect.y, modrect.x, modrect.y + modrect.h - 2, c1.r, c1.g, c1.b, c1.a);
                } else {
                    funkout_render_fill_rect(rect.x, rect.y, rect.w, rect.h,
                                             c.r, c.g, c.b, c.a);
                    funkout_render_draw_line(rect.x + 1, rect.y + rect.h - 1, rect.x + rect.w - 1, rect.y + rect.h - 1, c2.r, c2.g, c2.b, c2.a);
                    funkout_render_draw_line(rect.x + rect.w - 1, rect.y + 1, rect.x + rect.w - 1, rect.y + rect.h - 1, c2.r, c2.g, c2.b, c2.a);
                    funkout_render_draw_line(rect.x, rect.y, rect.x + rect.w - 2, rect.y, c1.r, c1.g, c1.b, c1.a);
                    funkout_render_draw_line(rect.x, rect.y, rect.x, rect.y + rect.h - 2, c1.r, c1.g, c1.b, c1.a);
                }
                if (client->game.blocks[dy][dx].definition.drop_type > FUNKOUT_DROP_TYPE_NONE
                    && client->game.blocks[dy][dx].definition.drop_type < MAX_FUNKOUT_DROP_TYPE) {
                    funkout_image_draw(client->drop_images[client->game.blocks[dy][dx].definition.drop_type],
                                       grid_rect.x + dx * block_size + offset.x,
                                       grid_rect.y + dy * block_size + offset.y,
                                       block_size, block_size,
                                       0,0,0,0,
                                       c.a, 0, 0, 0);
                }
            }
        }
    }
    for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++) {
        struct funkout_Player * player = client->game.players[i];
        if (player != NULL && player->lives >= 0 && player->destroyed_cooldown <= 0) {
            int radius = (float)block_size * player->radius;
            int px = (float)block_size * player->pos.x;
            int py = (float)block_size * player->pos.y;
            float alpha = 1.0;
            if (player->immortal)
                alpha = 0.5 + fmodf(client->game_timers.second_timer, 0.3);
            funkout_image_draw(client->player_images[0],
                               grid_rect.x + px - radius, grid_rect.y + py - radius, radius * 2, radius * 2,
                               0, 0, 0, 0,
                               alpha, 0, 0, 0);
            if (player->sticky) {
                funkout_image_draw(client->drop_images[FUNKOUT_DROP_TYPE_STICKY],
                                   grid_rect.x + px - block_size * 0.5, grid_rect.y + py - block_size * 0.5,
                                   block_size, block_size,
                                   0, 0, 0, 0,
                                   alpha, 0, 0, 0);
            }
        }
    }
    for (int i = 0; i < client->sprites.length; i++) {
        int px = (float)block_size * (client->sprites.sprites[i]->pos.x - client->sprites.sprites[i]->extents.x);
        int py = (float)block_size * (client->sprites.sprites[i]->pos.y - client->sprites.sprites[i]->extents.y);
        funkout_sprite_draw2(client->sprites.sprites[i],
                             grid_rect.x + px,
                             grid_rect.y + py,
                             client->sprites.sprites[i]->extents.x * 2 * (float)block_size,
                             client->sprites.sprites[i]->extents.y * 2 * (float)block_size);
    }
    struct funkout_Animation enemy_animation = {ENEMY_IMAGE_SIZE, 1.0, -1, 0};
    for (int i = 0; i < client->game.num_enemies; i++) {
        float scale_x = 1.0 + sin(client->game_timers.second_timer * 6.283185) * 0.05;
        float scale_y = 1.0 - sin(client->game_timers.second_timer * 6.283185) * 0.05;
        int radius = (float)block_size * client->game.enemies[i].definition.radius;
        int px = (float)block_size * (client->game.enemies[i].pos.x - client->game.enemies[i].definition.radius * scale_x);
        int py = (float)block_size * (client->game.enemies[i].pos.y - client->game.enemies[i].definition.radius * scale_y);
        if (client->game.enemies[i].stun_cooldown > 0 && FUNKOUT_GAME_ENEMY_STUN_COOLDOWN > 0) {
            float f = client->game.enemies[i].stun_cooldown / FUNKOUT_GAME_ENEMY_STUN_COOLDOWN;
            px += 0.1 * block_size * f * cosf(6.283185307179586 * 10 * sinf(client->game_timers.second_timer * 6.283185307179586 * 2));
            py += 0.1 * block_size * f * sinf(6.283185307179586 * 7 * cosf(client->game_timers.second_timer * 6.283185307179586 * 4.5));
        }
        enemy_animation.timestep = 0;
        funkout_animation_update(&enemy_animation, client->game.enemies[i].timestep);
        const struct funkout_Image * image = funkout_client_appearance_image(client, client->game.enemies[i].definition.appearance);
        if (image != NULL)
            funkout_animation_draw(&enemy_animation, image,
                                   grid_rect.x + px, grid_rect.y + py,
                                   radius * 2 * scale_x, radius * 2 * scale_y,
                                   1.0, 0, 0, 0);
        else
            funkout_render_fill_rect(grid_rect.x + px, grid_rect.y + py,
                                     radius * 2, radius * 2,
                                     1, 0, 0, 1);
        if (client->game.enemies[i].weapon_cooldown <= 0
            && client->game.enemies[i].firing_cooldown > 0
            && client->game.enemies[i].definition.weapon.firing_cooldown > 0) {
            int ppx = (float)block_size * client->game.enemies[i].pos.x;
            int ppy = (float)block_size * client->game.enemies[i].pos.y;
            if (client->game.enemies[i].definition.weapon.type == FUNKOUT_WEAPON_TYPE_STRAIGHT) {
                float scale = client->game.enemies[i].firing_cooldown / client->game.enemies[i].definition.weapon.firing_cooldown;
                scale = 1.0 - MINMAX(scale, 0, 1);
                int pradius = (float)block_size * client->game.enemies[i].definition.weapon.size;
                funkout_image_draw(client->images[FUNKOUT_CLIENT_IMAGE_PROJECTILE_BULLET],
                                   grid_rect.x + ppx - pradius * scale,
                                   grid_rect.y + ppy - pradius * scale,
                                   pradius * 2 * scale, pradius * 2 * scale,
                                   0, 0, 32, 32,
                                   1.0, 0, 0, 0);
            } else if (client->game.enemies[i].definition.weapon.type == FUNKOUT_WEAPON_TYPE_BEAM) {
                float f = client->game.enemies[i].firing_cooldown / client->game.enemies[i].definition.weapon.firing_cooldown;
                f = 1.0 - MINMAX(f, 0, 1);
                float scale = 0.1 + fabsf(sinf(f * 12.56)) * 0.5;
                struct funkout_AABB aabb = {client->game.enemies[i].pos, {client->game.enemies[i].definition.weapon.size, MAX(FUNKOUT_GAME_HEIGHT, FUNKOUT_GAME_WIDTH)}};
                struct funkout_Vector target_normal = {0, 1};
                if (client->game.enemies[i].definition.aim) {
                    target_normal.x = client->game.enemies[i].target.x - client->game.enemies[i].pos.x;
                    target_normal.y = client->game.enemies[i].target.y - client->game.enemies[i].pos.y;
                    funkout_vector_normalize(target_normal.x, target_normal.y, &target_normal);
                }
                aabb.extents.x *= scale;
                aabb.pos.x += aabb.extents.y * target_normal.x;
                aabb.pos.y += aabb.extents.y * target_normal.y;
                struct funkout_IRect irect = {
                    grid_rect.x + (aabb.pos.x - aabb.extents.x) * block_size,
                    grid_rect.y + (aabb.pos.y - aabb.extents.y) * block_size,
                    aabb.extents.x * block_size * 2,
                    aabb.extents.y * block_size * 2
                };
                float angle = atan2f(target_normal.y, target_normal.x) * 57.29577951308232;
                funkout_image_draw(client->images[FUNKOUT_CLIENT_IMAGE_PROJECTILE_BEAM],
                                   irect.x, irect.y, irect.w, irect.h,
                                   0,0,0,0,
                                   0.5, angle-90, 0, 0);
                funkout_image_draw(client->images[FUNKOUT_CLIENT_IMAGE_PROJECTILE_BEAM_POINT],
                                   grid_rect.x + ppx - aabb.extents.x * block_size,
                                   grid_rect.y + ppy - aabb.extents.x * block_size,
                                   aabb.extents.x * block_size * 2,
                                   aabb.extents.x * block_size * 2,
                                   0,0,0,0,
                                   1.0, 0, 0, 0);
            }
        }
    }
    struct funkout_Animation projectile_animation = {32, 0.25, -1, 0};
    int beam_count = 0;
    for (int i = 0; i < client->game.num_projectiles; i++) {
        int radius = (float)block_size * client->game.projectiles[i].radius;
        int px = (float)block_size * client->game.projectiles[i].pos.x;
        int py = (float)block_size * client->game.projectiles[i].pos.y;
        if (client->game.projectiles[i].type == FUNKOUT_WEAPON_TYPE_BEAM) {
            float scale = 0.6 + fabsf(sinf(client->game.projectiles[i].timestep * 12.56 * 2)) * 0.4;
            struct funkout_AABB aabb = {client->game.projectiles[i].pos, {client->game.enemies[i].definition.weapon.size, client->game.projectiles[i].length * 0.5}};
            struct funkout_Vector target_normal = client->game.projectiles[i].normal;
            funkout_vector_normalize(target_normal.x, target_normal.y, &target_normal);
            aabb.extents.x *= scale;
            aabb.pos.x += aabb.extents.y * target_normal.x;
            aabb.pos.y += aabb.extents.y * target_normal.y;
            struct funkout_IRect irect = {
                grid_rect.x + (aabb.pos.x - aabb.extents.x) * block_size,
                grid_rect.y + (aabb.pos.y - aabb.extents.y) * block_size,
                aabb.extents.x * block_size * 2,
                aabb.extents.y * block_size * 2
            };
            float angle = atan2f(target_normal.y, target_normal.x) * 57.29577951308232;
            funkout_image_draw(client->images[FUNKOUT_CLIENT_IMAGE_PROJECTILE_BEAM],
                               irect.x, irect.y, irect.w, irect.h,
                               0,0,0,0,
                               1.0, angle-90, 0, 0);
            funkout_image_draw(client->images[FUNKOUT_CLIENT_IMAGE_PROJECTILE_BEAM_POINT],
                               grid_rect.x + px - aabb.extents.x * block_size,
                               grid_rect.y + py - aabb.extents.x * block_size,
                               aabb.extents.x * block_size * 2,
                               aabb.extents.x * block_size * 2,
                               0,0,0,0,
                               1.0, 0, 0, 0);
            funkout_image_draw(client->images[FUNKOUT_CLIENT_IMAGE_PROJECTILE_BEAM_POINT],
                               grid_rect.x + px - aabb.extents.x * block_size * 2
                               + target_normal.x * (client->game.projectiles[i].length - aabb.extents.x) * block_size,
                               grid_rect.y + py - aabb.extents.x * block_size * 2
                               + target_normal.y * (client->game.projectiles[i].length - aabb.extents.x) * block_size,
                               aabb.extents.x * block_size * 4,
                               aabb.extents.x * block_size * 4,
                               0,0,0,0,
                               1.0, 0, 0, 0);
            beam_count++;
        } else if (client->game.projectiles[i].type == FUNKOUT_WEAPON_TYPE_STRAIGHT) {
            projectile_animation.timestep = 0;
            funkout_animation_update(&projectile_animation, client->game.projectiles[i].timestep);
            funkout_animation_draw(&projectile_animation, client->images[FUNKOUT_CLIENT_IMAGE_PROJECTILE_BULLET],
                                   grid_rect.x + px - radius,
                                   grid_rect.y + py - radius,
                                   radius * 2, radius * 2,
                                   1.0, 0, 0, 0);
        }
    }
    if (beam_count == 0) {
        Mix_FadeOutChannel(4, 250);
    }
    for (int i = 0; i < client->game.num_drops; i++) {
        if (client->game.drops[i].type > FUNKOUT_DROP_TYPE_NONE
            && client->game.drops[i].type < MAX_FUNKOUT_DROP_TYPE
            && client->game.drops[i].type != FUNKOUT_DROP_TYPE_DEATH) {
            int px = (float)block_size * (client->game.drops[i].pos.x - 0.5);
            int py = (float)block_size * (client->game.drops[i].pos.y - 0.5);
            funkout_image_draw(client->drop_images[client->game.drops[i].type],
                               grid_rect.x + px,
                               grid_rect.y + py,
                               block_size, block_size,
                               0,0,0,0,
                               1.0, 0, 0, 0);
        }
    }
    for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++) {
        struct funkout_Player * player = client->game.players[i];
        if (player != NULL && player->lives >= 0 && player->destroyed_cooldown <= 0) {
            for (int j = 0; j < player->num_balls; j++) {
                const struct funkout_Ball * ball = player->balls + j;
                int radius = (float)block_size * ball->radius;
                int px = (float)block_size * ball->pos.x;
                int py = (float)block_size * ball->pos.y;
                float alpha = 1.0;
                if (player->immortal)
                    alpha = 0.5 + fmodf(client->game_timers.second_timer, 0.3);
                funkout_image_draw(client->ball_images[0],
                                   grid_rect.x + px - radius, grid_rect.y + py - radius, radius * 2, radius * 2,
                                   0, 0, 0, 0,
                                   alpha, 0, 0, 0);
            }
        }
    }
    for (int i = 0; i < client->num_particles; i++) {
        if (client->particles[i].timestep <= 0 || client->particles[i].ttl <= 0)
            continue;
        int radius = (float)block_size * client->particles[i].size;
        int px = (float)block_size * client->particles[i].pos.x;
        int py = (float)block_size * client->particles[i].pos.y;
        float alpha = client->particles[i].color.a;
        alpha *= client->particles[i].timestep / client->particles[i].ttl;
        alpha = 1.0 - MINMAX(alpha, 0, 1);
        funkout_render_fill_rect(grid_rect.x + px - radius, grid_rect.y + py - radius,
                                 radius * 2, radius * 2,
                                 client->particles[i].color.r,
                                 client->particles[i].color.g,
                                 client->particles[i].color.b,
                                 alpha);
    }
    for (int i = 0; i < client->num_texts; i++) {
        int len = strlen(client->texts[i].text);
        int px = (float)block_size * client->texts[i].pos.x - (float)block_size * len * client->texts[i].size / 2;
        int py = (float)block_size * client->texts[i].pos.y - (float)block_size * len * client->texts[i].size / 2;
        float alpha = (client->texts[i].ttl > 0 ? client->texts[i].timestep / client->texts[i].ttl : 0.0);
        alpha = 1.0 - MINMAX(alpha, 0, 1);
        alpha *= alpha;
        funkout_image_draw_text(client->images[FUNKOUT_CLIENT_IMAGE_SMALL_FONT],
                                client->texts[i].text,
                                grid_rect.x + px,
                                grid_rect.y + py,
                                client->texts[i].size * (float)block_size,
                                alpha, 0, 0, 0);
    }
    for (int i = 0; i < client->game.num_drops; i++) {
        if (client->game.drops[i].type == FUNKOUT_DROP_TYPE_DEATH) {
            int px = (float)block_size * (client->game.drops[i].pos.x - 0.5);
            int py = (float)block_size * (client->game.drops[i].pos.y - 0.5);
            float extra_size = fabsf(block_size * 0.25 * sinf(client->game_timers.second_timer * 6.283185307179586 * 2.0));
            funkout_image_draw(client->drop_images[client->game.drops[i].type],
                               grid_rect.x + px + extra_size,
                               grid_rect.y + py + extra_size,
                               block_size - extra_size * 2,
                               block_size - extra_size * 2,
                               0,0,0,0,
                               1.0, 0, 0, 0);
        }
    }
    if (client->game.state == FUNKOUT_GAME_STATE_START) {
        int slen = strlen(client->game.level.name);
        struct funkout_IRect fit_rect;
        funkout_irect_fit(0, 0, block_size * slen, block_size,
                          grid_rect.x, grid_rect.y, grid_rect.w, grid_rect.h,
                          &fit_rect);
        float alpha = fmodf(client->game.start_cooldown, 0.25) * 2 + 0.5;
        funkout_image_draw_text(NULL, client->game.level.name, fit_rect.x, fit_rect.y, fit_rect.h, alpha, 0.0, 0.0, 0.0);
    } else if (client->game.state == FUNKOUT_GAME_STATE_END) {
        unsigned int highest_score = 0;
        for (int i = 0; i < FUNKOUT_MAX_PLAYERS; i++)
            if (client->game.players[i] != NULL && client->game.players[i]->level_score > highest_score)
                highest_score = client->game.players[i]->level_score;
        char tmps[128];
        snprintf(tmps, sizeof(tmps), "%u / %u", highest_score, client->game.max_level_score);
        int len = strlen(tmps);
        struct funkout_IRect fit_rect;
        funkout_irect_fit(0, 0, len, 1,
                          grid_rect.x, grid_rect.y + grid_rect.h / 2 - block_size, grid_rect.w, block_size,
                          &fit_rect);
        funkout_image_draw_text(NULL, tmps, fit_rect.x, fit_rect.y, fit_rect.h, 1.0, 0.0, 0.0, 0.0);
        snprintf(tmps, sizeof(tmps), "%u%%", highest_score * 100 / client->game.max_level_score);
        len = strlen(tmps);
        funkout_irect_fit(0, 0, len, 1,
                          grid_rect.x, grid_rect.y + grid_rect.h / 2 + block_size, grid_rect.w, block_size,
                          &fit_rect);
        funkout_image_draw_text(NULL, tmps, fit_rect.x, fit_rect.y, fit_rect.h, 1.0, 0.0, 0.0, 0.0);
    } else if (client->game.state == FUNKOUT_GAME_STATE_OVER) {
        struct funkout_IRect fit_rect;
        funkout_irect_fit(0, 0, block_size * 9, block_size,
                          grid_rect.x, grid_rect.y, grid_rect.w, grid_rect.h,
                          &fit_rect);
        funkout_image_draw_text(NULL, "game over", fit_rect.x, fit_rect.y, fit_rect.h, 1.0, 0.0, 0.0, 0.0);
    }
    funkout_render_set_clip(NULL);
}

void funkout_client_add_particles1(struct funkout_Client * client,
                                   int num,
                                   float x, float y,
                                   float direction_x, float direction_y,
                                   float spread,
                                   float speed,
                                   float speed_random,
                                   float ttl,
                                   float size,
                                   float r, float g, float b, float a)
{
    if (num > 0) {
        client->particles = realloc(client->particles, sizeof(struct funkout_ClientParticle) * (client->num_particles + num));
        for (int i = 0; i < num; i++) {
            struct funkout_Vector n = {direction_x, direction_y};
            float l = sqrtf(n.x * n.x + n.y * n.y);
            if (l > 0) {
                n.x /= l;
                n.y /= l;
            }
            client->particles[client->num_particles + i].pos.x = x;
            client->particles[client->num_particles + i].pos.y = y;
            client->particles[client->num_particles + i].ttl = ttl;
            client->particles[client->num_particles + i].timestep = 0;
            client->particles[client->num_particles + i].size = size;
            client->particles[client->num_particles + i].color.r = MINMAX(r, 0, 1);
            client->particles[client->num_particles + i].color.g = MINMAX(g, 0, 1);
            client->particles[client->num_particles + i].color.b = MINMAX(b, 0, 1);
            client->particles[client->num_particles + i].color.a = MINMAX(a, 0, 1);
            speed += (rand() % 1001) / 1000.0f;
            if (spread > 0) {
                float a = atan2(n.y, n.x);
                float ra = fmodf(rand(), spread) - spread / 2;
                ra *= 0.017453292519943295f;
                a += ra;
                client->particles[client->num_particles + i].vel.x = cosf(a) * speed;
                client->particles[client->num_particles + i].vel.y = sinf(a) * speed;
            } else {
                client->particles[client->num_particles + i].vel.x = n.x * speed;
                client->particles[client->num_particles + i].vel.y = n.y * speed;
            }
        }
        client->num_particles += num;
    }
}

void funkout_client_add_particles2(struct funkout_Client * client,
                                   int subdivisions,
                                   float x, float y,
                                   float ex, float ey,
                                   float speed,
                                   float ttl,
                                   float r, float g, float b, float a)
{
    if (ex > 0 && ey > 0 && subdivisions > 0) {
        float size = (2.0 * MIN(ex, ey)) / subdivisions;
        int num = subdivisions * subdivisions;
        client->particles = realloc(client->particles, sizeof(struct funkout_ClientParticle) * (client->num_particles + num));
        for (int by = 0; by < subdivisions; by++) {
            for (int bx = 0; bx < subdivisions; bx++) {
                int i = bx + by * subdivisions;
                float ptx = x - ex + bx * size;
                float pty = y - ey + by * size;
                struct funkout_Vector n = {- ex + bx * size, - ey + by * size};
                n.x += ((rand() % 100) - 50) / 1000.0;
                n.y += ((rand() % 100) - 50) / 1000.0;
                float l = sqrtf(n.x * n.x + n.y * n.y);
                if (l > 0.1) {
                    n.x /= l;
                    n.y /= l;
                } else {
                    n.x = 0;
                    n.y = 1.0;
                    l = 0.1;
                }
                l *= 2;
                float fspeed = (speed + l * l * l) / 2.0;
                client->particles[client->num_particles + i].pos.x = ptx;
                client->particles[client->num_particles + i].pos.y = pty;
                client->particles[client->num_particles + i].ttl = ttl;
                client->particles[client->num_particles + i].timestep = 0;
                client->particles[client->num_particles + i].size = size;
                client->particles[client->num_particles + i].color.r = MINMAX(r, 0, 1);
                client->particles[client->num_particles + i].color.g = MINMAX(g, 0, 1);
                client->particles[client->num_particles + i].color.b = MINMAX(b, 0, 1);
                client->particles[client->num_particles + i].color.a = MINMAX(a, 0, 1);
                client->particles[client->num_particles + i].vel.x = n.x * fspeed;
                client->particles[client->num_particles + i].vel.y = n.y * fspeed;
            }
        }
        client->num_particles += num;
    }
}

static void funkout_client_draw_title(const struct funkout_Client * client,
                                      int x, int y, int w, int h)
{
    struct funkout_AABB aabb = {{0, 0}, {FUNKOUT_GAME_WIDTH, FUNKOUT_GAME_HEIGHT + 1}};
    struct funkout_AABB bounding_aabb = {{x + w / 2, y + h / 2}, {w / 2, h / 2}};
    struct funkout_AABB fitted_aabb;
    funkout_aabb_fit(aabb.pos.x, aabb.pos.y, aabb.extents.x, aabb.extents.y,
                     bounding_aabb.pos.x, bounding_aabb.pos.y, bounding_aabb.extents.x, bounding_aabb.extents.y,
                     &fitted_aabb);
    struct funkout_IRect clip_irect = {fitted_aabb.pos.x - fitted_aabb.extents.x,
                                       fitted_aabb.pos.y - fitted_aabb.extents.y,
                                       fitted_aabb.extents.x * 2,
                                       fitted_aabb.extents.y * 2};
    funkout_render_set_clip(&clip_irect);
    funkout_render_fill_rect(fitted_aabb.pos.x - fitted_aabb.extents.x, fitted_aabb.pos.y - fitted_aabb.extents.y,
                             fitted_aabb.extents.x * 2, fitted_aabb.extents.y * 2,
                             0, 0, 0, 1);
    int block_size_x = fitted_aabb.extents.x * 2.0 / (float)FUNKOUT_GAME_WIDTH;
    int block_size_y = fitted_aabb.extents.y * 2.0 / (float)(FUNKOUT_GAME_HEIGHT + 1);
    int block_size = MIN(block_size_x, block_size_y);

    for (int i = 0; i < FUNKOUT_CLIENT_NUM_TITLE_STARS; i++) {
        struct funkout_Vector svec = {client->title_stars[i][0] * clip_irect.w / 32,
                                      client->title_stars[i][1] * clip_irect.h / 32};
        float ra = sinf((client->title_timers.ten_second_timer * 2.0) / 20.0 * 6.283185) * 6.283185 * 0.25;
        struct funkout_Vector vec = {cosf(ra) * svec.x - sin(ra) * svec.y, sin(ra) * svec.x + cos(ra) * svec.y};
        float dist = client->title_stars[i][2];
        float nl = sqrtf(vec.x * vec.x + vec.y * vec.y);
        if (nl > 0) {
            struct funkout_Vector normal = {vec.x / nl, vec.y / nl};
            vec.x += normal.x * nl * nl * dist * dist;
            vec.y += normal.y * nl * nl * dist * dist;
        }
        float f = MINMAX(dist, 0, 1);
        float c = 0.25 + f * 0.75;
        float size = MAX(1, f * block_size * 0.3);
        funkout_image_draw(client->images[FUNKOUT_CLIENT_IMAGE_STAR],
                           clip_irect.x + clip_irect.w / 2 + vec.x - size / 2,
                           clip_irect.y + clip_irect.h / 2 + vec.y - size / 2,
                           size, size,
                           0,0,0,0,
                           c, 360.0 * f, 0, 0);
    }
    
    struct funkout_AABB logo_aabb;
    funkout_aabb_fit(0, 0, 320, 64, fitted_aabb.pos.x, fitted_aabb.pos.y, fitted_aabb.extents.x - block_size, fitted_aabb.extents.y, &logo_aabb);
    struct funkout_IRect logo_irect = {logo_aabb.pos.x - logo_aabb.extents.x,
                                       fitted_aabb.pos.y - fitted_aabb.extents.y + block_size * 2,
                                       logo_aabb.extents.x * 2,
                                       logo_aabb.extents.y * 2};
    int shadows = 10;
    float nx = cosf((client->title_timers.second_timer + sinf(client->title_timers.ten_second_timer / 10.0 * 6.28)) * 6.28);
    float ny = sinf((client->title_timers.second_timer + sinf(client->title_timers.ten_second_timer / 10.0 * 6.28)) * 6.28);
    float distance = 0.5 + sin(client->title_timers.ten_second_timer / 5.0 * 6.28) / 2;
    for (int i = 0; i < shadows; i++) {
        float alpha = (float)i / (float)(shadows - 1);
        if (i < shadows - 1)
            alpha *= 0.5;
        float ff = fmodf(client->title_timers.second_timer + i / 4.0, 1.0);
        float scale = sin(ff * 6.283185) * 0.8;
        funkout_image_draw(client->images[FUNKOUT_CLIENT_IMAGE_LOGO],
                           logo_irect.x + nx * (shadows - i - 1) * block_size * distance - scale * logo_irect.w * 0.1,
                           logo_irect.y + ny * (shadows - i - 1) * block_size * distance - scale * logo_irect.h * 0.1,
                           logo_irect.w + scale * logo_irect.w * 0.2,
                           logo_irect.h + scale * logo_irect.h * 0.2,
                           0, 0, 0, 0,
                           alpha, 0, 0, 0);
    }
    funkout_image_draw_text(NULL,
                            SCROLL_TEXT,
                            clip_irect.x + clip_irect.w - client->scroll_text_offset * block_size,
                            clip_irect.y + clip_irect.h - block_size * 2 + sinf(client->title_timers.second_timer * 6.28) * block_size,
                            block_size, 1.0, 0, 0, 0);
    funkout_render_set_clip(NULL);
}

static void funkout_client_draw_menu(const struct funkout_Client * client,
                                     int x, int y, int w, int h)
{
    struct funkout_AABB aabb = {{0, 0}, {FUNKOUT_GAME_WIDTH, FUNKOUT_GAME_HEIGHT + 1}};
    struct funkout_AABB bounding_aabb = {{x + w / 2, y + h / 2}, {w / 2, h / 2}};
    struct funkout_AABB fitted_aabb;
    funkout_aabb_fit(aabb.pos.x, aabb.pos.y, aabb.extents.x, aabb.extents.y,
                     bounding_aabb.pos.x, bounding_aabb.pos.y, bounding_aabb.extents.x, bounding_aabb.extents.y,
                     &fitted_aabb);
    struct funkout_IRect clip_irect = {fitted_aabb.pos.x - fitted_aabb.extents.x,
                                       fitted_aabb.pos.y - fitted_aabb.extents.y,
                                       fitted_aabb.extents.x * 2,
                                       fitted_aabb.extents.y * 2};
    funkout_render_set_clip(&clip_irect);
    if (client->menu != NULL) {
        int menu_size = clip_irect.w / FUNKOUT_CLIENT_MENU_MAX_LEN;
        int y = menu_size * 8;
        int len = strlen(client->menu->title);
        struct funkout_IRect title_irect = {clip_irect.x + clip_irect.w / 2 - menu_size * len / 2,
                                            clip_irect.y + y,
                                            len * menu_size,
                                            menu_size};
        funkout_render_fill_rect(clip_irect.x, title_irect.y, clip_irect.w, title_irect.h,
                                 1.0, 1.0, 1.0, 0.8);
        funkout_image_draw_text(client->images[FUNKOUT_CLIENT_IMAGE_MENU_FONT],
                                client->menu->title,
                                title_irect.x, title_irect.y,
                                menu_size,
                                1.0, 0, 0, 0);
        if (client->menu->num_items > 0) {
            y += menu_size * 1.5;
            int selected_item = client->menu_item % client->menu->num_items;
            for (int i = 0; i < client->menu->num_items; i++) {
                char tmps[128];
                int menu_value = 0;
                if (client->menu == &MENU_OPTIONS) {
                    if (i == 1) {
                        menu_value = client->settings.volume;
                    } else if (i == 2)
                        menu_value = funkout_get_window_mode() == FUNKOUT_WINDOW_MODE_FULLSCREEN;
                }
                if (client->menu->items[i].range > 1)
                    snprintf(tmps, sizeof(tmps), "%s %d", client->menu->items[i].title, menu_value);
                else if (client->menu->items[i].range == 1)
                    snprintf(tmps, sizeof(tmps), "%s%s", client->menu->items[i].title, menu_value ? " yes" : " no");
                else
                    snprintf(tmps, sizeof(tmps), "%s", client->menu->items[i].title);
                len = strlen(tmps);
                int width = menu_size * len;
                float c = (selected_item == i ? 0.75 + sin(client->menu_timers.second_timer * 6.283185 * 4.0) * 0.25 : 1.0);
                struct funkout_IRect outer_irect = {0, 0, width, menu_size};
                outer_irect.x = clip_irect.x + clip_irect.w / 2 - outer_irect.w / 2;
                outer_irect.y = clip_irect.y + y + outer_irect.h / 2 - outer_irect.h / 2;
                if (selected_item == i) 
                    funkout_render_fill_rect(outer_irect.x, outer_irect.y, outer_irect.w, outer_irect.h, c, c, 0, 0.5);
                else
                    funkout_render_fill_rect(outer_irect.x, outer_irect.y, outer_irect.w, outer_irect.h, 0, 0, 0, 0.5);
                if (i != selected_item || fmodf(client->menu_timers.second_timer, 0.5) < 0.4)
                    funkout_image_draw_text(client->images[FUNKOUT_CLIENT_IMAGE_MENU_FONT],
                                            tmps,
                                            outer_irect.x, outer_irect.y,
                                            outer_irect.h,
                                            1.0, 0, 0, 0);
                y += menu_size;
                if (i == 0)
                    y += menu_size * 0.5;
            }
        }
    }
    funkout_render_set_clip(NULL);
}

void funkout_client_draw(const struct funkout_Client * client,
                         int x, int y, int w, int h)
{
    funkout_render_clear(0, 0, 0.25, 1.0);
    switch (client->state) {
    case FUNKOUT_CLIENT_STATE_GAME:
        funkout_client_draw_game(client, x, y, w, h);
        break;
    case FUNKOUT_CLIENT_STATE_TITLE:
        funkout_client_draw_title(client, x, y, w, h);
        break;
    default:
        break;
    }
    if (client->menu != NULL) {
        funkout_client_draw_menu(client, x, y, w, h);
    }
    funkout_render_present();
}

bool funkout_client_update_controls(struct funkout_Client * client)
{
    int num_keys = 65535;
    const Uint8 * keys = SDL_GetKeyboardState(&num_keys);
    float controls[MAX_FUNKOUT_CLIENT_CONTROL] = {0};
    if (num_keys > 0) {
        for (enum funkout_ClientControl i = 1; i < MAX_FUNKOUT_CLIENT_CONTROL; i++) {
            for (int j = 0; j < FUNKOUT_CLIENT_BINDING_ALTERNATIVES; j++) {
                if (client->bindings.key[i][j] > 0
                    && client->bindings.key[i][j] < INT_MAX
                    && (int)client->bindings.key[i][j] < num_keys) {
                    controls[i] += keys[client->bindings.key[i][j]];
                }
            }
        }
    }
    memset(client->rel_controls, 0, sizeof(client->rel_controls));
    bool ret = false;
    for (enum funkout_ClientControl i = 1; i < MAX_FUNKOUT_CLIENT_CONTROL; i++) {
        client->rel_controls[i] = controls[i] - client->controls[i];
        if (client->rel_controls[i] != 0)
            ret = true;
    }
    memcpy(client->controls, controls, sizeof(controls));
    return ret;
}

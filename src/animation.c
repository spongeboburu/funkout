/**
 * @file animation.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include <string.h>
#include <math.h>
#include <limits.h>
#include "funkout/animation.h"
#include "funkout/geometry.h"

float funkout_animation_progress(
    const struct funkout_Animation * animation)
{
    float duration = animation->duration;
    float timestep = MAX(0, animation->timestep);
    if (duration <= 0)
        return 0.0;
    if (animation->loops >= 0) 
        duration *= animation->loops + 1;
    else
        timestep = fmodf(timestep, duration);
    float r = timestep / duration;
    return MINMAX(r, 0, 1);
}

void funkout_animation_update(
    struct funkout_Animation * animation,
    float timestep)
{
    if (animation->duration > 0) {
        if (animation->timestep < 0)
            animation->timestep = 0;
        animation->timestep += MAX(0, timestep);
        if (animation->loops >= 0) {
            if (animation->timestep > animation->duration * (animation->loops + 1))
                animation->timestep = animation->duration * (animation->loops + 1);
        } else
            animation->timestep = fmodf(animation->timestep, animation->duration);
    } else
        animation->timestep = 0;
}

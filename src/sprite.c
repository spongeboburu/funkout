/**
 * @file sprite.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */
#include <string.h>
#include <stdio.h>
#include "funkout/sprite.h"

bool funkout_sprite_init(
    struct funkout_Sprite * sprite,
    float pos_x, float pos_y,
    float extent_x, float extent_y,
    const struct funkout_Animation * animation,
    const struct funkout_Image * image)
{
    memset(sprite, 0, sizeof(struct funkout_Sprite));
    sprite->alpha = 1.0;
    sprite->pos.x = pos_x;
    sprite->pos.y = pos_y;
    sprite->extents.x = extent_x;
    sprite->extents.y = extent_y;
    if (animation != NULL)
        sprite->animation = *animation;
    sprite->image = image;
    return true;
}

void funkout_sprite_deinit(struct funkout_Sprite * sprite)
{
    if (sprite->f_deinit != NULL)
        sprite->f_deinit(sprite);
}

struct funkout_Sprite * funkout_sprite_new(
    float pos_x, float pos_y,
    float extent_x, float extent_y,
    const struct funkout_Animation * animation,
    const struct funkout_Image * image)
{
    struct funkout_Sprite * sprite = malloc(sizeof(struct funkout_Sprite));
    if (!funkout_sprite_init(sprite, pos_x, pos_y, extent_x, extent_y,
                             animation, image)) {
        free(sprite);
        return NULL;
    }
    return sprite;
}

void funkout_sprite_free(struct funkout_Sprite * sprite)
{
    funkout_sprite_deinit(sprite);
    free(sprite);
}

bool funkout_sprite_draw(const struct funkout_Sprite * sprite)
{
    return funkout_animation_draw(&sprite->animation,
                                  sprite->image,
                                  sprite->pos.x - sprite->extents.x,
                                  sprite->pos.y - sprite->extents.y,
                                  sprite->extents.x * 2,
                                  sprite->extents.y * 2,
                                  sprite->alpha,
                                  sprite->rotation,
                                  sprite->align.x,
                                  sprite->align.y);
}

bool funkout_sprite_draw2(const struct funkout_Sprite * sprite, int x, int y, int w, int h)
{
    return funkout_animation_draw(&sprite->animation,
                                  sprite->image,
                                  x,
                                  y,
                                  w,
                                  h,
                                  sprite->alpha,
                                  sprite->rotation,
                                  sprite->align.x,
                                  sprite->align.y);
}

void funkout_sprite_update(struct funkout_Sprite * sprite, float timestep)
{
    if (sprite->f_update != NULL)
        sprite->f_update(sprite, timestep);
    else {
        funkout_animation_update(&sprite->animation, timestep);
        sprite->pos.x += sprite->velocity.x * timestep;
        sprite->pos.y += sprite->velocity.y * timestep;
        if (sprite->ttl > 0) {
            sprite->ttl -= timestep;
            if (sprite->ttl <= 0)
                sprite->remove_me = true;
        }
    }
}






void funkout_sprite_array_init(
    struct funkout_SpriteArray * array)
{
    array->length = 0;
    array->sprites = NULL;
}

void funkout_sprite_array_deinit(
    struct funkout_SpriteArray * array,
    bool keep_sprites)
{
    if (array->sprites != NULL) {
        if (!keep_sprites)
            for (int i = 0; i < array->length; i++)
                funkout_sprite_free(array->sprites[i]);
        free(array->sprites);
    }
}

struct funkout_Sprite * funkout_sprite_array_get(
    const struct funkout_SpriteArray * array, int index)
{
    if (index < 0)
        index += array->length;
    if (index >= 0 && index < array->length) 
        return array->sprites[index];
    return NULL;
}

int funkout_sprite_array_find(
    const struct funkout_SpriteArray * array,
    const struct funkout_Sprite * sprite)
{
    int first = 0;
    int last = array->length - 1;
    while (first < last) {
        if (array->sprites[first] == sprite)
            return first;
        if (array->sprites[last] == sprite)
            return last;
        first++;
        last--;
    }
    if (array->sprites[first] == sprite)
        return first;
    return -1;
}

void funkout_sprite_array_append(
    struct funkout_SpriteArray * array,
    struct funkout_Sprite * sprite)
{
    if (sprite != NULL) {
        array->sprites = realloc(array->sprites, sizeof(struct funkout_Sprite *) * (array->length + 1));
        array->sprites[array->length] = sprite;
        array->length++;
    }
}

bool funkout_sprite_array_remove_at(
    struct funkout_SpriteArray * array,
    int index,
    bool keep_sprite)
{
    if (index < 0)
        index += array->length;
    if (index >= 0 && index < array->length) {
        if (!keep_sprite)
            funkout_sprite_free(array->sprites[index]);
        if (array->length > 1) {
            if (index < array->length - 1)
                memmove(array->sprites + index,
                        array->sprites + index + 1,
                        sizeof(struct funkout_Sprite *)
                        * (array->length - index - 1));
            array->sprites = realloc(array->sprites, sizeof(struct funkout_Sprite *) * (array->length - 1));
            array->length--;
        } else {
            free(array->sprites);
            array->sprites = NULL;
            array->length = 0;
        }
        
        return true;
    }
    return false;
}

bool funkout_sprite_array_remove(
    struct funkout_SpriteArray * array,
    const struct funkout_Sprite * sprite,
    bool keep_sprite)
{
    int index = funkout_sprite_array_find(array, sprite);
    if (index >= 0)
        return funkout_sprite_array_remove_at(array, index, keep_sprite);
    return false;
}

int funkout_sprite_array_prune(
    struct funkout_SpriteArray * array,
    bool keep_sprite)
{
    int i = 0;
    int len = array->length;
    while (i < len) {
        if (array->sprites[i]->remove_me) {
            if (i < len - 1)
                memmove(array->sprites + i,
                        array->sprites + i + 1,
                        sizeof(struct funkout_Sprite *)
                        * (len - i - 1));
            len--;
        } else
            i++;
    }
    int ret = array->length - len;
    if (len != array->length) {
        if (len > 0) {
            array->sprites = realloc(array->sprites, sizeof(struct funkout_Sprite *) * len);
            array->length = len;
        } else {
            free(array->sprites);
            array->sprites = NULL;
            array->length = 0;
        }
    }
    return ret;
}

void funkout_sprite_array_update(
    struct funkout_SpriteArray * array,
    float timestep)
{
    for (int i = 0; i < array->length; i++) {
        funkout_sprite_update(array->sprites[i], timestep);
    }
}

#include "funkout/defs.h"

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_WALL = {
    FUNKOUT_BLOCK_TYPE_NORMAL,
    FUNKOUT_DROP_TYPE_NONE,
    -1,
    {0.5, 0.5, 0.5, 1.0},
    {0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_HP1 = {
    FUNKOUT_BLOCK_TYPE_NORMAL,
    FUNKOUT_DROP_TYPE_NONE,
    1,
    {1.0, 0.0, 0.0, 1.0},
    {0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_HP2 = {
    FUNKOUT_BLOCK_TYPE_NORMAL,
    FUNKOUT_DROP_TYPE_NONE,
    2,
    {0.0, 0.0, 1.0, 1.0},
    {0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_HP3 = {
    FUNKOUT_BLOCK_TYPE_NORMAL,
    FUNKOUT_DROP_TYPE_NONE,
    3,
    {1.0, 0.5, 0.0, 1.0},
    {0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_HP4 = {
    FUNKOUT_BLOCK_TYPE_NORMAL,
    FUNKOUT_DROP_TYPE_NONE,
    4,
    {0.0, 1.0, 0.0, 1.0},
    {0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_HP6 = {
    FUNKOUT_BLOCK_TYPE_NORMAL,
    FUNKOUT_DROP_TYPE_NONE,
    6,
    {1.0, 1.0, 0.0, 1.0},
    {0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_HP8 = {
    FUNKOUT_BLOCK_TYPE_NORMAL,
    FUNKOUT_DROP_TYPE_NONE,
    8,
    {0.0, 1.0, 1.0, 1.0},
    {0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_HP10 = {
    FUNKOUT_BLOCK_TYPE_NORMAL,
    FUNKOUT_DROP_TYPE_NONE,
    10,
    {1.0, 0.0, 1.0, 1.0},
    {0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_SPAWN = {
    FUNKOUT_BLOCK_TYPE_SPAWN,
    FUNKOUT_DROP_TYPE_NONE,
    -1,
    {0.25, 0.25, 0.25, 1.0},
    {0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_SPAWN_AMOEBA1 = {
    FUNKOUT_BLOCK_TYPE_SPAWN,
    FUNKOUT_DROP_TYPE_NONE,
    -1,
    {0.25, 0.25, 0.25, 1.0},
    {2, 4, &FUNKOUT_ENEMY_DEF_AOMEBA, 4.0, 4.0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_SPAWN_VIRUS1 = {
    FUNKOUT_BLOCK_TYPE_SPAWN,
    FUNKOUT_DROP_TYPE_NONE,
    -1,
    {0.25, 0.25, 0.25, 1.0},
    {1, 2, &FUNKOUT_ENEMY_DEF_VIRUS, 10.0, 10.0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_SPAWN_SATA1 = {
    FUNKOUT_BLOCK_TYPE_SPAWN,
    FUNKOUT_DROP_TYPE_NONE,
    -1,
    {0.25, 0.25, 0.25, 1.0},
    {1, 2, &FUNKOUT_ENEMY_DEF_SATA, 10.0, 10.0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_SPAWN_SATB1 = {
    FUNKOUT_BLOCK_TYPE_SPAWN,
    FUNKOUT_DROP_TYPE_NONE,
    -1,
    {0.25, 0.25, 0.25, 1.0},
    {1, 2, &FUNKOUT_ENEMY_DEF_SATB, 10.0, 10.0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_SPAWN_SATC1 = {
    FUNKOUT_BLOCK_TYPE_SPAWN,
    FUNKOUT_DROP_TYPE_NONE,
    -1,
    {0.25, 0.25, 0.25, 1.0},
    {4, 4, &FUNKOUT_ENEMY_DEF_SATC, 2.0, 2.0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_SPAWN_SATD1 = {
    FUNKOUT_BLOCK_TYPE_SPAWN,
    FUNKOUT_DROP_TYPE_NONE,
    -1,
    {0.25, 0.25, 0.25, 1.0},
    {4, 4, &FUNKOUT_ENEMY_DEF_SATD, 2.0, 2.0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_SPAWN_PLAGUE1 = {
    FUNKOUT_BLOCK_TYPE_SPAWN,
    FUNKOUT_DROP_TYPE_NONE,
    -1,
    {0.25, 0.25, 0.25, 1.0},
    {1, 1, &FUNKOUT_ENEMY_DEF_PLAGUE, 10.0, 10.0}
};

const struct funkout_BlockDef FUNKOUT_BLOCK_DEF_SPAWN_TANK1 = {
    FUNKOUT_BLOCK_TYPE_SPAWN,
    FUNKOUT_DROP_TYPE_NONE,
    -1,
    {0.25, 0.25, 0.25, 1.0},
    {1, 1, &FUNKOUT_ENEMY_DEF_TANK, 10.0, 10.0}
};

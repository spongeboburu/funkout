/**
 * @file error.c
 * @author Copyright (c) 2019 Peter Gebauer
 * @date 2019
 * @copyright MIT license (see LICENSE)
 * @note This file is part of 'funkout'.
 */

#include "funkout/error.h"

char FUNKOUT_ERROR_MESSAGE[FUNKOUT_ERROR_MSG_SIZE];
struct funkout_Error FUNKOUT_ERROR;

const struct funkout_Error * funkout_get_error(void)
{
    return &FUNKOUT_ERROR;
}

void funkout_set_error_va(int code, const char * format, va_list args)
{
    FUNKOUT_ERROR.code = code;
    FUNKOUT_ERROR.msg = FUNKOUT_ERROR_MESSAGE;
    vsnprintf(FUNKOUT_ERROR_MESSAGE, sizeof(FUNKOUT_ERROR_MESSAGE), format, args);
}

void funkout_set_error(int code, const char * format, ...)
{
    va_list args;
    va_start(args, format);
    funkout_set_error_va(code, format, args);
    va_end(args);
}

void funkout_clear_error(void)
{
    FUNKOUT_ERROR.code = 0;
    FUNKOUT_ERROR_MESSAGE[0] = 0;
}

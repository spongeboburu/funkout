#include <stdlib.h>

#ifdef USE_PLATFORM_TIME
#ifdef __linux__
#include <time.h>
#else
#include <sys/time.h>
#endif
#else
#include <SDL2/SDL.h>
#endif

#include "funkout/time.h"

double funkout_time(void)
{
#ifdef USE_PLATFORM_TIME
#ifdef __linux__
    struct timespec tp;
    if (clock_gettime(CLOCK_MONOTONIC, &tp) < 0) {
        return 0;
    }
    return (double)tp.tv_sec + (double)tp.tv_nsec / 1000000000.0;
#else
    struct timeval tv = {0}; 
    gettimeofday(&tv, NULL);
    return (double)tv.tv_sec + (double)tv.tv_usec / 1000000.0;
#endif
#else
    Uint32 t = SDL_GetTicks();
    return (double)t / 1000.0;
#endif
}


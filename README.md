# Funkout

An open source game similar to breakout.

MIT-license (see LICENSE).


## Requirements

Build system:

- cmake 3.6 (or higher) [https://cmake.org/]
- Python 3.4 (or higher) [https://python.org]

Libraries:

- SDL2 2.0.3 (or higher) [https://libsdl.org]
- SDL2_image 2.0.3 (or higher) [https://www.libsdl.org/projects/SDL_image/]
- SDL2_mixer 2.0.3 (or higher) [https://www.libsdl.org/projects/SDL_mixer/]
- Gtk3 (optional, only for editor) [https://www.gtk.org/]

The cmake build includes an option to build the entire SDL2 stack from scratch.

## Building

In the project root:

```
mkdir build
cd build
cmake ..
make
```

To build the editor simple run `make funkout-editor` at the end.

The following cmake variables can be set to ON:

- `Funkout_BUILD_TESTS` : Build test binaries.
- `Funkout_BUILD_STATIC` : Build a static binary.
- `Funkout_BUILD_SDL2_STACK` : Build SDL2 from scratch.
- `Funkout_BUILD_EDITOR` : Build the editor for `all`.

To set a variable simply run cmake from the build directory and then rebuild:

```
cmake .. -DFunkout_BUILD_SDL2_STACK=ON
make
```

or use the built-in cmake UI (again, from the build directory):

```
ccmake ..
```

## Game command line options

You can show the command line help like this:

```
funkout -h
```

which will show

```
TODO!
```
